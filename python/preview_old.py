#!/usr/bin/python
# -*- coding: utf8 -*-

import sys
import mysql.connector
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import datetime

if len(sys.argv) > 1 :
    mensagemId = int(sys.argv[1])
    db = mysql.connector.connect(user='root', password='qMf#xz?B+6?@',
                          host='127.0.0.1',
                          database='thweb_sistema',
                          charset='utf8',
                          use_unicode=True)
    cursor = db.cursor(buffered=True)
    cursor.execute("set names utf8;")
    cursor.execute('SET CHARACTER SET utf8;')
    cursor.execute('SET character_set_connection=utf8;')
    queryMensagem = ("select usuarios.nome as usuario,"
                     "itens.nome as item,"
                     "assuntos.nome as assunto,"
                     "mensagens.texto as texto "
                     "from mensagens inner join "
                     "usuarios on "
                     "usuarios.id = mensagens.usuario_id "
                     "inner join assuntos on "
                     "assuntos.id = mensagens.assunto_id "
                     "inner join itens on "
                     "itens.id = assuntos.item_id "
                     "where mensagens.id = %i" % mensagemId)
    cursor.execute(queryMensagem)
    row = cursor.fetchone()
    #mensagem = bytes(row[3],'latin1').decode('utf8')
    mensagem = row[3].encode('latin1')
    mensagem+= ("<br /><br /><i>Esta mensagem foi gerada automaticamente"
            "<br />Por favor não responda este email</i><br /><br /><br />")
    item = row[1].encode('latin1')
    assunto = row[2].encode('latin1')
    usuario = row[0].encode('latin1')
    print("assunto = %s - %s" % (item,assunto))
    print(mensagem)
    print("usuario = %s" % usuario)
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    
    db.commit()
    cursor.close()
    db.close()

<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after the core bootstrap.php
 *
 * This is an application wide file to load any function that is not used within a class
 * define. You can also use this to include or require any files in your application.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 * This is related to Ticket #470 (https://trac.cakephp.org/ticket/470)
 *
 *
  App::build(array(
     'plugins' => array('/home/admin/sistema/app/plugins/'),
      'models' =>  array('/home/admin/sistema/app/models/'),
      'views' => array('/home/admin/sistema/app/views/'),
      'controllers' => array('/home/admin/sistema/app/controllers/'),
      'components' => array('/home/admin/sistema/app/controllers/components/'),
      'helpers' => array('/home/admin/sistema/app/views/helpers/'),
      'vendors' => array('/home/admin/sistema/app/vendors/'),
      'shells' => array('/home/admin/sistema/app/shells/'),
      'locales' => array('/home/admin/sistema/app/locale/')
  ));
*/

/**
 * As of 1.3, additional rules for the inflector are added below
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

?>

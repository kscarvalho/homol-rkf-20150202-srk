<?php

class Cronograma extends AppModel {
	var $name = 'Cronograma';
	
	var $belongsTo = array('Turma', 'Item');
	
	var $statuses = array( 'aberto' => 'Não Iniciado' , 'iniciado' => 'Iniciado' , 'terminado' => 'Terminado');
	
	var $recursive = 2;
	
}
?>
<?php

class Catalogo extends AppModel {

    var $name = 'Catalogo';
    var $hasMany = array(
        'CatalogoFoto' => array(
            'className' => 'CatalogoFoto',
            'order' => 'ordem'
        ),
        'CatalogoAnexo',"CatalogoEventoItem");
    
    function listarCatalogosDeItem($itemId) {
        $conditions = array(
            "CatalogoEventoItem.catalogo_item_id" => $itemId,
            "Catalogo.desativado" => 0,
        );
        return $this->listarCatalogos($conditions);
    }
    
    function listarCatalogos($conditions) {
        $this->unbindModelAll();
        $this->bindModel(array(
            "hasMany" => array("CatalogoFoto" => array(
                'className' => 'CatalogoFoto',
                'order' => 'CatalogoFoto.ordem asc'
            ))
        ),false);
        $options = array(
            'joins' => array(
                array(
                    "table" => "catalogos_eventos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoEventoItem",
                    "conditions" => array(
                        "Catalogo.id = CatalogoEventoItem.catalogo_id"
                    )
                ),
                array(
                    "table" => "catalogos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoItem",
                    "conditions" => array(
                        "CatalogoItem.id = CatalogoEventoItem.catalogo_item_id"
                    )
                ),
                array(
                    "table" => "tipos_eventos",
                    "type" => "inner",
                    "alias" => "TiposEvento",
                    "conditions" => array(
                        "TiposEvento.id = CatalogoItem.tipos_evento_id",
                        "TiposEvento.ativo = 1"
                    )
                ),
            ),
            'conditions' => $conditions,
            'fields' => array("Catalogo.*")
        );
        return $this->find('all',$options);
    }

}
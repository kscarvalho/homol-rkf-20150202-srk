<?php
class CampanhasUsuarioCampanhasExtra extends AppModel {
	var $name = 'CampanhasUsuarioCampanhasExtra';
	var $useTable = 'campanhas_usuarios_campanhas_extras';
	
	var $belongsTo = array(
		'CampanhasUsuario', 'CampanhasExtra'
	);
	
	var $validate = array(
		'quantidade' => array(
			'numeric' => array(
                'rule' => 'numeric',
                'required' => true,
                'message' => 'Preencha a quantidade somente com numeros.'
            )
		)
    );
}
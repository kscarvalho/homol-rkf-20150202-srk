<?php

class Informativo extends AppModel {
	
	var $nome = "Informativo";
	var $validate = array(
		'mensagem' => array(
			'rule' => 'notEmpty',
			'message' => 'Digite a mensagem do seu informativo'
		),
		'titulo' => array(
			'rule' => 'notEmpty',
			'message' => 'Digite o titulo do seu informativo'
		)
	);
	
	var $hasMany = array(
		'InformativoUsuario' => array(
			'className' => 'InformativoUsuario',
			'foreignKey' => 'informativo_id'
		)
	);
}
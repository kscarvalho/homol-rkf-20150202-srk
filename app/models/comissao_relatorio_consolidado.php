<?php

class ComissaoRelatorioConsolidado extends AppModel {

    var $name = 'ComissaoRelatorioConsolidado';
    var $belongsTo = array('Turma', 'Usuario');
    var $useTable = 'comissao_relatorio_consolidado';

}

?>
<?php

class UploadPagamento extends Model {
    
    var $name = "UploadPagamento";
    var $belongsTo = array ('Usuario');
    
    function file_path(){
        return Configure::read("FileDirectoryPathUploadPagamentos");
    }
    
    function beforeSave($options = array()) {
        if(isset($this->data[$this->name]['tmp_name'])) {
            $data = fread(fopen($this->data[$this->name]['tmp_name'],"r"),
                    $this->data[$this->name]['tamanho']);
            if($data) {
                $this->data[$this->name]['arquivo'] = $data;
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    function createController() {
        App::import('Core', 'Controller');
        App::import('Core', 'Router');
        App::import('Core', 'View');
        App::import('Controller', 'Pagamentos');
        $controller =& new Controller();
        $view =& new View($controller);
        $pagamentos =& new PagamentosController();
        return $pagamentos;
    }
}
?>
<?php

class AtividadeLocal extends AppModel {

    var $name = 'AtividadeLocal';
    var $useTable = 'atividade_locais';
    var $hasMany = array(
        'Atividade' => array(
            'className' => 'Atividade',
            'foreignKey' => 'local_id',
            'unique' => false,
        ),
    );
}

?>
<?php

class FormandoMapaMesa extends AppModel {

    var $name = 'FormandoMapaMesa';

    function bindUsuario(){
    	$this->bindModel(
    		array(
    			'belongsTo' => array(
	                'ViewFormandos' => array(
	                    'foreignKey' => false,
	                    'conditions' => 'ViewFormandos.codigo_formando = FormandoMapaMesa.codigo_formando',
                        'type' => 'INNER'
	                )
                )
            )
    	);
    }

    function getMesaPorNomeTurmaMapa($nome, $turma_mapa_id, $codigo_formando){
        return $this->find('all', array(
        	'conditions' => array(
        		'status' => 1,
        		'mesa' => $nome,
        		'turma_mapa_id' => $turma_mapa_id,
        		'NOT' => array(
        			'codigo_formando' => $codigo_formando
        		),
        	),
        ));
    }
}

?>
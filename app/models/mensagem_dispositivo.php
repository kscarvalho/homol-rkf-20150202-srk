<?php

class MensagemDispositivo extends AppModel {
    var $name = 'MensagemDispositivo';
    var $useTable = 'mensagens_dispositivos';
    var $belongsTo = array('UsuarioDispositivo', 'MensagemUsuario');
}
<?php

class ParceriaTurma extends Model {
	var $name = "ParceriaTurma";
	var $actsAs = array('Containable');
	var $useTable = "parcerias_turmas";
	
	var $belongsTo = array('Parceria','Turma');

}
?>
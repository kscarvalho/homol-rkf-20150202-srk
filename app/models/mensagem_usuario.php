<?php

class MensagemUsuario extends AppModel {
	
	var $name = 'MensagemUsuario';
	var $useTable = 'mensagens_usuarios';
	var $belongsTo = array('Usuario','Mensagem');
	
}
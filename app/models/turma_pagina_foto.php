<?php

class TurmaPaginaFoto extends AppModel {

    var $name = 'TurmaPaginaFoto';
    var $belongsTo = array('TurmaPagina');
    var $validate = array(
        'arquivo' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        )
    );
    var $recursive = 2;

}

?>
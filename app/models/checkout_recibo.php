<?php

class CheckoutRecibo extends AppModel {

    var $name = "CheckoutRecibo";
    var $belongsTo = array(
        'Financeiro' => array(
            'className' => 'Usuario',
            'foreignKey' => 'financeiro_id'
        ),
        'Atendente' => array(
            'className' => 'Usuario',
            'foreignKey' => 'atendente_id'
        )
    );
    var $hasMany = array('CheckoutReciboPagamento');

}

?>
<?php

class Parceria extends AppModel {

    var $name = 'Parceria';
    var $actsAs = array('Containable');
    var $belongsTo = array('Parceiro');
    var $hasMany = array('FotoParceria');
    var $hasAndBelongsToMany = array(
        'Turma',
        'Categoria' => array(
            'className' => 'Categoria',
            'joinTable' => 'parcerias_categorias',
            'foreignKey' => 'parceria_id',
            'associationForeignKey' => 'categoria_id',
            'unique' => true
        )
    );

    function verificarParceiro($parceiro_id) {
        $parceiro = $this->Parceiro->findById($parceiro_id);

        return !empty($parceiro);
    }

    function verificaDataFinal($data_final) {
        $data_inicio = date('Y-m-d', strtotime($this->data['Parceria']['data-inicio']));
        $data_final = date('Y-m-d', strtotime($this->data['Parceria']['data-fim']));

        return $data_final > $data_inicio;
    }

}

?>
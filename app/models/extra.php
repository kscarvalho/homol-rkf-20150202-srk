<?php

class Extra extends AppModel {

    var $name = 'Extra';
    //var $useTable = 'itens';
    // 
    var $belongsTo = array('Evento');
    //var $actsAs = array('Containable');

    /*
     * Tabela no banco está assim:
      `id` int(11) NOT NULL,
      `evento_id` int(11) NOT NULL,
      `nome` varchar(50),
      `descricao` text
     */
    var $hasAndBelongsToMany = array(
        'Campanha' =>
        array(
            'className' => 'Campanha',
            'joinTable' => 'campanhas_extras',
            'foreignKey' => 'campanha_id',
            'associationForeignKey' => 'extra_id',
            'unique' => true,
        )
    );

}

?>
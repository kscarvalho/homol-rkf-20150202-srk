<?php

class ProtocoloCancelamento extends AppModel {
	
	var $name = 'ProtocoloCancelamento';
	
	var $useTable = 'protocolos_cancelamentos';
	
	var $belongsTo = 'Protocolo';
}

?>
<?php

class ParceriaUsuario extends Model {
	var $name = "ParceriaUsuario";
	var $actsAs = array('Containable');
	var $useTable = "parcerias_usuarios";
	var $belongsTo = array('Parceria','Usuario');
}
?>
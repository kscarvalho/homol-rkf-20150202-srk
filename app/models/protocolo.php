<?php

class Protocolo extends AppModel {
    var $name = "Protocolo";
    var $belongsTo = array(
        'Formando' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        ),
        'Atendente' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_criador'
        )
    );
    var $hasMany = array('CheckoutFormandoPagamento','CheckoutUsuarioItem');
}
?>
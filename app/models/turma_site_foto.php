<?php

class TurmaSiteFoto extends AppModel {

    var $name = 'TurmaSiteFoto';
    var $belongsTo = array('Turma');
    var $validate = array(
        'arquivo' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        )
    );

}

?>
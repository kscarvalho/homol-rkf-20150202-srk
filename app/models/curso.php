<?php

class Curso extends AppModel {

    var $name = 'Curso';

	var $actsAs = array('Containable');

	var $belongsTo = 'Faculdade';
	
	var $hasMany = array(
		'CursoTurma' =>
			array(
                'className'              => 'CursoTurma',
                'joinTable'              => 'cursos_turmas',
                'foreignKey'             => 'curso_id',
                'unique'                 => true,
			)
	);
	
	var $recursive = 2;

	var $validate = array(
		'nome' => array(
			'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
		)
    );
	
	function procurarTodosUniversidadeFaculdadeCursos() {
		$cursos = $this->query("Select Curso.id, Curso.nome, Curso.sigla, Faculdade.id, Faculdade.nome, Faculdade.nome as faculdade_nome, Faculdade.sigla,
			Faculdade.universidade_id, Faculdade.universidade_nome, Faculdade.universidade_sigla FROM 
			cursos AS Curso LEFT JOIN (SELECT Faculdade.id, Faculdade.nome, Faculdade.sigla, Universidade.id AS
			universidade_id, Universidade.nome as universidade_nome, Universidade.sigla AS universidade_sigla FROM 
			faculdades AS Faculdade LEFT JOIN universidades AS Universidade ON 
			Faculdade.universidade_id = Universidade.id) AS Faculdade ON Curso.faculdade_id = Faculdade.id ORDER BY universidade_nome, faculdade_nome;"
		);
		
		function dividirFaculdadesUniversidades($curso){
			$curso['Universidade']['sigla'] = array_pop($curso['Faculdade']);
			$curso['Universidade']['nome'] = array_pop($curso['Faculdade']);
			$curso['Universidade']['id'] = array_pop($curso['Faculdade']);
			return $curso;
		}
		return array_map('dividirFaculdadesUniversidades', $cursos);
		
	}
	
}

?>
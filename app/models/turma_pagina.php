<?php

class TurmaPagina extends AppModel {

    var $name = 'TurmaPagina';
    var $belongsTo = array('Turma');
    var $hasMany = array(
        'TurmaPaginaFoto' => array(
            'order' => 'TurmaPaginaFoto.ordem asc'
        )
    );
    var $validate = array(
        'menu' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        ),
        'titulo' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        )
    );
    var $recursive = 2;

}

?>
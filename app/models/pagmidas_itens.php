<?php

class PagmidasItens extends Model {
	var $name = "PagmidasItens";
	var $useTable = 'pagmidas_itens';
	var $validate = array (
		'nome' => array(
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Digite o nome do Item.'
		)
	);
}
?>
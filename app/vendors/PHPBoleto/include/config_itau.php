<?php

$valorFormatado = $dadosboleto["valor_boleto"];
$dadosboleto["valor_boleto"] = str_replace('.','',$dadosboleto["valor_boleto"]);
$valor = formata_numero($dadosboleto["valor_boleto"],10,0,"valor");
$dadosboleto["valor_boleto"] = $valorFormatado;
$dadosboleto["cedente"] = "ZAPE GEST&Atilde;O DE VALORES";
// DADOS DA SUA CONTA - ITA�
$dadosboleto["agencia"] = "7215"; // Num da agencia, sem digito
$dadosboleto["conta"] = "05371";	// Num da conta, sem digito
$dadosboleto["conta_dv"] = "6"; 	// Digito do Num da conta
$dadosboleto["carteira"] = "175";
$dadosboleto["codigo_banco"] = "341";
$dadosboleto["codigo_cliente"] = "1111";
$dadosboleto["codigo_cliente"] = "{$dadosboleto["conta"]}{$dadosboleto["conta_dv"]}";
$nummoeda = "9";
$dadosboleto['codigo_banco_com_dv'] = geraCodigoBanco($dadosboleto['codigo_banco']);
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);
$codigocliente = formata_numero($dadosboleto["codigo_cliente"], 7, 0);
$conta = formata_numero($dadosboleto["conta"],5,0);
$conta_dv = formata_numero($dadosboleto["conta_dv"],1,0);
$agencia = formata_numero($dadosboleto["agencia"],4,0);
//$dadosboleto["numero_documento"] = str_pad($dadosboleto['usuarioId'], 4, 0, STR_PAD_RIGHT);
$nnum = formata_numero($dadosboleto["nosso_numero"],8,0);
$mod1 = modulo_10($agencia.$conta.$dadosboleto["carteira"].$nnum);
$mod2 = modulo_10($agencia.$conta);
$dadosboleto["data_documento"] = date("d/m/Y");
$dadosboleto["data_processamento"] = date("d/m/Y");
$barra = "{$dadosboleto['codigo_banco']}{$nummoeda}{$fator_vencimento}{$valor}".
    "{$dadosboleto["carteira"]}{$nnum}".modulo_10($agencia.$conta.$dadosboleto["carteira"].$nnum).
    $agencia.$conta.modulo_10($agencia.$conta).'000';
$dv = digitoVerificador_barra($barra);
$linha = substr($barra, 0, 4) . $dv . substr($barra, 4);
$linha = substr($barra,0,4).$dv.substr($barra,4,43);
$nossonumero = $dadosboleto["carteira"].'/'.$nnum.'-'.modulo_10($agencia.$conta.$dadosboleto["carteira"].$nnum);
//$dadosboleto["nosso_numero"] = $nossonumero;
$dadosboleto["agencia_codigo"] = "{$agencia} / {$conta}-".modulo_10($agencia.$conta);
$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id'); ?>
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Item</label>
			<?php echo $form->select('item_id', $itens, array('selected' => $this->data['Cronograma']['item_id'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
			
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data Limite</label>
	<?php echo $form->input('data_limite_aux', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'id' => 'datepicker')); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Status</label>
			<?php echo $form->select('status', $statuses, array('selected' => $this->data['Cronograma']['status'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
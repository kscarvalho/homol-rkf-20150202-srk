<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>
<?php echo $form->input('id'); ?>
<?php echo $form->input('turma_id', array('hidden' => true, 'label' => false)); ?>
<?php echo $form->input('item_id', array('hidden' => true, 'label' => false)); ?>


<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Item</label>
	<span class="grid_5 alpha first"><?php echo $this->data['Item']['nome']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data Limite</label>
	<?php echo $form->input('data_limite_aux', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'id' => 'datepicker')); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Status</label>
	<?php echo $form->select('status', $statuses, array('selected' => $this->data['Cronograma']['status'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
	
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Conclusão</label>
	<?php echo $form->input('conclusao', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>


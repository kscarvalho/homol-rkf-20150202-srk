<span id="conteudo-titulo" class="box-com-titulo-header">Cronograma</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table cellpadding="50">
			<thead>
				<tr>
					<th scope="col"><?php echo 'Item'; ?></th>
					<th scope="col"><?php echo $paginator->sort('Status', 'status');; ?></th>
					<th scope="col"><?php echo $paginator->sort('Data Limite', 'data_limite'); ?></th>			
					<th scope="col"><?php echo 'Pendências RK'; ?></th>
					<th scope="col"><?php echo 'Pendências Comissão'; ?></th>
					<th scope="col"><?php echo 'Conclusão'; ?></th>
					<th scope="col"> &nbsp;</th>		
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="6"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($cronogramas as $cronograma): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<?php $item_id = $cronograma['Item']['id'];?>
					<td colspan="1" width="20%"><?php echo $cronograma['Item']['nome'];?></td>
					<td colspan="1" width="10%"><?php echo $cronograma['Cronograma']['status'];?></td>
					<td colspan="1" width="10%"><?php echo date_format( $datas->create_date_time_from_format('Y-m-d', $cronograma['Cronograma']['data_limite']),'d/m/Y');?></td>
					<td colspan="1" width="10%"><?php echo $itens[$item_id]['pendencias_as'];?></td>
					<td colspan="1" width="10%"><?php echo $itens[$item_id]['pendencias_comissao'];?></td>
					<td colspan="1" width="25%"><?php 
					$conclusao = $cronograma['Cronograma']['conclusao'];
					if(strlen($conclusao) > 100) $conclusao = substr($conclusao,0,99) . '...';
					echo $conclusao;
					?></td>										
					<td colspan="1" width="15%">
					<?php 
					 echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'cronogramas', 'action' =>'visualizar', $cronograma['Cronograma']['id']), array('class' => 'submit button'));
					echo '&nbsp;';
					 echo $html->link('Entrar', array($this->params['prefix'] => true, 'controller' => 'itens', 'action' =>'visualizar', $cronograma['Cronograma']['item_id']), array('class' => 'submit button'));
					?>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
		</table>
	</div>
</div>
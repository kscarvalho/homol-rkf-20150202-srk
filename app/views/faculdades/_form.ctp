<?php echo $form->input('id',  array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Universidade</label>
	<?php echo $form->select('universidade_id', $universidade_select, array('empty'=>false, 'selected' => $this->data['Faculdade']['universidade_id']), array('class' => 'grid_11 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));?>
</p>	
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Sigla</label>
	<?php echo $form->input('sigla', array('class' => 'grid_4 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));?>
</p>	
	
	
	

<?php
$largura = 100.0/(sizeof($anos)+2);
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
#tabela-cursos tbody tr td { padding:0!important }
</style>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: loadThis"
                href='<?= $this->webroot . $this->params['prefix'] ?>/faculdades/relatorio'></a>
            <?=$faculdade['Universidade']['nome']?>
            <a class="metro-button reload pull-right" data-bind="click: function() { reload() }"></a>
        </h2>
    </div>
</div>
<br />
<div class="row-fluid">
    <?php if (sizeof($faculdades) == 0) { ?>
    <h3 class='fg-color-red'>Nenhuma Turma Para Esta Faculdade</h3>
    <?php } else { ?>
    <div class="row-fluid">
        <div class="offset2 span2 bg-color-gray">
            N&atilde;o Existe
        </div>
        <div class="span2 bg-color-red">
            Descartada
        </div>
        <div class="span2 bg-color-yellow">
            Aberta
        </div>
        <div class="span2 bg-color-greenDark">
            Fechada
        </div>
        <div class="span2 bg-color-greenLight">
            Conclu&iacute;da
        </div>
    </div>
    <br />
    <table class="table table-condensed table-striped" id="tabela-cursos">
        <thead>
            <tr>
                <th scope="col" width="<?=$largura*2;?>%">Curso</th>
                <?php foreach($anos as $ano) : ?>
                <th scope="col" width="<?=$largura;?>%"><?=$ano?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
        <?php foreach($faculdades as $faculdade) : ?>
        <?php if(count($faculdades) > 1) : ?>
            <tr>
                <td class="header" colspan="<?=count($anos)+1?>">
                    <?=$faculdade['nome'] ?>
                </td>
            </tr>
        <?php endif; ?>
        <?php $cursos = $faculdade['cursos']; ?>
        <?php foreach ($cursos as $curso): ?>
            <tr>
                <td width="<?=$largura*2;?>%"><?=$curso['nome']?></td>
                <?php foreach($anos as $ano) : ?>
                <?php if(isset($curso[$ano])) : ?>
                <?php $span = 12 / count($curso[$ano]); ?>
                <td width="<?=$largura;?>%">
                    <div class="row-fluid">
                        <?php foreach($curso[$ano] as $cursoAno) : ?>
                        <?php
                            if($cursoAno['status'] == 'descartada')
                                $class = 'bg-color-red';
                            elseif($cursoAno['status'] == 'concluida')
                                $class = 'bg-color-greenLight';
                            elseif($cursoAno['status'] == 'fechada')
                                $class = 'bg-color-greenDark';
                            else
                                $class = 'bg-color-yellow';
                        ?>
                        <div class="span<?="{$span} {$class}"?>">
                            <a type="button" class="fg-color-white"
                                href="/<?=$this->params['prefix']?>/turmas/selecionar/<?=$cursoAno['id']?>">
                                <?=$cursoAno['id']?>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </td>
                <?php else : ?>
                <td width="<?=$largura;?>%" class="bg-color-gray">
                    
                </td>
                <?php endif; ?>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php } ?>
</div>

<?php
//Faculdade
//Curso
//Previsão Conclusão
//Data Assinatura Contrato
//No de Formandos Aderentes
//Valor total do contrato
//Valor por Formando ( calcular automaticamente??)
//Data Assinatura Documento  
//Lista de Nomes e RGs da comissão.
?>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Faculdade</label>
	<?php echo $form->input('faculdade', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Cursos</label>
	<?php echo $form->textarea('cursos', array('rows' => 10,'class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Previsão de Conclusão</label>
	<?php echo $form->input('previsao_conclusao', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Número de Formandos</label>
	<?php echo $form->input('num_formandos', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Valor Total do Contrato</label>
	<?php echo $form->input('valor_contrato', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Valor por Formando</label>
	<?php echo $form->input('valor_formando', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Data a constar no contrato</label>
	<?php echo $form->input('data_contrato', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Lista de Nomes e RGs</label>
	<?php echo $form->textarea('nomes_rgs', array('rows' => 10,'class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>
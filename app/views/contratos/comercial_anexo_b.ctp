<?php
/* 
 * Dados recebidos do controller são:
 * $parametros['faculdade']
 * $parametros['curso']
 * $parametros['numero_aderentes']
 * $turma['data_conclusao']
 * $turma['valor_formando']
 * $turma['data_assinatura']
 * $turma['benefício_comissao']
 * $turma['negociacoes_contratuais']
 * $turma['valor_total_contrato']
 */

App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("RK Formaturas http://rkformaturas.com.br/"); 

$textfont = 'helvetica';
 
$tcpdf->SetAuthor("RK Formaturas");
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetMargins(30, 40, 30);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);
 
$tcpdf->AddPage();

$lista_comissao = $this->data['Contrato']['nomes_rgs'];

$lista_cursos = $this->data['Contrato']['cursos'];

$faculdade = $this->data['Contrato']['faculdade']; 

// create some HTML content
$htmlcontent = 

"
<p><b>ANEXO B - ATA DE FECHAMENTO</b></p>

<p>Faculdade: <b>". $faculdade ."</b></p>

<p>Curso(s): <b>".$lista_cursos."</b></p>

<p>Conclusão: <b>".$this->data['Contrato']['previsao_conclusao']."</b></p>

<p>Todas as negociações extracontratuais estabelecidas entre A CONTRATADA e A CONTRATANTE deverão ser preenchidas nesse documento. Não serão consideradas informações anotadas a caneta, ou seja, todas as informações deverão ser preenchidas e impressas de forma uniforme: </p>

<p><b>".$this->data['Contrato']['negociacoes_contratuais']."</b></p>

".$lista_comissao;

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0, 'J');

echo $tcpdf->Output(date('Y-m-d H:i:s').' - Anexo B.pdf', 'D'); 
?>
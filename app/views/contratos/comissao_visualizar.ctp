<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Contrato</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Tipo de Contrato</label>
			<span class="grid_11 alpha first"> <?php echo $contrato['Contrato']['tipo']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Nome</label>
			<span class="grid_11 alpha first"> <?php echo $contrato['Contrato']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Enviado por</label>
			<span class="grid_11 alpha first"> <?php echo $contrato['Usuario']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Tamanho</label>
			<label class="grid_6 alpha">Tipo</label>
			<span class="grid_5 alpha first"><?php echo $contrato['Contrato']['tamanho']?></span>
			<span class="grid_5 alpha"><?php echo $contrato['Contrato']['tipo']?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Baixar', array($this->params['prefix'] => true, 'controller' => 'Contratos', 'action' => 'baixar', $contrato['Contrato']['id']), array('class' => 'submit ')) ?>
		</p>
		
		
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    var context = ko.contextFor($("#content-body")[0]);
    $(".open-modal").click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        bootbox.dialog('Carregando',[{
            label: 'Cancelar'
        }],{
            remote: url
        });
    });
    $(".remover").click(function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var mensagem = "Deseja remover o " + $(this).attr('title') + "?";
        bootbox.confirm(mensagem,function(response) {
            if(response)
                context.$data.showLoading(function() {
                    $.getJSON(url,{},function() {
                        context.$data.reload();
                    }).always(function() {
                        context.$data.reload();
                    });
                });
        });
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Anexos
        <a href="/<?=$this->params['prefix']?>/contratos/inserir"
            data-bind="click: loadThis"
            class="pull-right button mini bg-color-blueDark">
            Adicionar Anexo
        </a>
    </h2>
</div>
<?php $session->flash(); ?>
<?php if(sizeof($contratos) > 0) : ?>
<div class="row-fluid">
    <span class="label label-info pointer">
        <h2><?=$nomeContrato;?></h2>
    </span>
    <br />
    <br />
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="40%">Tipo</th>
                <th scope="col" width="20%">Gerado</th>
                <th scope="col" width="40%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($contratos as $contrato) : ?>
            <tr>
                <td><?=$contrato['nome']; ?></td>
                <td><?=$contrato['contrato'] ? "Sim" : "Não"?></td>
                <td>
                    <?php if($contrato['contrato']) : ?>
                    <div class="dropdown">
                        <a class="dropdown-toggle button mini default"
                            data-toggle="dropdown" href="#">
                            Op&ccedil;&otilde;es
                            <i class="icon-arrow-20"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/<?=$this->params['prefix']?>/contratos/visualizar/<?=$contrato['tipo']?>/"
                                    tabindex="-1" target='_blank'>
                                    Visualizar
                                </a>
                                <a href="/<?=$this->params['prefix']?>/contratos/visualizar/<?=$contrato['tipo']?>/download"
                                    tabindex="-1" target='_blank'>
                                    Baixar
                                </a>
                                <a href="/<?=$this->params['prefix']?>/contratos/remover/<?=$contrato['tipo']?>"
                                    tabindex="-1" class="remover" title="<?=$contrato['nome']; ?>">
                                    Remover
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php else : ?>
                    <a href="/<?=$this->params['prefix']?>/contratos/gerar/<?=$contrato['tipo']?>/html"
                        tabindex="-1" class='button mini default open-modal'>
                        Visualizar
                    </a>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Não há contrato para esta turma.</h2>
    <?php endif; ?>
</div>
<span id="conteudo-titulo" class="box-com-titulo-header">Gerar Contrato</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<?php
	if(isset($template)){
	
		echo $form->create('Contrato', array('url' => "/{$this->params['prefix']}/contratos/gerarpdf/{$template}"));

		echo $form->input('template', array('value' => $template, 'hiddenField' => true));
		include("_form_$template.ctp");
		?>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'gerar') ,array('class' => 'cancel')); ?>
			<?php echo $form->end(array('label' => 'Gerar', 'div' => false, 'class' => 'submit'));?>
		</p>
	<?php
	} else {	
		
		echo $html->link('Contrato Base',array($this->params['prefix'] => true, 'action' => 'gerar', 'base') ,array('class' => 'submit'));
		echo $html->link('Anexo B',array($this->params['prefix'] => true, 'action' => 'gerar', 'anexo_b') ,array('class' => 'submit'));
		echo $html->link('Anexo B2 ( Benefícios Comissão )',array($this->params['prefix'] => true, 'action' => 'gerar', 'anexo_b2') ,array('class' => 'submit'));
		echo $html->link('Anexo C',array($this->params['prefix'] => true, 'action' => 'gerar', 'anexo_c') ,array('class' => 'submit'));
		echo $html->link('Anexo D',array($this->params['prefix'] => true, 'action' => 'gerar', 'anexo_d') ,array('class' => 'submit'));
		echo $html->link('Anexo E',array($this->params['prefix'] => true, 'action' => 'gerar', 'anexo_e') ,array('class' => 'submit'));

	}
	?>
</div>

<?php
//Faculdade
//Data de Assinatura
?>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Faculdade</label>
	<?php echo $form->input('faculdade', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha">Data de Assinatura do Contrato</label>
	<?php echo $form->input('data_contrato', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
</p>
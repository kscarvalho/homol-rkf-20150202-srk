<script type="text/javascript" src="<?=$this->webroot?>metro/js/max/bootstrap/fileupload.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<style type="text/css">
    .button.botao{
        margin-top: 0;
        min-height: 30px;
        height: 30px;
        line-height: 10px
    }
    .fileupload .uneditable-input{
        vertical-align: top;
    }
    #tipos_chzn{
        width: 436px!important
    }
</style>
<script type='text/javascript'>
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        $('.fileupload').bind('loaded',function(e) {
            var position = e.src.indexOf("base64,");
            if(position > 0){
                $("#binario").val(e.src.substring(position + 7));
                $("#tamanho").val(e.file.size);
                $("#extensao").val(e.file.type);
                $("#arquivo").val(e.file.name);
            }else{
                $("#binario").val("");
            }
            return;
        });
        $("#contrato").submit(function(e){
            e.preventDefault();
            if($("#tipos").val() == "" || $("#binario").val() == ""){
                bootbox.alert("Você deve selecionar um tipo de contrato e um arquivo.");
            }else{
                var context = ko.contextFor($(".metro-button.back")[0]);
                context.$data.showLoading(function() {
                    var dados = {data:{
                       Contrato: {
                           src: $("#binario").val(),
                           tamanho: $("#tamanho").val(),
                           extensao: $("#extensao").val(),
                           nome: $("#arquivo").val(),
                           tipo: $("#tipos").val(),
                       }
                    }};
                    $.ajax({
                        url: $("#contrato").attr("action"),
                        dataType: "json",
                        type: "POST",
                        data: dados,
                        complete: function() {
                            context.$data.page('/<?=$this->params['prefix']?>/contratos/listar');
                        }
                    });
                });
            }
            
        });
    });
</script>
<div class="row-fluid">
   <h2>
       <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/contratos/listar"?>') }">
       </a>
       Adicionar Contrato
   </h2>
</div>
<?php $session->flash(); ?>
<?php echo $form->create('Contrato', array('url' => "/{$this->params['prefix']}/contratos/inserir", 'type' => 'file', 'id' => 'contrato')); ?>
<?php echo $form->hidden('binario', array('id' => 'binario')); ?>
<?php echo $form->hidden('tamanho', array('id' => 'tamanho')); ?>
<?php echo $form->hidden('extensao', array('id' => 'extensao')); ?>
<?php echo $form->hidden('arquivo', array('id' => 'arquivo')); ?>
<div id="row-fluid">
        <label>Tipo de Contrato</label>
        <?php echo $form->select('tipo', $tipos_contratos, array('selected' => $this->data['Contrato']['tipo'] ), 
                    array('empty' => null, 'class' => 'chosen', 'label' => false, 
                    'div' => 'input-control text', 'error' => false, 'id' => 'tipos', 'style' => 'width: 388px'));
        ?>
    <div class="fileupload fileupload-new" data-provides="fileupload" style="margin-top: 50px;"
        data-reader="true">
        <div class="input-append">
          <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
              <span class="fileupload-preview"></span>
          </div>
            <span class="button btn-file botao bg-color-gray fileupload-new">
              <span>Selecione o Contrato</span>
              <input type="file"/>
            </span>
            <span class="button btn-file botao bg-color-orange fileupload-exists">
              <span>Alterar</span>
              <input type="file"/>
            </span>
            <a href="#" class="button fileupload-exists botao bg-color-red" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button bg-color-gray'));?>
</div>
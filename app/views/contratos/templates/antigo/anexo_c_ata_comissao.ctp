<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO C – COMISSÃO DE FORMATURA</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="border-bottom:solid 1px #999">
Fica estipulado a criação de comissão de formatura do curso, faculdade e conclusão acima listados, 
representando os alunos nos assuntos refente a formatura
<br />
<br />
Os membros são:
<br />
</p>
<p></p>
<p></p>
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>
<?php
/* 
 * Dados recebidos do controller são:
 * $parametros['faculdade']
 * $parametros['curso']
 * $parametros['numero_aderentes']
 * $turma['data_conclusao']
 * $turma['valor_formando']
 * $turma['data_assinatura_contrato']
 * $turma['beneficios_comissao']
 * $turma['negociacoes_contratuais']
 * $turma['valor_total_contrato']
 */

App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("RK Formaturas http://rkformaturas.com.br/"); 

$textfont = 'helvetica';
 
$tcpdf->SetAuthor("RK Formaturas");
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetMargins(30, 40, 30);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);
 
$tcpdf->AddPage();
 
// create some HTML content
$htmlcontent = 

"
<p><b><p><b>ANEXO D – CONTRATO DE ADESÃO E COMPROMISSO </b></p>

<p>PREENCHIMENTO LEGÍVEL(Máquina ou letra de forma)</p>

<p>Pelo presente instrumento particular de prestação de serviços e na melhor forma de direito, o(a) Sr(a):</p>

<p>Nome do aluno: |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>Data de Nascimento: |_||_|/|_||_|/|_||_|, RG Nº: |_||_||_||_||_||_||_||_||_||_|, </p>

<p>CPF: |_||_||_|.|_||_||_|.|_||_||_|-|_||_|,</p>

<p><b>DADOS CADASTRAIS</b>: (os dados cadastrais devem ser preenchidos com atenção par o correto cadastramento e comunicação com os formandos).</p>

<p>Rua / AV. |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>Nº |_||_||_||_||_||_| compl. |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>Bairro: |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>CEP: |_||_||_||_||_|-|_||_||_| Município: |_||_||_||_||_||_||_||_||_||_||_||_||_|,</p>

<p>Telefone: |_||_||_|-|_||_||_||_||_||_||_||_|, (Com.) |_||_||_|-|_||_||_||_||_||_||_||_|</p>

<p>Celular. |_||_||_|-|_||_||_||_||_||_||_||_|</p>

<p>E-mail: |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>Aluno da: <b>".$this->data['Contrato']['faculdade']."</b></p>

<p>Curso: |_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_||_|</p>

<p>Sala/Turma: |_||_||_||_|, Período: |_|M/|_|V/|_|N,</p>

<p>Numeração para calçado |_|, Tamanho para camisetas |_| e Altura |_|.</p>

<p>(obs: este item é meramente informativo, não obrigando a empresa contratada ao fornecimento de nenhum brinde associado a estes dados).</p>

<p>Nome para correspondência (reescreva aqui), com o máximo de atenção e capricho, e, principalmente de forma legível, o nome que você quer que apareça em todos os processos referentes às festividades de sua formatura, tais como: Informativos, correspondências, materiais gráficos (convites, crachás, etc.), cerimonial, clipes, entre outros. Favor incluir acentos ou outros caracteres.</p>

<p><b>OPÇÕES DE PAGAMENTO</b> (os dados financeiros, bem como a opção de pagamento deverão ser preenchidos para a validação do contrato).</p>

<p>Valor R$ |_||_||_||_|,|_||_| Número de parcelas |_||_| Data de Primeira parcela |_||_|/|_||_|/|_||_|</p>

<p>Opção de pagamento: |_|Cheque pré-datado |_| Boletos Bancários.</p>

<p><b>Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a comissão de Formatura de seu curso e a empresa ÁS EVENTOS LTDA</b>, inscrita no CNPJ/MF sob Nº 06.163.144/0001-45, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000 São Paulo – SP, representada neste ato pelo sócio – diretor RACHID SADER, portador do RG: Nº 26.572.035-7, contrato esse assinado em <b>".$this->data['Contrato']['data_contrato']."</b>, que será denominado <b>CONTRATO COLETIVO</b> e regido nos seguintes moldes:</p>

<p>1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos assuntos relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, ratificando os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem necessários à realização dos eventos.</p>

<p>2. As informações importantes a respeito do seu contrato estarão disponibilizadas no link:</p>

<p><a href=\"http://rkformaturas.com.br/int.espa%C3%A7odosformandos/index.php\"><b>http://rkformaturas.com.br/int.espaçodosformandos/index.php</b></a>. Caso precise d login e senha da sua turma, entre em contato com a sua comissão ou diretamente com a ÁS Eventos.</p>

<p>3. A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais com forma de pagamento, renegociação de dívida ou cancelamento de contrato em nome do formando.</p>

<p>4. O formando acima qualificado autoriza a RK Formaturas a representá-lo na contratação e pagamento de fornecedores conforme descrito no CONTRATO COLETIVO. O valor pago pelo CONTRATANTE permanecerá em posse da CONTRATADA de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a CONTRATADA é intermediária, ficando tão-somente com a comissão que lhe faz jus.</p>

<p>5. O(A) CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de formatura descritos no CONTRATO COLETIVO e resumidos no boletim informativo entregue para o formando junto com esse contrato.</p>

<p>6. <b>No momento da retirada de convites o formando que optar por boletos bancários deverá quitar as parcelas em atraso e trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que tiver optado por cheques pré-datado e ainda não tiver os entregues, deverá entregar os mesmos.</b></p>

<p>7. Os valores consignados neste contrato serão atualizados de 12 em 12 messes, a partir da assinatura do Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura e a ÁS EVENTOS LTDA, (<b>".$this->data['Contrato']['data_contrato']."</b>) adotando-se a variação de IGP M/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. A ÁS Eventos não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.</p>

<p>8. Os convites só serão entregues depois de feito o acerto financeiro, isto é, pendências devidamente quitadas, isto inclui os valores relativos o IGPM, cheques trocados e entregues, conforme explicitado nos itens 6 e 7.</p>

<p>9. No caso de rescisão contratual, o aluno poderá rescindir até 90 dias antes do término do ano letivo de conclusão do curso, desde que pago a título de multa o valor dos itens ou brindes já entrego ou contratado para o formando adicionado de 20% de valor total do contrato. Após este período será exigido o pagamento integral do Contrato, exceto devido a motivos justificados e comprovados, tais como reprovação ou transferência por motivo s profissionais, nesse caso ficando o formando libre da multa.</p>

<p>10. Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.</p>

<p>11. Considerando que os convites de luxo serão montados com fotos da turma e/ou formando, a Comissão de Formatura avisará todos os formandos da data/local e horário marcado junto à CONTRATADA para ser fotografado, estando o formando ciente que sua ausência isentará a CONTRATADA de qualquer responsabilidade referente às fotografias no convite.</p>

<p>12. A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa.</p>

<p>13. Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.</p>

<p>E assim, por estarem justos e contratados.</p>

<p>São Paulo,______de______________de 20______</p>

<p>__________________________ __________________________</p>

<p>CONTRATADO CONTRATANTE</p>

</b></p>

";

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0, 'J');

echo $tcpdf->Output(date('Y-m-d H:i:s').' - Anexo D.pdf', 'D'); 
?>
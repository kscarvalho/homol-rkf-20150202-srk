<style type='text/css'>
thead th { background:#ccc; padding:3px!important }
thead tr th { padding-top:20px }
th,td { font-weight:normal; text-align:center!important }
tr td { padding:6px 3px!important }
.legenda { border:none; margin-bottom:10px }
.legenda:not(:first-child) { margin-top:20px }
.legenda h3 { margin:10px 0; font-size:16px; font-weight:bold }
h3.red { font-size:15px; color:red; text-align:left }
</style>
<label id="conteudo-titulo" class="box-com-titulo-header">Relat&oacute;rio financeiro</label>
<div class="clearfix">
	<?php foreach($formandos as $formando) : ?>
	<div class="legenda" style="overflow:hidden; width:95%">
		<h3><?=$formando['codigo_formando']?> - <?=$formando['nome']?> - Aderido em <?=date('d/m/Y',strtotime($formando['data_adesao']))?></h3>
		<?php if($formando['situacao'] == 'cancelado') : ?>
			<h4>Contrato Cancelado</h4>
		<?php else : ?>
		<table>
			<thead>
				<tr>
					<th>Total<br />Ades&atilde;o</th>
					<th>Valor pago<br />Ades&atilde;o</th>
					<th>Parcelas<br />Ades&atilde;o</th>
					<th>Parcelas Abertas<br />Ades&atilde;o</th>
					<th>Total<br />Igpm</th>
					<th>Pagamento<br />Igpm</th>
					<th>Total<br />Extras</th>
					<th>Valor Pago<br />Extras</th>
					<th>Saldo<br />Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>R$<?=number_format($formando['total_adesao'],2,',','.');?></td>
					<td>R$<?=number_format($formando['pago_adesao'],2,',','.');?></td>
					<td><?=$formando['qtde_parcelas']?></td>
					<td><?=$formando['parcelas_abertas']?></td>
					<td>R$<?=number_format($formando['total_igpm'],2,',','.');?></td>
					<td>R$<?=number_format($formando['pago_igpm'],2,',','.');?></td>
					<td>R$<?=number_format($formando['total_extras'],2,',','.');?></td>
					<td>R$<?=number_format($formando['pago_extras'],2,',','.');?></td>
					<td>R$<?=number_format($formando['total_aberto'],2,',','.');?></td>
				</tr>
			</tbody>
		</table>
		<table>
			<?php if(sizeof($formando['extras']) > 0 ) : ?>
			<thead>
				<tr>
					<th>Evento</th>
					<th>Campanha</th>
					<th>Extra</th>
					<th>Quantidade<br />Retirada</th>
					<th>Quantidade<br />Cancelada</th>
					<th>Quantidade<br />A Retirar</th>
					<th>Quantidade<br />Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($formando['extras'] as $extra) : ?>
				<tr>
					<td><?=$extra['ViewRelatorioExtras']['evento']?></td>
					<td><?=$extra['ViewRelatorioExtras']['campanha']?></td>
					<td><?=$extra['ViewRelatorioExtras']['extra']?></td>
					<td><?=$extra['ViewRelatorioExtras']['quantidade_retirada']?></td>
					<td><?=$extra['ViewRelatorioExtras']['quantidade_cancelada']?></td>
					<td><?=$extra['ViewRelatorioExtras']['status'] == 'a retirar' ?
						$extra['ViewRelatorioExtras']['quantidade_total']-$extra['ViewRelatorioExtras']['quantidade_retirada']-$extra['ViewRelatorioExtras']['quantidade_cancelada'] : 0; ?></td>
					<td><?=$extra['ViewRelatorioExtras']['quantidade_total']?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			<?php else : ?>
			<tr>
				<td><h3 class="red">Formando n&atilde;o comprou extras</h3></td>
			</tr>
			<?php endif; ?>
		</table>
		
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
</div>
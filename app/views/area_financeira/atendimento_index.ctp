<script type="text/javascript">
	jQuery(function($) {
 		$('span[id*="data-despesa"]').bind("click", function() {
 	 		if(!$(this).hasClass('clicado')) {
				var despesa = $(this).attr("id").substring(13);
				$(this).addClass("clicado");
				$(this).fadeOut(500, function() {
					var input = "<input type='text' name='alt-data-despesa' style='padding:3px; font-size:11px; width:70px' value='" + $(this).html();
					input += "' />&nbsp;";
					var button = "<span class='submit button' onclick='alteraData(" + despesa + ")' id='ok-despesa-" + despesa + "'>Ok</span>";
					$(this).html(input + button);
					$(this).fadeIn(500);
				});
 	 		}
		});
		/*
 		$('span[id*="valor-despesa"]').bind("click", function() {
 	 		if(!$(this).hasClass('clicado')) {
				var despesa = $(this).attr("id").substring(14);
				$(this).addClass("clicado");
				$(this).fadeOut(500, function() {
					var input = "<input type='text' name='alt-valor-despesa' title='" + $(this).html() + "' style='padding:3px; font-size:11px; width:70px' ";
					input += "value='" + $(this).html() + "' />&nbsp;";
					var button = "<span class='submit button' onclick='alteraValor(" + despesa + ")' id='ok-valor-despesa-" + despesa + "'>Ok</span>";
					$(this).html(input + button);
					$(this).fadeIn(500);
				});
 	 		}
		});
		*/
 	});

 	function alteraData(despesa) {
 	 	var button = $("#ok-despesa-"+despesa);
 	 	var input = button.prev();
 	 	if(input.val() != "") {
	 	 	var span = $("#data-despesa-"+despesa);
	 	 	button.fadeOut(500, function() {
	 	 	 	span.append("<img src='<?=$this->webroot?>img/load.gif' width='12' /></center><div>");
	 	 	 	var url = "/<?=$this->params["prefix"]?>/area_financeira/despesa_alterar_data";
		 	 	$.ajax({
		 			url : url,
		 			dataType : "json",
		 			type : "POST",
		 			data : { "despesa" : despesa , "data" : input.val() },
		 			success : function(response) {
		 				if(response.error) {
			 				alert('erro ao alterar data da despesa');
							span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.fadeIn(500);
							});
		 				} else {
		 					span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.css("color","green");
			 					span.fadeIn(500);
							});
		 				}
		 			},
		 			error : function(response) {
		 				alert("Erro ao alterar data da despesa");
		 			}
		 	 	});
	 	 	});
 	 	} else {
 	 	 	alert("digite a data de vencimento");
 	 	}
 	}

 	function alteraValor(despesa) {
 		var button = $("#ok-valor-despesa-"+despesa);
 	 	var input = button.prev();
 	 	if(input.val() != "") {
	 	 	var span = $("#valor-despesa-"+despesa);
	 	 	button.fadeOut(500, function() {
	 	 	 	span.append("<img src='<?=$this->webroot?>img/load.gif' width='12' /></center><div>");
	 	 	 	var url = "/<?=$this->params["prefix"]?>/area_financeira/despesa_alterar_valor";
		 	 	$.ajax({
		 			url : url,
		 			dataType : "json",
		 			type : "POST",
		 			data : { "despesa" : despesa , "valor" : input.val() },
		 			success : function(response) {
			 			if(response.error) {
			 				alert('erro ao alterar valor da despesa');
							span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.fadeIn(500);
							});
		 				} else {
		 					span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.css("color","green");
			 					span.fadeIn(500);
			 					$("#saldo-parcela-"+despesa).html("R$"+input.val());
			 					saldoAntigo = $("#saldo-total").html().substring(3);
			 					saldoNovo = parseFloat(saldoAntigo) + parseFloat(response.diferenca);
			 					$("#saldo-total").html("R$ " + saldoNovo);
							});
		 				}
		 			},
		 			error : function(response) {
		 				alert("Erro ao alterar valor da despesa");
		 			}
		 	 	});
	 	 	});
 	 	} else {
 	 	 	alert("digite a data de vencimento");
 	 	}
 	}

 </script>
<?php include("formando_index.ctp") ?>
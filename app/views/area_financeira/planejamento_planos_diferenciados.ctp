<script type="text/javascript">
$(document).ready(function() {
    $(".abrir-modal").click(function(e) {
        e.preventDefault();
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: $(this).attr('href')
        });
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Parcelamentos
    </h2>
</div>
<?php
if(count($planos) > 0) :
    $session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis'); ?>
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th scope="col" width="25%">
                <?=$paginator->sort('Atendente', 'atendente_id',$sortOptions); ?>
            </th>
            <th scope="col" width="30%">
                <?=$paginator->sort('Formando', 'Formando',$sortOptions); ?>
            </th>
            <th scope="col" width="15%">
                <?=$paginator->sort('Data do pedido', 'data_cadastro',$sortOptions); ?>
            </th>
            <th scope="col" width="10%">
                <?=$paginator->sort('Status', 'status',$sortOptions); ?>
            </th>
            <th scope="col" width="20%"></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($planos as $plano) : ?>
        <tr>
            <td><?=$plano['Atendente']['nome']; ?></td>
            <td><?=$plano['Formando']['nome']; ?></td>
            <td><?=date("d/m/Y",strtotime($plano["ParcelamentoDiferenciado"]["data_cadastro"])); ?></td>
            <td><?=ucfirst($plano["ParcelamentoDiferenciado"]["status"])?></td>
            <td style="text-align:center">
                <a class="button mini visualizar default abrir-modal"
                    href="/<?=$this->params["prefix"]?>/area_financeira/planos_diferenciados/<?=$plano['ParcelamentoDiferenciado']['id']?>">
                    Detalhes
                </a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3" class="paginacao">
                <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
            </td>
            <td colspan="3">
                <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Funcionários')); ?>
            </td>
        </tr>
    </tfoot>
</table>
<?php else : ?>
<h2 class="fg-color-red">Nenhum plano encontrado</h2>
<?php endif; ?>

<?php
    $paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('.bancarios').click(function() {
        dir = $(this).attr('dir');
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var url = '<?="/atendimento/area_financeira/dados_bancarios/"?>'+dir;
        context.$data.showLoading(function() {
            $.ajax({
                url : url,
                type: "POST",
                complete : function() {
                    context.$data.page('/atendimento/area_financeira/dados_bancarios/'+dir);
                }
            });
        });
    });
    $('.finalizar').click(function() {
        dir = $(this).attr('dir');
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var url = '<?="/atendimento/area_financeira/dados_bancarios/"?>'+dir;
        context.$data.showLoading(function() {
            $.ajax({
                url : url,
                type: "POST",
                complete : function() {
                    context.$data.page('area_financeira/deposito_formando_finalizar/'+dir);
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Solicitações de Devolução
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
<?php if (sizeof($formandos) == 0 ) { ?>
    <h2 class="fg-color-red">Nenhuma solicitação de devolução.</h2>
<?php } else { ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col"><?=$paginator->sort('Codigo Formando', 'ViewFormandos.codigo_formando',$sortOptions); ?></th>
                    <th scope="col"><?=$paginator->sort('nome', 'ViewFormandos.nome',$sortOptions); ?></th>
                    <th scope="col"><?=$paginator->sort('Nr Protocolo', 'Protocolo.protocolo',$sortOptions); ?></th>
                    <th scope="col"><?=$paginator->sort('Status', 'Protocolo.status',$sortOptions); ?></th>
                    <th scope="col"><?=$paginator->sort('Valor', 'ProtocoloCancelamento.valor_cancelamento',$sortOptions); ?></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($formandos as $formando): ?>
                <tr>
                    <td width="10%"><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td width="20%"><?=$formando['ViewFormandos']['nome']; ?></td>
                    <td width="10%"><?=$formando['Protocolo']['protocolo']; ?></td>
                    <td width="20%"><?=$formando['Protocolo']['status']?></td>
                    <td width="10%">R$ <?=$formando['ProtocoloCancelamento']['valor_cancelamento']?></td>
                    <td width="20%" style="text-align: center">
                        <button class="mini default bg-color-red bancarios" type="button" dir="<?=$formando['ViewFormandos']['id']?>">
                            Dados Bancários
                        </button>
                        <?php if($formando['ProtocoloCancelamento']['banco'] != ''){ ?>
                        <button class="mini default bg-color-greenDark finalizar" type="button" dir="<?=$formando['ViewFormandos']['id']?>">
                            Finalizar
                        </button>
                        <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" class="paginacao">
                        <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
<?php } ?>
</div>


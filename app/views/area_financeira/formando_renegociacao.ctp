<script type="text/javascript">
$(document).ready( function() {
	for(a = 1; a < 32; a++)
		$("#dia").append("<option value='" + a + "'>" + (a < 10 ? "0" : "") + a + "</option>");
	tabelaDatas = "<tr>";
	tabelaValores = "<tr>";
	<?php foreach($parcelas as $parcela) : ?>
		$("#mes-ano").append("<option value='<?=date("m/Y",strtotime($parcela["Parcelamento"]["data"]))?>'><?=date("m/Y",strtotime($parcela["Parcelamento"]["data"]))?></option>");
		tabelaDatas+= "<td><?=date("m/Y",strtotime($parcela["Parcelamento"]["data"]))?></td>";
		tabelaValores+= "<td>R$<?=$parcela["Parcelamento"]["valor"]?></td>";
	<?php endforeach; ?>
	tabelaDatas+= "</tr>";
	tabelaValores+= "</tr>";
	info = "<table><tr><td colspan='2'><h2>Formando</h2></td></tr>";
	info+= "<tr><td>Valor Base do Contrato</td><td>R$<?=number_format($valorTotal, 2, ',', '.'); ?></td></tr>";
	info+= "<tr><td>Valor Pago</td><td>R$<?=number_format($valorPago, 2, ',', '.'); ?></td></tr>";
	info+= "<tr><td>Valor a Liquidar</td><td>R$<?=number_format($valorEmAberto, 2, ',', '.'); ?></td></tr>";
	info+= "</table>";
	$("#valores-novos").html(info);
	$("#valores-novos").show();
	var parcelamentos = <?=json_encode($parcelamentos)?>;
	$("#mes-ano").change( function() {
		var data = $(this).val().split("/");
		data = data[1] + "-" + data[0] + "-01";
		var html = "<h3>Parcelamentos dispon&iacute;veis</h3><br /><ul style='margin:0; padding:0; width:180px'>";
		var somaValor = 0;
		var valorAtual;
		$.each(parcelamentos,function(i,parcelamento) {
			if(parcelamento.Parcelamento.data == data) {
				var parcelas = parseInt(parcelamento.Parcelamento.parcelas);
				var id = parcelamento.Parcelamento.id;
				valorAtual = parseFloat(parcelamento.Parcelamento.valor);
				var valor = ((valorAtual - <?=$valorPago?>) / parcelas).toFixed(2);
				html += "<li style='padding:0 0 3px 5px; border-bottom:solid 1px #B5B1B3; margin-bottom:5px'>";
				html += "<input type='radio' name='parcelamento' value='" + id + ":" + valor + "' />&nbsp;&nbsp;&nbsp;<span>";
				html += (parcelas < 10 ? "0" + parcelas : parcelas) + "</span>&nbsp; X&nbsp; de&nbsp; R$ <span>" + (valor < 10 && valor > 0 ? "0" + valor : valor) + "</span></li>";
				somaValor += parseFloat(valor);
			}
		});
		$("body").append("<input type='hidden' value='" + valorAtual + "' name='valor-atual' id='valor-atual' />");
		if(somaValor < 0) {
			html = "<h3>Encotramos um erro no seu cadastro.<br />";
			html += "Por favor entre em contato conosco no n&uacute;mero (11) 5585 - 1294 .";
		} else {
			$("#submit").html("<span class='button submit' id='renegociar' style='margin-left:15px'>Finalizar</span>");
		}
		html += "</ul>";
		$("#parcelamentos").html(html);
		$("input[name=parcelamento]").click( function() {
			var valor = $("#valor-atual");
			var valorAtual = valor.val();
			var parcelas = $(this).next().html();
			var valorParcela = $(this).next().next().html();
			html = "<table><tr><td colspan='2'><h2>Valores Atualizados Para As Condi&ccedil;&otilde;es Escolhidas</h2></td></tr>";
			html += "<tr><td>Valor Atual</td><td>R$ " + valorAtual + "</td></tr>";
			html += "<tr><td>Valor Base Para Renegocia&ccedil;&atilde;o</td><td>R$ " + (valorAtual - <?=$valorPago?>).toFixed(2);
			html += " <strong style='color:green; font-weight:400'>(Valor Atual - Valor Pago = Valor Base)</strong></td></tr>";
			html += "<tr><td>Quantidade de Parcelas Escolhidas</td><td>" + parcelas + "</td></tr>";
			html += "<tr><td>Valor Por Parcela</td><td>R$ " + valorParcela + "</td></tr>";
			$("#valores-novos").html(html);
			$("#valores-novos").show();
		});
		$("#renegociar").click( function() {
			var parcelamento = $("input[name=parcelamento]:checked");
			dados = parcelamento.val().split(":");
			var parcelas = parseInt(dados[0]);
			var valorParcela = dados[1];
			if(isNaN(parcelas)) {
				alert("Selecione um parcelamento");
			} else if($("#dia").val() == "") {
				alert("Selecione o dia de pagamentos das parcelas");
			} else {
				$(this).fadeOut(500, function() {
					var load = "<img src='<?=$this->webroot?>img/load.gif' width='15' />";
					$(this).parent().append(load);
					url = "/<?=$this->params["prefix"]?>/area_financeira/renegociar";
					$.ajax({
	 	 	 	 		url : url,
	 	 	 	 		data : { "usuario" : $("#id-usuario").val() , "parcelamento" : parcelas , "valorParcela" : valorParcela , "diaVencimento" : $("#dia").val() },
	 	 	 	 		type : "POST",
	 	 	 	 		dataType : "json",
	 	 	 	 		success : function(response) {
		 	 	 	 		if(response.erro)
		 	 	 	 			$("#parcelamentos").html("<h1>Ocorreu um erro com os nossos servidores. Entre em contato conosco</h1>");
		 	 	 	 		else
		 	 	 	 			$("#parcelamentos").html("<h1>Seu contrato foi renegociado com sucesso!</h1>");
		 	 	 	 		$("#dados").fadeOut(500);
	 	 	 	 		},
	 	 	 	 		error : function() {
		 	 	 	 		alert("Erro ao conectar servidor. Tente novamente mais tarde.");
	 	 	 	 		}
	 	 	 		});
				});
			}
		});
	});
});
</script>
<style type="text/css">
.info { margin:20px 0 20px 0!important }
.info tr:first-child { background-color:#333333; color:white }
.info tr td { padding:5px!important; text-align:left!important }
</style>
<label id="conteudo-titulo" class="box-com-titulo-header">Relat&oacute;rio financeiro</label>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div style="overflow:hidden; width:100%">
	<div class="legenda" style="width:30%">
		<table>
			<tr>
				<td colspan="2"><h2>Resumo Financeiro</h2></td>
			</tr>
			<tr>
				<td>Valor Pago</td>
				<td><?=$valorPago > 0 ? "<em style='color:green'>R$ $valorPago</em>" : "-"?></td>
			</tr>
			<tr>
				<td>Quantidade de Parcelas</td>
				<td><em><?=$formando['ViewFormandos']['parcelas_adesao']?></em></td>
			</tr>
		</table>
	</div>
	<div class="legenda" style="min-width:30%" id='valores-novos'></div>
	</div>
	<input type="hidden" name="id-usuario" id='id-usuario' value="<?=$formando['ViewFormandos']['id']?>" />
	<div style="clear:both;"></div>
	<br />
	<h1>Selecione a data da primeira parcela</h1>
	<br />
	<br />
	<p style="font-size:14px" id='dados'>
		Dia&nbsp;
		<select name='dia' id='dia'>
			<option value="">Selecione</option>
		</select>
		&nbsp;M&ecirc;s/Ano&nbsp;
		<select name='mes-ano' id='mes-ano'>
			<option value="">Selecione</option>
		</select>
		<span id='submit'></span>
	</p>
	<br />
	<br />
	<p id="parcelamentos" style="font-size:14px"></p>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$("#formulario").submit(function(e) {
		e.preventDefault();
		window.location.href = $("#formulario").attr("action")+"/"+$("#input").val();
	});
})
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Dados Financeiros de Turma</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/area_financeira/despesas_turma",
			'class' => 'procurar-form-inline','id' => 'formulario')); ?>
		<span>Código da Turma: <?=$form->input('codigo_formando',
				array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'id' => 'input')); ?> </span>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca','id' => 'buscar')); ?>
		<div style="clear:both;"></div>
	</div>
</div>
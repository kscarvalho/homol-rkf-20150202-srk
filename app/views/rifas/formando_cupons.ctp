<?php $session->flash(); ?>
<?php if (!empty($rifa)) : ?>
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/cupons.css">
    <style type="text/css">
        h3 { line-height:37px }
        h3 i { position:relative; top:2px; font-size:22px!important; margin-right:5px }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".marcar-venda").click(function() {
                var cupom = $(this).attr('dir');
                var td = $(this).parent();
                if (!td.children('.campo-venda').hasClass('campo-venda')) {
                    var input = '<div class="campo-venda" style="display:none"><input type="text" class="input-small nome" />' +
                            '&nbsp;<button type="button" class="mini bg-color-blue salvar-venda" onclick="salvarVenda(this)">' +
                            'Salvar</button><button type="button" class="mini bg-color-red cancelar-venda">' +
                            '<i class="icon-cancel-3"></i></button></div>';
                    td.append(input);
                }
                var campoVenda = td.children('.campo-venda');
                $(this).fadeOut(500, function() {
                    campoVenda.fadeIn(500, function() {
                        campoVenda.children('.nome').focus()
                    });
                });
            });
            $("#cupons").on('click','.cancelar-venda', function() {
                var campoVenda = $(this).parent();
                var botaoVenda = campoVenda.prev();
                campoVenda.fadeOut(500, function() {
                    botaoVenda.fadeIn(500)
                });
            });
        })
        function salvarVenda(obj) {
            $(obj).attr('disabled', 'disabled');
            var campoVenda = $(obj).parent();
            var td = campoVenda.parent();
            var botaoVenda = campoVenda.prev();
            var nome = campoVenda.children('.nome').val();
            if (nome == "") {
                bootbox.alert('Digite o nome antes de marcar');
                $(obj).removeAttr('disabled', 'disabled');
            }
            var cupom = botaoVenda.attr('dir');
            var url = '<?= $html->url(array($this->params['prefix'] => true, "controller" => "rifas", "action" => "atualiza_nome")); ?>';
            $.ajax({
                url: url,
                data: {data: {nome: nome, cupom_id: cupom}},
                type: "POST",
                dataType: "html",
                async: true,
                cache: false,
                success: function(response) {
                    if (response == 'sucesso') {
                        campoVenda.fadeOut(500, function() {
                            td.html("<b class='fg-color-green strong'>" + nome + "</b>");
                            td.next().html('Vendido');
                        });
                    } else if (response == 'not_authorized') {
                        bootbox.alert('Ação não autorizada');
                    } else if (response == 'vendido') {
                        bootbox.alert('Cupom já marcado como vendido');
                    } else {
                        bootbox.alert('Erro ao marcar cupom como vendido');
                    }
                },
                error: function() {
                    bootbox.alert('Erro ao enviar dados');
                },
                complete: function() { }
            });
        }
    </script>
    <div class="row-fluid">
        <div class="span12">
            <h2 class="fg-color-red">
                <a class="metro-button reload fg-color-red border-color-red" data-bind="click: function() { reload() }"></a>
                <?= $rifa['Rifa']['nome']; ?>
            </h2>
        </div>
    </div>
    <br />
    <div class="alert alert-error">
        Sorteio do(s) pr&ecirc;mio(s) ser&aacute; em <?=$rifa['Rifa']['data_sorteio'] ?>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h3 class="fg-color-yellow"><i class="icon-trophy"></i> 1&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_1'] ?></h3>
            <?php if (!empty($rifa['Rifa']['premio_2'])) : ?>
                <h3 class="fg-color-grayDark"><i class="icon-trophy"></i> 2&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_2'] ?></h3>
    <?php endif; ?>
    <?php if (!empty($rifa['Rifa']['premio_3'])) : ?>
                <h3 class="fg-color-orangeDark"><i class="icon-trophy"></i> 3&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_3'] ?></h3>
    <?php endif; ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <a href="/<?= $this->params['prefix'] ?>/rifas/imprimir" class="button" target="_blank">
                <i class="icon-printer"></i>
                Imprimir Bloco de Rifas
            </a>
        </div>
        <div class="span6">
            <a href="/<?= $this->params['prefix'] ?>/rifas/imprimir_cartela" class="button" target="_blank">
                <i class="icon-printer"></i>
                Imprimir Cartela
            </a>
        </div>
    </div>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <?php $sortOptions = array('data-bind' => 'click: loadThis'); ?>
    <div class="row-fluid">
        <table class="table" id="cupons">
            <thead>
                <tr>
                    <th scope="col" class="border-left"><?= $paginator->sort('Numero', 'numero', $sortOptions); ?></th>
                    <th scope="col"><?= $paginator->sort('Nome Comprador', 'nome_comprador', $sortOptions); ?></th>
                    <th scope="col"><?= $paginator->sort('Status', 'status', $sortOptions); ?></th>
                    <th scope="col"class="border-left"><?= $paginator->sort('Numero', 'numero', $sortOptions); ?></th>
                    <th scope="col"><?= $paginator->sort('Nome Comprador', 'nome_comprador', $sortOptions); ?></th>
                    <th scope="col"><?= $paginator->sort('Status', 'status', $sortOptions); ?></th>				
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3" class="border-left">
    <?= $paginator->counter(array('format' => 'Total: %count% cupons | %start% de %end% | página %page% de %pages%')); ?>
                    </td>
                    <td colspan="3" class="border-left">
                <?= $paginator->numbers(array('separator' => ' ', 'data-bind' => 'click: loadThis')); ?>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <?php for ($i = 0; $i < sizeof($cupons1); $i++) { ?>
        <?php
        $cupom1 = $cupons1[$i];
        $cupom2 = $cupons2[$i];
        ?>
                    <tr>
                        <td width="10%" class="border-left"><?= $cupom1['Cupom']['numero'] % 100000; ?></td>
                        <td width="30%">
        <?php if ($cupom1['Cupom']['nome_comprador']) : ?>
                                <b class='fg-color-green strong'><?= $cupom1['Cupom']['nome_comprador']; ?></b>
                            <?php else : ?>
                                <button type="button" class="bg-color-gray mini marcar-venda" dir='<?= $cupom1['Cupom']['id'] ?>'>
                                    <i class="icon-pencil"></i>
                                    Marcar Como Vendido
                                </button>
        <?php endif; ?>
                        </td>
                        <td width="10%"><?= ucwords($cupom1['Cupom']['status']); ?></td>
                            <?php if (!empty($cupom2['Cupom']['numero'])) : ?>
                            <td width="10%" class="border-left"><?= $cupom2['Cupom']['numero'] % 100000; ?></td>
                            <td width="30%">
            <?php if ($cupom2['Cupom']['nome_comprador']) : ?>
                                    <b class='fg-color-green strong'><?= $cupom2['Cupom']['nome_comprador']; ?></b>
                                <?php else : ?>
                                    <button type="button" class="bg-color-gray mini marcar-venda" dir='<?= $cupom2['Cupom']['id'] ?>'>
                                        <i class="icon-pencil"></i>
                                        Marcar Como Vendido
                                    </button>
                            <?php endif; ?>
                            </td>
                            <td width="10%"><?=ucwords($cupom2['Cupom']['status']); ?></td>
                    <?php else : ?>
                            <td colspan="3" class="border-left"></td>
        <?php endif; ?>
                    </tr>
    <?php } ?>
            </tbody>
        </table>
    </div>
<?php else : ?>
    <div class="row-fluid">
        <div class="span12">
            <h2>
                <a class="metro-button back" data-bind="click: function() { hideHomeButton() }"></a>
                Voltar
            </h2>
        </div>
    </div>
    <h2 class="fg-color-red">Nenhuma Rifa Encontrada.</h2>
<?php endif; ?>


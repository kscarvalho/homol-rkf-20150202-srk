<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.alterar').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/rifas/alterar/"?>'+
                            $(this).attr('dir')
            });
        });
        $('.inserir').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/rifas/inserir/"?>'
            });
        });
        $('.status').click(function() {
            var status = $(this).text();
            dir = $(this).attr('dir');
            ativo = $(this).attr('ativo') == 0 ? 1 : 0;
            bootbox.confirm('Tem certeza que deseja <b>' + status + '</b> esta rifa?',function(response) {
                if(response) {
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    var url = '<?="/{$this->params['prefix']}/rifas/alterar_status/"?>'+dir+'/'+ativo;
                    context.$data.showLoading(function() {
                        bootbox.hideAll();
                        $.ajax({
                            url : url,
                            dataType : "json",
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                }
            });
        });
        $(".turmas").click(function(){
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                context.$data.page('<?="/{$this->params['prefix']}/rifas/turmas/"?>');
            });
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Lista de Rifas
        </h2>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <?=$form->create('Rifa',array(
        'url' => "/{$this->params['prefix']}/rifas/listar/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('nome',
                array('label' => 'Nome', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        <div class="span4">
            <label>&nbsp;</label>
            <button type="button" class="button mini pull-right bg-color-yellow turmas">
                Turmas Relacionadas
                <i class="icon-search-2"></i>
            </button>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type="button" class="button mini pull-right bg-color-greenDark inserir">
                Inserir Rifa
                <i class="icon-upload"></i>
            </button>
        </div>
    </div>
<?php if (sizeof($rifas > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="20%"><?=$paginator->sort('Nome', 'Rifa.nome',$sortOptions); ?></th>
                <th scope="col" width="35%"><?=$paginator->sort('Prêmios', 'Rifa.premios',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Data Sorteio', 'Rifa.data_sorteio',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Ativa', 'Rifa.ativa',$sortOptions); ?></th>
                <th scope="col" width="20%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($rifas as $rifa) : ?>
            <tr>
                <td width="25%"><?=$rifa['Rifa']['nome']; ?></td>
                <td width="35%"><?php 
                                    echo substr($rifa['Rifa']['premio_1'].'<br />',0,50);
                                    echo substr($rifa['Rifa']['premio_2'].'<br />',0,50);
                                    echo substr($rifa['Rifa']['premio_3'],0,50);
                                ?>
                </td>
                <td width="20%"><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($rifa['Rifa']['data_sorteio']))?></td>
                <td width="5%" style="text-align:center"><?=($rifa['Rifa']['ativa'] == 1) ? 'Sim' : 'Não' ?></td>
                <td width="15%" style="text-align:center">
                    <button type="button" class="default mini bg-color-orangeDark status" dir="<?=$rifa['Rifa']['id']?>" ativo="<?=$rifa['Rifa']['ativa']?>">
                            <?=($rifa['Rifa']['ativa'] == 1) ? 'Desativar' : 'Ativar'  ?>
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="3">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Rifas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Rifa Encontrada.</h2>
<?php endif; ?>
</div>
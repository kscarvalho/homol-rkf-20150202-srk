<table>
    <thead>
        <tr>
            <th scope="col">Cod Formando</th>
            <th scope="col">Rifa</th>				
            <th scope="col">No</th>
            <th scope="col">Data Gerado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($cupons as $cupom): ?>
        <tr>
            <td><?=$cupom['Usuario']['codigo_formando']; ?></td>
            <td><?=$cupom['Rifa']['nome']; ?></td>
            <td><?=str_pad($cupom['Cupom']['numero'] % 100000, 5, '0',STR_PAD_LEFT); ?></td>
            <td>
                <?php if($form->validarData($cupom['Cupom']['data_cadastro'],'Y-m-d H:i:s')) : ?>
                <?=date('d/m/Y', strtotime($cupom['Cupom']['data_cadastro'])); ?>
                <?php else : ?>
                Indefinido
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
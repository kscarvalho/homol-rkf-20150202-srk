<script type="text/javascript">
    $(function() {
        $( "#datepicker_data_sorteio" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>
<?php echo $form->input('id', array('hiddenField' => true)); ?>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">1&deg; Prêmio</label>
	<?php echo $form->input('premio_1', array('class' => 'grid_16 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_16'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">2&deg; Prêmio</label>
	<?php echo $form->input('premio_2', array('class' => 'grid_16 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_16'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">3&deg; Prêmio</label>
	<?php echo $form->input('premio_3', array('class' => 'grid_16 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_16'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data do Sorteio</label>
	<?php echo $form->input('data-sorteio', array('class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker_data_sorteio'));?>
</p>
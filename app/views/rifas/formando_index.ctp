
<script type="text/javascript">
	
	function resetaInputNome(obj) {
		if(obj.val() == "") {
			obj.val("Insira o nome do comprador");
			obj.css('color',"#888");
		}
	}
	
	$(document).ready(function() {
		$(".link_marcar_vendido").click(function($this) {
			$(this).parent().fadeOut(
				'fast',
				function () {
						resetaInputNome($(this).parent().children('.form-marcar-vendido').children('.input-nome-comprador')); 
						$(this).parent().children('.form-marcar-vendido').fadeIn('slow');
						
				});

		});
		
		
		
		$(".input-nome-comprador").blur(function() {
			resetaInputNome($(this));
		});
		
		
		$(".input-nome-comprador").focus(function() {
			if($(this).val() == "Insira o nome do comprador") {
				$(this).val("");
			}
		});
		
		$(".botao_salvar").click(function() {
			numero_cupom =  $(this).parent().children('.input-numero-cupom').val();
			id_cupom =  $(this).parent().children('.input-id-cupom').val();
			nome_comprador = $(this).parent().children('.input-nome-comprador').val();
			confirmado = confirm("Deseja marcar o cupom " + numero_cupom + " como vendido para " + nome_comprador + " ? Não será possível fazer modificações após a confirmação.");
			
			if(!confirmado) {
				return false;
			}
			
			$(this).attr('id','working');
			obj = $(this);
			$.post('<?php echo $html->url(array($this->params['prefix'] => true,"controller" => "rifas",    "action" => "atualiza_nome"));?>', { data: { nome: nome_comprador, cupom_id: id_cupom} },
				function(data) {
					if(data == 'sucesso') {
						$('#working').parent('.form-marcar-vendido').fadeOut('slow', function() {
							$('#working').parent().parent().children('.nome-holder').css('display','none');
							$('#working').parent().parent().children('.nome-holder')[0].innerHTML = nome_comprador;
							$('#working').parent().parent().children('.nome-holder').fadeIn('slow');
							$('#working').parent().parent().next('td')[0].innerHTML = 'Vendido';
							$('#working').attr('id','');
						});

					} else if ( data == 'not_authorized') {
						$('#working').attr('id','');
						alert('Ação não autorizada');
					} else if ( data == 'vendido') {
						$('#working').attr('id','');
						alert('Erro: O cupom já estava marcado como vendido.');
					} else {
						$('#working').attr('id','');
						alert('Ocorreu um erro ao atualizar o cupom.');
					}
					
					
				});
			
			
				
		});

		
	});
	
	
	
</script>

<span id="conteudo-titulo" class="box-com-titulo-header">Rifa Online</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<h1><b> <?php echo $rifa['Rifa']['nome'];?></b></h1>
	<div class="grid_full alpha omega detalhes">
		<p class="grid_11 alpha omega first" style="margin-top: 10px;font-size: 14px;">
			<label class="grid_11 alpha omega">Sorteio dia</label>
			<span class="grid_11 alpha first"><?php echo $rifa['Rifa']['data_sorteio'];?></span>
		</p>
		
		<p class="grid_11 alpha omega first" style="margin-top: 10px;font-size: 14px;">
			<label class="grid_11 alpha omega">Pr&ecirc;mios</label>
			<span class="grid_11 alpha first">
				<?php
					echo "1&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_1']."<br />";
					if ($rifa['Rifa']['premio_2'] != "") {
						echo "2&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_2']."<br />";
					}
					if ($rifa['Rifa']['premio_3'] != "") {
						echo "3&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_3'];
					}
				?>
			</span>
		</p>
		
		<?php if ($rifa['Rifa']['numero_sorteado'] != "") { ?>
			<p class="grid_11 alpha omega first" style="margin-top: 10px;font-size: 14px;">
				<label class="grid_11 alpha omega">N&uacute;mero sorteado</label>
				<span class="grid_11 alpha first">
					<?php
						echo $rifa['Rifa']['numero_sorteado'];
					?>
				</span>
			</p>
		<?php } ?>
	</div>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Imprimir Bloco de Rifas',array($this->params['prefix'] => true, 'action' => 'imprimir'), array('target' => '_blank')); ?>
		<?php echo '<br />&nbsp;<br /><br />'; ?>
		<?php echo $html->link('Imprimir Cartela',array($this->params['prefix'] => true, 'action' => 'imprimir_cartela'), array('target' => '_blank')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela-cupons">
		<table class="tabela-cupons">
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Numero', 'numero'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome Comprador', 'nome_comprador'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Status', 'status'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Numero', 'numero'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome Comprador', 'nome_comprador'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Status', 'status'); ?></th>				
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"><?php echo $paginator->counter(array('format' => 'Cupons %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% cupons')); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php for($i=0; $i<sizeof($cupons1) ; $i++) {
				$cupom1 = $cupons1[$i];
				$cupom2 = $cupons2[$i];
				?>
				<tr class="<?=$isOdd ? "odd" : ""?>">
					<td colspan="1" width="10%">
						<?php echo $cupom1['Cupom']['numero'];?></td>
					<td colspan="1" width="30%">
						<?php 
							if($cupom1['Cupom']['nome_comprador'] != "") {
								echo $cupom1['Cupom']['nome_comprador'];
							} else {
								?>
								<span class="nome-holder">
								</span>
								<span class="botao_marcar_vendido">
									<a class="link_marcar_vendido">Marcar como vendido</a>
								</span>
								<span class="form-marcar-vendido" style="display:none">
									<?php $form->create('Cupom', array('url' => "/{$this->params['prefix']}/rifas/atualiza_nome", 'default' => false));?>
									<input type="hidden" class="input-numero-cupom" value="<?php echo $cupom1['Cupom']['numero'];?>" />
									<input type="hidden" class="input-id-cupom" value="<?php echo $cupom1['Cupom']['id'];?>" />
									<input type="text" class="input-nome-comprador" size="30"> </input>
									<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit botao_salvar'));?>
								</span>
								<?php		
							}
						?>
					</td>
					<td colspan="1" width="10%" style="border-right: 1px dashed #bbb">
						<?php echo ucwords($cupom1['Cupom']['status']);?>
					</td>
					
					<td colspan="1" width="10%">
					<?php echo $cupom2['Cupom']['numero'];?>
					</td>
					<td colspan="1" width="30%">
						<?php 
							if($cupom2['Cupom']['nome_comprador'] != "") {
								echo $cupom2['Cupom']['nome_comprador'];
							} else {
								?>
								<span class="nome-holder">
								</span>
								<span class="botao_marcar_vendido">
									<a class="link_marcar_vendido">Marcar Vendido</a>
								</span>
								<span class="form-marcar-vendido" style="display:none">
									<?php $form->create('Cupom', array('url' => "/{$this->params['prefix']}/rifas/atualiza_nome", 'default' => false));?>
									<input type="hidden" class="input-numero-cupom" value="<?php echo $cupom2['Cupom']['numero'];?>" />
									<input type="hidden" class="input-id-cupom" value="<?php echo $cupom2['Cupom']['id'];?>" />
									<input type="text" class="input-nome-comprador" size="30"> </input>
									<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit botao_salvar'));?>
								</span>
								<?php		
							}
						?>
					</td>
					<td colspan="1" width="10%">
						<?php echo ucwords($cupom2['Cupom']['status']);?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php }; ?>
			</tbody>
		</table>
	</div>
</div>
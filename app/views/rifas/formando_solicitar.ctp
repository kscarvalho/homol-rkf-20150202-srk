<style type="text/css">
    h3 { line-height:37px }
    h3 i { position:relative; top:2px; font-size:22px!important; margin-right:5px }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $(".bootmodal").click(function(e) {
            e.preventDefault();
            bootbox.alert($("#" + $(this).attr("id") + "-content").html());
        });

    });

    var context = ko.contextFor(document.getElementById("content-body"));

    function enviarSolicitacao() {
        var url = "/<?= $this->params["prefix"] ?>/rifas/solicitar";
        $.ajax({
            url: url,
            data: {data: {Termos: {aceito: 1}}},
            type: "POST",
            dataType: "json",
            async: true,
            complete: function() {
                context.$data.reload();
            }
        });
    }

    function solicitar(loading,reload) {
        if (!$("#termos-rifa").is(":checked")) {
            bootbox.dialog("Você deve aceitar os termos e condições para prosseguir.", [{
                    'label': 'Li e Aceito os Termos e Condi&ccedil;&otilde;es',
                    'class': 'btn-success',
                    'callback': function() {
                        context.$data.showLoading(function() {
                            enviarSolicitacao()
                        });
                    }
                }, {
                    'label': 'Voltar'
                }]);
        } else {
            context.$data.showLoading(function() {
                enviarSolicitacao();
            });
        }
    }
</script>
<div class="row-fluid">
    <div class="span12">
        <h2 class="fg-color-red">
            <a class="metro-button reload fg-color-red border-color-red" data-bind="click: function() { reload() }"></a>
            <?= $rifa['Rifa']['nome']; ?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<br />
<div class="alert alert-error">
    Sorteio do(s) pr&ecirc;mio(s) ser&aacute; em <?= date('d/m/Y', strtotime($rifa['Rifa']['data_sorteio'])) ?>
</div>
<div class="row-fluid">
    <div class="span12">
        <h3 class="fg-color-yellow"><i class="icon-trophy"></i> 1&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_1'] ?></h3>
        <?php if (!empty($rifa['Rifa']['premio_2'])) : ?>
            <h3 class="fg-color-grayDark"><i class="icon-trophy"></i> 2&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_2'] ?></h3>
        <?php endif; ?>
        <?php if (!empty($rifa['Rifa']['premio_3'])) : ?>
            <h3 class="fg-color-orangeDark"><i class="icon-trophy"></i> 3&ordm; Pr&ecirc;mio - <?= $rifa['Rifa']['premio_3'] ?></h3>
        <?php endif; ?>
    </div>
</div>
<br />
<hr />
<div class="row-fluid">
    <div class="span6">
        <label>&nbsp;</label>
        <label class="input-control checkbox">
            <input type="checkbox" id="termos-rifa">
            <span class="helper">&nbsp;Li e Aceito os Termos e Condi&ccedil;&otilde;es da Rifa Online</span>
        </label>
    </div>
    <div class="span6">
        <button class="command-button bg-color-grayDark white bootmodal" id="termos-condicoes">
            Termos e Condi&ccedil;&otilde;es da Rifa Online
            <small>Clique Aqui Para Ler os Termos</small>
        </button>
    </div>
</div>
<br />
<div class="row-fluid">
    <button type="button" class="big default"
            data-bind="click: function() {
            solicitar(showLoading,page) }">
        <i class="iconui-bookmark iconui-large"></i>
        Solicitar Cupons
    </button>
</div>
<div style='display:none' id="termos-condicoes-content">
    <h2>Termos e Condi&ccedil;&otilde;es da Rifa Online</h2>
    <br />
    Os prêmios serão sorteados com os números da loteria federal.
    <br />
    Os prêmios deverão ser retirados <span class="label">PELO FORMANDO</span> que vendeu a rifa em um prazo máximo de 15 dias após o sorteio.
    <br />
    <br />
    <span class="label label-info">IMPORTANTE!</span> Cada formando ter&aacute; direito a apenas uma rifa.
    <br />
    Se gerar os cupons para esta rifa, n&atilde;o poder&aacute; participar de outras rifas.
</div>
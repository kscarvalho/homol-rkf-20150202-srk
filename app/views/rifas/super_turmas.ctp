<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.inserir').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/rifas/relacionar/"?>'
            });
        });
        $('.status').click(function() {
            var status = $(this).text();
            dir = $(this).attr('dir');
            ativo = $(this).attr('ativo') == 0 ? 1 : 0;
            bootbox.confirm('Tem certeza que deseja <b>' + status + '</b> as rifas desta turma?',function(response) {
                if(response) {
                    var context = ko.contextFor($(".metro-button.back")[0]);
                    var url = '<?="/{$this->params['prefix']}/rifas/alterar_status_turmas/"?>'+dir+'/'+ativo;
                    context.$data.showLoading(function() {
                        bootbox.hideAll();
                        $.ajax({
                            url : url,
                            dataType : "json",
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                }
            });
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
        <h2>
            <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/rifas/listar"?>')}"></a>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Lista de Turmas Relacionadas
        </h2>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <?=$form->create('RifaTurma',array(
        'url' => "/{$this->params['prefix']}/rifas/turmas/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('turma_id',
                array('label' => 'Turma', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        <div class="span6">
            <label>&nbsp;</label>
            <button type="button" class="button mini pull-right bg-color-greenDark inserir">
                Relacionar Turma
                <i class="icon-upload"></i>
            </button>
        </div>
    </div>
<?php if (sizeof($turmas > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="20%"><?=$paginator->sort('Turma', 'RifaTurma.id',$sortOptions); ?></th>
                <th scope="col" width="35%"><?=$paginator->sort('Rifa', 'Rifa.nome',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Cupons', 'RifaTurma.cupons',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Ativa', 'RifaTurma.ativa',$sortOptions); ?></th>
                <th scope="col" width="20%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td width="25%"><?=$turma['RifaTurma']['turma_id']; ?></td>
                <td width="35%"><?=$turma['Rifa']['nome']; ?></td>
                <td width="20%"><?=$turma['RifaTurma']['cupons']; ?></td>
                <td width="5%" style="text-align:center"><?=($turma['RifaTurma']['ativo'] == 1) ? 'Sim' : 'Não' ?></td>
                <td width="15%" style="text-align:center">
                    <button type="button" class="default mini bg-color-blueDark status" dir="<?=$turma['RifaTurma']['turma_id']?>" ativo="<?=$turma['RifaTurma']['ativo']?>">
                            <?=($turma['RifaTurma']['ativo'] == 1) ? 'Desativar' : 'Ativar'  ?>
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="5">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Turmas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Turma Encontrada.</h2>
<?php endif; ?>
</div>
<?php echo $form->input('id', array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Tipo de Evento</label>
	<?php echo $form->input('nome', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>


<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Template</label>
	<?php echo $form->select('template', $templates, array('selected' => $this->data['TiposEvento']['template'] ), array('empty' => null, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Descrição</label>
	<?php echo $form->input('descricao', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>
<h2><?php echo "Replicar Mapa - ".$turma['Turma']['id']." - ".$evento['Evento']['nome'] ?></h2>

<div class="row-fluid">
    <?php echo $form->hidden('evento_id', array('value' => $evento['Evento']['id'])) ?>
    <?php echo $form->input('turma_id', array('label' => 'Turma', 'type' => 'text','div' => 'span3 input-control text')) ?>
    <label>&nbsp;</label>
    <button class="bg-color-blueDark procurar">
        <i class="icon-search-2"></i>
        Procurar
    </button>
</div>

<div class="listaMapas"></div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".procurar").click(function(e){
            e.preventDefault();
            $('.listaMapas').html('<div class="cycle-loader"></div>');
            $.ajax({
                url: '/<?=$this->params['prefix']?>/mapa_mesas/procurar_mapa/',
                data: {
                    data: {
                        turma_id: $("#turma_id").val()
                    }
                },
                type: 'POST',
                complete: function(data){
                    $(".listaMapas").html(data.responseText);
                }
            });
        });
    });
</script>
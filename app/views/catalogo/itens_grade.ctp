<link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.2">
<div class="row grade-itens">
    <?php foreach($catalogos as $i => $catalogo) : ?>
        <div data-catalogo-id="<?=$catalogo['Catalogo']['id']?>" class="thumbnail col-lg-3">
            <div class="caption">
                <?php if($grupo_usuario == 'marketing'): ?>
                    <span class="text-danger pull-left">
                        <i class="icon-remove desativar" data-id="<?=$catalogo['Catalogo']['id']?>"></i>
                    </span>
                <?php endif; ?>
                <span class="text-danger pull-right">
                    <i class="faicon faicon-refresh"></i><?=date("d/m",strtotime($catalogo['Catalogo']['data_ultima_alteracao']))?>
                </span>
            </div>
            <a class="" href="/<?=$this->params['prefix']?>/catalogo/detalhes/<?=$catalogo['Catalogo']['id']?>">
                <?php if(count($catalogo["CatalogoFoto"]) > 0) : ?>
                    <img src="/fotos/fit/<?=base64_encode("upload/catalogo_fotos/{$catalogo["CatalogoFoto"][0]["id"]}/default.{$catalogo["CatalogoFoto"][0]["ext"]}")?>/400/150" />
                <?php else : ?>
                    <img src="/fotos/notFound/400/300" />
                <?php endif; ?>
                <div class="caption">
                    <span class="text-danger grade-titulo">
                        <?=$catalogo['Catalogo']['nome']?>
                    </span>
                </div>
            </a>
        </div>
        <?=($i+1) % 4 == 0 ? '</div><div class="row grade-itens">' : ""?>
        <div class="caption">
        </div>
    <?php endforeach; ?>
</div>
<script>
    $(".desativar").click(function(){
        var catalogoId = $(this).data("id");
        if(confirm("Você deseja realmente desativar este item?")){
            $.ajax({
                url: "/<?=$this->params['prefix']?>/catalogo/detalhes/"+catalogoId+"/1?>",
                type: "POST",
                success: function() {
                    $("[data-catalogo-id='"+catalogoId+"']").fadeOut("slow");
                }
            });
        }
    });
</script>
<div class="linha-titulo">
    <div class="text-danger text-uppercase titulo">
        Selecione
        <div class="visualizacao text-muted">
            <span>Visualização</span>
            <a href="/<?=$this->params['prefix']?>/catalogo/item/<?=$itemSelecionado["CatalogoItem"]['id']?>/grade"
                class="<?=$view == 'grade' ? 'text-muted' : 'text-danger'?>">
                <i class="faicon faicon-lg faicon-th-large"></i>
                grade
            </a>
            <a href="/<?=$this->params['prefix']?>/catalogo/item/<?=$itemSelecionado["CatalogoItem"]['id']?>/lista"
                class="<?=$view == 'lista' ? 'text-muted' : 'text-danger'?>">
                <i class="faicon faicon-lg faicon-th-list"></i>
                lista
            </a>
        </div>
    </div>
</div>
<?php if(count($catalogos) > 0) : ?>
<?php include("itens_{$view}.ctp"); ?>
<?php else : ?>
<h3 class="text-danger">Nenhum item cadastrado</h3>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Igpm', array('url' => "/{$this->params['prefix']}/igpm/alterar/" . $this->data['Igpm']['id'], 'id' => 'form')); ?>
	
	<?php echo $form->input('id', array('hiddenField' => true)); ?>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('valor', array('label' => 'Valor', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('mes', array('label' => 'Mês', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('ano', array('label' => 'Ano', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button max bg-color-greenDark'>
                Salvar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>
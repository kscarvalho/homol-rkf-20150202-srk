<div class='box-com-titulo'>
	<span class="box-com-titulo-header clearfix" style="margin-bottom:25px!important">Transações</span>
	<?php $session->flash(); ?>
	<div class="grid_full alpha omega first" id="conteudo-container">
		<?php
			if(!empty($transacao['PagmidasResponses'])) {
				$ultimoResponse = end($transacao['PagmidasResponses']);
				if(isset($codigos[$ultimoResponse['status_transacao']]))
					$codigo = $codigos[$ultimoResponse['status_transacao']];
				else
					$codigo = array();
			} else {
				$ultimoResponse = array();
				$codigo = array();
			}
		?>
		<?php if(!empty($codigo)) { if($codigo['tipo'] == 'transitorio') { ?>
		<div class="clearfix grid_full">
			<?=$html->link('Verificar Transação',array($this->params['prefix'] => true, 'action' => 'transacao','verificar',$transacao['PagmidasTransacoes']['id']),array('class' => 'submit button')); ?>
			<div style="clear:both; min-height:30px"></div>
		</div>
		<?php } } ?>
		<div class="clearfix grid_7 alpha">
			<?php
				if(!empty($ultimoResponse))
					if($ultimoResponse['autorizacao'] != null)
					echo "<h1>Transa&ccedil;&atilde;o: {$ultimoResponse['autorizacao']}</h1>";
			?>
			<div class="legenda" style="margin:15px 20px 0 0; border:1px solid #989999">
				<table>
					<tr>
						<td>
							<table>
								<tr>
									<td colspan="2"><h2>Dados do Formando</h2></td>
								</tr>
								<tr height="5">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td width="170">Nome</td>
									<td><?=$transacao['ViewFormandos']['nome']?></td>
								</tr>
								<tr>
									<td width="170">Código</td>
									<td><?=$transacao['ViewFormandos']['codigo_formando']?></td>
								</tr>
								<tr height="15">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"><h2>Dados do Cartão</h2></td>
								</tr>
								<tr height="5">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td width="170">Bandeira</td>
									<td>
										<?php if(!empty($transacao['PagmidasCartoes']['bandeira_id'])) : ?>
											<img src="<?="{$this->webroot}img/{$bandeiras[$transacao['PagmidasCartoes']['bandeira_id']]['imagem']}"?>" width="35" />
										<?php endif; ?>
									</td>
								</tr>
								<tr>
									<td width="170">N&uacute;mero</td>
									<td><?=$transacao['PagmidasCartoes']['numero_cartao']?></td>
								</tr>
								<tr>
									<td width="170">C&oacute;digo de Seguran&ccedil;a</td>
									<td><?=$transacao['PagmidasCartoes']['codigo_seguranca']?></td>
								</tr>
								<tr>
									<td width="170">Data de Validade</td>
									<td><?=$transacao['PagmidasCartoes']['data_validade']?></td>
								</tr>
								<tr height="15">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"><h2>Dados do Titular</h2></td>
								</tr>
								<tr height="5">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td width="170">Nome</td>
									<td><?=$transacao['PagmidasCartoes']['nome']?></td>
								</tr>
								<tr>
									<td width="170">Endereço</td>
									<td><?="{$transacao['PagmidasCartoes']['end_rua']}, {$transacao['PagmidasCartoes']['end_numero']}"?></td>
								</tr>
								<tr>
									<td width="170">&nbsp;</td>
									<td><?=substr($transacao['PagmidasCartoes']['end_cep'],0,5)."-".substr($transacao['PagmidasCartoes']['end_cep'],-3)?></td>
								</tr>
								<tr>
									<td width="170">&nbsp;</td>
									<td><?="{$transacao['PagmidasCartoes']['end_bairro']} - {$transacao['PagmidasCartoes']['end_cidade']} / {$transacao['PagmidasCartoes']['end_uf']}"?></td>
								</tr>
								<tr>
									<td width="170">Tel Residencial</td>
									<td><?=$transacao['PagmidasCartoes']['tel_residencial']?></td>
								</tr>
								<tr>
									<td width="170">Tel Celular</td>
									<td><?=$transacao['PagmidasCartoes']['tel_celular']?></td>
								</tr>
								<tr height="15">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"><h2>Dados da Compra</h2></td>
								</tr>
								<tr height="5">
									<td colspan="2"></td>
								</tr>
								<tr>
									<td width="170">Item</td>
									<td><?=$transacao['PagmidasItens']['nome']?></td>
								</tr>
								<tr>
									<td width="170">Valor</td>
									<td>R$<?=number_format($transacao['PagmidasTransacoes']['valor'],2,',','.');?></td>
								</tr>
								<tr>
									<td width="170">Parcelas</td>
									<td><?=$transacao['PagmidasCartoes']['parcelas']?></td>
								</tr>
								<?php if(!empty($ultimoResponse)) : ?>
								<tr height="65">
									<td width="170"><strong>&Uacute;timo Status</strong></td>
									<td><?=!empty($codigo) ? $codigo['mensagem'] : "Processando"?></td>
								</tr>
								<?php endif; ?>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="clearfix grid_7">
			<h1>Hist&oacute;rico de Requisições</h1>
			<div class="grid_full container-tabela">
				<table class="tabela-boletos">
					<thead>
						<th scope="col">Status</th>
						<th scope="col">Data</th>
						<th scope="col">Hora</th>
					</thead>
					<tbody>
						<?php if(!empty($transacao['PagmidasResponses'])) : ?>
							<?php $isOdd = false; ?>
							<?php foreach($transacao['PagmidasResponses'] as $response) : ?>
							<tr class="linha-despesa hover<?=$isOdd ? " odd" : ""?>">
								<td>
									<?php if(isset($codigos[$response['status_transacao']]['mensagem'])) : ?>
									<?=$codigos[$response['status_transacao']]['mensagem']?>
									<?php else : ?>
									Processando
									<?php endif; ?>
								</td>
								<td>
									<?=date("d/m/Y",strtotime($response['data_cadastro']))?>
								</td>
								<td>
									<?=date("H:i:s",strtotime($response['data_cadastro']))?>
								</td>
							</tr>
							<?php $isOdd = !($isOdd); ?>
							<?php endforeach; ?>
						<?php else : ?>
							<tr>
								<td colspan="3">
									<h2>Essa transação não tem histórico de requisições</h2>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
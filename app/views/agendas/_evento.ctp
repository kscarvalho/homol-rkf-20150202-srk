<span id="conteudo-titulo" class="box-com-titulo-header">Detalhe do evento</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <div class="detalhes">
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Título:</label>
            <span class="grid_8 alpha first"><?php echo $evento['Agenda']['titulo'] ?></span>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Descrição: </label>
            <span class="grid_8 alpha first"><?php echo $evento['Agenda']['descricao'] ?></span></p>
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Local:</label> 
            <span class="grid_8 alpha first"> <?php echo $evento['Agenda']['local'] ?> </span></p>
        <?php
        $dateTime = $datas->create_date_time_from_format('Y-m-d H:i:s', $evento['Agenda']['data']);
        $date = date_format($dateTime, 'd/m/Y');
        $time = date_format($dateTime, 'H:i');
        ?>
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Data:</label> 
            <span class="grid_8 alpha first"> <?php echo $date ?> às <?php echo $time ?> </span></p>
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Criado por:</label> 
            <span class="grid_8 alpha first"><?php echo $evento['Criador']['nome'] ?></span></p>

        <?php
        if (isset($evento['Turma'])) {
            echo '<p class="grid_11 alpha omega">';
            echo '<label class="grid_5 alpha ">Turma:</label>';
            echo '<span class="grid_8 alpha first">' . $evento['Turma']['nome'] . '</span>';
            
            echo '</p><p class="grid_11 alpha omega">';
            echo '<label class="grid_5 alpha ">Compartilhamento Interno:</label>';
            if ($evento['Agenda']['visivelComercial']) {
                echo '<span class="grid_8 alpha first">Comercial</span>';
            }
            if ($evento['Agenda']['visivelPlanejamento']) {
                echo '<span class="grid_8 alpha first">Planejamento</span>';
            }
            if ($evento['Agenda']['visivelAtendimento']) {
                echo '<span class="grid_8 alpha first">Atendimento</span>';
            }
            if ($evento['Agenda']['visivelSuper']) {
                echo '<span class="grid_8 alpha first">Super</span>';
            }
            echo '</p>';
        }
        ?>

        <p class="grid_11 alpha omega">
            <?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'action' => 'index'), array('class' => 'cancel')); ?>
            <?php
            if ($evento['Criador']['id'] == $usuario['Usuario']['id']) {
                echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => 'editar', $evento['Agenda']['id']), array('class' => 'submit '));
                echo $html->link('Apagar', array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => 'remover', $evento['Agenda']['id']), array('class' => 'submit '));
            }
            ?>
        </p>
    </div>
</div>
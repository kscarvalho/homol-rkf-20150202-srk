
<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Usuários</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Nome</label>
			<span class="grid_8 alpha first"><?php echo $usuario['Usuario']['nome']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Nível</label>
			<label class="grid_6 alpha">Grupo</label>
			<span class="grid_5 alpha first"><?php echo $usuario['Usuario']['nivel']; ?></span>
			<span class="grid_5 alpha"><?php echo $usuario['Usuario']['grupo']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">E-mail</label>
			<span class="grid_8 alpha first"><?php echo $usuario['Usuario']['email']; ?></span>
		</p>	
		
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Data Criação</label>
			<label class="grid_6 alpha">Data Alteração</label>
			<span class="grid_5 alpha first"><?php echo $usuario['Usuario']['data_criacao']; ?></span>
			<span class="grid_5 alpha"><?php echo $usuario['Usuario']['data_ultima_alteracao']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true,'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true,'controller' => 'Usuarios', 'action' => 'editar', $usuario['Usuario']['id']), array('class' => 'submit ')); ?>
			<?php echo $html->link('Adicionar Outro', array($this->params['prefix'] => true,'controller' => 'Usuarios', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>
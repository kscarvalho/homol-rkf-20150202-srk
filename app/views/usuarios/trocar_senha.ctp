<style type="text/css">
    h3 { text-align: center; color: white }
</style>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span6 alert alert-info">
        <h3>Recuperar Senha</h3>
    </div>
</div>
<?=$form->create('Usuario', array('id' => 'form')); ?>
<div class="row-fluid">
    <label>Chave de Recuperação
        <h6>Insira no campo abaixo a chave enviada para seu email.</h6>
    </label>
    <?=$form->input('Usuario.chave_de_recuperacao', array('class' => 'input-control text span6', 'label' => false, 'div' => false, 'error' => false)); ?>
    <label>Senha</label>
    <?php echo $form->input('Usuario.senha', array('class' => 'input-control text span6', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => false)); ?>
    <label>Confirmar Senha</label>
    <?php echo $form->input('Usuario.confirmar', array('class' => 'input-control text span6', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => false)); ?>
    <br />
    <br />
    <?=$form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'button bg-color-greenDark'));?>
</div>


<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.chosen').chosen({width:'100%'});
    $("#form").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<?=$form->create('TurmaUsuarios' ,array(
    'url' => "/{$this->params['prefix']}/usuarios/trocar_turma/{$usuario['Usuario']['id']}",
    'id' => 'form')); ?>
<div class="row-fluid">
    <div class="row-fluid">
        <div class="span3">
            <?=$form->input('turma_id', 
                array('options' => $turmas, 'type' => 'select', 
                    'class' => 'chosen', 'label' => 'Turmas', 'div' => false, 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button mini bg-color-greenDark'>
                Enviar
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
</div>


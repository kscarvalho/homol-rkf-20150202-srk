<script type="text/javascript">
 	jQuery(function($) {
 		$("#FormandoProfileCpf").setMask("999.999.999-99");
 		$("#FormandoProfileTelResidencial").setMask("phone");
 		$("#FormandoProfileTelComercial").setMask("phone");
 		$("#FormandoProfileTelCelular").setMask("(99) 99999-9999");
 		$("#FormandoProfileTelPai").setMask("phone");
 		$("#FormandoProfileTelCelPai").setMask("(99) 99999-9999");
 		$("#FormandoProfileTelMae").setMask("phone");
 		$("#FormandoProfileTelCelMae").setMask("(99) 99999-9999");
 		$("#FormandoProfileEndCep").setMask("99999-999");
 		$("#FormandoProfileData-nascimento").setMask("99-99-9999");
		$("#upload-image").colorbox({iframe:true, width:"800", height:"500"});
 	});
</script>

<p class="grid_full alpha omega titulo">
	Dados pessoais
</p>
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Foto</label>
	<p id="foto-formando" class="grid_11 alpha omega first"><img src="<?php echo $this->webroot.$this->data['FormandoProfile']['diretorio_foto_perfil']; ?>"></img></p>
	<p id="aviso-upload" class="grid_11 alpha omega first" style="display:none;color: #B00000;"><b>Aten&ccedil;&atilde;o: para finalizar a mudan&ccedil;a de foto clique em salvar logo abaixo.</b></p>
	<?php echo $form->hidden('FormandoProfile.diretorio_foto_perfil', array('id' => "diretorio-upload")); ?>
	<p class="grid_11 alpha omega first"><?php echo $html->link('Enviar foto',array('controller' => 'formandos', 'action' => 'uploadimg1') ,array('id' => 'upload-image', 'class' => 'cancel grid_4 first alpha omega', 'style' => 'float:left; margin-left:0;')); ?>
	</p>
	
</p>

<p class="grid_6 omega">
	<label class="grid_11 alpha omega">Curso</label>
	<?php echo $form->select('FormandoProfile.curso_turma_id', $lista_curso_turma_id, null, array('class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
	<p class="grid_11 alpha omega first">
<label class="grid_11 alpha omega">E-mail</label>
<?php echo $form->input('email', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 alpha first">
	<label class="grid_11 alpha">Data de Nascimento</label>
	<?php echo $form->input('FormandoProfile.data-nascimento', array( 'class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 omega">
	<label class="grid_11 alpha omega">Sexo</label>
	<?php echo $form->select('FormandoProfile.sexo', array('M'=>'Masculino', 'F'=>'Feminino'), null, array('class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 alpha first">
	<label class="grid_11 alpha omega">Tam. Camiseta</label>
	<?php echo $form->select('FormandoProfile.tam_camiseta', array('PP'=>'PP', 'P'=>'P', 'M' =>'M', 'G' => 'G' , 'GG' => 'GG' ), null, array('class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 omega">
	<label class="grid_11 alpha">Tam. Calçado</label>
	
	<?php 
		$numero_calcado; 
		for ($i = 33; $i <= 43; $i+=2) {
			$j=$i+1;
			$numero_calcado[$i]="$i / $j";
			}
		echo $form->select('FormandoProfile.numero_havaiana', $numero_calcado,null, array('empty'=>true, 'class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_6 alpha first">
	<label class="grid_11 alpha omega">RG</label>
	<?php echo $form->input('FormandoProfile.rg', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 omega">
	<label class="grid_16 alpha omega">CPF</label>
	<?php echo $form->input('FormandoProfile.cpf', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 alpha first">
	<label class="grid_16 alpha omega">Telefone Residencial</label>
<?php echo $form->input('FormandoProfile.tel_residencial', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4">
	<label class="grid_16 alpha omega">Telefone Comercial</label>
	<?php echo $form->input('FormandoProfile.tel_comercial', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4">
	<label class="grid_7 alpha omega">Celular</label>
<?php echo $form->input('FormandoProfile.tel_celular', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 omega">
	<label class="grid_11 alpha omega">Nextel</label>
<?php echo $form->input('FormandoProfile.nextel', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_8 alpha first">
	<label class="grid_full alpha omega">Nome do Pai</label>
<?=$form->input('FormandoProfile.nome_pai', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 omega">
	<label class="grid_8 alpha omega">Telefone Pai</label>
<?=$form->input('FormandoProfile.tel_pai', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 omega">
	<label class="grid_8 alpha omega">Celular Pai</label>
<?=$form->input('FormandoProfile.tel_cel_pai', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_8 alpha first">
	<label class="grid_ful alpha omega">Nome da Mãe</label>
<?=$form->input('FormandoProfile.nome_mae', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 omega">
	<label class="grid_8 alpha omega">Telefone Mãe</label>
<?=$form->input('FormandoProfile.tel_mae', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_4 omega">
	<label class="grid_8 alpha omega">Celular Mãe</label>
<?=$form->input('FormandoProfile.tel_cel_mae', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_full alpha omega titulo">
	Dados residenciais
</p>	
<p class="grid_10 alpha ">
	<label class="grid_11 alpha omega">Endereço</label>
	<?php echo $form->input('FormandoProfile.end_rua', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_5 omega">
	<label class="grid_11 alpha omega">CEP</label>
	<?php echo $form->input('FormandoProfile.end_cep', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_2 alpha first">
	<label class="grid_11 alpha omega">Número</label>
	<?php echo $form->input('FormandoProfile.end_numero', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_5">
	<label class="grid_11 alpha omega">Complemento</label>
<?php echo $form->input('FormandoProfile.end_complemento', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_6 omega">
	<label class="grid_11 alpha omega">Bairro</label>
	<?php echo $form->input('FormandoProfile.end_bairro', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_5 alpha first">
	<label class="grid_11 alpha omega">Cidade</label>
	<?php echo $form->input('FormandoProfile.end_cidade', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_5 omega">
	<label class="grid_11 alpha omega">Estado</label>
	<?php $uf_brasil = array(
		'AC' => 'Acre',
		'AL' => 'Alagoas',
		'AP' => 'Amapá',
		'AM' => 'Amazonas',
		'BA' => 'Bahia',
		'CE' => 'Ceará',
		'DF' => 'Distrito Federal',
		'ES' => 'Espírito Santo',
		'GO' => 'Goiás',
		'MA' => 'Maranhão',
		'MT' => 'Mato Grosso',
		'MS' => 'Mato Grosso do Sul',
		'MG' => 'Minas Gerais',
		'PA' => 'Pará',
		'PB' => 'Paraíba',
		'PR' => 'Paraná',
		'PE' => 'Pernambuco',
		'PI' => 'Piauí',
		'RJ' => 'Rio de Janeiro',
		'RN' => 'Rio Grande do Norte',
		'RS' => 'Rio Grande do Sul',
		'RO' => 'Rondonia',
		'RR' => 'Roraima',
		'SC' => 'Santa Catarina',
		'SP' => 'São Paulo',
		'SE' => 'Sergipe',
		'TO' => 'Tocantins');
	echo $form->select('FormandoProfile.end_uf', $uf_brasil, 'SP', array('empty'=>false, 'class' => 'grid_full first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<?php if($this->params['prefix'] == 'atendimento') : ?>
<p class="grid_full alpha first">
	<label class="grid_5 alpha omega">Senha</label>
	<?php echo $form->password('senha', array('class' => 'grid_5 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
</p>
<p class="grid_full alpha first">
	<label class="grid_5 alpha omega">Confirmar Senha</label>
	<?php echo $form->password('confirmar', array('class' => 'grid_5 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
</p>
<?php endif; ?>
<div style='clear:both;'></div>

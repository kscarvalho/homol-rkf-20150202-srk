<?php echo $form->input('id',  array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Universidade</label>
	<?php echo $form->select('universidade_id', $universidade_select, null, array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'onchange' => 'universidadeMudou()',  'empty' => '-- Selecione uma universidade --' , 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Faculdade</label>
	<?php echo $form->select('faculdade_id', $faculdade_select, null, array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'empty' => '-- Selecione uma faculdade --', 'error' => array('wrap' => 'span', 'class' => 'grid_10')));?>
</p>	
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));?>
</p>	
<p class="grid_11 alpha omega">
	<label class="grid_11 alpha omega">Sigla</label>
	<?php echo $form->input('sigla', array('class' => 'grid_4 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));?>
</p>	
<select id="selectDeListaDeTodasFaculdades" style="display:none;"></select>
<script type="text/javascript">
	var faculdadeUniversidade = new Array();
	faculdadeUniversidade[0] = new Array("");
	
	<?php
		foreach ($chaves_faculdade_universidade as $universidadeId => $faculdadeArray) {
			echo 'faculdadeUniversidade[' . $universidadeId . '] = [';
			for ($day=0; $day < sizeof($faculdadeArray); $day++) { 
				echo $faculdadeArray[$day];
				if(end($faculdadeArray) != $faculdadeArray[$day])
					echo ', ';
			}
			echo "];\n";
		}
	?>
	
	
	$(document).ready(function(){
		//$('#selectDeListaDeTodasFaculdades').html($('#CursoFaculdadeId').html());
		$('#CursoFaculdadeId option').each( function() {
			if ($(this).val() != '' && $(this).val() != undefined) {
				//Eh necessario guardar o option em outro select escondido para depois adiciona-lo ao select do formul�rio
				//pois dar display:none no option funciona apenas em alguns navegadores
				$(this).appendTo('#selectDeListaDeTodasFaculdades');
			}	
		});
		$('#CursoUniversidadeId').attr('value', '<?php echo $this->data['Faculdade']['universidade_id'] ?>');
		universidadeMudou();
		$('#CursoFaculdadeId').attr('value', '<?php echo $this->data['Faculdade']['id'] ?>');
	});
	
	
	function universidadeMudou(){		
		var universidadeEscolhida = $('#CursoUniversidadeId').val();
		if(universidadeEscolhida == undefined) universidadeEscolhida = 0;
		
		$('#CursoFaculdadeId option').appendTo('#selectDeListaDeTodasFaculdades');
		
		for (var i = 0; i < faculdadeUniversidade[universidadeEscolhida].length; ++i) {
			$('#selectDeListaDeTodasFaculdades option[value=' + faculdadeUniversidade[universidadeEscolhida][i] + ']').appendTo('#CursoFaculdadeId');
			
		}
		$('#CursoFaculdadeId').attr('value', "");
	};
</script>
<?php

class CoresHelper extends AppHelper {
	
	var $cores = array('blue','blueDark','green','greenDark','red','yellow','orange',
			'orangeDark','pink','pinkDark','purple','darken','gray','grayDark');
	
	function corRandomica() {
		return $this->cores[rand(0,count($this->cores)-1)];
	}
	
}
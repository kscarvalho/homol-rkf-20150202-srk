<?php

class CalendarioHelper extends AppHelper {

    var $helpers = array('Html', 'Form', 'Text');

    function html($year = '', $month = '', $data = '', $month_action='calendario', $day_action = 'evento', $add_action = 'adicionar') {

        $output = '';
        $month_list = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        $day_list = array('Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab');
        $day = 1;
        $today = 0;

        // Se mes/ano são inválidos ou vazios, então use a mes/ano atual
        $valid = 0;
        if ($year == '' || $month == '' || strlen(strval($year)) != 4 || $month > 13 || $month < 0) {
            $year = intval(date('Y'));
            $month = intval(date('m'));
        }
        $year = intval($year);
        $month = intval($month);


        //configura os meses adjacentes, colocando um ano consistente
        $next_month = $month + 1;
        $prev_month = $month - 1;

        $next_year = $year;
        $prev_year = $year;

        if ($next_month == 13) {
            $next_month = 1;
            $next_year = $year + 1;
        }

        if ($prev_month == 0) {
            $prev_month = 12;
            $prev_year = $year - 1;
        }

        // configura o dia atual, caso o mes e o ano são mesmos que o atual
        if ($year == date('Y') && $month == intval(date('m'))) {
            $today = date('j');
        }

        // total de dias no mes e ano selecionado
        $days_in_month = date("t", mktime(0, 0, 0, $month, 1, $year));

        // dia da semana do primeiro dia do mes
        $first_day_in_month = date('w', mktime(0, 0, 0, $month, 1, $year));

        $output .= '<div class="calendario">';

        $output .= ' <div class="top"> ';
        $output .= ' <div class="top-nav"> ';
        $output .= $this->Html->link(__('<', true), array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => $month_action, $prev_year, $prev_month));
        $output .= ' </div> ';
        $output .= ' <div class="top-nav"> ';
        $output .= $this->Html->link(__('>', true), array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => $month_action, $next_year, $next_month));
        $output .= ' </div> ';
        $output .= ucfirst($month_list[$month - 1]) . ' ' . $year;
        $output .= ' <div class="top-add"> ';
		if($this->params['prefix'] != 'comissao') {
        	$output .= $this->Html->link(__('Adicionar', true), array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => $add_action));
		}
        $output .= ' </div> ';
        $output .= ' </div> ';

        $output .= '<table>';

        $output .= '<thead>';
        $output .= '<tr>';
        for ($i = 0; $i < 7; $i++) {
            $output .= '<th class="cell-header" scope="col">' . $day_list[$i] . '</th>';
        }
        $output .= '</tr>';
        $output .= '</thead>';

        $output .= '<tbody>';
        while ($day <= $days_in_month) {
            $output .= '<tr>';

            for ($i = 0; $i < 7; $i++) {
                $cell = '&nbsp;';
                if (isset($data[$day])) {
                    $cell = '';
                    $contador = 0;
                    foreach ($data[$day] as $event) {

                        if ($contador == 4) {
                            //TODO: colocar um link para lista de eventos do dia (AJAX?)
                            $cell .= "Mais Eventos";
                            break;
                        } else {
                            $event_title = $this->Text->truncate($event['Agenda']['titulo'], 20, array('ending' => '...', 'exact' => true, 'html' => true));
                            $cell .= $this->Html->link($event_title, array(
                                $this->params['prefix'] => true,
                                'controller' => 'agendas',
                                'action' => $day_action,
                                $event['Agenda']['id']));
                            $cell .= "<br />";
                            $contador++;
                        }
                    }
                }
                $class = '';
                if ($i == 0 || $i == 6) {
                    $class = ' class="cell-weekend" ';
                }
                if ($day == $today) {
                    $class = ' class="cell-today" ';
                }
                if (($day > 1 || $day_list[$first_day_in_month] == $day_list[$i]) && ($day <= $days_in_month)) {
                    $output .= '<td ' . $class . '>';
                    $output .= '<div class="cell-number">' . $day . '</div>';
                    $output .= '<div class="clear">&nbsp;</div>';
                    $output .= '<div class="cell-data">' . $cell . '</div>';
                    $output .= '</td>';
                    $day++;
                } else {
                    $output .= '<td ' . $class . '>&nbsp;</td>';
                }
            }
            $output .= '</tr>';
        }

        $output .= '</tbody>';
        $output .= '</table>';
        $output .= '</div>';

        return $output;
    }

}

?>
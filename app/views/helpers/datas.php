<?php

/*
	@author Vinícius Alves Oyama
	Helper utilizado para formatar datas
*/

class DatasHelper extends AppHelper {

    function create_date_time_from_format($fmt, $strdate) {
		$data_formatada = '';
		
		if($strdate != '') {
			if (preg_match('/d\-m\-Y*/', $fmt)) {
				$arr_datetime = explode(' ', $strdate);
				$data = $arr_datetime[0];
				
				$hora = '';
				if(isset($arr_datetime[1])) 
					$hora = $arr_datetime[1];

				$arr_data = explode('-', $data);

				$data_formatada = $arr_data[2] . '-' . $arr_data[1] . '-' . $arr_data[0] . ' ' . $hora;

			} else 	if (preg_match('/Y\-m\-d*/', $fmt)) {
				$arr_datetime = explode(' ', $strdate);
				$data = $arr_datetime[0];
				
				$hora = '';
				if(isset($arr_datetime[1])) 
					$hora = $arr_datetime[1];

				$arr_data = explode('-', $data);

				$data_formatada = $arr_data[2] . '-' . $arr_data[1] . '-' . $arr_data[0] . ' ' . $hora;

			}
		}
		
		$obj = new DateTime($data_formatada);
		return $obj;
	}
}

?>

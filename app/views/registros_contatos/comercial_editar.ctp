<span id="conteudo-titulo" class="box-com-titulo-header">Editar Registro de Contato</span>
<div id="conteudo-container">
	<?php echo $form->create('RegistrosContato', array('url' => "/{$this->params['prefix']}/RegistrosContatos/editar")); ?>
		<?php include	('_form.ctp'); ?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'RegistrosInternos' , 'action' => 'index') , array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
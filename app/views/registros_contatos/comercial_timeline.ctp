<script type="text/javascript">
    $(document).ready(function() {
        $("#chart").bind('carregado',function() {
            var grafico = <?=json_encode($grafico)?>;
            var ser = [];
            $.each(grafico,function(i,g) {
                var s = {
                    name: g.nome,
                    data : []
                };
                $.each(g.data,function(i,d) {
                    s.data.push([parseInt(d[0]['time']*1000),parseInt(d[0]['qtde'])]);
                });
                ser.push(s);
            });
            $(this).highcharts({
                chart: {
                    type: 'line',
                    width: $("#content-body").width(),
                    zoomType : 'x',
                    height: 300
                },
                credits : {
                    enabled : false
                },
                exporting : {
                    enabled : false
                },
                title: {
                    text: 'Contatos',
                    align: 'left'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: {
                        month: '%m/%y',
                        week: '%e/%m/%y',
                        day: '%e/%m/%y',
                    },
                    offset : 10
                },
                yAxis: {
                    title: {
                        enabled : false
                    },
                    min: 1,
                    allowDecimals : false
                },
                tooltip: {
                    formatter: function() {
                            return '<b>'+ this.series.name +'</b><br/>'+
                            Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y +' m';
                    }
                },
                plotOptions: {
                    spline: {
                        lineWidth: 4,
                        states: {
                            hover: {
                                lineWidth: 5
                            }
                        }
                    }
                },
                series: ser
            });
        });
        if(window.Highcharts == undefined) {
            var s = document.getElementsByTagName('script')[0];
            var high = document.createElement('script');
            high.type = 'text/javascript';
            high.async = false;
            high.src = "<?= $this->webroot ?>metro/js/max/highcharts/highcharts.js";
            var mod = document.createElement('script');
            mod.type = 'text/javascript';
            mod.async = false;
            mod.src = "<?= $this->webroot ?>metro/js/max/highcharts/modules/exporting.js";
            s.parentNode.insertBefore(high, s);
            s.parentNode.insertBefore(mod, s);
            mod.onload = function() {
                $("#chart").trigger({ type: 'carregado' });
            };
        } else {
            $("#chart").trigger({ type: 'carregado' });
        }
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Timeline de contato
        </h2>
    </div>
</div>
<div class="row-fluid">
    <div id="chart" class="span12"></div>
</div>
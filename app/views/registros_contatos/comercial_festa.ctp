<?php
$disabled = array('error' => false);
if(strtotime(date('Y-m-d 00:00:00')) > strtotime($this->data['RegistrosContato']['data']) && $festaId)
    $liberada = false;
else
    $liberada = true;
if(!$liberada)
    $disabled['disabled'] = 'disabled';
?>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<style type="text/css">
.well { padding:6px; background:white }
.well .button, .well button { margin:0 }
.nav-list.scrollable { height:250px; overflow-x: auto; margin-bottom:5px }
.nav-list li a { position:relative; cursor:pointer; padding:3px 10px; font-size:12px }
.nav-list li a.selecionado { background:#333; color:white }
.nav-list li { margin-bottom: 5px }
.nav-list li a .convidados { position:absolute; top:6px; right:7px; padding:2px 0;
    font-size:11px; line-height:11px }
.nav-list li a .badge { padding:2px 7px }
</style>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        $('.datepicker').datepicker();
        $("#formulario-registro").submit(function(e) {
            e.preventDefault();
        });
        $("#convidar-turma").click(function() {
            var li = $("#modelo").clone();
            var id = $("#lista-turmas").val();
            var turma = $("#lista-turmas option:selected").text();
            li.children('a').attr({
                class:'',
                'data-id':id
            }).find('.turma').html(turma);
            li.children('a').find('.convidados').html(0);
            $("#lista-turmas option:selected").remove();
            $("#lista-turmas").trigger("liszt:updated");
            $("#turmas").append(li);
            li.fadeIn(500);
        });
        $("#turmas").on('click','[data-id]',function(e) {
            e.preventDefault();
            var turma = this;
            id = $(turma).data('id');
            registro = $("#RegistrosContatoId").val();
            url = '/<?=$this->params['prefix']?>/registros_contatos/festa_formandos/'+id+'/'+registro;
            $("#formandos").html('');
            $.getJSON(url,function(data) {
                $.each(data.formandos,function(i,formando) {
                    <?php if($liberada) : ?>
                    if($('.formando.remover.' + formando.ViewFormandos.id).hasClass('formando'))
                        classe = 'inserido';
                    else if(formando.RegistrosContato.id != null)
                        classe = 'selecionado inserido';
                    else if($('.formando.' + formando.ViewFormandos.id).hasClass('formando'))
                        classe = 'selecionado';
                    else
                        classe = '';
                    var li = "<li><a data-id='"+formando.ViewFormandos.id+
                        "' data-turma='"+formando.ViewFormandos.turma_id+
                        "' class='" + classe + "'>"+formando.ViewFormandos.nome+
                        "</a></li>";
                    <?php else : ?>
                    if(formando.RegistrosContato.id != null) {
                        var li = "<li><a data-id='"+formando.RegistrosContato.id+
                        "' data-compareceu='"+formando.RegistrosContato.compareceu+
                        "'>"+formando.ViewFormandos.nome;
                        if(formando.RegistrosContato.compareceu == 0)
                            li+= '<span class="pull-right label">N&atilde;o Compareceu</span>';
                        else
                            li+= '<span class="pull-right label label-success">Compareceu</span>';
                        li+= "</a></li>";
                    } else {
                        li = '';
                    }
                    <?php endif; ?>
                    $("#formandos").append(li);
                });
            }).always(function() {
                $("#turmas li a.selecionado").removeClass('selecionado');
                $(turma).addClass('selecionado');
            });
        });
        <?php if($liberada) : ?>
        $("#formandos").on('click','[data-id]',function(e) {
            e.preventDefault();
            formando = this;
            turma = $("#turmas li a.selecionado").data('id');
            convidados = parseInt($("#turmas li a.selecionado .convidados").text());
            if($(formando).hasClass('selecionado')) {
                /*
                convidados--;
                if($(formando).hasClass('inserido'))
                    $("<input>",{
                        type:'hidden',
                        class: 'formando remover ' + $(formando).data('id'),
                        name:'data[remover_usuarios][]',
                        value:$(formando).data('id')
                    }).appendTo($("#formulario-registro"));
                else
                    $('.formando.' + $(formando).data('id')).remove();
                */
            } else {
                convidados++;
                var id = $('.formando.usuario').length;
                if($(formando).hasClass('inserido'))
                    $('.formando.' + $(formando).data('id')).remove();
                else {
                    $("<input>",{
                        type:'hidden',
                        'data-turma' : turma,
                        class: 'formando usuario ' + $(formando).data('id'),
                        name:'data[RegistroContatoUsuario]['+id+'][usuario_id]',
                        value:$(formando).data('id')
                    }).appendTo($("#formulario-registro"));
                    $("<input>",{
                        type:'hidden',
                        'data-turma' : turma,
                        class: 'formando ' + $(formando).data('id'),
                        name:'data[RegistroContatoUsuario]['+id+'][consultor_id]',
                        value:<?=$usuario['Usuario']['id']?>
                    }).appendTo($("#formulario-registro"));
                    $("<input>",{
                        type:'hidden',
                        'data-turma' : turma,
                        class: 'formando ' + $(formando).data('id'),
                        name:'data[RegistroContatoUsuario]['+id+'][data_cadastro]',
                        value:'<?=date('Y-m-d H:i:s')?>'
                    }).appendTo($("#formulario-registro"));
                }
                $(formando).toggleClass('selecionado');
            }
            $("#turmas li a.selecionado .convidados").text(convidados);
        });
        <?php else : ?>
        $("#formandos").on('click','[data-id]',function(e) {
            e.preventDefault();
            formando = this;
            span = $(formando).find('.label');
            span.attr('class','label pull-right').text('Aguarde');
            compareceu = $(formando).data('compareceu') == 0 ? 1 : 0;
            url = '/<?=$this->params['prefix']?>/registros_contatos/festa_compareceu/';
            dados = {
                data : {
                    id : $(formando).data('id'),
                    compareceu : compareceu
                } 
            };
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                success : function(response) {
                    if(response.erro == 0) {
                        span.addClass('label-important').text('Erro');
                    } else {
                        if(compareceu == 1)
                            span.addClass('label-success').html('Compareceu');
                        else
                            span.html('N&atilde;o Compareceu');
                        $(formando).data('compareceu',compareceu);
                    }
                },
                error : function() {
                    span.addClass('label-important').text('Erro');
                }
            });
        });
        <?php endif; ?>
        $('#formulario-registro').validate({
            sendForm : true,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario-registro").serialize();
                var url = $("#formulario-registro").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            }
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload"
            data-bind="click: reload"></a>
        <a class="metro-button back" href="<?="/{$this->params['prefix']}/registros_contatos/festas"?>"
            data-bind="click: loadThis"></a>
        Festa
        <?php
            if($festaId) {
                if(count($this->data['RegistroContatoUsuario']) > 0) { ?>
        <a class="mini bg-color-greenDark button pull-right" target="_blank"
            href="<?="/{$this->params['prefix']}/registros_contatos/festa_lista_convidados/{$festaId}"?>">
            Lista de Convidados
        </a>
        <?php } } ?>
    </h2>
</div>
<?=$form->create('RegistrosContato', array(
    'url' => "/{$this->params['prefix']}/registros_contatos/festa",
    'id' => 'formulario-registro'
));
echo $form->hidden('RegistrosContato.id');
?>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span5">
        <label class="required" for="assunto">Descri&ccedil;&atilde;o</label>
        <?=$form->input('assunto',array_merge(array(
            'label' => false,
            'id' => 'assunto',
            'data-description' => 'notEmpty',
            'data-describedby' => 'assunto',
            'div' => 'input-control text',
            'data-required' => 'true'),$disabled)); ?>
    </div>
    <div class="span7">
        <label class="required" for="local">Local</label>
        <?=$form->input('local',array_merge(array(
            'label' => false,
            'id' => 'local',
            'data-description' => 'notEmpty',
            'data-describedby' => 'local',
            'div' => 'input-control text',
            'data-required' => 'true'),$disabled)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <label class="required">Data</label>
        <?=$form->input('data-hora',array(
            'label' => false,
            'class' => 'datepicker',
            'div' => 'input-control text',
            'error' => false,
            'data-required' => 'true')); ?>
    </div>
    <div class="span9">
        <label>Detalhes</label>
        <?=$form->input('detalhes',array_merge(array(
            'label' => false,
            'type' => 'text',
            'div' => 'input-control text'),
            $disabled)); ?>
    </div>
</div>
<?php if($festaId) : ?>
<h3>
    Convidados Para a Festa
</h3>
<br />
<?php if($liberada) : ?>
<div class="alert alert-error">
    <span class="strong">Importante!</span>
    <br />
    Para convidar o formando, selecione a turma e em seguida clique sobre o nome do formando.
    <br />
    Os formandos convidados ter&atilde;o o nome em destaque.
    <br />
    Para convidar formandos de outras turmas selecione a turma na lista abaixo e clique em "Inserir Turma".
</div>
<div class="row-fluid">
    <div class="span4 offset5">
        <?=$form->input('turmas', array(
            'options' => $listaTurmas,
            'type' => 'select',
            'class' => 'chosen',
            'empty' => 'Selecione Uma Turma',
            'label' => false,
            'id' => 'lista-turmas',
            'div' => false)); ?>
    </div>
    <div class="span3">
        <button type="button" class="pull-right default" id="convidar-turma">
            Inserir Turma
        </button>
    </div>
</div>
<?php endif; ?>
<div class="row-fluid">
    <div class="span5">
        <div class='well'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>
                        Turmas
                        <span class=" convidados">
                            N&ordm; Convidados
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable" id="turmas">
                <li id="modelo" class="hide">
                    <a data-id="">
                        <span class='turma'></span>
                        <span class="badge convidados"></span>
                    </a>
                </li>
                <?php if(count($turmasConvidadas) > 0) : foreach($turmasConvidadas as $turmaId => $turma) : ?>
                <li>
                    <a data-id="<?=$turmaId?>">
                        <span class='turma'>
                            <?=$turmaId?> - <?=$turma['nome']?>
                        </span>
                        <span class="badge convidados">
                            <?=$turma['convidados']?>
                        </span>
                    </a>
                </li>
                <?php endforeach; endif; ?>
            </ul>
        </div>
    </div>
    <div class="span7">
        <div class='well'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>Formandos</a>
                </li>
            </ul>
            <ul class="nav nav-list scrollable" id="formandos">
                
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>
<button type="submit" class="bg-color-red">
    Enviar
</button>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    
    $('#content-body').tooltip({ selector: '[rel=tooltip]'});
    
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Festas
            <?php if($permissao) : ?>
            <a href='<?="/{$this->params['prefix']}/registros_contatos/festa/"?>'
               data-bind="click:loadThis" class="pull-right button mini bg-color-blue">
                Nova Festa
                <i class="icon-plus"></i>
            </a>
            <?php endif; ?>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <?php if (sizeof($registros) == 0) { ?>
    <h3 class='fg-color-red'>Nenhum Registro de Festa</h3>
    <?php } else { ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="20%"><?=$paginator->sort('Usuário', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Data', 'RegistrosContato.data',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Descrição', 'RegistrosContato.assunto',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Local', 'RegistrosContato.local',$sortOptions); ?></th>
                <th scope="col" width="20%" style="text-align:center">Convidados</th>
                <th scope="col" width="25%"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($registros as $registro): ?>
            <tr>
                <td><?=$registro['Usuario']['nome']?></td>
                <td><?=date('d/m/Y',strtotime($registro['RegistrosContato']['data']));?></td>
                <td><?=ucfirst($registro['RegistrosContato']['assunto']); ?></td>
                <td><?=$registro['RegistrosContato']['local']; ?></td>
                <td style="text-align:center">
                    <?php if(count($registro['RegistroContatoUsuario']) > 0) : $com = array(); ?>
                    <?php foreach($registro['RegistroContatoUsuario'] as $registroUsuario) $com[] = $registroUsuario['Usuario']['nome']; ?>
                    <span class="label label-inverse" rel="tooltip"
                        title="<?=implode(', ',$com)?>" data-html="true">
                        <?=implode('<br />', array_slice($com, 0,3))?>
                    </span>
                    <?php else : ?>
                        Nenhum
                    <?php endif; ?>
                </td>
                <td dir="<?=$registro['RegistrosContato']['id']?>">
                    <a class="mini default button" data-bind="click:loadThis"
                        href="<?="/{$this->params['prefix']}/registros_contatos/festa/{$registro['RegistrosContato']['id']}"?>">
                        Visualizar
                    </a>
                    <?php if(count($registro['RegistroContatoUsuario']) > 0) : ?>
                    <a class="mini bg-color-greenDark button" target="_blank"
                        href="<?="/{$this->params['prefix']}/registros_contatos/festa_lista_convidados/{$registro['RegistrosContato']['id']}"?>">
                        Lista de Convidados
                    </a>
                    <?php endif; ?>
                    <?php if($permissao) : ?>
                    <a href="<?="/{$this->params['prefix']}/registros_contatos/festa_status_turmas/{$registro['RegistrosContato']['id']}"?>"
                        data-bind="click: loadThis" class="mini bg-color-purple button">
                        Status
                    </a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php } ?>
</div>

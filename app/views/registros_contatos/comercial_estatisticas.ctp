<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var usuarios = <?=json_encode($usuarios)?>;
        var tipos = <?=json_encode($tipos)?>;
        $("#chart").bind('carregado',function() {
            $(".chosen").chosen({width:'100%'});
            var config = {
                chart: {
                    type: 'column',
                    spacingLeft: 0,
                    width: $("#content-body").width()
                },
                title: {
                    align: 'left',
                    style : {
                        paddingBottom : '20px'
                    }
                },
                credits : {
                    enabled : false
                },
                xAxis: {
                    categories: [],
                    labels: {
                        rotation: -80,
                        align: 'right',
                        style: {
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        enabled : false
                    }
                },
                legend : {
                    enabled : false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0
                    }
                },
                exporting : {
                    enabled : false
                },
                series: []
            };
            function alterarGrafico(tipo) {
                var cat = [];
                var data = [];
                
                $.each(usuarios,function(i,usuario) {
                    cat.push(usuario.nome);
                    data.push(usuario.data[tipo]);
                });
                var series = [{
                    name: tipos[tipo],
                    data: data,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 3,
                        y: 8,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }];
                config.series = series;
                config.xAxis.categories = cat;
                config.title.text = "Estatisticas de contatos via " + tipos[tipo];
                $("#chart").highcharts(config);
            }
            $("#tipos").change(function() {
                alterarGrafico($(this).val());
            });
            alterarGrafico($("#tipos").val());
        });
        if(window.Highcharts == undefined) {
            var s = document.getElementsByTagName('script')[0];
            var high = document.createElement('script');
            high.type = 'text/javascript';
            high.async = false;
            high.src = "<?= $this->webroot ?>metro/js/max/highcharts/highcharts.js";
            var mod = document.createElement('script');
            mod.type = 'text/javascript';
            mod.async = false;
            mod.src = "<?= $this->webroot ?>metro/js/max/highcharts/modules/exporting.js";
            s.parentNode.insertBefore(high, s);
            s.parentNode.insertBefore(mod, s);
            mod.onload = function() {
                $("#chart").trigger({ type: 'carregado' });
            };
        } else {
            $("#chart").trigger({ type: 'carregado' });
        }
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Estatisticas de contato
            <div class="span3 pull-right">
                <?=$form->input('tipos',array(
                    'options' => $tipos,
                    'type' => 'select',
                    'class' => 'chosen',
                    'id' => 'tipos',
                    'label' => false,
                    'div' => false)); ?>
            </div>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <div id="chart" class="span12" style="height: 400px"></div>
</div>
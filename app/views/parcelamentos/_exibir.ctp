<script type="text/javascript">
$(document).ready(function() {
    $('.apagar').click(function() {
        id = $(this).data("id");
        bootbox.confirm("<h3 class='fg-color-red'> Tem certeza que deseja apagar este parcelamento?</h3>",function(response) {
            if(response) {
                var context = ko.contextFor($(".metro-button.back")[0]);
                var url = '<?="/{$this->params['prefix']}/parcelamentos/apagar/"?>' + id;
                context.$data.showLoading(function() {
                    bootbox.hideAll();
                    $.ajax({
                        url : url,
                        dataType : "json",
                        complete : function() {
                            context.$data.page("/<?=$this->params['prefix']?>/parcelamentos/listar");
                        }
                    });
                });
            }
        });
    });
});
</script>

<?php
$largura = 10.0/(sizeof($dados)+1);
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: function() {
               page('<?="/{$this->params['prefix']}/parcelamentos/listar"?>') }"></a>
            <?=$parcelamaker['Parcelamento']['titulo']?>
            <button type="button" class="button mini pull-right bg-color-red apagar" data-id="<?=$parcelamaker['Parcelamento']['id']?>">
                Apagar
                <i class="icon-remove"></i>
            </button>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="<?=$largura;?>%">Parcelas</th>
                <?php foreach(array_keys($dados) as $mes) : ?>
                <th scope="col" width="<?=$largura;?>%"><?=$mes; ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listaParcelas as $numparcela): ?>
            <tr>
                <td width="<?=$largura;?>%"><?=$numparcela; ?></td>
                <?php foreach(array_keys($dados) as $data) : ?>
                <td>
                    <?=isset($dados[$data][$numparcela]) ? $dados[$data][$numparcela] : '-' ?>
                </td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
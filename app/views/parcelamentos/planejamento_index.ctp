<span id="conteudo-titulo" class="box-com-titulo-header">Turmas gerenciadas</span>
<div class="tabela-adicionar-item">
	<?php echo $html->link('Upload do ParcelaMaker',array($this->params['prefix'] => true,'action' => 'upload')); ?>
	<div style="clear:both;"></div>
</div>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col">T&iacute;tulo</th>
                    <th scope="col"> &nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($parcelamentos as $parcelamento): ?>
                	<tr class="<?=$isOdd ? "odd" : ""?>">
                		<td colspan="1">
                			<?=$parcelamento["Parcelamento"]["titulo"] == "" ? "Sem t&iacute;tutlo" : $parcelamento["Parcelamento"]["titulo"]?>
                		</td>
                        <td  colspan="1">
                            <?=$html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'parcelamentos', 'action' => 'visualizar', $parcelamento['Parcelamento']['id']), array('class' => 'submit button'));?>
                        </td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
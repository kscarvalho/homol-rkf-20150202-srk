<div class="grid_full alpha omega">
<p class="grid_5 omega alpha first ch-field ch-notice">
	<label class="grid_5 alpha omega">Email</label>
	<?=$formando['email']?>
</p>

<p class="grid_3 omega ch-field ch-notice">
	<label class="grid_10 alpha">Tel Res.</label>
	<?=$formando['tel_residencial']?>
</p>

<p class="grid_3 omega ch-field ch-notice">
	<label class="grid_10 alpha">Tel Cel.</label>
	<?=$formando['tel_celular']?>
</p>

<p class="grid_3 omega ch-field ch-notice">
	<label class="grid_10 alpha">Tel Com.</label>
	<?=$formando['tel_comercial']?>
</p>

<p class="grid_5 alpha omega first ch-field ch-notice">
	<label class="grid_5 alpha omega">Nome</label>
	<?=$formando['nome']?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_3 alpha">RG</label>
	<?=$formando['rg']?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_3 alpha">CPF</label>
	<?=$formando['cpf']?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_10 alpha">Data Nasc.</label>
	<?=$formando['data_nascimento']?>
</p>

<p class="grid_5 omega alpha first ch-field ch-notice">
	<label class="grid_5 alpha omega">Cep</label>
	<?=$formando['end_cep']?>
</p>

<p class="grid_6 omega ch-field">
	<label class="grid_6 alpha">Endereço</label>
	<?=$formando['end_rua']?>
</p>

<p class="grid_2 omega ch-field">
	<label class="grid_10 alpha">Número</label>
	<?=$formando['end_numero']?>
</p>

<p class="grid_3 omega alpha ch-field first">
	<label class="grid_6 alpha omega">Bairro</label>
	<?=$formando['end_bairro']?>
</p>

<p class="grid_3 omega ch-field">
	<label class="grid_4 alpha">Cidade</label>
	<?=$formando['end_cidade']?>
</p>

<p class="grid_5 omega ch-field">
	<label class="grid_5 alpha">Estado</label>
	<?=$formando['end_uf']?>
</p>
<?php if($this->layout != "impressao") : ?>
<p class="grid_5 omega alpha first">
	<span class="submit button botao-proximo">Próximo</span>
</p>
<?php endif; ?>
</div>
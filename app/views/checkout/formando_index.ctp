<?php
echo $html->script('jquery.tools.min');
echo $html->css('tooltip');
echo $html->css('checkout');
?>
<script type="text/javascript">
$(function() {
	$( "#tabs" ).tabs();
	
	$(".botao-proximo").click(function() {
		indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
		indiceDaProximaTab = parseInt(indiceTabSelecionada) + 1;
		nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
		$("#tabs").tabs( "select" , nomeDaProximaTab );
		window.top.location.href = "#conteudo";
		return false;
	});
	
	$(".botao-anterior").click(function() {
		indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
		indiceDaProximaTab = parseInt(indiceTabSelecionada) - 1;
		nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
		$("#tabs").tabs( "select" , nomeDaProximaTab );
		window.top.location.href = "#conteudo";
		return false;
	});

	$("#finalizar").live('click', function() {
		var dados = "";
		$('.input-checkout').each(function() {
			campo = $(this).attr('name');
			valor = $(this).val();
			dados+= campo + "=" + valor + "&";
		});
		dados = dados.substring(0, dados.length-1);
		$.colorbox({
			width : 700,
			height: 600,
			html : '<p id="response">Aguarde</p>',
			escKey : false,
 			overlayClose : false,
 			close : false,
			onLoad : function() { $('#cboxClose').remove(); },
			onComplete : function() {
				var url = "/<?=$this->params["prefix"]?>/checkout/finalizar";
				$.ajax({
			 		url : url,
			 		data : dados,
			 		type : "POST",
			 		dataType : "json",
			 		success : function(response) {
			 	 		if(response.error) {
				 	 		$('#response').html(response.message);
			 	 		} else {
				 	 		var html = "O processo de  checkout foi finalizado com sucesso<br />";
				 	 		html+= "Obrigado <?=$formando['nome']?>. Clique no botão abaixo para imprimir o recibo de checkout<br /><br />";
				 	 		html+= '<?=$html->link('Imprimir',array($this->params['prefix'] => true,'controller' => 'checkout', 'action' => 'imprimir'),array('class' => 'submit button', 'target' => '_blank')); ?>';
				 	 		$('#response').html(html);
			 	 		}
			 		},
			 		error : function() {
			 	 		alert("Ocorreu um erro com os nossos servidores. Por favor tente mais tarde.");
			 		}
			 	});
			}
		});
	});
});
</script>
<style type="text/css">
.div-tab { padding:10px 0 0 0!important; font-size:12px }
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Checkout de Formando</span>
<div id="conteudo-container">
	<?php include('_info.ctp')?>
	<?php if($formando['realizou_checkout'] == 1) : ?>
	<?=$html->link('Imprimir',array($this->params['prefix'] => true,'controller' => 'checkout', 'action' => 'imprimir'),array('class' => 'submit button', 'target' => '_blank')); ?>
	<?php else : ?>
	<div id="tabs" style='border:none!important' class="grid_full alpha omega">
		<ul>
			<li id="tab-0"><a href="#tabs-dados-formando" rel="tabs-dados-formando">Dados Cadastrais</a></li>
			<li class="seta"></li>
			<li id="tab-1"><a href="#tabs-dados-financeiros" rel="tabs-dados-financeiros">Dados Financeiros</a></li>
		</ul>
		<div id="tabs-dados-formando" class="grid_full alpha omega div-tab">
			<p style='font-size:12px'>
				<br />
				Caro formando, é muito importante neste momento que os seus dados cadastrais estejam atualizados.
				<br />
				Eles serão utilizados na maior parte dos processos realizados pela RK, como,
				<br />
				cerimonial na valsa, reitirada de brindes, e principalmente, a sua festa de formatura.
				<br />
				<br />
				Verifique abaixo seus dados cadastrais
			</p>
			<br />
			<?php include('_form_formando.ctp')?>
		</div>
		<div id="tabs-dados-financeiros" class="grid_full alpha omega div-tab">
			<?php include('despesas_formando.ctp')?>
		</div>
	</div>
	<?php endif; ?>
	<div style="clear:both;"></div>
</div>
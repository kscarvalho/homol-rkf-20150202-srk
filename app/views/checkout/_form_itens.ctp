<span id="conteudo-titulo" class="grid_full box-com-titulo-header">Convites Vendidos no Checkout</span>
<div id="conteudo-container">
	<?php
		$session->flash();
		echo $form->create('CheckoutConviteTurma', array('url' => "/{$this->params['prefix']}/checkout/itens"));
		echo $form->hidden('CheckoutConviteTurma.id');
		echo $form->hidden('CheckoutMesaTurma.id');
	?>
	<p class="grid_full alpha omega" style='margin-top:10px'>
		<h1>Convites</h1>
	</p>
	<p class="grid_full alpha omega" style='margin-top:10px'>
		<label class="grid_full alpha omega">Qtd. máxima de convites por formando</label>
		<?php echo $form->input('CheckoutConviteTurma.convites_por_formando', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_full alpha omega">Preço do convite</label>
		<?php echo $form->input('CheckoutConviteTurma.valor', array('class' => 'grid_6 alpha omega first', 'label' => false, 'id' => 'valor', 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_full alpha omega">Qtd. Máxima de convites</label>
		<?php echo $form->input('CheckoutConviteTurma.maximo_convites', array('class' => 'grid_6 alpha omega first', 'label' => false, 'id' => 'valor', 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	
	<p class="grid_full alpha omega" style='margin-top:10px'>
		<h1>Mesas</h1>
	</p>
	
	<p class="grid_full alpha omega" style='margin-top:10px'>
		<label class="grid_full alpha omega">Qtd. máxima de mesas por formando</label>
		<?php echo $form->input('CheckoutMesaTurma.mesas_por_formando', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_full alpha omega">Preço da mesa</label>
		<?php echo $form->input('CheckoutMesaTurma.valor', array('class' => 'grid_6 alpha omega first', 'label' => false, 'id' => 'valor', 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_full alpha omega">Qtd. Máxima de mesas</label>
		<?php echo $form->input('CheckoutMesaTurma.maximo_mesas', array('class' => 'grid_6 alpha omega first', 'label' => false, 'id' => 'valor', 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_4'))); ?>
	</p>
	<p class="grid_6 alpha omega">
		<?=$form->end(array('label' => 'Confirmar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
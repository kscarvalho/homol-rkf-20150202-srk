<div class='adicionar-itens clearfix' style='width:100%;float:left; margin-bottom:40px'>
	<table style='width:70%'>
		<thead>
			<tr>
				<th>Item</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<tr class='pendencias' dir='adesao'>
				<td>Adesão</td>
				<td class='saldo-adesao'></td>
			</tr>
			<tr class='pendencias' dir="campanhas">
				<td>Campanhas</td>
				<td class='saldo-campanhas'></td>
			</tr>
			<?php if($itensExtras) { ?>
			<?php foreach($itensExtras as $itemExtra) { ?>
			<tr class='pendencias' dir="item-<?=$itemExtra['id']?>">
				<td><?=$itemExtra['titulo']?></td>
				<td class='saldo-item-<?=$itemExtra['id']?>'>-</td>
			</tr>
			<?php } ?>
			<?php } ?>
			<tr>
				<td class='saldo-total-label'></td>
				<td class='saldo-total'></td>
			</tr>
		</tbody>
	</table>
	<table style='width:50%; float:left'>
		<thead>
			<tr>
				<th colspan="2">Confira os itens da festa de formatura</th>
			</tr>
		</thead>
		<?php if($itensExtras) { ?>
		<?php foreach($itensExtras as $itemExtra) { ?>
			<tr>
				<td><?=$itemExtra['titulo']?></td>
				<td class='qtde-item-<?=$itemExtra['id']?>'>-</td>
			</tr>
		<?php } ?>
		<?php } ?>
	</table>
	<table style='width:70%' id='tabela-pagamentos'>
		<thead>
			<tr>
				<th colspan='5'>Pagamentos</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<span id='finalizar-checkout' style='display:none' class='button submit'>FINALIZAR CHECKOUT</span>
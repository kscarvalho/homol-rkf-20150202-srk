<script>
	$(function() {
		$("#qtde-convites-extras").change( function() {
			checkout.convites.comprar(parseInt($(this).val()));
			checkout.convites.parcelamento.padrao();
		});
		$("#qtde-mesas-extras").change( function() {
			checkout.mesas.comprar(parseInt($(this).val()));
			checkout.mesas.parcelamento.padrao();
		});
	});
</script>
<div class='adicionar-itens'>
<?php if(!empty($convitesExtras)) { ?>
	<?php if(!isset($convitesExtras['error'])) { ?>
	<table style='width:95%'>
		<thead>
			<tr>
				<th>Preço Por Convite</th>
				<th>Qtde. Máxima de Convites</th>
				<th>Selecione</th>
				<th>Valor Total Dos Convites</th>
			</tr>
		</thead>
		<tr>
			<td class='preco-convites'>R$<?=number_format($convitesExtras['CheckoutConviteTurma']['valor'],2,',','.');?></td>
			<td><?=$convitesExtras['CheckoutConviteTurma']['convites_por_formando'];?></td>
			<td>
				<?=$form->input('qtdeConvitesExtras', array('options' => $selectConvitesExtras, 'type' => 'select', 'id' => 'qtde-convites-extras', 'label' => false, 'div' => false)); ?>
			</td>
			<td class='saldo-convites'>-</td>
		</tr>
	</table>
	<?php } else { ?>
	<h2 class='titulo'><?=$convitesExtras['error']?></h2>
	<?php } ?>
<?php } else { ?>
	<h2 class='titulo'>Essa turma não tem venda de convites extras disponível</h2>
<?php } ?>
	<br />
<?php if(!empty($mesasExtras)) { ?>
	<?php if(!isset($mesasExtras['error'])) { ?>
	<table style='width:95%'>
		<thead>
			<tr>
				<th>Preço Por Convite</th>
				<th>Qtde. Máxima de Mesas</th>
				<th>Selecione</th>
				<th>Valor Total Dos Mesas</th>
			</tr>
		</thead>
		<tr>
			<td class='preco-mesas'>R$<?=number_format($mesasExtras['CheckoutMesaTurma']['valor'],2,',','.');?></td>
			<td><?=$mesasExtras['CheckoutMesaTurma']['mesas_por_formando'];?></td>
			<td>
				<?=$form->input('qtdeMesasExtras', array('options' => $selectMesasExtras, 'type' => 'select', 'id' => 'qtde-mesas-extras', 'label' => false, 'div' => false)); ?>
			</td>
			<td class='saldo-mesas'>-</td>
		</tr>
	</table>
	<?php } else { ?>
	<h2 class='titulo'><?=$mesasExtras['error']?></h2>
	<?php } ?>
<?php } else { ?>
	<h2 class='titulo'>Essa turma não tem venda de mesas extras disponível</h2>
<?php } ?>
	<p>
		<br /><br />
		<input type="submit" class="submit botao-anterior" value="Anterior">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" class="submit botao-proximo" value="Próximo"></td>
	</p>
</div>
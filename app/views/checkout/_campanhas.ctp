<?php
$isOdd = false;
$campanhaUsuarioId = 0;
$exibirCancelamento = false;
?>
<style type="text/css">
.tabela-campanhas tfoot tr td { padding:20px }
.tabela-campanhas:last-child tfoot tr td { padding-bottom:40px }
</style>
<script type="text/javascript">
$(document).ready(function() {
	$(".cancelar").live('click',function() {
		if(confirm("Tem certeza que quer cancelar essa campanha?")) {
			var campanha = $(this).attr('dir').substring(9);
			var url = "/<?=$this->params["prefix"]?>/checkout/cancelar_campanha";
			var that = this;
			$(this).text('Aguarde');
	 	 	$.ajax({
	 	 		url : url,
	 	 		data : { "usuario" : <?=$formando['id']?>, campanha : campanha },
	 	 		type : "POST",
	 	 		dataType : "json",
	 	 		async : true,
	 	 		success : function(response) {
 	 	 			if(response.error == true) {
 	 	 	 			alert(response.message);
 	 	 	 			$(that).text('Cancelar esta Campanha');
 	 	 			} else {
 	 	 	 			checkout.campanhas.debitar(parseFloat(response.totalCampanha));
 	 	 	 			checkout.campanhas.atualizar();
 	 	 	 			$(that).removeClass('cancelar').addClass('descancelar');
 	 	 	 			$(that).text('Desfazer Cancelamento');
 	 	 			}
	 	 		},
	 	 		error : function() {
 	 	 			alert("Erro ao listar parcelas. Tente novamente mais tarde.");
	 	 		}
	 	 	});
		}
	});
	$(".descancelar").live('click',function() {
		if(confirm("Tem certeza que quer desfazer o cancelamento dessa campanha?")) {
			var campanha = $(this).attr('dir').substring(9);
			var url = "/<?=$this->params["prefix"]?>/checkout/reverter_cancelamento_campanha";
			var that = this;
			$(this).text('Aguarde');
	 	 	$.ajax({
	 	 		url : url,
	 	 		data : { "usuario" : <?=$formando['id']?>, campanha : campanha },
	 	 		type : "POST",
	 	 		dataType : "json",
	 	 		async : true,
	 	 		success : function(response) {
 	 	 			if(response.error == true) {
 	 	 	 			alert(response.message);
 	 	 	 			$(that).text('Cancelar esta Campanha');
 	 	 			} else {
 	 	 	 			checkout.campanhas.debitar(parseFloat(response.totalCampanha));
 	 	 	 			checkout.campanhas.atualizar();
 	 	 	 			$(that).removeClass('descancelar').addClass('cancelar');
 	 	 	 			$(that).text('Cancelar esta Compra');
 	 	 			}
	 	 		},
	 	 		error : function() {
 	 	 			alert("Erro ao listar parcelas. Tente novamente mais tarde.");
	 	 		}
	 	 	});
		}
	});
});
</script>
<div class="grid_full alpha omega first" id="conteudo-container">
	<!-- Exibir resumo de extras -->
	<h2><b>Resumo de Extras</b></h2>
	<?php
	if( count($resumoExtras) == 0 ) {
		echo '<p style="color:red">Nenhum pagamento de extra efetuado foi encontrado.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<?php foreach($resumoExtras as $extraResumo) : ?>
		<?php if($campanhaUsuarioId != $extraResumo['ViewRelatorioExtras']['campanhas_usuario_id'] && $exibirCancelamento) : ?>
			</tbody>
		</table>
		<?php endif; ?>
		<?php if($campanhaUsuarioId != $extraResumo['ViewRelatorioExtras']['campanhas_usuario_id']) : ?>
		<?php $campanhaUsuarioId = $extraResumo['ViewRelatorioExtras']['campanhas_usuario_id']; $exibirCancelamento = true; ?>
		<table class="tabela-campanhas">
			<thead>
				<tr>
					<th scope="col">Evento</th>
					<th scope="col">Campanha</th>
					<th scope="col">Extra</th>
					<th scope="col">Qtd.</th>
					<th scope="col">Valor</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan='3' align="right">
						Valor Pago: R$<?=number_format($extraResumo['ViewRelatorioExtras']['valor_pago'],2,',','.')?>
					</td>
					<td colspan='2'>
						<span class='submit button <?=$extraResumo['ViewRelatorioExtras']['cancelada'] == 0 ? "cancelar" : "descancelar"?>'
							dir='campanha-<?=$extraResumo['ViewRelatorioExtras']['campanhas_usuario_id']?>'>
							<?=$extraResumo['ViewRelatorioExtras']['cancelada'] == 0 ? "Cancelar esta Compra" : "Desfazer Cancelamento"?>
						</span>
					</td>
				</tr>
			</tfoot>
			<tbody>
		<?php endif; ?>
			<tr class='<?=$isOdd ? "odd " : ""?>linha-despesa'>
				<td  colspan="1" width="20%"><?=$extraResumo['ViewRelatorioExtras']['evento']; ?></td>
				<td  colspan="1" width="20%"><?=$extraResumo['ViewRelatorioExtras']['campanha']; ?></td>
				<td  colspan="1" width="20%"><?=$extraResumo['ViewRelatorioExtras']['extra']; ?></td>
				<td  colspan="1" width="20%"><?=$extraResumo['ViewRelatorioExtras']['quantidade_total']; ?></td>
				<td  colspan="1" width="20%">R$<?=number_format($extraResumo['ViewRelatorioExtras']['valor_campanha'],2,',','.')?></td>
			</tr>
		<?php $isOdd = !$isOdd; endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    td:last-child{
        text-align: center
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.alterar').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/checkout/alterar/"?>'+
                            $(this).attr('dir')
            });
        });
        $('.inserir').click(function() {
            bootbox.dialog('Carregando',[{
                    label: 'Fechar'
                }],{
                    remote: '<?="/{$this->params['prefix']}/checkout/inserir_itens/"?>'
            });
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Itens Checkout
            <button type="button" class="button mini pull-right bg-color-pink inserir">
                Inserir Item
                <i class="icon-upload"></i>
            </button>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <?=$form->create('CheckoutItem',array(
        'url' => "/{$this->params['prefix']}/checkout/listar/",
        'id' => 'form-filtro')) ?>
<?php if (!empty($itens)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="30%">Título</th>
                <th scope="col" width="10%">Itens por Pessoa</th>
                <th scope="col" width="20%">Valor</th>
                <th scope="col" width="10%">Qtde Total</th>
                <th scope="col" width="10%">Qtde Convites</th>
                <th scope="col" width="10%">Qtde Mesas</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($itens as $item) : ?>
            <tr>
                <td><?=$item['CheckoutItem']['titulo']; ?></td>
                <td><?=$item['CheckoutItem']['itens_por_pessoa']; ?></td>
                <td>R$ <?=number_format($item['CheckoutItem']['valor'], 2, ',', '.');?></td>
                <td><?=$item['CheckoutItem']['quantidade']; ?></td>
                <td><?=$item['CheckoutItem']['quantidade_convites']; ?></td>
                <td><?=$item['CheckoutItem']['quantidade_mesas']; ?></td>
                <td>
                    <button type="button" class="default mini bg-color-orangeDark alterar" dir="<?=$item['CheckoutItem']['id']?>">
                            Alterar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Item Checkout Encontrado.</h2>
<?php endif; ?>
</div>
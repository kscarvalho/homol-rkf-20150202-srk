<?php
echo $html->script('jquery.tools.min');
echo $html->script('checkout');
echo $html->css('tooltip');
?>
<script>

	$(function() {
		
		$( "#tabs" ).tabs();
		
		$(".botao-proximo").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) + 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs( "select" , nomeDaProximaTab );
			window.top.location.href = "#conteudo";
			return false;
		});
		
		$(".botao-anterior").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) - 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs( "select" , nomeDaProximaTab );
			window.top.location.href = "#conteudo";
			return false;
		});
		
		$(".form-error").each(function() {
			idTabPai = $(this).parents("div").get(0).id;
			$("a[rel="+idTabPai+"]").parents("li").addClass("erro");
		});

		$('.obs').keyup( function() {
			$("."+$(this).attr('dir')).val($(this).val());
		});

		$("#finalizar-checkout").click( function() {
			var despesa = 0;
			var finalAdesao = 0;
			var finalCampanha = 0;
			var finalConvites = 0;
			var finalCredito = 0;
			if(!isNaN(stringMoneyToNumber($('.saldo-adesao').html()))) {
				despesa = parseFloat(stringMoneyToNumber($('.saldo-adesao').html()));
				if(despesa > 0)
					finalAdesao = {'forma_pagamento' : $("#formaPagamentoAdesao").val(),'parcelas' : $("#parcelasPagamentoAdesao").val(), 'valor_pago' : despesa, 'obs' : $('.obsPagamentoAdesao').val() };
			}
			if(!isNaN(stringMoneyToNumber($('.saldo-campanhas').html()))) {
				despesa = parseFloat(stringMoneyToNumber($('.saldo-campanhas').html()));
				if(despesa > 0)
					finalCampanha = {'forma_pagamento' : $("#formaPagamentoCampanha").val(),'parcelas' : $("#parcelasPagamentoCampanha").val(), 'valor_pago' : despesa,'obs' : $('.obsPagamentoCampanha').val() };
			}
			if(!isNaN(stringMoneyToNumber($('.saldo-convites').html()))) {
				despesa = parseFloat(stringMoneyToNumber($('.saldo-convites').html()));
				if(despesa > 0)
					finalConvites = {'forma_pagamento' : $("#formaPagamentoConvites").val(),'parcelas' : $("#parcelasPagamentoConvites").val(), 'valor_pago' : despesa,'obs' : $('.obsPagamentoConvites').val(), 'qtde' : parseInt($('#qtde-convites').html()) };
			}
			if(!isNaN(stringMoneyToNumber($('.saldo-credito').html()))) {
				despesa = parseFloat(stringMoneyToNumber($('.saldo-convites').html()));
				if(despesa > 0)
					finalCredito = {'forma_pagamento' : $("#formaPagamentoCredito").val(),'parcelas' : $("#parcelasPagamentoCredito").val(), 'valor_pago' : despesa,'obs' : $('.obsPagamentoCredito').val() };
			}
			var usuario = { "id" : <?=$formando['id']?>, 'qtdeParcelasAbertas' : $('#qtdeParcelasAbertas').val() };
			$.colorbox({
 	 			width:"500",
 	 			height:"400",
 	 			escKey : true,
 	 			html: "<p id='response'>Aguarde</p>",
 	 			overlayClose : false,
 	 			close : true,
 	 			top : 0,
 	 			onLoad : function() {
 	 	 			$.ajax({
 	 	 				url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "checkout", "action" => "finalizar"));?>",
 	 	 				type : 'POST',
 						data : { 'adesao' : finalAdesao, 'campanhas' : finalCampanha, 'convites' : finalConvites, 'avulso' : finalCredito, 'usuario' : usuario },
 						dataType : 'json',
 						error : function(response) {
 							$("#response").html('Ocorreu um erro ao conectar com o servidor. Tente novamente mais tarde');
 						},
 						success : function(response) {
 	 						if(response.error) {
 	 							$("#response").html(response.message);
 	 						} else {
 	 							var html = "O Checkout foi concluído com sucesso<br />";
 	 							html += "Protocolo: " + response.protocolo + "<br /><br />";
 	 							html += "Clique no link abaixo para mais informações<br /><br />";
 	 							html+= '<a href="/<?=$this->params['prefix']?>/solicitacoes/visualizar/' + response.protocolo + '" class="submit button">Info Checkout</a>';
 	 							$("#response").html(html);
 	 						}
 						}
 	 	 			});
 	 	 		}
 	 	 	});
		});
		
		iniciaSaldoAdesao();
		iniciaSaldoCampanhas();
		iniciaValorPago();
		atualizaSaldoConvites(true);
		atualizaCredito(true);
		atualizaSaldoTotal();
		
	});
	
</script>
<style type="text/css">
p { font-size:12px }
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
.ui-tabs .ui-tabs-panel { padding:10px 0 0 0!important }
#conteudo-container { margin:10px 0 0 0 !important }
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
.adicionar-itens { font-size:12px }
.adicionar-itens table { width:95%; margin-top:20px }
.adicionar-itens table thead tr th { background:white; border:none; border-bottom:solid 1px #333; padding-bottom:5px; font-size:15px; text-align:left }
.adicionar-itens table tr { background:white; border:none }
.adicionar-itens table tr td { padding:5px; text-align:left }
.upper { text-transform:capitalize; }
.resumo-checkout p { font-size:14px; line-height:30px; }
.resumo-checkout span.pendencia { color:red }
.resumo-checkout span.pendencia.green { color:green }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Checkout de Formando</span>
<div id="conteudo-container">
	<?=$form->hidden('qtdeParcelasAbertas',array('value' => $qtdeParcelasAbertas,'id' => 'qtdeParcelasAbertas'))?>
	<?php include('_info_formando.ctp')?>
	<div id="tabs" style='border:none!important' class="grid_full alpha omega">
		<ul>
			<li id="tab-0"><a href="#tabs-dados-formando" rel="tabs-dados-formando">Dados Financeiros</a></li>
			<li class="seta"></li>
			<li id="tab-1"><a href="#tabs-adicionar-itens" rel="tabs-adicionar-itens">Adicionar Itens</a></li>
			<li class="seta"></li>
			<li id="tab-2"><a href="#tabs-forma-pagamento" rel="tabs-forma-pagamento">Forma de Pagamento</a></li>
			<li class="seta"></li>
			<li id="tab-3"><a href="#tabs-revisao-checkout" rel="tabs-revisao-checkout">Finalizar</a></li>
		</ul>
		<div id="tabs-dados-formando" class="grid_full alpha omega">
			<?php include('despesas_formando_checkout.ctp')?>
		</div>
		<div id="tabs-adicionar-itens" class="grid_full alpha omega">
			<?php include("_adicionar_itens.ctp")?>
		</div>
		<div id="tabs-forma-pagamento" class="grid_full alpha omega">
			<?php include("_forma_pagamento.ctp")?>
		</div>
		<div id="tabs-revisao-checkout" class="grid_full alpha omega">
			<?php include('_finalizar.ctp')?>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>
<script>
	$(function() {
		
		$(".parcelamento").click( function() {
			var dir = $(this).attr('dir');
			var item = eval("checkout." + dir);
			$.colorbox({
				width : 700,
				height: 600,
				html : montaBoxParcelamento(item,dir),
				onComplete : function() { montarParcelas(item,dir); }
			});
		});

		$('.input-valor-parcelamento').live('keyup',function() {
			if(isNaN(stringToFloat(stringMoneyToNumber($(this).val()),2)))
				$(this).val('');
			$('#soma-parcelamentos').html(number_format(calculaValoresParcelamentos(),2,',','.'));
		});

		$('.parcelas').change( function() {
			var item = eval("checkout." + $(this).attr('dir'));
			item.parcelamento.padrao();
		});

		$('.remover-parcelamento').live('click', function(){
			$(this).parent().parent().hide('slow');
		});

		$('.adicionar-parcelamento').live('click', function(){
			adicionarParcela('','','','');
		});

		$("#finalizar-parcelamento").live('click', function() {
			var div = $("#box-parcelamentos");
			var dir = div.attr('dir');
			var table = div.children('table');
			var saldo = div.children('input[name="saldo-parcelamento"]').val();
			var soma = 0;
			var parcelasTemp = [];
			var erro = false;
			table.children("tbody").children("tr:visible").each(function(i,value) {
				var valor = $(this).children('.valor-parcelamento').children('input').val();
				if(valor == "") {
					message = "Digite o valor da " + (i+1) + "&ordf; parcela ";
					exibirMensagem(message);
					erro = true;
					return false;
				}
				valor = stringToFloat(stringMoneyToNumber(valor),2);
				var data = $(this).children('.data-parcelamento').children('input').val();
				if(data == "") {
					message = "Digite a data de vencimento da " + (i+1) + "&ordf;  parcela";
					exibirMensagem(message);
					erro = true;
					return false;
				}
				var tipo = $(this).children('.tipo-parcelamento').children('select').val();
				var obs = $(this).children('.obs-parcelamento').children('textarea').val();
				parcelasTemp[i] = {valor:valor,tipo:tipo,data_vencimento:data,obs:obs};
				soma+= valor;
			});
			soma = stringToFloat(soma,2);
			if(!erro) {
				if(soma != saldo) {
					exibirMensagem("A soma das parcelas, " + soma + ", é diferente do saldo, " + saldo);
				} else {
					var item = eval("checkout." + dir);
					item.parcelamento.zerar();
					$.each(parcelasTemp,function(i,parcela) {
						item.parcelamento.inserir({valor:parcela.valor,tipo:parcela.tipo,data_vencimento:parcela.data_vencimento,obs:parcela.obs},1);
					});
					$.colorbox.close();
				}
			}
		});
	});

	function calculaValoresParcelamentos() {
		var valorTotal = 0;
		$('.input-valor-parcelamento').each(function() {
			var valor = stringToFloat(stringMoneyToNumber($(this).val()),2);
			if(!isNaN(valor))
				valorTotal += valor;
		});
		return valorTotal;
	}

	function exibirMensagem(message) {
		var div = $('.erro-parcelamentos');
		div.html(message);
		div.fadeIn(500);
		setTimeout(function() { div.fadeOut(500); },5000);
	}

	function montaBoxParcelamento(item,dir) {
		if(item.saldo < 0) {
			html = "<div id='box-parcelamentos' dir='" + dir + "'>";
			html+= "<div class='message error erro-parcelamentos' style='display:none'></div>";
			html+= "<input type='hidden' name='saldo-parcelamento' value='" + item.saldo*-1 + "' />";
			html+= "<p>A soma das parcelas deve atingir o saldo de R$" + number_format(item.saldo*-1,2,',','.');
			html+= "<br /><br />Soma = R$<span id='soma-parcelamentos'>" + number_format(item.saldo*-1,2,',','.') + "</span></p>";
			html+= headerParcelamentos();
			html+= "</div>";
		} else {
			html = "Não há saldo negativo para o item";
		}
		return html;
	}

	function headerParcelamentos() {
		var html = "<table class='tabela-boletos tabela-parcelamentos' id='tabela-parcelamentos'>";
		html+= "<thead><tr><th>Valor</th><th>Data de Vencimento</th>";
		html+= "<th>Tipo</th><th>Observações</th><th>&nbsp;</th></tr></thead>";
		html+= "<tfoot><tr><td colspan='2'><div class='adicionar-parcelamento'>Adicionar</div></td>";
		html+= "<td colspan='2'><span class='submit button' id='finalizar-parcelamento'>OK</td></tr></tfoot>";
		html+= "<tbody></tbody>";
		return html;
	}

	function adicionarParcela(valor,data,tipo,obs) {
		var html = "<tr><td class='valor-parcelamento'><input class='input-valor-parcelamento' type='text' value='" + number_format(valor,2,',','.') + "' /></td>";
		html+= "<td class='data-parcelamento'><input class='input-data-parcelamento' type='text' value='" + data + "' /></td>";
		html+= "<td class='tipo-parcelamento'><select class='input-tipo-parcelamento'>";
		html+= "<option value='cheque'" + (tipo != 'cheque' && tipo != "" ? "" : " selected='selected'") + ">Cheque</option>";
		html+= "<option value='dinheiro'" + (tipo == 'dinheiro' ? " selected='selected'" : "") + ">Dinheiro</option>";
		html+= "<option value='comprovante'" + (tipo == 'comprovante' ? " selected='selected'" : "") + ">Comprovante</option>";
		html+= "</select></td>";
		html+= "<td class='obs-parcelamento'><textarea class='input-obs-parcelamento'>" + obs + "</textarea></td>";
		html+= "<td><div class='remover-parcelamento'></div></td></tr>";
		$("#tabela-parcelamentos tbody").append(html);
		$(".input-data-parcelamento").setMask({mask:"99/99/9999",autoTab:false});
	}

	function montarParcelas(item,dir) {
		if(item.parcelamento.parcelas.length < 1) {
			var parcelas = $('select.parcelas[dir='+dir+']').val();
			var tipo = $('select.pagamento-forma[dir='+dir+']').val();
			var valor = item.saldo*-1 / parcelas;
			var date = new Date();
			for(var a = 1; a <= parcelas; a++) {
				date.setMonth(parseInt(date.getMonth())+1);
				var dataVencimento = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
				item.parcelamento.inserir({valor:valor,tipo:tipo,data_vencimento:dataVencimento},1);
				adicionarParcela(valor,dataVencimento,tipo,'');
			}
		} else {
			$.each(item.parcelamento.parcelas, function(parcela,dados) {
				var valor = dados.valor != undefined ? dados.valor : "";
				var dataVencimento = dados.data_vencimento != undefined ? dados.data_vencimento : "";
				var tipo = dados.tipo != undefined ? dados.tipo : "";
				var obs = dados.obs != undefined ? dados.obs : "";
				adicionarParcela(valor,dataVencimento,tipo,'');
			});
		}
	}
	
</script>
<div class='adicionar-itens'>
<table style='width:95%'>
	<thead>
		<tr>
			<th>Item</th>
			<th>Valor</th>
			<th>Forma de Pagamento</th>
			<th>Parcelas</th>
			<th></th>
		</tr>
	</thead>
	<tr class='pendencias' dir='adesao'>
		<td>Adesão</td>
		<td class='saldo-adesao'></td>
		<td>
			<?=$form->input('formaPagamentoAdesao',
					array('options' => $formaPagamentoAdesao, 'class' => 'pagamento-forma', 'dir' => 'adesao', 'type' => 'select', 'id' => 'formaPagamentoAdesao',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<?=$form->input('parcelasPagamentoAdesao',
					array('options' => $parcelasPagamentoAdesao, 'class' => 'parcelas', 'dir' => 'adesao', 'type' => 'select', 'id' => 'parcelasPagamentoAdesao',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<span class="submit parcelamento" dir='adesao'>Inserir Parcelas</span>
		</td>
	</tr>
	<tr class='pendencias' dir="campanhas">
		<td>Campanhas</td>
		<td class='saldo-campanhas'></td>
		<td>
			<?=$form->input('formaPagamentoCampanha',
					array('options' => $formaPagamentoCampanha, 'class' => 'pagamento-forma', 'dir' => 'campanhas', 'type' => 'select', 'id' => 'formaPagamentoCampanha',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<?=$form->input('parcelasPagamentoCampanha',
					array('options' => $parcelasPagamentoCampanha, 'class' => 'parcelas', 'dir' => 'campanhas', 'type' => 'select', 'id' => 'parcelasPagamentoCampanha',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<span class="submit parcelamento" dir='campanhas'>Inserir Parcelas</span>
		</td>
	</tr>
	<tr class='pendencias' dir='convites'>
		<td>Convites Extras</td>
		<td class='saldo-convites'></td>
		<td>
			<?=$form->input('formaPagamentoConvites',
					array('options' => $formaPagamentoConvites, 'class' => 'pagamento-forma', 'dir' => 'convites', 'type' => 'select', 'id' => 'formaPagamentoConvites',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<?=$form->input('parcelasPagamentoConvites',
					array('options' => $parcelasPagamentoConvites, 'class' => 'parcelas', 'dir' => 'convites', 'type' => 'select', 'id' => 'parcelasPagamentoConvites',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<span class="submit parcelamento" dir='convites'>Inserir Parcelas</span>
		</td>
	</tr>
	<tr class='pendencias' dir='mesas'>
		<td>Mesas Extras</td>
		<td class='saldo-mesas'></td>
		<td>
			<?=$form->input('formaPagamentoMesas',
					array('options' => $formaPagamentoConvites, 'class' => 'pagamento-forma', 'dir' => 'mesas', 'type' => 'select', 'id' => 'formaPagamentoMesas',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<?=$form->input('parcelasPagamentoMesas',
					array('options' => $parcelasPagamentoConvites, 'class' => 'parcelas', 'dir' => 'mesas', 'type' => 'select', 'id' => 'parcelasPagamentoMesas',
							'label' => false, 'div' => false)); ?>
		</td>
		<td>
			<span class="submit parcelamento" dir='mesas'>Inserir Parcelas</span>
		</td>
	</tr>
	<tr>
		<td colspan='2' style='padding-top:5px'><input type="submit" class="submit botao-anterior" value="Anterior"></td>
		<td colspan='3' style='padding-top:5px'><input type="submit" class="submit botao-proximo" value="Próximo"></td>
	</tr>
</table>
</div>
<div class='parcelamentos' style='display:none'>
</div>
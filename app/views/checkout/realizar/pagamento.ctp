<script type="text/html" id="tmpPagamentos">
    <!-- ko if: $index() == 0 -->
    <div class="row-fluid">
        <div class="span4">
            <button type="button" class="default bg-color-greenDark input-block-level"
                data-bind="click: $root.novaParcela">
                Nova Parcela
            </button>
        </div>
    </div>
    <div class="row-fluid">
        <h4 class="fg-color-red"><b>ATENÇÃO:</b> Utilizar somente ponto nos campos de valores!</h4>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <label>Valor</label>
        </div>
        <div class="span2">
            <label>Tipo</label>
        </div>
        <div class="span3">
            <label>Data Venc</label>
        </div>
    </div>
    <!-- /ko -->
    <div class="row-fluid">
        <div class="span2">
            <div class="input-control text">
                <input type="text" class="" data-bind="value: valor, valueUpdate:'afterkeydown'" />
            </div>
        </div>
        <div class="span2">
            <?=$form->input(false,array(
                'options' => $tipoPagamento,
                'type' => 'select',
                'class' => 'selectpicker',
                'data-width' => '100%',
                'data-container' => 'body',
                'data-bind' => 'value: $data.tipo',
                'label' => false,
                'div' => 'input-control text')); ?>
        </div>
        <div class="span3" data-bind="visible: $data.tipo() != 'dinheiro'">
            <?=$form->input(false,array(
                'type' => 'text',
                'class' => 'datepicker',
                'data-bind' => "value: data_vencimento,valueUpdate:'afterkeydown'",
                'label' => false,
                'div' => 'input-control text')); ?>
        </div>
        <div class="span4">
            <div class="btn-group grupo" data-toggle="buttons-checkbox">
                <button type="button" class="button btn mini bg-color-gray checkout" data-referente="0">Checkout</button>
                <button type="button" class="button btn mini bg-color-gray campanha" data-referente="0">Campanha</button>
                <button type="button" class="button btn mini bg-color-gray adesao" data-referente="0">Adesão</button>
            </div>
        </div>
    </div>
</script>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input(false,array(
            'options' => $formaPagamento,
            'type' => 'select',
            'empty' => 'Selecione a forma de pagamento',
            'id' => 'forma-pagamento',
            'class' => 'selectpicker',
            'data-width' => '100%',
            'data-container' => 'body',
            'data-bind' => 'value: formaDePagamento',
            'label' => 'Forma de pagamento',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3" data-bind="visible: formaDePagamento() != 'dinheiro' && formaDePagamento() != ''">
        <?=$form->input(false,array(
            'type' => 'text',
            'id' => 'data-base',
            'readonly' => 'readonly',
            'class' => 'datepicker',
            'data-bind' => 'value: dataBase',
            'label' => 'Data Base',
            'div' => 'input-control text')); ?>
    </div>
    <?php 
        if($qtdeParcelas != 1){
            $q = array(); 
            for($a=2;$a<=$qtdeParcelas;$a++) 
                $q[$a] = $a;
        }else{
            (int) $q = array(1 => 1);
        }
    ?>
    <div class="span2" data-bind="visible: formaDePagamento() == 'parcelar' && dataBase() != ''">
        <?=$form->input(false,
            array(
                'label' => 'Parcelas',
                'div' => 'input-control text',
                'error' => false,
                'class' => 'selectpicker',
                'type' => 'select',
                'options' => $q,
                'data-width' => '100%',
                'data-bind' => 'value: qtdeParcelas',
                'data-container' => 'body',
                'title' => 'Selecione'
            )); ?>
    </div>
    <div class="span3" data-bind="visible: formaDePagamento() == 'parcelar' && dataBase() != '' && qtdeParcelas() > 0">
        <label>&nbsp;</label>
        <button type="button" class="default bg-color-blueDark input-block-level parcelar"
            data-bind="click: function() { gerarParcelas(<?=$formando['Usuario']['id']?>) }">
            Parcelar
        </button>
    </div>
</div>
<div class="row-fluid" data-bind="visible: pagamentos().length > 0">
    <div class="row-fluid" data-bind="template: {
        name: 'tmpPagamentos', foreach: pagamentos(), afterAdd: camposPagamento }">
    </div>
</div>
<div class="row-fluid" data-bind="visible:pagamentos().length > 1 && saldo() + somaPagamentos() != 0">
    <h3 class="fg-color-red" data-bind="visible:saldo() + somaPagamentos() > 0">
        <em data-bind="text: formatarValor(saldo() + somaPagamentos())"></em> acima do valor total
    </h3>
    <h3 class="fg-color-red" data-bind="visible:saldo() + somaPagamentos() < 0">
        <em data-bind="text: formatarValor(Math.abs(saldo() + somaPagamentos()))"></em> abaixo do valor total
    </h3>
</div>
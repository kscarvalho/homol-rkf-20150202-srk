<?php if(count($campanhas) > 0) : ?>
<?php foreach($campanhas as $campanha) : ?>
<h2 class="fg-color-red"><?=$campanha['nome']?></h2>
<br />
<?php foreach($campanha['compras'] as $campanhaUsuarioId => $compra) : ?>
<table class="table table-condensed table-striped campanhas" data-campanha-usuario="<?=$campanhaUsuarioId?>">
    <?php if($compra['cancelada'] != 1) : ?>
    <thead>
        <tr>
            <th colspan="8">
                Compra efetuada em <?=date('d/m/Y',strtotime($compra['data_compra']))?> 
                &agrave;s <?=date('H:i',strtotime($compra['data_compra']))?>
                &nbsp;&nbsp;&nbsp;                
                <span class="strong"><?=ucfirst($compra['status'])?></span>
                <span class="strong pull-right pointer fg-color-red"
                    data-cancelada="<?=$compra['cancelada']?>"
                    data-campanha="<?=$campanhaUsuarioId?>">
                </span>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="3">
                Valor da compra: R$<?=number_format($compra['valor_campanha'], 2, ',', '.') ?>
            </td>
            <td colspan="3">
                Valor pago: R$<?=number_format($compra['valor_pago'], 2, ',', '.') ?>
            </td>
            <td colspan="2">
                Parcelas: <?=count($compra['despesas'])?>
            </td>
        </tr>
        <?php foreach($compra['itens'] as $item) : ?>
        <tr data-quantidade-mesas="<?=$item['quantidade_mesas']?>"
            data-quantidade-convites="<?=$item['quantidade_convites']?>"
            data-item-nome="<?=$item['nome']?>"
            data-item-quantidade="<?=$item['quantidade']?>">
            <td colspan="8">
                <?=$item['evento']?> - <?=$item['quantidade']?> <?=$item['nome']?>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="bg-color-grayDark" width="12%">Parcela</td>
            <td class="bg-color-grayDark" width="15%">Valor</td>
            <td class="bg-color-grayDark" width="15%">Data Venc</td>
            <td class="bg-color-grayDark" width="15%">Data Pagam</td>
            <td class="bg-color-grayDark" width="15%">Valor Cred</td>
            <td class="bg-color-grayDark" width="13%">Status</td>
            <td class="bg-color-grayDark" width="15%">Saldo</td>
        </tr>
        <?php foreach($compra['despesas'] as $despesa) : ?>
        <tr data-despesa="<?=$despesa['id']?>" class="despesas">
            <td><?= $despesa['parcela'] . ' de ' . $despesa['total_parcelas']; ?></td>
            <td>R$<?=number_format($despesa['valor'], 2, ',', '.') ?></td>
            <?php if (in_array($despesa['status'],array('atrasada','aberta'))) : ?>
            <td data-vencimento="<?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>">
            <?php else : ?>
            <td>
            <?php endif; ?>
                <span class="pointer">
                    <?=date('d/m/Y', strtotime($despesa['data_vencimento']))?>
                </span>
            </td>
            <td>
                <?php
                if(!empty($despesa['data_pagamento']))
                    echo date('d/m/Y', strtotime($despesa['data_pagamento']));
                else
                    echo '-';
                ?>
            </td>
            <td>R$<?=number_format($despesa['valor_pago'], 2, ',', '.') ?></td>
            <td data-status="status"><?=$despesa['status']?></td>
            <?php if($despesa['saldo_parcela'] == 0) : ?>
            <td>-</td>
            <?php elseif($despesa['saldo_parcela'] > 0) : ?>
            <td class="fg-color-orange saldo" data-saldo="<?=$despesa['saldo_parcela']?>">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php else : ?>
            <td class="fg-color-green">
                R$<?= number_format(abs($despesa['saldo_parcela']), 2, ',', '.'); ?>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
        <tr style="height:15px"><td class="bg-color-white" colspan="8"></td></tr>
    </tbody>
    <?php endif; ?>
</table>
<br />
<?php endforeach; ?>
<?php endforeach; ?>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma venda de campanha encontrada</h2>
<?php endif; ?>

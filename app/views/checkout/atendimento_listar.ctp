<span id="conteudo-titulo" class="box-com-titulo-header">
    Lista Checkouts Realizados
</span>
<div id="conteudo-container">
    <div class="tabela-adicionar-item">
            <?=$form->create(false, array('url' => "/{$this->params['prefix']}/checkout/listar", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')); ?>
            <span>Busca: <?=$form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false )); ?> </span>
            <?=$form->input('filtro-tipo', array('options' => $filtroBusca, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
            &nbsp;
            <?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
            <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th>C&oacute;digo</th>
                    <th>Formando</th>
                    <th>Data</th>
                    <th>Dinheiro</th>
                    <th>Cheques/Qtde</th>
                    <th>Cheques/Total</th>
                    <th>Visualizar</th>
                </tr>
            </thead>
            <?php if(count($listaCheckout) > 0) : ?>
            <?php foreach($listaCheckout as $checkout) : ?>
            <tbody>
                <tr>
                    <td width="10%"><?=$checkout['codigo']?></td>
                    <td width="30%"><?=$checkout['formando']?></td>
                    <td width="10%"><?=date('d/m/Y',strtotime($checkout['data']))?></td>
                    <td width="10%"><?=number_format($checkout['dinheiro'], 2, ',', '.');?></td>
                    <td width="15%"><?=$checkout['cheque']['qtde']?></td>
                    <td width="10%"><?=number_format($checkout['cheque']['total'], 2, ',', '.');?></td>
                    <td width="30%"><?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'solicitacoes', 'action' =>'visualizar', $checkout['protocolo']), array('class' => 'submit button')); ?></td>
                </tr>
            </tbody>
            <?php endforeach; ?>
            <?php else : ?>
            <tr>
                <td colspan='6'><h2>Nenhum Checkout Encontrado</h2></td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
</div>

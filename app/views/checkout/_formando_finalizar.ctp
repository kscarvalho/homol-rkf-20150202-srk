<p class="grid_16 omega alpha first">
	Caro formando, finalizando o checkout você não poderá mais gerar boletos pelo site.
	<br />
	Se você encontrou divergência em alguma informação fornecida durante o checkout,
	<br />
	por favor descreva o caso no campo abaixo para facilitar a entrega dos seus convites para a formaturas.
	<br />
	<br />
	<?=$form->textarea('FormandoProfile.obs_checkout',array('class' => 'input-checkout')); ?>
	<br />
	<br />
	<span class="submit button" id='finalizar'>Finalizar</span>
</p>
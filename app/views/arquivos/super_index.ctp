<span id="conteudo-titulo" class="box-com-titulo-header">Cursos</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $html->link('Adicionar', array($this->params['prefix'] => true, 'action' => 'adicionar'), array('class' => 'tabela-adicionar-item')); ?>
	
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Tipo', 'tipo'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Tamanho', 'tamanho'); ?></th>
					<th scope="col"> &nbsp;</th>				
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>				
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($arquivos as $arquivo): ?>
				<?php if($isOdd):?><tr class="odd"><?php else:?><tr><?php endif;?>
					<td colspan="1" width="10%"><?php echo $arquivo['Arquivo']['id']; ?></td>
					<td colspan="1" width="40%"><?php echo $arquivo['Arquivo']['nome']; ?></td>
					<td colspan="1" width="10%"><?php echo $arquivo['Arquivo']['tipo']; ?></td>
					<td colspan="1" width="15%"><?php echo $arquivo['Arquivo']['tamanho']; ?> Bytes</td>
					<td  colspan="1" width="25%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'], 'controller' => 'Arquivos', 'action' =>'visualizar', $arquivo['Arquivo']['id']), array('class' => 'submit button')); ?>
					</td>
				</tr>
				<?php if($isOdd){ $isOdd = false; } else {$isOdd = true; } ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
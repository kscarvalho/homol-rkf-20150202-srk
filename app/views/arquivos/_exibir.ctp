<style type='text/css'>
.thumbframe { position:relative; width:100%; height:90%; min-height:300px;
    overflow:hidden }
.thumbframe iframe { position:absolute; top:0; left:0; width:100%; border:none;
    margin:0; padding:0; -webkit-transform-origin: 0 0; -moz-transform-origin: 0 0;
    transform-origin: 0 0 }
</style>
<div class="row-fluid">
<?php if(!empty($response['mensagem'])) { ?>
    <h2 class="fg-color-red"><?=$response['mensagem']?></h2>
    <br />
<?php } ?>
<?php if($arquivo) { ?>
    <div class="row-fluid">
    <?php if(!$response['erro']) { ?>
        <div class='span8'>
            <div class='thumbframe'>
                <iframe src="/<?=$this->params['prefix']?>/arquivos/preview/<?=$arquivo['Arquivo']['id']?>">
                </iframe>
            </div>
        </div>
<?php if($response['scale']) { ?>
<script type="text/javascript">
$(document).ready(function() {
    var containerWidth = $('.thumbframe').width();
    $('iframe').load(function() {
        var iframeWidth = $(this).contents().width();
        var iframeHeight = $(this).contents().height();
        var scale = "0."+Math.floor((containerWidth*100)/iframeWidth);
        //console.log(scale);
        $(this)
            .css('width',iframeWidth)
            .css('-moz-transform','scale('+scale+')')
            .css('-webkit-transform','scale('+scale+')')
            .css('-o-transform','scale('+scale+')')
            .css('transform','scale('+scale+')')
            .css('height',iframeHeight);
    })
})
</script>
<?php } ?>
    <?php } ?>
        <div class='span4'>
            <?=$html->link('Baixar Arquivo',
                array($this->params['prefix'] => true,
                    'controller' => 'Arquivos',
                    'action' => 'baixar',
                    $arquivo['Arquivo']['id']),
                array(
                    'class' => 'button btn-block'
                )) ?>
        </div>
    </div>
<?php } ?>
</div>
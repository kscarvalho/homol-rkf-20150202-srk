<div class="row-fluid">
    <div class="span6">
        <h3>Informa&ccedil;&otilde;es Ades&atilde;o</h3>
        <br />
        <table class="table table-condensed table-striped">
            <tbody>
                <tr>
                    <td class="header">C&oacute;digo</td>
                    <td><?= $formando['codigo_formando'] ?></td>
                </tr>
                <tr>
                    <td class="header">Grupo</td>
                    <td><?= ucfirst($formando['grupo']) ?></td>
                </tr>
                <tr>
                    <td class="header">Data Contrato Turma</td>
                    <td><?= date('d/m/Y', strtotime($turma['data_assinatura_contrato'])) ?></td>
                </tr>
                <tr>
                    <td class="header">Data Contrato Ades&atilde;o</td>
                    <td><?= date('d/m/Y', strtotime($formando['data_adesao'])) ?></td>
                </tr>
                <tr>
                    <td class="header">Data IGPM</td>
                    <td>
                        <?= !empty($turma['data_igpm']) ? date('d/m/Y', strtotime($turma['data_igpm'])) : "N&atilde;o Aplicado" ?>
                    </td>
                </tr>
                <tr>
                    <td class="header">Convites Contrato</td>
                    <td><?= $formando['convites_contrato'] ?></td>
                </tr>
                <tr>
                    <td class="header">Mesas Contrato</td>
                    <td><?= $formando['mesas_contrato'] ?></td>
                </tr>
                <tr>
                    <td class="header">N&ordm; Parcelas Ades&atilde;o</td>
                    <td><?= $formando['parcelas_adesao'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="span6">
        <h3>Resumo Financeiro</h3>
        <br />
        <table class="table table-condensed table-striped">
            <tbody>
                <tr>
                    <td class="header">Valor de Contrato</td>
                    <td>R$<?= number_format($totalAdesao, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de IGPM</td>
                    <td>R$<?= number_format($totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de Contrato c/ correção</td>
                    <td>R$<?= number_format($totalAdesao + $totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de Extras</td>
                    <td>R$<?= number_format($totalExtras, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Total Recebido</td>
                    <td>R$<?= number_format($totalPago, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header <?= (int) $saldoAtual < 0 ? "bg-color-red" : "" ?>">
                        <?= (int) $saldoAtual < 0 ? "Saldo Negativo" : ((int) $saldoAtual > 0 ? "Saldo Positivo" : "Saldo Total") ?>
                    </td>
                    <td class="<?= $saldoAtual < 0 ? "strong fg-color-red" : "" ?>">
                        R$<?= number_format(abs($saldoAtual), 2, ',', '.'); ?>
                    </td>
                </tr>
                <tr>
                    <?php if($formando['situacao'] == 'cancelado'){ ?>
                    <td class="header bg-color-red">
                        Contrato Cancelado
                    </td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>
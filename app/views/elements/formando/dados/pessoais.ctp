<?php $buttonAfter = '<button class="helper" onclick="return false" ' .
        'tabindex="-1" type="button"></button>'; ?>
<div class="row-fluid">
    <div class="span5">
            <label>Nome</label>
            <?=$form->input('Usuario.nome',
                array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text',
                    'error' => false, 'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>Email</label>
        <?=$form->input('Usuario.email',
            array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text',
                'error' => false, 'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>Email Alternativo</label>
        <?=$form->input('Usuario.email_secundario',
            array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text',
                'error' => false, 'after' => $buttonAfter)); ?>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class='span11'>
        <?=$form->input('FormandoProfile.curso_turma_id',
            array('label' => 'Curso', 'disabled' => 'disabled', 'type' => 'select','options' => $lista_curso_turma_id,
                'class' => 'selectpicker', 'data-placeholder' => 'Curso', 'div' => 'input-control','error' => false)); ?>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span3">
        <label>Data Nascimento</label>
        <?=$form->input('FormandoProfile.data_nascimento',
            array('label' => false, 'disabled' => 'disabled', 'type' => 'text',
                'dateFormat' => 'DMY', 'class' => 'datepicker','div' => 'input-control',
                'error' => false)); ?>
    </div>
    <div class="span3">
        <label>Sexo</label>
        <label class="input-control switch sexo" onclick="">
                <input type="checkbox" <?=$usuario['FormandoProfile']['sexo'] == "M" ? 'checked="checked"' : ''?>
                        name='data[FormandoProfile][sexo]' id='sexoFormando' class="disabled"
                        disabled="disabled">
                <span class="helper sexo-selecionado">
                        <?=$usuario['FormandoProfile']['sexo'] == "M" ? "Masc" : "Fem"?>
                </span>
        </label>
    </div>
    <div class="span2">
        <?=$form->input('FormandoProfile.tam_camiseta',
            array('label' => 'Camiseta', 'disabled' => 'disabled','type' => 'select',
                'class' => 'selectpicker', 'options' => $fields['tam_camiseta'],'div' => 'input-control',
                'error' => false)); ?>
    </div>
    <?php $calcado = array(); for($a = 33; $a <= 49; $a+=2) $calcado[$a] = "$a / ".($a+1)?>
    <?php if ($turma['Turma']['id'] != 5848){ ?>
    <div class="span3">
        <?=$form->input('FormandoProfile.numero_havaiana',
            array('label' => 'Calçado', 'disabled' => 'disabled','type' => 'select',
                'class' => 'selectpicker', 'options' => $calcado, 'div' => 'input-control','error' => false)); ?>
    </div>
    <?php } ?>
</div>
<div class="row-fluid">
    <?php if($turma['Turma']['id'] == 5863){ ?>
    <div class="span3">
        <?=$form->input('FormandoProfile.tam_moleton',
            array('label' => 'Moleton', 'disabled' => 'disabled','class' => 'chosen','type' => 'select',
                'options' => $fields['tam_moleton'],'div' => 'input-control',
                'error' => false)); ?>
    </div>
    <?php } ?>
    <div class="span3">
        <label>RG</label>
        <?=$form->input('FormandoProfile.rg',
            array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text',
                'error' => false, 'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>CPF</label>
        <?=$form->input('FormandoProfile.cpf',
            array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text',
                'error' => false, 'mask' => '999.999.999-99', 'after' => $buttonAfter)); ?>
    </div>
    <?php if($this->params['prefix'] == 'atendimento') : ?>
    <div class="span3">
        <label>Forma Pagamento</label>
        <?=$form->input('FormandoProfile.forma_pagamento', array(
            'options' => array('boleto_online' => 'Boleto Online', 'boleto_impresso' => 'Boleto Impresso'),
            'type' => 'select',
            'class' => 'selectpicker forma-pagamento',
            'label' => false,
            'div' => 'input-control text')); ?>
        </div>
    <?php endif; ?>
</div>
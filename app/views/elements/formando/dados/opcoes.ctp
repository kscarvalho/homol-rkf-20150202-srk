<?php $buttonAfter = '<button class="helper" onclick="return false" '.
        'tabindex="-1" type="button"></button>'; ?>
<div class="row-fluid opcoes">
    <div class="span12">
        <button type="button" class="default" id="habilitar">
            Editar Dados
            <i class="icon-pencil "></i>
        </button>
        <button type="button" class="bg-color-blue exibir-opcoes"
            dir="senha">
            Alterar Senha
            <i class="icon-unlocked"></i>
        </button>
        <button type="button" class="bg-color-red exibir-opcoes"
            dir="foto">
            Alterar Foto de Perfil
            <i class="icon-picture-2"></i>
        </button>
    </div>
</div>
<br />
<div class="row-fluid hide opcoes" dir="foto">
    <div class="fileupload <?=!empty($fotoPerfil) ? "fileupload-exists" : 'fileupload-new'?>"
            data-provides="fileupload" style="width:240px;">
        <div>
            <span class="btn-file">
                <a class="button mini default fileupload-new">Selecionar Imagem</a>
                <a class="button mini fileupload-exists">
                    <i class="icon-retweet"></i>
                    Alterar
                </a>
                <input id="foto-perfil" type="file" />
            </span>
            <!--
            <a class="button bg-color-red fg-color-white mini fileupload-exists"
                data-dismiss="fileupload">
                <i class="icon-cancel"></i>
                Remover
            </a>
            -->
        </div>
        <div class="fileupload-new thumbnail" style="width:240px; height:170px;">
            <img src="<?="{$this->webroot}metro/img/no-image.gif"?>" />
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail"
            style="width:240px; height:170px">
        <?php if(!empty($fotoPerfil)):?>
            <img src="<?="{$this->webroot}$fotoPerfil"?>" />
        <?php endif; ?>
        </div>
    </div>
    <button class="default hide bg-color-blue"
        id="enviar-foto-nova">
        <i class="icon-upload-2"></i>
        Confirmar Altera&ccedil;&atilde;o
    </button>
    <button class="default ocultar-opcoes">
        <i class="icon-locked"></i>
        Cancelar
    </button>
</div>
<div class="row-fluid hide opcoes" dir="senha">
    <div class="span3">
        <label>Senha</label>
        <?=$form->input('senha', array('label' => false,
            'div' => 'input-control password',
            'type' => 'password', 'error' => false,
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label>Confirmar Senha</label>
        <?=$form->input('confirmar', array('label' => false,
            'div' => 'input-control password',
            'type' => 'password', 'error' => false,
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span6">
        <label>&nbsp;</label>
        <button class="fg-color-white bg-color-blue" id="enviar-nova-senha">
            <i class="icon-redo"></i>
            Atualizar Senha
        </button>
        <button class="default ocultar-opcoes">
            <i class="icon-locked"></i>
            Cancelar
        </button>
    </div>
</div>

					<div class="row-fluid">
						<div class="span6">
							<h3>Resumo Financeiro</h3>
							<br />
							<table class="table table-condensed table-striped">
								<tbody>
									<tr>
										<td class="header">Valor de Contrato</td>
										<td>R$<?=number_format($usuario['FormandoProfile']['valor_adesao'],2,',','.');?></td>
									</tr>
									<tr>
										<td class="header">Valor de IGPM</td>
										<td>R$<?=number_format($totalIGPM,2,',','.');?></td>
									</tr>
									<tr>
										<td class="header">Valor de Contrato c/ correção</td>
										<td>R$<?=number_format($usuario['FormandoProfile']['valor_adesao']+$totalIGPM,2,',','.');?></td>
									</tr>
									<tr>
										<td class="header">N&ordm; de Parcelas</td>
										<td><?=$usuario['FormandoProfile']['parcelas_adesao']?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<br />
					<div class="row-fluid">
						<button type="button" class="default"
							data-bind="click: function() { page('/<?=$this->params["prefix"]?>/area_financeira/dados') }">
							Mais Informa&ccedil;&otilde;es
						</button>
					</div>
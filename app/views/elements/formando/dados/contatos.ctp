					<div class="row-fluid">
						<div class="span3">
							<label>Tel Comercial</label>
							<?=$form->input('FormandoProfile.tel_comercial',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => 'phone',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span3">
							<label>Tel Resid</label>
							<?=$form->input('FormandoProfile.tel_residencial',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => 'phone',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span3">
							<label>Celular</label>
							<?=$form->input('FormandoProfile.tel_celular',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => '(99) 99999-9999',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span2">
							<label>Nextel</label>
							<?=$form->input('FormandoProfile.nextel',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<label>Nome Pai</label>
							<?=$form->input('FormandoProfile.nome_pai',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span5">
							<label>Nome M&atilde;e</label>
							<?=$form->input('FormandoProfile.nome_mae',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span3">
							<label>Tel Pai</label>
							<?=$form->input('FormandoProfile.tel_pai',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => 'phone',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span3">
							<label>Celular Pai</label>
							<?=$form->input('FormandoProfile.tel_cel_pai',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => 'phone',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span3">
							<label>Tel M&atilde;e</label>
							<?=$form->input('FormandoProfile.tel_mae',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => 'phone',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span2">
							<label>Celular M&atilde;e</label>
							<?=$form->input('FormandoProfile.tel_cel_mae',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false, 'mask' => '(99) 99999-9999',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
					</div>
					<br />
					<div class="row-fluid">
						<div class="span6">
							<label>Cep&nbsp;&nbsp;&nbsp;<span style="font-size:12px"><em id="cep-load"></em></span></label>
							<?=$form->input('FormandoProfile.end_cep',
									array('label' => false, 'div' => 'input-control text small','error' => false,
										'mask' => '99999-999', 'disabled' => 'disabled',
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
							<a class="button" id="busca-cep" disabled="disabled">Buscar CEP</a>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span7">
							<label>Rua</label>
							<?=$form->input('FormandoProfile.end_rua',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span2">
							<label>N&ordm;</label>
							<?=$form->input('FormandoProfile.end_numero',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span2">
							<label>Compl</label>
							<?=$form->input('FormandoProfile.end_complemento',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
					</div>
					<?php $uf = array(
							'AC' => 'Acre',
							'AL' => 'Alagoas',
							'AP' => 'Amapá',
							'AM' => 'Amazonas',
							'BA' => 'Bahia',
							'CE' => 'Ceará',
							'DF' => 'Distrito Federal',
							'ES' => 'Espírito Santo',
							'GO' => 'Goiás',
							'MA' => 'Maranhão',
							'MT' => 'Mato Grosso',
							'MS' => 'Mato Grosso do Sul',
							'MG' => 'Minas Gerais',
							'PA' => 'Pará',
							'PB' => 'Paraíba',
							'PR' => 'Paraná',
							'PE' => 'Pernambuco',
							'PI' => 'Piauí',
							'RJ' => 'Rio de Janeiro',
							'RN' => 'Rio Grande do Norte',
							'RS' => 'Rio Grande do Sul',
							'RO' => 'Rondonia',
							'RR' => 'Roraima',
							'SC' => 'Santa Catarina',
							'SP' => 'São Paulo',
							'SE' => 'Sergipe',
							'TO' => 'Tocantins');
					?>
					<div class="row-fluid">
						<div class="span4">
							<label>Bairro</label>
							<?=$form->input('FormandoProfile.end_bairro',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<div class="span4">
							<label>Cidade</label>
							<?=$form->input('FormandoProfile.end_cidade',
									array('label' => false, 'disabled' => 'disabled', 'div' => 'input-control text','error' => false,
										'after' => '<button class="helper" onclick="return false" tabindex="-1" type="button"></button>')); ?>
						</div>
						<?=$form->input('FormandoProfile.end_uf',
								array('label' => 'UF', 'disabled' => 'disabled', 'div' => 'input-control text span3','error' => false,
									'type' => 'select', 'options' => $uf, 'class' => 'chosen')); ?>
					</div>
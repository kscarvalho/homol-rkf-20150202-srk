					<div class="row-fluid">
						<div class="span6">
							<h3>Resumo Financeiro</h3>
							<br />
							<table class="table table-condensed table-striped">
								<tbody>
									<tr>
										<td class="header">Data de Contrato da Turma</td>
										<td><?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']));?></td>
									</tr>
									<tr>
										<td class="header">Data de Ades&atilde;o</td>
										<td><?=date('d/m/Y',strtotime($usuario['FormandoProfile']['data_adesao']));?></td>
									</tr>
									<tr>
										<td class="header">Data de IGPM</td>
										<td><?=date('d/m/',strtotime($turma['Turma']['data_assinatura_contrato'])).date('Y');?></td>
									</tr>
									<tr>
										<td class="header">Mesas de Contrato</td>
										<td><?=$usuario['FormandoProfile']['mesas_contrato']?></td>
									</tr>
									<tr>
										<td class="header">Convites de Contrato</td>
										<td><?=$usuario['FormandoProfile']['convites_contrato']?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
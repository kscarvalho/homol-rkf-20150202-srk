<style type="text/css">
#mapa { border-collapse:collapse;
    background-image: url("/<?=$mapa['EventoMapa']['arquivo']?>");
    background-repeat:no-repeat;
    background-size: 100% auto;
}
#mapa tr td { border: solid 1px #333333; background:rgba(255,255,255,.4); cursor:pointer; }
#mapa tr td.selecionavel { background:none!important; }
</style>
<script type="text/javascript">
(function($) {
    var isMouseDown = false;
    $("#mapa td")
        .mousedown(function () {
            isMouseDown = true;
            verificarSelecionados(this);
            return false;
        })
        .mouseover(function () {
            if (isMouseDown) {
                verificarSelecionados(this);
        }
    });
    function verificarSelecionados(el) {
        $(el).toggleClass("selecionavel");
        if($(".selecionavel:not(.inserido)").length > 0 ||
                $(".inserido:not(.selecionavel)").length > 0)
            $("#enviar-locais").fadeIn(500);
        else
            $("#enviar-locais").fadeOut(500);
    };
    
    $(document).mouseup(function () {
        isMouseDown = false;
    });
    $(document).ready(function() {
        $("#enviar-locais").click(function() {
            var mapaId = $("#mapa").data('id');
            var dados = {
                data : {
                    inserir : [],
                    remover : []
                }
            };
            $(".selecionavel:not(.inserido)").each(function() {
                dados.data.inserir.push({
                    EventoMapaLocal : {
                        evento_mapa_id : mapaId,
                        evento_mapa_linha_id : $(this).parent().data('id'),
                        evento_mapa_coluna_id : $(this).data('id'),
                        usuario_id : '<?=$usuario['Usuario']['id']?>'
                    }
                });
            });
            $(".inserido:not(.selecionavel)").each(function() {
                dados.data.remover.push($(this).data('id'));
            });
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $.ajax({
                    url : '<?=$this->here?>',
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function(response) {
                        context.$data.reload();
                    }
                });
            });
        });
    });
})(jQuery);
</script>
<div class="row-fluid">
    <div class="span12">
        <h2 style="width:<?=$mapa['EventoMapa']['largura']?>px">
            <a class="metro-button reload" data-bind="click: reload"></a>
            Configura&ccedil;&atilde;o do Mapa
            <button type="button" class="default pull-right hide" id="enviar-locais">
                Atualizar
            </button>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<table id="mapa" data-id="<?=$mapa['EventoMapa']['id']?>"
    style="width:<?=$mapa['EventoMapa']['largura']?>px">
    <tbody>
        <?php foreach($linhas as $linha) : ?>
        <tr data-id="<?=$linha['EventoMapaLinha']['id']?>"
            style="height:<?=$linha['EventoMapaLinha']['altura']?>px">
            <?php foreach($colunas as $coluna) : ?>
            <?php if(isset($locais[$linha['EventoMapaLinha']['id']][$coluna['EventoMapaColuna']['id']])) : ?>
            <td data-id="<?=$locais[$linha['EventoMapaLinha']['id']][$coluna['EventoMapaColuna']['id']]?>"
                style="width:<?=$coluna['EventoMapaColuna']['largura']?>px"
                class="inserido selecionavel"></td>
            <?php else : ?>
            <td data-id="<?=$coluna['EventoMapaColuna']['id']?>"
                style="width:<?=$coluna['EventoMapaColuna']['largura']?>px"></td>
            <?php endif; ?>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

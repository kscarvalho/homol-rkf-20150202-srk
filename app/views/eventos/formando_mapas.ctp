<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var context = ko.contextFor($("#content-body")[0]);
    $('#eventos').selectpicker();
    $("#eventos").change(function() {
        if($(this).val() != "")
            $("#escolher-festa").fadeIn(500);
        else
            $("#escolher-festa").fadeOut(500);
    });
    $("#escolher-festa").click(function() {
        var festa = $("#eventos").val();
        var url = '/<?=$this->params['prefix']?>/eventos/mapa/'+festa;
        context.$data.page(url);
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Selecionar Festa
    </h2>
</div>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input(false,array(
            'type' => 'select',
            'options' => $eventos,
            'empty' => 'Selecione uma festa',
            'class' => 'selectpicker',
            'id' => 'eventos',
            'data-width' => '100%',
            'label' => false,
            'div' => 'input-control text'
        )); ?>
    </div>
    <div class="span3">
        <button type="button" class="default input-block-level hide"
            id="escolher-festa">
            Escolher Local na Festa
        </button>
    </div>
</div>
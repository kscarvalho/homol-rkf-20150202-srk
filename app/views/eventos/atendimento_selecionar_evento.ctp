<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker({width:'100%'});
    $(".enviar").click(function(){
        evento = $('.selectpicker option:selected').val();
        uid = '/' + '<?=$uid?>';
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $.ajax({
                complete: function() {
                    context.$data.page('atendimento/eventos/mapa/' + evento + uid);
                }
            });
        });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Selecione o Evento 
    </h2>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Evento.nome', array(
            'options' => array($eventos),
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Eventos',
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <button type='submit' class='button max bg-color-greenDark enviar'>
            Enviar
            <i class='icon-ok'></i>
        </button>
    </div>
</div>
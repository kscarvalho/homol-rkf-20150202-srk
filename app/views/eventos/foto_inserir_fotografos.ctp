<script type="text/javascript">
    $(document).ready(function(){
        var salvar = $("<button>",{
            type: 'submit',
            class: 'button bg-color-greenDark',
            id: 'salvar',
            text: 'Salvar'
        });
        $('.modal-footer').prepend(salvar);
        $('.button.default').addClass('bg-color-red');
        $('.button.default').text('Sair');
        $("#salvar").click(function(e) {
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var dados = $("#form").serialize();
            var url = $("#form").attr('action');
            bootbox.hideAll(); 
            context.$data.showLoading(function() {
                $.ajax({
                    url: url,
                    data: dados,
                    type: "POST",
                    dataType: "json",
                    complete: function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
</script>
<div class="row-fluid">
    <?php $session->flash(); ?>
    <?php echo $form->create('Evento', array('url' => "/foto/eventos/inserir_fotografos/" . $this->data['Evento']['id'], 'id' => 'form')); ?>
    <div class="row-fluid">
        <div class="span12">
            <label>Informações Fotógrafos</label>
            <?php echo $form->input("Evento.informacoes_fotografos", array('class' => 'input-control text', 'label' => false, 
                 'div' => false, 'error' => false, 'style' => 'width: 865px'));?>
        </div>
    </div>
</div>
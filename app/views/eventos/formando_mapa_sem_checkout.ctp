<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
    <meta charset="utf-8">
</head>
<body>  
<?php $session->flash(); ?>
<div class="row-fluid">
    <h1>
        Olá, infelizmente ainda não é o momento para essa operação.
    </h1>
    <h2>
        Favor entre em contato com atendimento e verifique as suas informações de checkout.
    </h2>
</div>
</body>
</html>

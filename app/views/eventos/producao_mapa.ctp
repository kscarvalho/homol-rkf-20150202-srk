<?php include('_mapa.ctp'); ?>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/construtor-1.0.0.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        /**
         *  Funções de manipulação de paineis
         */
        
        function habilitarPainelCriarGrupo() {
            $("#containerCriarGrupo").show();//removeClass("hide");
        }

        function desabilitarPainelCriarGrupo() {
            $("#containerCriarGrupo").hide();//addClass("hide");
        }

        function habilitarPainel(){
            $("#containerPainel").fadeTo( "fast", 1 ); 
        }

        function desabilitarPainel(){
            $("#containerPainel").hide(); 
        }

        function habilitarRotacionar (){
            $("#containerPainel").fadeTo( "fast", 1 ); 
            $("#containerPainel button").prop( "disabled", false );  
        }

        function desabilitarRotacionar () {
            $("#containerPainel").hide(); 
            $("#containerPainel button").prop( "disabled", true );     
        }
       
       /**
        * Inicialização de Ferramenta de Mapa de Mesas
        */
       
        palco = new ConstrutorLayout.Palco("meucanvas");
        palco.desenharBackground();

        palco.canvas.keydown(function(event){
            if(event.keyCode == 16){
                palco.pressionandoShift = true;
            }else{
                if(palco.pressionandoShift && palco.objetoSelecionado != undefined){
                    if(palco.objetoSelecionado.tipo == "grupo_mesas"){
                        palco.objetoSelecionado.keyPressed(event.keyCode);
                        palco.objetoSelecionado.desenhar();
                    }
                }
            }
        });

        palco.canvas.keyup(function(event){
            if(event.keyCode == 16){
                palco.pressionandoShift = false;
            }
        });

        palco.canvas.focus();
        
        params = <?php echo $mapa_parametros ?>;
        MapaDeMesa.MesaFactory = MapaDeMesa.ConstrutorLayout.MesaFactory;
        fabrica = new MapaDeMesa.MesaFactory(palco,params.raio,params.distancia);

        fabricaRetangulo = new ConstrutorLayout.RectangleFactory(palco);
        
        layout = new ConstrutorLayout.GerenciadorLayout(palco,fabrica,fabricaRetangulo);
        layout_json = '<?php echo isset($mapaLayout) ? $mapaLayout : ''; ?>';

        layout.desserializarLayout(layout_json,fabrica);

        $("#containerCriarGrupo .raioMesa").val(MapaDeMesa.RAIO_MESAS);
        $("#containerCriarGrupo .distanciaMesa").val(MapaDeMesa.DISTANCIA_MESAS);

        //botao que habilita a div de linhas e colunas
        $("#criarPainelHandler").click(function() {
            $("#editarObjetosHandler").addClass('hide');
            $("#criarObjetosHandler").removeClass('hide');
            $("#containerCriarGrupo").show();
            $("#containerPainel").hide();
        });

        $("#criarRetanguloHandler").click(function() {
            $("#editarObjetoRetanguloHandler").addClass('hide');
            $("#criarObjetoRetanguloHandler").removeClass('hide');
            $("#containerCriarRetangulo").show();
            $("#containerPainel").hide();
        });

        $("#cancelarObjetosHandler").click(function(){
            $("#containerCriarGrupo").hide();
            $("#containerPainel").show();
        });

        $("#cancelarObjetoRetanguloHandler").click(function(){
            $("#containerCriarRetangulo").hide();
            $("#containerPainel").show();
        });

        $("#mapearReferenciaCheckbox").change(function(){
            palco.ajustarMapearReferencia($(this).is(':checked'));
        });

        $("#editarPainelHandler").click(function(){
            if(palco.objetoSelecionado.tipo == "grupo_mesas"){
                $("#containerCriarGrupo .linha").val(palco.objetoSelecionado.linhas);
                $("#containerCriarGrupo .coluna").val(palco.objetoSelecionado.colunas);
                $("#containerCriarGrupo .raioMesa").val(palco.objetoSelecionado.raioMesas);
                $("#containerCriarGrupo .distanciaMesa").val(palco.objetoSelecionado.distanciaMesas);
                $("#containerCriarGrupo .tipoReferencia").val(palco.objetoSelecionado.tipoReferencia);
                $("#containerCriarGrupo .setor").val(palco.objetoSelecionado.setor);

                $("#criarObjetosHandler").addClass('hide');
                $("#editarObjetosHandler").removeClass('hide');
                $("#containerCriarGrupo").show();
            }else if(palco.objetoSelecionado.tipo == "retangulo"){
                $("#containerCriarRetangulo #texto").val(palco.objetoSelecionado.texto);
                document.getElementById('corPreenchimento').color.fromString(palco.objetoSelecionado.corPreenchimento.replace('#',''));
                document.getElementById('corTexto').color.fromString(palco.objetoSelecionado.corTexto.replace('#',''));

                $("#criarObjetoRetanguloHandler").addClass('hide');
                $("#editarObjetoRetanguloHandler").removeClass('hide');
                $("#containerCriarRetangulo").show();
            }
            $("#containerPainel").hide();
        });
        
        $("#criarObjetosHandler").click(function(){

            var linha = $("#containerCriarGrupo .linha").val();
            var coluna = $("#containerCriarGrupo .coluna").val();
            var raioMesa = $("#containerCriarGrupo .raioMesa").val();
            var distanciaMesa = $("#containerCriarGrupo .distanciaMesa").val();
            var tipoReferencia = $("#containerCriarGrupo .tipoReferencia").val();
            var setor = $("#containerCriarGrupo .setor").val();

            var gruposMesas = palco.retornarObjetosPorTipo("grupo_mesas");
            $.each(gruposMesas, function(i,v){
                v.ajustarRaioMesas(raioMesa).ajustarDistanciaMesas(distanciaMesa).ajustarTipoReferencia(tipoReferencia).desenhar();
            });

            var objeto_grupo = {
                'linhas': linha,
                'colunas': coluna,
                'raioMesas': raioMesa,
                'distanciaMesas': distanciaMesa,
                'tipoReferencia': tipoReferencia,
                'setor': setor
            };

            var grupo = fabrica.criarGrupoMesa (objeto_grupo);
            palco.adicionarObjeto(grupo);
            palco.mapearGrupoMesas();

            $("#containerCriarGrupo").hide();

            habilitarPainel();
        })

        $("#editarObjetosHandler").click(function(){
            var linha = $("#containerCriarGrupo .linha").val();
            var coluna = $("#containerCriarGrupo .coluna").val();
            var raioMesa = $("#containerCriarGrupo .raioMesa").val();
            var distanciaMesa = $("#containerCriarGrupo .distanciaMesa").val();
            var tipoReferencia = $("#containerCriarGrupo .tipoReferencia").val();
            var setor = $("#containerCriarGrupo .setor").val();

            var objeto_grupo = {
                'linhas': linha,
                'colunas': coluna,
                'raioMesas': raioMesa,
                'distanciaMesas': distanciaMesa,
                'tipoReferencia': tipoReferencia,
                'setor': setor
            };

            fabrica.editarGrupoMesa (objeto_grupo);
            palco.mapearGrupoMesas();
            palco.canvas.drawLayers();

            $("#containerCriarGrupo").hide();

            habilitarPainel();
        });

        $("#criarObjetoRetanguloHandler").click(function(){

            var texto = $("#containerCriarRetangulo #texto").val();
            var corPreenchimento = "#" + $("#containerCriarRetangulo #corPreenchimento").val();
            var corTexto = "#" + $("#containerCriarRetangulo #corTexto").val();

            var retangulo = fabricaRetangulo.criarRetangulo (texto, corPreenchimento, corTexto);
            palco.adicionarObjeto(retangulo);

            $("#containerCriarRetangulo").hide();

            //habilita rotação
            desabilitarPainelCriarGrupo();
            habilitarRotacionar();
        })

        $("#editarObjetoRetanguloHandler").click(function(){
            var texto = $("#containerCriarRetangulo #texto").val();
            var corPreenchimento = "#" + $("#containerCriarRetangulo #corPreenchimento").val();
            var corTexto = "#" + $("#containerCriarRetangulo #corTexto").val();

            fabricaRetangulo.editarRetangulo(texto, corPreenchimento, corTexto);

            $("#containerCriarRetangulo").hide();
            desabilitarPainelCriarGrupo();
            habilitarPainel();
        });

        $("#mesaConfigToggle").click(function(){
            $("#mesaConfig").toggle();
        })

        $("input#salvarHandler").click(function() {
            var json = layout.serializarLayout();    
            $('#mapaLayout').val(json);
            $.ajax ({
                //prefixo, controller, action e parametro,
                url : '/producao/eventos/salvar_layout/',
                data : {
                    MapaLayout : json,
                    TurmaMapaID : $("#turmaMapaID").val()
                },
                type : "POST",
                complete: function(response) { 
                        $('#conteudo').html( response.responseText);
                        alert('Layout salvo com sucesso!');
                }
            });
        });
        

        $("#containerPainel #avancar").click(function() {
            rotacaoAtual = palco.objetoSelecionado.rotacao;   
            palco.objetoSelecionado.rotacionar(rotacaoAtual+15).desenhar();

        });

        $("#containerPainel #voltar").click(function() {
            rotacaoAtual = palco.objetoSelecionado.rotacao;
            palco.objetoSelecionado.rotacionar(rotacaoAtual-15).desenhar();

        });

        $("#containerPainel #apagarPainelHandler").click(function() {
            palco.removerGrupoSelecionado();
        });

        $("#configurar-mapa").click(function() {
            dir = $(this).attr('dir');
            var context = ko.contextFor($("#content-body")[0]);
        
            context.$data.showLoading(function() {

                var url = '/<?=$this->params['prefix']?>/eventos/mapa_selecionar_locais/' + dir;
                if($("#src").val() == '') {
                    context.$data.page(url);
                } else {
                    var dados = {
                        src : $("#src").val()
                    };
                  $.ajax ({
                        //prefixo, controller, action e parametro
                        url :    'planejamento/eventos/salvar_layout/',
                        
                        data : {
                            MapaLayout : $("#mapalayout").val(),
                            TurmaMapaID : $("#turmamapaID").val()
                        },
                        type : "POST",
                        complete: function(response) { 
                                $('#conteudo').html( response.responseText);
                        }
                    });

                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datetimepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id', array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>


<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Local</label>
	<?php echo $form->input('local', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Endereço do Local</label>
	<?php echo $form->input('local_endereco', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Mapa do Local</label>
	<?php echo $form->input('local_mapa', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Convites Formandos</label>
	<?php echo $form->input('convites_formandos', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Convites Extras</label>
	<?php echo $form->input('convites_extras', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Data</label>
	<?php echo $form->input('data-hora', array( 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker')); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Duração</label>
	<?php echo $form->input('duracao', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Traje</label>
	<?php echo $form->input('traje', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Banda</label>
	<?php echo $form->input('banda', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Atrações</label>
	<?php echo $form->input('atracoes', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Buffet</label>
	<?php echo $form->input('buffet', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Bar</label>
	<?php echo $form->input('bar', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Brindes</label>
	<?php echo $form->input('brindes', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Outras Informações</label>
	<?php echo $form->input('outras_informacoes', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>



<?php include("_form_" . $tipoEvento['TiposEvento']['template'] . ".ctp"); ?>
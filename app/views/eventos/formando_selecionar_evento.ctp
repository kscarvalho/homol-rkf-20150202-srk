<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker({width:'100%'});
    $(".enviar").click(function(){
        //pega o value (id)
        valor = $('.selectpicker option:selected').val();
        var evento_id = $("select[name='data[Evento][id]']").val();
        $('#conteudo').html('<div class="row-fluid content-loader" id="content-loading"><div class="cycle-loader"></div></div>');
        $.ajax ({
            //prefixo, controller, action e parametro
            url : '/formando/eventos/mapa/' + valor,
            data : {
                Evento : evento_id
            },
            type : "POST",
            complete: function(response) { 
                $('#conteudo').html(response.responseText );
            }
        });
    });
});
</script>
<?php if($data_liberacao && strtotime($data_liberacao) < time()) : ?>
    <?php $session->flash(); ?>
    <div class="row-fluid">
        <h2>
            Selecione o Evento
        </h2>
        <br/>
        <h5> 
            Favor selecionar o evento em que será feita a escolha das mesas.
        </h5>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <?=$form->input('Evento.nome', array(
                'options' => array($eventos),
                'type' => 'select',
                'class' => 'selectpicker',
                'label' => 'Eventos',
                'div' => 'input-control text')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span2">
            <button type='submit' class='button max bg-color-greenDark enviar'>
                Selecionar a mesa
                <i class='icon-ok'></i>
            </button>
        </div>
    </div>
<?php else : ?>
    <?php $session->flash(); ?>
    <div class="row-fluid">
        <?php if($data_liberacao) : ?>
            <h2>Olá! Poderá Selecionar a mesa no dia <?php echo Date('d/m/Y \a\s H:i',strtotime($data_liberacao))?></h2><BR>
            <small class="fg-color-gray"><span class="fg-color-red">Observação:</span> O horário está sujeito à alterações. Comunicaremos através de informativos. <BR>
            Para maiores informações entre em contato com o atendimento.</small>
        <?php else : ?>
            <h2>Olá! Ainda não é o momento para utilizar esse recurso.</h2>
        <?php endif; ?>
    </div>
<?php endif; ?>
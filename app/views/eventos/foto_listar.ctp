<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<script type="text/javascript">
$(document).ready(function() {
    $('.fotografos').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/eventos/inserir_fotografos/"?>'+
                    $(this).attr('dir')
        });
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: function() { 
            reload() }"></a>
            Eventos <?=$turma['Turma']['id']?>
    </h2>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if(!empty($eventos)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%">Id</th>
                <th scope="col" width="20%">Nome</th>				
                <th scope="col" width="20%">Evento</th>
                <th scope="col" width="20%">Local</th>
                <th scope="col" width="20%">Data</th>
                <th scope="col" width="10%">&nbsp</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($eventos as $evento): ?>
            <tr>
                <td colspan="1"><?php echo $evento['Turma']['id'];?></td>
                <td colspan="1"><?php echo $evento['Turma']['nome'];?></td>
                <td colspan="1"><?php echo $evento['TiposEvento']['nome']?></td>
                <td colspan="1"><?php echo $evento['Evento']['local']?></td>
                <td><?=($evento['Evento']['data'] > 0) ? date('d/m/Y', strtotime($evento['Evento']['data'])) : "<em class='fg-color-red'>Não Cadastrado</em>"?></td>
                <td colspan="1" style="text-align: center">
                    <button class="mini fotografos bg-color-greenDark" type="button" dir="<?=$evento['Evento']['id']?>">
                        Fotógrafos<i class="icon-user"></i>
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum Evento Cadastrado Para Esta Turma.</h2>
    <?php endif; ?>
</div>
<?php $session->flash(); ?>
<?php if($evento) : ?>
	<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jcanvas.min.js"></script>
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/main.js"></script>
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/agendador-mesas-1.0.0.js"></script>
</script>
<style type="text/css">
	.menu {
		position:fixed;
		background: white;
		right:15px;
		top:60px;
		padding: 5px;
	}
	.info-formando table { 
		box-shadow: 5px 3px 4px #D0D0D0;
		border: #B2B2B2 solid 1px;
	}
	.info-formando table td { 
		padding: 5px;
	}
</style>
	<?php if(!empty($formado_mapa_horario) && strtotime($formado_mapa_horario['FormandoMapaHorario']['data_liberacao']) < time() && ($formado_mapa_horario['FormandoMapaHorario']['status'] == 1 || $usuarioReal['Usuario']['grupo'] == 'planejamento')) : ?>
		
		<img id="mapa_imagem" src="<?php echo $webroot.$mapa_src ?>" class="hide"  />
		<h2 class="fg-color-red">
			<?="{$evento['TiposEvento']['nome']} - {$evento['Evento']['nome']}"?>
		</h2>
		<br />
		<?php if(isset($sucesso)): ?>
			<h2 class="fg-color-red">
				<?php if(!$sucesso): ?>
					<?php echo 'A mesa selecionada já foi escolhida.'; ?>
				<?php else: ?>
					<?php echo 'Mesa(s) escolhida(s)!'; ?>
				<?php endif; ?>
			</h2>
		<?php endif; ?>
		<?=$form->hidden(false,array('id' => 'src'));?>
		<div class="row-fluid striped">
			<input type="hidden" id="turmaMapaID" name="turmaMapaID" value="<?php echo $turmaMapa; ?>" />
			<div class="row-fluid"id="menu">
				<div class="span9 pull-left">
					<canvas width="964" id="meucanvas" height="792">
					</canvas>
				</div>
				<div class="menu info-formando">
					<h3>Informações da Mesa</h3>
					<div class="mesa-info">
						<p></p>
					</div>
					<table class="table-striped sombra" >
						<tr>
							<td>Mesas de Contrato</td>
							<td><?=$mesas['contrato']?> mesa (s)</td>
						</tr>
						<tr>
							<td>Mesas de Campanha</td>
							<td><?=$mesas['campanhas']?> mesa (s)</td>
						</tr>
						<tr>
							<td>Mesas de Checkout</td>
							<td><?=$mesas['checkout']?> mesa (s)</td>
						</tr>
					</table>
					<br/>
					<p>
						<button id="salvarPosicao" class="bg-color-greenDark" type="button">
							Salvar
						</button>
					</p>
					<br/>
					<h4 id="exibir-legenda"><a href="javascript:void(0)">Legenda</a></h4>
					<div id='legenda' >
						<table class="table-striped sombra">
							<tr>
								<td align="center"><img src="../../img/green_button.png"></td>
								<td>Mesa Livre</td>
							</tr>
							<tr>
								<td align="center"><img src="../../img/red_button.png"></td>
								<td>Sua(s) mesa(s)</td>
							</tr>
							<tr>
								<td align="center"><img src="../../img/yellow_button.png"></td>
								<td>Mesa Reservada</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	<?php else: ?>
	    <div class="row-fluid" style="margin-top: 60px;">
			<?php if(!empty($formado_mapa_horario) && $formado_mapa_horario['FormandoMapaHorario']['data_liberacao'] && $formado_mapa_horario['FormandoMapaHorario']['status'] == 1) : ?>
				<h2 class="fg-color-red">
	                Olá! Poderá Selecionar a mesa no dia <?php echo Date('d/m/Y \a\s H:i',strtotime($formado_mapa_horario['FormandoMapaHorario']['data_liberacao']))?>
				</h2>
				<small class="fg-color-gray">
					<span class="fg-color-red">Observação:</span> O horário está sujeito à alterações. Comunicaremos através de informativos. <BR>
	            	Para maiores informações entre em contato com o atendimento.
            	</small>
			<?php elseif(isset($formado_mapa_horario['FormandoMapaHorario']['status']) && $formado_mapa_horario['FormandoMapaHorario']['status'] == 0): ?>
				<h2 class="fg-color-red">
					Olá! A seleção de mesa para esse evento já foi encerrada.
				</h2>
            <?php else : ?>
				<h2 class="fg-color-red">
	                Olá! Ainda não é o momento para utilizar esse recurso.
				</h2>
            <?php endif; ?>
		</div>
	<?php endif ?>
<?php else : ?>
	<h2 class="fg-color-red">Nenhuma festa encontrada para esta turma</h2>
<?php endif; ?>
	<script type="text/javascript">
		/**
		 * Função de Callback chamada ao passar mouse em cima de uma mesa
		 * @param  {[type]} mesa Objeto Mesa
		 * @return {[type]}      [description]
		 */
		 function mostrarInfoMesa(mesa) {
		 	$('.mesa-info p').html('Mesa '+mesa.formaMesa.letra+mesa.formaMesa.numero+' - ' + mesa.texto);
		 }
		/**
		* Inicialização de Ferramenta de Mapa de Mesas
		*/
		MapaDeMesa.QUANTIDADE_MESAS = <?php echo $mesas['campanhas'] + $mesas['checkout'] + $mesas['contrato']?>;
		MapaDeMesa.ID_FORMANDO   = '<?php echo $usuario['ViewFormandos']['codigo_formando'];?>';
		MapaDeMesa.NOME_FORMANDO = "<?php echo $usuario['Usuario']['nome'];?>";
		MapaDeMesa.SETOR_FORMANDO = "<?php echo utf8_encode($formado_mapa_horario['FormandoMapaHorario']['setor']); ?>";

	    palco = new AgendadorMesas.Palco("meucanvas");
	    palco.desenharBackground();

	    MapaDeMesa.MesaFactory = MapaDeMesa.AgendadorMesas.MesaFactory;
	    
	    params = <?php echo $mapa_parametros ?>;
	    fabrica = new MapaDeMesa.MesaFactory(palco,params.raio,params.distancia);

	    fabricaRetangulo = new AgendadorMesas.RectangleFactory(palco);

	    palco.aoPassarEmCimaMesaListener(mostrarInfoMesa);

		layout = new AgendadorMesas.GerenciadorLayout(palco);
		layout_json = '<?php echo isset($mapaLayout) ? $mapaLayout : ''; ?>';
		mesasSelecionadas_json = '<?php echo isset($mesasSelecionadas) ? $mesasSelecionadas : ''; ?>';

	    layout.desserializarLayout(layout_json, fabrica,fabricaRetangulo);
	    palco.desserializarEscolhidas(mesasSelecionadas_json);

		$("#salvarPosicao").click(function() {
			var json = palco.serializarEscolhidas();
			var turma_mapa_id = $("#turmaMapaID").val();
			$('#conteudo').html('<div class="row-fluid content-loader" id="content-loading"><div class="cycle-loader"></div></div>');
			$.ajax ({
				url : '/formando/eventos/salvarMesas/',
				data : {
					mesasEscolhidas : json,
					TurmaMapaID : turma_mapa_id
				},
				type : "POST",
				complete: function(response) { 
					$('#conteudo').html(response.responseText);
				}
			});
		});

		$("#exibir-legenda").click(function(){
			$("#legenda").toggle();
		});
	</script>
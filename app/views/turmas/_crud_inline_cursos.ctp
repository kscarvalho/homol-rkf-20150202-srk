<? //debug($cursosRelacionados); ?>
<ul id="adicionar-curso-inline">
	<?php foreach ($cursosRelacionados as $curso): ?>
		<li>
			<a class="remover-inline"></a>
			<?php echo $curso['Curso']['nome'] . ", turno: " . 
					$curso['CursoTurma']['turno'] . " (" . 
					$curso['Faculdade']['nome'] . ", " .
					$curso['Universidade']['nome'] . ")" ?>
			<span class="curso_id" style="display:none"><?php echo $curso['CursoTurma']['curso_id']?></span>
			<span class="turno" style="display:none"><?php echo $curso['CursoTurma']['turno']?></span>
		</li>
	<?php endforeach; ?>
</ul>

<?php echo $form->input('adicionar_cursos_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'adicionar_cursos_array')); ?>
<?php echo $form->input('remover_cursos_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'remover_cursos_array'));?>
<?php
	$universidades = array();
	foreach ($arvoreCursos as $universidadeId => $arrayFaculdades) {
		$universidades[$universidadeId] = $arrayFaculdades['nome'];	
	}
?>

<div class="adicionar-inline-form" id="adicionar-inline-form-curso" style="display:none;">
	<p class="grid_11 alpha omega">
	<label>Selecione a Universidade</label>
	<?php echo $form->select('universidade_id', $universidades, null, 
					array('class' => 'grid_11 alpha omega', 'label' => false, 'div' => false, 
					'onchange' => 'universidadeMudou()', 'empty' => '-- Selecione uma universidade --')); ?>
	</p>
	<p class="grid_11 alpha omega">
	<label>Selecione a Faculdade</label>
	<?php echo $form->select('faculdade_id', array(), null, 
					array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 
					'onchange' => 'faculdadeMudou()', 'empty' => '-- Selecione uma faculdade --')); ?>
	</p>
	<p class="grid_11 alpha omega">
	<label>Selecione o Curso</label>
	<?php echo $form->select('curso_id', array(), null, 
					array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 
					'empty' => '-- Selecione um curso --')); ?>
	</p>
	<p class="grid_11 alpha omega">
	<label>Selecione o Periodo do Curso</label>
	<?php echo $form->select('turno', $periodo, null, 
					array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 
					'empty' => '-- Selecione um período --')); ?>
	</p>
</div>
<p class="grid_11 alpha omega">
<a class="adicionar-inline" id="adicionar-inline-curso">Adicionar Curso</a>
<a class="esconder-inline" id="esconder-inline-curso" style="display:none;">Esconder</a>
</p>
<?php //debug($cursosRelacionados); ?>
<?php //debug($this->data); ?>
<select id="selectDeListaDeTodasFaculdades" style="display:none;"></select>
<select id="selectDeListaDeTodosCursos" style="display:none;"></select>
<script type="text/javascript">
var arvoreCursos = <?php echo $javascript->Object($arvoreCursos); ?>;
var uE = '';
var fE = '';
var selectsInvisiveis = true;

$(document).ready(function(){
	// Zera os forms do crud inline de cursos
	initializeForm();
	
	// Quando passar o mouse encima do link de deletar, o item da lista inteiro deve ser realcado
	$('li a.remover-inline').hover(function(){
		$(this).parent().addClass('hover');
	}, function(){
		$(this).parent().removeClass('hover');
	})
	
	// Quando clicar em remover, insere uma acao em um campo escondido para que seja deletado pelo controller
	$('li a.remover-inline').click(function(){
		removerInline($(this));

	});
	
	// O link de adicionar pode ser clicado tanto para abrir o box 
	// quanto para adicionar um novo curso à turma
	$('#adicionar-inline-curso').click(function(){
		
		if(selectsInvisiveis){
			// Mostra os selects de um curso e tambem o botao de esconde-los novamente
			$('#adicionar-inline-form-curso').show("slow");
			$('#esconder-inline-curso').show();
			selectsInvisiveis = false;
		} else {
			// Adiciona um curso ao input escondido, se este já não estiver sendo exibido
			var cE = $('#TurmaCursoId').val();
			var turno = $('#TurmaTurno').val();
			
			// Verifica se esta duplicado
			$('ul#adicionar-curso-inline li span.curso_id').each(function(){
				var turnoCadastrado = $(this).parent().find('span.turno').html();
				if(cE != '' && cE == $(this).html() && turno == turnoCadastrado) {
					erroInsercaoCursoDuplicado($(this).parent());
					cE = '';
				}
			});
			// Insere no input escondido e na listagem sendo mostrada
			if(cE != '') {
				oldValue = $('#adicionar_cursos_array').val();
				if(oldValue == ''){
					$('#adicionar_cursos_array').val('{curso_id:' + cE + ', turno:' + turno +'}');
				} else {
					$('#adicionar_cursos_array').val(oldValue + '{curso_id:' + cE + ', turno:' + turno +'}');
				}
				$('ul#adicionar-curso-inline').append('<li><a class="remover-inline"></a>' + "\n" +
				 				arvoreCursos[uE][fE][cE]['nome'] + ', ' + 
								'turno: ' + turno + ' (' + 
								arvoreCursos[uE][fE]['nome'] + ', ' + 
								arvoreCursos[uE]['nome'] + ')' + "\n" +
								'<span class="curso_id" style="display:none">' + 
								arvoreCursos[uE][fE][cE]['id'] + '</span>' +
								'<span class="turno" style="display:none">' +
								turno + '</span></li>');
				// Se clicar no input deste li deve ser removido
				$('ul#adicionar-curso-inline li a.remover-inline:last').click(function() {
					removerInline($(this));
				});
			}
		}
	});
	
	$('#esconder-inline-curso').click(function(){
		$('#adicionar-inline-form-curso').hide("slow");
		$(this).hide();
		selectsInvisiveis = true;
	});

	
});

function removerInline(objeto){
	if(confirm("Voce está prestes a deletar um curso, está certo disso?")) {
		var curso = $(objeto).parent().find('span.curso_id').html();
		var turno = $(objeto).parent().find('span.turno').html();
		if($('#remover_cursos_array').val() == '')
			$('#remover_cursos_array').val('{curso_id:' + curso + ', turno:' + turno +'}');
		else {
			var oldValue = $('#remover_cursos_array').val();
			$('#remover_cursos_array').val(oldValue + ',{curso_id:' + curso + ', turno:' + turno +'}');
		}
		$(objeto).parent().remove();
	}
	
}

function initializeForm() {
	$('#TurmaUniversidadeId').attr('value', '');
	$('#TurmaFaculdadeId').append('<option value=""></option>').attr('value', '');
	$('#TurmaCursoId').append('<option value=""></option>').attr('value', '');
	$('#TurmaTurno').attr('value', '')
	$('#remover_cursos_array').val('');
	$('#adicionar_cursos_array').val('');
}

var objetoRealcado;
function erroInsercaoCursoDuplicado(objeto) {
	var intervalo = 100;
	
	objetoRealcado = objeto
	
	realcarObjeto();
	setTimeout('removerRealce();', intervalo);
	setTimeout('realcarObjeto();', intervalo*2);
	setTimeout('removerRealce();', intervalo*3);
	setTimeout('realcarObjeto();', intervalo*4);
	setTimeout('removerRealce();', intervalo*5);
	setTimeout('realcarObjeto();', intervalo*6);	
	setTimeout('removerRealce();', intervalo*7);
	setTimeout('realcarObjeto();', intervalo*8);	
	setTimeout('removerRealce();', intervalo*9);
	setTimeout('realcarObjeto();', intervalo*10);	
	setTimeout('removerRealce();', intervalo*11);
	
}

function removerRealce() {
	$(objetoRealcado).css('color', '').css('font-weight','');
}
function realcarObjeto() {
	$(objetoRealcado).css('color', '#B00000');//.css('font-weight','bold');
}


function universidadeMudou() {
	uE = $('#TurmaUniversidadeId').val(); // Id da Universidade Escolhida
	$('#TurmaFaculdadeId option').remove();
	$('#TurmaFaculdadeId').append('<option value=""></option>').attr('value', '');
	$('#TurmaCursoId option').remove();
	$('#TurmaCursoId').append('<option value=""></option>').attr('value', '');
	if(uE != "")
		$.each(arvoreCursos[uE], function(i, v) {
			if(parseInt(i) == i) { // Testar se é numérico
				$('#TurmaFaculdadeId').append('<option value="' + 
						arvoreCursos[uE][i]['id'] + '">'+ 
						arvoreCursos[uE][i]['nome'] + '</option>');
			}
		});
}

function faculdadeMudou() {
	fE = $('#TurmaFaculdadeId').val();
	$('#TurmaCursoId option').remove();
	$('#TurmaCursoId').append('<option value=""></option>').attr('value', '');
	if(fE != '')
		$.each(arvoreCursos[uE][fE], function(i, v) {
			if(parseInt(i) == i) { // Testar se é numérico
				$('#TurmaCursoId').append('<option value="' + 
						arvoreCursos[uE][fE][i]['id'] + '">'+ 
						arvoreCursos[uE][fE][i]['nome'] + '</option>');
			}
		});

}
</script>

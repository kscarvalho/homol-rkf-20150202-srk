<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>
<?php echo $form->input('id',  array('hiddenField' => true)); ?>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Ano da Formatura</label>
	<?php echo $form->input('ano_formatura', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Semestre da Formatura</label>
	<?php echo $form->input('semestre_formatura',  array('options' => $semestre_formaturas, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Nome da Turma</label>
	<?php echo $form->input('nome', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span'))); ?>
</p>

<div class="grid_11 alpha omega crud-inline">
	<label class="grid_0 alpha omega">Cursos Relacionados</label>
	<?php include('_crud_inline_cursos.ctp'); ?>
</div>

<div class="grid_11 alpha omega crud-inline">
	<label class="grid_0 alpha omega">Usuarios Relacionados</label>
	<?php include('_crud_inline_usuarios.ctp'); ?>
</div>


<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Expectativa de Formandos</label>
	<?php echo $form->input('expectativa_formandos', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Expectativa de Fechamento</label>
	<?php echo $form->input('expectativa_fechamento', array('options' => $expectativa_fechamentos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<?php if(!empty($this->data['Turma']['data_assinatura_contrato'])) : ?>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Status</label>
	<?php echo $form->input('Turma.status', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Ades&otilde;es</label>
	<?php echo $form->input('Turma.adesoes', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Checkout</label>
	<?php echo $form->input('Turma.checkout', array('type' => 'select', 'options' => $checkout,'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<?php elseif($this->params['prefix'] == "super") : ?>
<p class="grid_11 alpha omega">
	<strong>A data de assinatura do contrato deve ser preenchida para liberar adesões</strong>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Data de Assinatura do Contrato</label>
	<?php echo $form->input('data-assinatura', array( 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker')); ?>
</p>
<?php endif; ?>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Como Chegou?</label>
	<?php echo $form->input('Turma.como_chegou', array('options' => $como_chegous, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Detalhes de como Chegou</label>
	<?php echo $form->textarea('como_chegou_detalhes', array('class' => 'grid_10 alpha omega', 'cols' => 30, 'rows' => 10, 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Pretensão</label>
	<?php echo $form->input('Turma.pretensao', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>




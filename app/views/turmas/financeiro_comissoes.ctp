<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    .center { text-align: center!important }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $(".pagar").click(function(){
        dir = $(this).attr('dir');
        bootbox.alert("Confirma o pagamento?");
        $('.button').addClass('bg-color-blueDark');
        $('.button').text('Desistir');
        var sim = $("<button>",{
            type: 'submit',
            class: 'button bg-color-greenDark',
            id:'sim',
            text:'Sim'
        });
        $('.modal-footer').prepend(sim);
        $('#sim').click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            var url = '<?="/{$this->params['prefix']}/turmas/pagamento_comissao/"?>' + dir;
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.ajax({
                    url : url,
                    type: "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
});
</script>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() {
               reload() }"></a>
                Comissões Comercial
        </h2>
    </div>
</div>
<?php if (sizeof($comissoes) > 0) : ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="20%"><?=$paginator->sort('Vendedor', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Formandos', 'ReciboComissao.total_formandos',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Valor', 'ReciboComissao.valor',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Data', 'ReciboComissao.data_cadastro',$sortOptions); ?></th>
                <th scope="col" width="15%"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($comissoes as $comissao) :
            ?>
            <tr>
                <td><?=$comissao['Usuario']['nome']?></td>
                <td><?=$comissao['ReciboComissao']['total_formandos']?></td>
                <td>R$ <?=number_format($comissao['ReciboComissao']['valor'], 2, ',', '.');?></td>
                <td><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($comissao['ReciboComissao']['data_cadastro']));?></td>
                <?php if($comissao['ReciboComissao']['comissao_paga'] == 0) : ?>
                <td class="center">
                    <button class="mini bg-color-orangeDark pagar" type="button" dir="<?=$comissao['ReciboComissao']['id']?>">
                        Pagar Comissão
                    </button>
                </td>
                <?php else : ?>
                <td class="center">
                    <span class="fg-color-greenDark">
                        <strong>Pago</strong>
                    </span>
                </td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Comissões')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma comissão a pagar encontrada.</h2>
<?php endif; ?>
</div>
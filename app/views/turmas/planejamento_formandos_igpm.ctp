<div class="row-fluid">
    <h2>
        <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/pendencias/parcelamentos"?>') }">
        </a>
        Formandos Turma <?="{$turma['Turma']['id']} - {$turma['Turma']['nome']}"?>
    </h2>
</div>
<?php
$session->flash();
if(sizeof($formandos) > 0) :
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true,$turma['Turma']['id'])));
    $sortOptions = array('data-bind' => 'click: loadThis'); ?>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%"><?=$paginator->sort('COD', 'codigo_formando',$sortOptions); ?></th>
                <th scope="col" width="30%"><?=$paginator->sort('Nome', 'nome',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Data Adesão', 'data_adesao',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Valor Contrato', 'valor_adesao',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Valor IGPM', 'soma_igpm',$sortOptions); ?></th>
                <th scope="col" width="15%">Contrato + IGPM</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($formandos as $formando): ?>
            <tr>
                <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                <td><?=$formando['ViewFormandos']['nome']?></td>
                <td><?=!empty($formando['ViewFormandos']['data_adesao']) ?
                    date('d/m/Y',strtotime($formando['ViewFormandos']['data_adesao'])) :
                    'Data invalida'?></td>
                <td>R$<?=number_format($formando['ViewFormandos']['valor_adesao'], 2, ',', '.') ?></td>
                <td>R$<?=number_format($formando[0]['soma_igpm'], 2, ',', '.') ?></td>
                <td>R$<?=number_format($formando[0]['soma_igpm']+$formando['ViewFormandos']['valor_adesao'], 2, ',', '.') ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhum Formando Encontrado</h2>
<?php endif; ?>

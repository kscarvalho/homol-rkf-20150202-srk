<script type="text/javascript">
    $(document).ready(function() {
        $("#verificar-site").click(function() {
            $("#resp-site").attr('class','pull-right').text('Aguarde');
            site = $("#site").val();
            if(site == "") {
                $("#resp-site").addClass('fg-color-red').text('Digite o site');
                $("#site").focus();
            } else {
                var url = '/turmas/verificar_disponibilidade_site/'+site;
                $.ajax({
                    url: url,
                    dataType: 'json',
                    success: function(response) {
                        if(response.turmas > 0)
                            $("#resp-site").addClass('fg-color-red')
                                .text('Não Disponível');
                        else
                            $("#resp-site").addClass('fg-color-green')
                                .text('Disponível');
                    },
                    error: function() {
                        $("#resp-site").addClass('fg-color-red').text('Erro ao validar site');
                    }
                })
            }
        });
        $("#formulario").submit(function(e) {
            e.preventDefault();
            site = $("#site").val();
            if(site != '') {
                bootbox.dialog('Tem Certeza Que Deseja Criar Este Site?',[{
                    label: 'Criar',
                    class: 'bg-color-red',
                    callback: function() {
                        var context = ko.contextFor($(".metro-button.reload")[0]);
                        context.$data.showLoading(function() {
                            var dados = $("#formulario").serialize();
                            var url = $("#formulario").attr('action');
                            $.ajax({
                                url : url,
                                data : dados,
                                type : "POST",
                                dataType : "json",
                                complete : function() {
                                    context.$data.reload();
                                }
                            });
                        });
                    }
                },{
                    label: 'Cancelar',
                    class: 'bg-color-blue'
                }])
            } else {
                bootbox.alert("Digite o Site");
            }
        })
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Criar Site
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<?=$form->create('Turma', array(
    'id' => 'formulario',
    'url' => '/'. $this->params['prefix'] . "/turmas/criar_site/"
)); ?>
<div class='row-fluid'>
    <div class='span6'>
        <label>Site<span id="resp-site"></span></label>
        <?=$form->input('site',
            array(
                'label' => false,
                'div' => 'input-control text',
                'id' => 'site',
                'error' => false));
        ?>
    </div>
    <div class='span6'>
        <label>&nbsp;</label>
        <a class="button default" id="verificar-site">Verificar Disponibilidade</a>
    </div>
</div>
<button type='submit' class="default bg-color-red">
    <i class="icon-checkmark"></i>
    Criar Site
</button>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>
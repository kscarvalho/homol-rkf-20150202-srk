
<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Turma</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">			
			<label class="grid_11 alpha">Nome da Turma</label>
			<span class="grid_11 alpha"><?=$turma['Turma']['nome']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Ano da Formatura</label>
			<label class="grid_6 alpha">Semestre da Formatura</label>
			<span class="grid_5 alpha first"><?=$turma['Turma']['ano_formatura']; ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['semestre_formatura']; ?></span>			
		</p>
		<p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Cursos:</label>
            <?php foreach ($turma['CursoTurma'] as $cursoturma) : ?>
                <span class="grid_11 alpha first">
                    <?php
                    $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['sigla'];
                    if ($universidade == '')
                        $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['nome'];

                    $faculdade = $cursoturma['Curso']['Faculdade']['sigla'];
                    if ($faculdade == '')
                        $faculdade = $cursoturma['Curso']['Faculdade']['nome'];
                    echo $universidade . ' - ' .
                    $faculdade . ' - ' .
                    $cursoturma['Curso']['nome'] . ' - ' .
                    $cursoturma['turno'];
                    ?>
                </span>
<?php endforeach; ?>
        </p>
		<p class="grid_11 alpha omega">			
			<label class="grid_6 alpha">Expectativa de Formandos</label>
			<span class="grid_6 alpha first"><?=$turma['Turma']['expectativa_formandos']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Expectativa de Fechamento</label>
			<label class="grid_6 alpha">Data de Abertura da Turma</label>
			<span class="grid_5 alpha first"><?=ucfirst($turma['Turma']['expectativa_fechamento']); ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['data_abertura_turma']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
            <label class="grid_11 alpha">Consultor<?php if(count($turma['Usuario'])>1) echo 'es'; ?></label>
            <?php foreach($turma['Usuario'] as $u) {
                echo '<span class="grid_11 alpha">';
                echo $u['nome'] . ' - ' . $u['email'];
                echo '</span>';
            }?>
        </p>

		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Número de Pessoas na Comissão</label>
			<span class="grid_11 alpha first"><?=$turma['Turma']['pessoas_comissao']; ?></span>
		</p>

		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Data de Assinatura do Contrato</label>
			<label class="grid_6 alpha">Memorando</label>
			<span class="grid_5 alpha first"><?=$turma['Turma']['data_assinatura_contrato']; ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['memorando']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Fundo de Caixa Usado</label>
			<label class="grid_6 alpha">IGPM</label>
			<label class="grid_5 alpha">Multa Por Atraso</label>
			<span class="grid_5 alpha first"><?=$turma['Turma']['fundo_caixa_usado']; ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['igpm']; ?></span>
			<span class="grid_5 alpha"><?=number_format($turma['Turma']['multa_por_atraso'],2,',','')?>%</span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Status</label>
			<label class="grid_6 alpha">Ades&otilde;es</label>
			<label class="grid_5 alpha">Checkout</label>
			<span class="grid_5 alpha first"><?=ucfirst($turma['Turma']['status']); ?></span>
			<span class="grid_6 alpha" style="text-transform:capitalize;"><?=$turma['Turma']['adesoes']; ?></span>
			<span class="grid_5 alpha" style="text-transform:capitalize;"><?=$turma['Turma']['checkout']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Como Chegou?</label>
			<label class="grid_6 alpha">Detalhes de Como Chegou</label>
			<span class="grid_5 alpha first"><?=ucfirst($turma['Turma']['como_chegou']); ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['como_chegou_detalhes']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Pretensão</label>
			<label class="grid_6 alpha">Análise da RK</label>
			<span class="grid_5 alpha first"><?=ucfirst($turma['Turma']['pretensao']); ?></span>
			<span class="grid_6 alpha"><?=$turma['Turma']['analise_as']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Benefícios da Comissão</label>
			<span class="grid_5 alpha first"><?=$turma['Turma']['beneficios_comissao']; ?></span>
		</p>
		<p class="grid_6 alpha omega">
			<?=$html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?=$html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Turmas', 'action' => 'editar', $turma['Turma']['id']), array('class' => 'submit ')); ?>
			<?=$html->link('Adicionar Outra', array($this->params['prefix'] => true, 'controller' => 'Turmas', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>
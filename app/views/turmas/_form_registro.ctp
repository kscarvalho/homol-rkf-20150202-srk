<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datetimepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Memorando</label>
	<?php echo $form->textarea('memorando', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Expectativa de Fechamento</label>
	<?php echo $form->input('expectativa_fechamento', array('options' => $expectativa_fechamentos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Concorrentes</label>
	<?php echo $form->input('concorrentes', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Data Prev. Fechamento</label>
	<?php echo $form->input('data_prevista_fechamento', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>

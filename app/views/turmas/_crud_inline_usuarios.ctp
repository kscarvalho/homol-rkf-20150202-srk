<ul id="remover-usuario-inline">
	<?php if(isset($this->data['Usuario'])):?>
	<?php foreach ($this->data['Usuario'] as $us): ?>
		<?php if($us['grupo'] == 'planejamento' || $us['grupo'] == 'comercial' || $us['grupo'] == 'atendimento'): ?>
		<li>
			<a class="remover-usuario-inline"></a>
			<?php echo $us['email'].", id: ".$us['id']." - ".$us['grupo']; ?>
			<span class="usuario_id" style="display:none"><?php echo $us['id']?></span>
		</li>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php endif?>
</ul>

<?php echo $form->input('adicionar_usuarios_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'adicionar_usuarios_array')); ?>
<?php echo $form->input('remover_usuarios_array', array('style' => 'display:none;', 'label' => false, 'div' => false, 'id' => 'remover_usuarios_array'));?>

<div class="adicionar-inline-form" id="adicionar-inline-form-usuarios" style="display:none;">
	<p class="grid_11 alpha omega">
	<label>Selecione o Usuário</label>
	<?php
	$selectUsuarios = array();
	foreach ($usuarios as $i => $usuario) {
		if(empty($selectUsuarios[$usuario['grupo']])) $selectUsuarios[$usuario['grupo']] = array();
		$selectUsuarios[$usuario['grupo']] += array($usuario['id'] => $usuario['email']);
	}
	?>
	<?php echo $form->select('usuario_inline_id', $selectUsuarios, null, 
					array('class' => 'grid_11 alpha omega', 'label' => false, 'div' => false, 
								'empty' => '-- Selecione um usuário --')); ?>
	</p>
</div>
<p class="grid_11 alpha omega">
	<a class="adicionar-inline" id="adicionar-inline-usuario">Adicionar Usuário</a>
	<a class="esconder-inline" id="esconder-inline-usuario" style="display:none;">Esconder</a>
</p>

<script type='text/javascript'>
var formOpened = false;
var listaUsuarios = new ListaUsuariosInline();
$(function(){
	selectUsuarios = <?php echo $javascript->Object($usuarios); ?>;
	$.each(selectUsuarios, function(i, usuario){
		us = new Usuario();
		us.setarUsuario(usuario.id, usuario.email, usuario.grupo);
		listaUsuarios.usuarios.push(us);
	});
	
	
	$('#remover-usuario-inline a').live({
		mouseover: function(){
			$(this).parent().addClass('hover');
		}, 
		mouseout: function(){
			$(this).parent().removeClass('hover');
		}
	});
	
	$('#remover-usuario-inline a').live('click', function(){
		idUsuario = $(this).parent().find('span.usuario_id').html();
		oldValue = $('#remover_usuarios_array').val();
		if(oldValue == ""){
			$('#remover_usuarios_array').val(idUsuario)
		} else {
			$('#remover_usuarios_array').val(oldValue+","+idUsuario)
		}
		$(this).parent().remove();
	});
	
	$('#adicionar-inline-usuario').live('click', function(){
		if(!formOpened) {
			$('#adicionar-inline-form-usuarios').show('slow');			
			$('#esconder-inline-usuario').show();
			formOpened = true;
		} else {
			idUsuario = $('#TurmaUsuarioInlineId').val();
			if(listaUsuarios.colocarNaLista(listaUsuarios.pegarUsuarioPorId(idUsuario))){
				oldValue = $('#adicionar_usuarios_array').val();
				if(oldValue == ""){
					$('#adicionar_usuarios_array').val(idUsuario)
				} else {
					$('#adicionar_usuarios_array').val(oldValue+","+idUsuario)
				}
			}
		}
	});
	
	$('#esconder-inline-usuario').live('click', function(){
		$('#adicionar-inline-form-usuarios').hide('slow');
		$(this).hide();
		formOpened = false;
	})
});

function ListaUsuariosInline(){
	this.selector = "#remover-usuario-inline";
	this.usuarios = new Array();
}

function Usuario(){
	this.id = "";
	this.email = "";
	this.grupo = "";
}

Usuario.prototype.setarUsuario = function(idUsuario, email, grupo) {
	this.id = idUsuario;
	this.email = email;
	this.grupo = grupo
}

ListaUsuariosInline.prototype.pegarUsuarioPorId = function(idArg){
	var user;
	$.each(this.usuarios, function(i, usuario){
		if(usuario.id == idArg)
			user = usuario;
	});
	if(user) return user;
	else return null
}

ListaUsuariosInline.prototype.colocarNaLista = function(usuario){
	alreadyExists = false;
	$.each($(this.selector+' span.usuario_id'), function(i, span){
		if($(span).html() == usuario.id) alreadyExists = $(span).parent();
	});
	
	if(!alreadyExists){
		htmlToAppend = "<li>"+
											"<a class='remover-usuario-inline'></a>"+	
											usuario.email+", id: "+usuario.id +" - "+usuario.grupo+
											"<span class='usuario_id' style='display:none'>"+usuario.id+"</span>"
									 "</li>";
		$(this.selector).append(htmlToAppend);
		return true;
	} else {
		erroInsercaoCursoDuplicado(alreadyExists);
		return false;
	}
}
</script>
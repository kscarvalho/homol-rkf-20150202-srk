<style text="text/css">
tr:nth-child(even) td { background-color:gray; color:white; }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Turmas</span>
<div id="conteudo-container">
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <div class="tabela-adicionar-item">
        <?=$html->link('Gerar Excel',array($this->params['prefix'] => true, 'action' => 'relatorio_fechamento_excel')); ?>
        <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col" width="5%"><?=$paginator->sort('Id', 'Turma.id'); ?></th>
                    <th scope="col" width="30%"><?=$paginator->sort('Nome', 'Turma.nome'); ?></th>
                    <th scope="col" width="5%"><?=$paginator->sort('Expectativa Formandos', 'expectativa_formandos'); ?></th>
                    <th scope="col" width="5%"><?=$paginator->sort('Formandos', 'formandos'); ?></th>
                    <th scope="col" width="5%"><?=$paginator->sort('Aderidos', 'aderidos'); ?></th>
                    <th scope="col" width="5%"><?=$paginator->sort('Checkout', 'checkout'); ?></th>
                    <th scope="col" width="15%"><?=$paginator->sort('Data Assinatura', 'Turma.data_assinatura_contrato'); ?></th>
                    <th scope="col" width="10%"><?=$paginator->sort('Ano/Sem Conclusao', 'ano'); ?></th>
                    <th scope="col" width="20%">Consultores</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td colspan="1"><?=$paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($turmas as $turma): ?>
                    <tr>
                        <td><?=$turma['Turma']['id']; ?></td>
                        <td><?=$turma['Turma']['nome']; ?></td>
                        <td><?=$turma['Turma']['expectativa_formandos']; ?></td>
                        <td><?=$turma[0]['formandos']; ?></td>
                        <td><?=$turma[0]['aderidos']; ?></td>
                        <td><?=$turma[0]['checkout']; ?></td>
                        <td>
                            <?php if(!empty($turma['Turma']['data_assinatura_contrato'])) : ?>
                            <?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>
                            <?php else : ?>
                            Indefinida
                            <?php endif; ?>
                        </td>
                        <td><?="{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                        <td><?=$turma[0]['consultores']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
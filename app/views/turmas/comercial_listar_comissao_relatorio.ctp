<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<?php $session->flash(); ?>
<?php if (sizeof($aPagar) > 0) : ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Comissões a receber
    </div>
</div>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%">
                    Turma
                </th>
                <th scope="col" width="10%">
                    Código Formando
                </th>
                <th scope="col" width="35%">
                    Nome
                </th>
                <th scope="col" width="10%">
                    Porcentagem
                </th>
                <th scope="col" width="10%">
                    Valor Adesão
                </th>
                <th scope="col" width="10%">
                    Valor Comissão
                </th>
                <th scope="col" width="15%">
                    Valor Somado
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
                $valorTotalComissao = 0;
                $totalFormandos = 0;
                foreach($aPagar as $pagar) : 
                    $valor = $pagar['ViewFormandos']['valor_adesao'] * $porcentagem['ComissaoVendedor']['porcentagem'] / 100;
                    $valorTotalComissao += $valor;
                    $totalFormandos++;
            ?>
            <tr>
                <td><?=$pagar['ViewFormandos']['turma_id']; ?></td>
                <td><?=$pagar['ViewFormandos']['codigo_formando']; ?></td>
                <td><?=$pagar['ViewFormandos']['nome']; ?></td>
                <td><?=number_format($porcentagem['ComissaoVendedor']['porcentagem'], 1); ?> %</td>
                <td>R$ <?=number_format($pagar['ViewFormandos']['valor_adesao'], 2, ',', '.'); ?></td>
                <td>R$ <?=number_format($valor, 2, ',', '.'); ?></td>
                <td>R$ <?=number_format($valorTotalComissao, 2, ',', '.'); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao"></td>
                <td class="fg-color-red" colspan="2">Formandos: <?=$totalFormandos?></td>
                <td class="fg-color-greenDark">A Receber: R$ <?=number_format($valorTotalComissao, 2, ',', '.');?></td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma comissão a receber encontrada.</h2>
<?php endif; ?>
</div>
<script type="text/javascript">
    $(function() {
        $( ".datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>


<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Código</label>
	<?php echo $form->input('codigo', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<div class="grid_11 alpha omega crud-inline">
	<label class="grid_0 alpha omega">Cursos</label>
	<?php include('_crud_inline_cursos.ctp'); ?>
</div>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Expectativa de Formandos</label>
	<?php echo $form->input('expectativa_formandos', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Ano de Formatura</label>
	<?php 
	$anoatual = Date('Y') + 1;
	for($i = $anoatual - 5 ; $i < $anoatual + 5 ; $i++) {
		$anos[$i] = $i;
	}
	
	echo $form->input('ano_formatura', array('options' => $anos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Semestre de Formatura</label>
	<?php 
	
	echo $form->input('semestre_formatura', array('options' => array(1 => 1 , 2 => 2), 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
    <label class="grid_0 alpha">Memorando</label>
    <?php echo $form->input('Turma.memorando', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
    <label class="grid_0 alpha">Fundo de caixa utilizado</label>
    <?php echo $form->input('Turma.fundo_caixa_usado', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
<p class="grid_11 alpha omega">
    <label class="grid_0 alpha">IGPM</label>
    <?php echo $form->input('Turma.igpm', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_0 alpha">Data última correção do IGPM</label>
    <?php echo $form->input('data-igpm', array( 'class' => 'grid6 alpha omega datepicker', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Data de Assinatura do Contrato</label>
	<?php echo $form->input('data-assinatura', array( 'class' => 'grid6 alpha omega datepicker', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>	
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Convites Inclusos na Adesão</label>
	<?php echo $form->input('Turma.convites_contrato', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha omega">Mesas Inclusas na Adesão</label>
	<?php echo $form->input('Turma.mesas_contrato', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
</p>
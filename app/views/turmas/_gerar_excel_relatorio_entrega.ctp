<?php 
	foreach ($infos as $relatorio){
		$turma = substr($relatorio['codigo'], 0,4);
		$relatorioExcel[] = array(
				"Cod" => $relatorio['codigo'],
				"Formando" =>  $relatorio['nome'],
				"Campanha" => $relatorio['nomeCampanha'],
				"Tipo" => $relatorio['tipo'],
				"Status" => $relatorio['status'],
				"Convites" => $relatorio['convites'],
				"Mesas" => $relatorio['mesas']
			);
	}
	
	$excel->generate($relatorioExcel, 'relatorio_'.$turma);
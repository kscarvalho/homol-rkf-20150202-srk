<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.selectpicker').selectpicker({width:'100%'});
    });
</script>
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('Turma.fee', array(
            'label' => 'FEE',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.periodicidade_transferencia', array(
            'label' => 'Periodicidade Transferência',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.nome_destinatario', array(
            'label' => 'Nome Destinatário',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.cpf_cnpj_destinatario', array(
            'label' => 'CPF/CNPJ Destinatário',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('Turma.banco_destinatario', array(
            'label' => 'Banco Destinatário',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.agencia_destinatario', array(
            'label' => 'Agência Destinatário',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.conta_destinatario', array(
            'label' => 'Conta Destinatário',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.qtde_parcelas', array(
            'label' => 'Qtde Máx Parcelas',
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('Turma.vencimentos_disponiveis', array(
            'options' => array(
                '05, 15, 25' => '05, 15, 25',
                '10, 20, 30' => '10, 20, 30',
            ),
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Vencimentos Disponíveis',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.aplica_igpm', array(
            'options' => array(
                '0' => 'Não',
                '1' => 'Sim',
            ),
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Aplica IGPM?',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.aplica_multa', array(
            'options' => array(
                '0' => 'Não',
                '1' => 'Sim',
            ),
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Aplica Multa?',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Turma.transferencia_mensal', array(
            'options' => array(
                '0' => 'Não',
                '1' => 'Sim',
            ),
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Transferência Mensal?',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
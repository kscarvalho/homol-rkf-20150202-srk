<?php $paginator->options(array('url' => array($this->params['prefix'] => true,$turma['Turma']['id']))); ?>
<span id="conteudo-titulo" class="box-com-titulo-header"><?=$turma['Turma']['id']?> <?=$turma['Turma']['nome']?></span>
<div id="conteudo-container">
    <div class="tabela-adicionar-item">
        <?=$html->link("Voltar",
            array($this->params['prefix'] => true,
                'controller' => 'turmas',
                'action' =>'validar_igpm'
            ),
            array('style' => 'float:left'));?>
        <div style="clear:both;"></div>
    </div>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col" width="20%"><?=$paginator->sort('COD', 'codigo_formando'); ?></th>
                    <th scope="col" width="40%"><?=$paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col" width="20%"><?=$paginator->sort('IGPM Aplicado', 'despesas_igpm'); ?></th>
                    <th scope="col" width="20%"><?=$paginator->sort('Parcelas Aplicadas', 'despesas_igpm'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($formandos as $formando): ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']?></td>
                    <td><?=$formando['ViewFormandos']['nome']?></td>
                    <td><?=$formando[0]['despesas_igpm'] > 0 ? 'Sim' : 'Não'?></td>
                    <td><?=$formando[0]['despesas_igpm']?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
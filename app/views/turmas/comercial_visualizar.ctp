<?php
//print_r(array_keys($turma['CursoTurma'][0]['Curso']['Faculdade']));
//echo "<br><br>";
//print_r($turma['CursoTurma']);
?>
<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Turma</span>
<div id="conteudo-container">
        <?php $session->flash();?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Código:</label>
			<span class="grid_2 alpha first"><?php echo $turma['Turma']['id']; ?></span>
		</p>
		

		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Cursos:</label>
			<?php foreach($turma['CursoTurma'] as $cursoturma) :?>
				<span class="grid_11 alpha first">
					<?php
					$universidade = $cursoturma['Curso']['Faculdade']['Universidade']['sigla'];
					if($universidade == '') $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['nome'];
					
					$faculdade = $cursoturma['Curso']['Faculdade']['sigla'];
					if($faculdade == '')$faculdade = $cursoturma['Curso']['Faculdade']['nome'];
					echo 	$universidade . ' - '. 
							$faculdade . ' - '. 
							$cursoturma['Curso']['nome'] . ' - ' . 
							$cursoturma['turno']; 
					?>
				</span>
			<?php endforeach;?>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Data de Formatura</label>
			<span class="grid_2 alpha first"><?php echo $turma['Turma']['ano_formatura']. '.' . $turma['Turma']['semestre_formatura']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Expectativa de Formandos</label>
			<label class="grid_6 alpha">Data de Abertura da Turma</label>
			<span class="grid_5 alpha first"><?php echo $turma['Turma']['expectativa_formandos']; ?></span>
			<span class="grid_6 alpha"><?php echo date('d/m/Y',strtotime($turma['Turma']['data_abertura_turma'])); ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Número de Pessoas na Comissão</label>
			<span class="grid_11 alpha first"><?php echo $turma['Turma']['pessoas_comissao']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Como Chegou?</label>
			<span class="grid_11 alpha first"><?php echo $turma['Turma']['como_chegou']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Detalhes de Como Chegou</label>
			<span class="grid_11 alpha"><?php echo $turma['Turma']['como_chegou_detalhes']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Status</label>
			<label class="grid_6 alpha">Data de Assinatura do Contrato</label>
			<span class="grid_5 alpha first"><?php echo ucfirst($turma['Turma']['status']); ?></span>
			<span class="grid_6 alpha">
				<?php 
					if (!empty($turma['Turma']['data_assinatura_contrato']))
						echo date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato'])); 
					else
						echo "-";
				?>
			</span>
		</p>
		
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Pretensão</label>
			<span class="grid_11 alpha first"><?php echo ucfirst($turma['Turma']['pretensao']); ?></span>
		</p>
		<p class="grid_6 alpha omega">
			<?php 
                        if(strcmp($turma['Turma']['status'],'fechada')!=0)
                            echo $html->link('Fechar com a turma',array($this->params['prefix'] => true, 'action' => 'fechar') ,array('class' => 'submit', 'onclick' => "javacript: if(confirm('Deseja realmente fechar com a turma?')) return true; else return false;")); ?>
			<?php echo $html->link('Editar',array($this->params['prefix'] => true, 'action' => 'editar') ,array('class' => 'submit')); ?>
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>
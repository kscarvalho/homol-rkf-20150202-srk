<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Relatório de Fechamento
        </h2>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <?=$form->create('Turma',array(
        'url' => "/{$this->params['prefix']}/turmas/relatorio_fechamento_novo/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('id',
                array('label' => 'ID', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span10">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
            <a class='button mini bg-color-blueDark pull-right' href='<?="/{$this->params['prefix']}/turmas/relatorio_fechamento_excel"?>'
            target='_blank'>
            Gerar Excel
            <i class='icon-file-excel'></i>
        </a>
        </div>
    </div>
<?php if (sizeof($turmas > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%"><?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Expectativa Formandos', 'expectativa_formandos.nome',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Formandos', 'Formandos',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Aderidos', 'aderidos',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Checkout', 'checkout',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Data Assinatura', 'Turma.data_assinatura_contrato',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Ano/Sem Conclusao', 'ano',$sortOptions); ?></th>
                <th scope="col" width="30%">Consultores</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td width="10%"><?=$turma['Turma']['id']; ?></td>
                <td width="20%"><?=$turma['Turma']['nome']; ?></td>
                <td width="5%"><?=$turma['Turma']['expectativa_formandos']; ?></td>
                <td width="5%"><?=$turma[0]['formandos']; ?></td>
                <td width="5%"><?=$turma[0]['aderidos']; ?></td>
                <td width="5%"><?=$turma[0]['checkout']; ?></td>
                <td width="10%">
                    <?php if(!empty($turma['Turma']['data_assinatura_contrato'])) : ?>
                        <?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>
                    <?php else : ?>
                        Indefinida
                    <?php endif; ?>
                </td>
                <td width="10%" style="text-align:center"><?="{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                <td width="30%"><?=$turma[0]['consultores'];?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Turmas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Turma Encontrada.</h2>
<?php endif; ?>
</div>
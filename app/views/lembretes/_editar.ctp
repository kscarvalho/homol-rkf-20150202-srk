<span id="conteudo-titulo" class="box-com-titulo-header">Editar Lembrete</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Lembrete', array('url' => "/{$this->params['prefix']}/lembretes/editar")); ?>
		<?php include	('_form.ctp'); ?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true,'controller' => 'principal', 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
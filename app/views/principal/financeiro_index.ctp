<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <div class="tile icon app bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/pagamentos/upload_pagamentos') }">
            <div class="tile-content">
                <span class="img icon-upload"></span>
            </div>
            <div class="brand"><span class="name">Importar Pagamentos</span></div>
        </div>
        <div class="tile icon app bg-color-darken"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/formandos/listar') }">
            <div class="tile-content">
                <span class="img icon-printer"></span>
            </div>
            <div class="brand"><span class="name">Gerar Boletos</span></div>
        </div>
        <div class="tile icon app bg-color-greenDark"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/pagamentos') }">
            <div class="tile-content">
                <span class="img icon-feather"></span>
            </div>
            <div class="brand"><span class="name">Gerar Recibo de Pagamentos</span></div>
        </div>
        <div class="tile icon app bg-color-red"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/pagamentos/comprovante') }">
            <div class="tile-content">
                <span class="img icon-book-2"></span>
            </div>
            <div class="brand"><span class="name">Gerar Recibo de Comprovantes</span></div>
        </div>
        <div class="tile icon app bg-color-orange"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/recibos') }">
            <div class="tile-content">
                <span class="img icon-database"></span>
            </div>
            <div class="brand"><span class="name">Recibos</span></div>
        </div>
        <div class="tile icon app bg-color-purple"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/lista_pagamentos') }">
            <div class="tile-content">
                <span class="img icon-search-2"></span>
            </div>
            <div class="brand"><span class="name">Listar Pagamentos</span></div>
        </div>
        <div class="tile icon app bg-color-brown"
            data-bind="click: function(data, event) {
                page('<?= $this->webroot . $this->params['prefix'] ?>/turmas/comissoes') }">
            <div class="tile-content">
                <span class="img icon-archive"></span>
            </div>
            <div class="brand"><span class="name">Comissões Comercial</span></div>
        </div>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('/turmas/ficha_evento') }">
            <div class="tile-content">
                <div class="img icon-drawer-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Ficha Evento</span>
            </div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('turmas/analise_temporada') }">
            <div class="tile-content">
                <span class="img icon-amazon-2"></span>
            </div>
            <div class="brand"><span class="name">Análise Temporada</span></div>
        </div>
        <div class="tile icon bg-color-gray"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/usuarios/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Todos Formandos</span>
            </div>
        </div>
    </div>
</div>
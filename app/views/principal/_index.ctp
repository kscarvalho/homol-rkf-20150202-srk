    <span id="secao-turmas" class="box-com-titulo-header">Home</span>
    <div id="conteudo-container">
		<?php $session->flash(); ?>
        <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<div class="tabela-adicionar-item" style="margin-top: 30px;">
			<?php echo $form->create('Turmas', array('url' => "/{$this->params['prefix']}/turmas/procurar", 'class' => 'procurar-form-inline')) ?>
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha omega">Busca por id da turma</label>
				<?php echo $form->input('chave', array('class' => 'grid_6', 'label' => false, 'div' => false)); ?>
			
				<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca', 'style' => 'margin-left: 5px; !important')) ?>
			</p>

			
			<div style="clear:both;"></div>
		</div>

		<p class='titulo-secao'>Lembretes</p>
		<div class="tabela-adicionar-item">
		    <?php echo $html->link('Adicionar', array($this->params['prefix'] => true, 'controller' => 'lembretes','action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
		</div>

		    <div class="container-tabela">
		        <table>
		            <thead>
		                <tr>
		                    <th scope="col"><?php echo $paginator->sort('Título', 'titulo'); ?></th>
		                    <th scope="col"><?php echo $paginator->sort('Texto', 'texto'); ?></th>
		                    <th scope="col"> &nbsp;</th>
		                </tr>
		            </thead>
		            <tfoot>
		                <tr>
		                    <td colspan="2"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
		                        <span class="paginacao">
		                            <?php echo $paginator->numbers(array('separator' => ' ')); ?>
		                        </span>
		                    </td>
		                    <td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
		                </tr>
		            </tfoot>
		            <tbody>
		                <?php $isOdd = false; ?>
		                <?php foreach ($lembretes as $lembrete): ?>
		                    <?php if ($isOdd): ?>
		                        <tr class="odd">
		                        <?php else: ?>
		                        <tr>
		                        <?php endif; ?>
		                        <td  colspan="1" width="10%"><?php echo $lembrete['Lembrete']['titulo']; ?></td>
		                        <td  colspan="1" width="60%">
		                            <?php echo substr($lembrete['Lembrete']['texto'], 0, 120); ?>
		                            <?php echo (strlen($lembrete['Lembrete']['texto']) >= 120) ? ('...') : ('') ?>
		                        </td>
		                        <td  colspan="1" width="30%">
		                            <?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'lembretes', 'action' => 'visualizar', $lembrete['Lembrete']['id']), array('class' => 'submit button')); ?>
		                            <?php echo $html->link('Editar', array($this->params['prefix'] => true,'controller' => 'lembretes', 'action' => 'editar', $lembrete['Lembrete']['id']), array('class' => 'submit button')) ?>
		                            <?php echo $html->link('Deletar', array($this->params['prefix'] => true,'controller' => 'lembretes', 'action' => 'deletar', $lembrete['Lembrete']['id']), array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o lembrete {$lembrete['Lembrete']['titulo']}?')) return true; else return false;")); ?>
		                        </td>
		                    </tr>
		                    <?php $isOdd = !($isOdd); ?>
		                <?php endforeach; ?>
		            </tbody>
		        </table>
		    </div>
			<p class="grid_full">
				<?php echo $html->link('Agenda', array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => 'index'), array('class' => 'link-agenda')); ?>
			</p>

</div>

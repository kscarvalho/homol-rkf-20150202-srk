<?php
echo $html->script('jquery.tools.min');
echo $html->css('tooltip');
?>
<script type="text/javascript">
jQuery(function($) {
	jQuery(".mensagem").tooltip();
});
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Turmas gerenciadas</span>
<div id="conteudo-container">
	<?php $session->flash(); /* ?>
	
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/formandos/index", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')); ?>
		<span>Busca: <?=$form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<div style="clear:both;"></div>
	</div>
	*/ ?>
	<p>
		Passe o mouse na mensagem pra visualizar o texto
	</p>
    <div class="container-tabela">
    	<table>
            <thead>
                <tr>
                	<th scope="col" width="30%"><?=$paginator->sort('Titulo', 'titulo'); ?></th>
                    <th scope="col" width="50%"><?=$paginator->sort('Mensgaem', 'mensagem'); ?></th>
                    <th scope="col" width="20%"><?=$paginator->sort('Data de Envio', 'data_cadastro'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="2"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td><?=$paginator->counter(array('format' => 'Total : %count% ')); ?></td>
                </tr>
            </tfoot>
            <tbody>
            	<?php if(count($informativos) > 0) :?>
                <?php $isOdd = false; ?>
                <?php foreach ($informativos as $informativo): ?>
                	<?php $mensagem = $informativo['Informativo']['mensagem'];?>
                	<tr class="hover<?=$isOdd ? " odd" : ""?>">
                		<td colspan="1"><?=$informativo['Informativo']['titulo']?></td>
                        <td colspan="1" class='mensagem' title='<?=$mensagem?>'>
                        	<?=strlen($mensagem) > 80 ? substr($mensagem,0,80) . "..." : $mensagem?>
                        </td>
                        <td colspan="1"><?=date('d/m/Y', strtotime($informativo['Informativo']['data_cadastro'])) ?></td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                	<td colspan='3'>Nenhum informativo foi encontrado</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
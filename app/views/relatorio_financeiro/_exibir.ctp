<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Relat&oacute;rio Financeiro
        </h2>
    </div>
</div>
<div class="row-fluid">
    <?php if(count($formandos) > 0) :?>
    <h3><?=$resumo["total"]?> Formandos Cadastrados</h3>
    <br />
    <div class="row-fluid">
        <div class="span3 bg-color-gray">
            <?=$resumo["cancelados"]?> Cancelado(s)
        </div>
        <div class="span3 bg-color-red">
            <?=$resumo["inadimplentes"]?> Inadimplente(s)
        </div>
        <div class="span3 bg-color-yellow">
            <?=$resumo["inativos"]?> Inativo(s)
        </div>
        <div class="span3 bg-color-green">
            <?=$resumo["ativos"]?> Ativo(s)
        </div>
    </div>
    <br />
    <table class="table table-condensed table-bordered">
        <thead>
            <tr>
                <th scope="col"><?=$paginator->sort('Cód. Formando', 'codigo_formando',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Nome', 'nome',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Curso', 'curso_nome',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Status', 'status',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Total de Parcelas', 'total_parcelas',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Valor Total', 'valor_total',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Data 1a Parcela', 'data_primeira_parcela',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Parcelas Pagas', 'parcelas_pagas',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Total Pago', 'total_pago',$sortOptions); ?></th>
                <th scope="col"><?=$paginator->sort('Parcelas Atrasadas', 'parcelas_atrasadas',$sortOptions); ?></th>
                <th scope="col">Saldo em Aberto</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($formandos as $formando): ?>
            <?php
                $saldoAberto = $formando['ViewRelatorioFinanceiro']['valor_total'] -
                        $formando['ViewRelatorioFinanceiro']['total_pago'];
                if($formando["ViewRelatorioFinanceiro"]["grupo"] == "comissao")
                    $class = "bg-color-blueDark";
                elseif($formando["ViewRelatorioFinanceiro"]["status"] == "Cancelado")
                    $class = "bg-color-gray";
                elseif($formando["ViewRelatorioFinanceiro"]["status"] == "Inativo")
                    $class = "bg-color-yellow";
                elseif($formando["ViewRelatorioFinanceiro"]["status"] == "Inadimplente")
                    $class = "bg-color-red";
                else
                    $class = "bg-color-green";
            ?>
            <tr class="<?=$class?>" style="opacity:.8">
                <td width="10%"><?=$formando['ViewRelatorioFinanceiro']['codigo_formando']; ?></td>
                <td width="20%"><?=$formando['ViewRelatorioFinanceiro']['nome']; ?></td>
                <td width="20%"><?=$formando['ViewRelatorioFinanceiro']['curso_nome']; ?></td>
                <td><?=$formando["ViewRelatorioFinanceiro"]["status"] ?></td>
                <td><?=$formando['ViewRelatorioFinanceiro']['total_parcelas']; ?></td>
                <td>R$<?=number_format($formando['ViewRelatorioFinanceiro']['valor_total'], 2, ',' , '.'); ?></td>
                <td><?=$formando['ViewRelatorioFinanceiro']["data_primeira_parcela"]; ?></td>
                <td><?=$formando['ViewRelatorioFinanceiro']['parcelas_pagas']; ?></td>
                <td>R$<?=number_format($formando['ViewRelatorioFinanceiro']['total_pago'], 2, ',' , '.'); ?></td>
                <td><?=$formando['ViewRelatorioFinanceiro']['parcelas_atrasadas']; ?></td>
                <td>R$<?=number_format($saldoAberto, 2, ',' , '.'); ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">
        N&atilde;o foi encontrado registro financeiro desta turma
    </h2>
    <?php endif; ?>
</div>
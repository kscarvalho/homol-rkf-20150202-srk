<span id="conteudo-titulo" class="box-com-titulo-header">Recebimento de Boletos</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Pagamentos', array('url' => "/{$this->params['prefix']}/pagamentos/processa_boletos",'type' => 'file')); ?>
	
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha">Arquivo xls com lista de boletos:</label>
		<?php echo $form->file('arquivo', array('class' => 'grid_11 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span')))?>
	</p>


	<p class="grid_11 alpha omega">
		<span class="grid_11 alpha omega">
			<?php echo $form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));?>
		</span>
	</p>
</div>
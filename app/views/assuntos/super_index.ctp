<span id="conteudo-titulo" class="box-com-titulo-header">Inbox</span>

<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('pen', 'pendencia'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Assuntos %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($assuntos as $assunto): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $assunto['Assunto']['nome'];?></td>
					<td colspan="1"><?php echo $assunto['Item']['nome'];?></td>
					<td colspan="1"><?php echo $html->link('Visualizar Mensagens', array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' =>'visualizar', $item['Item']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
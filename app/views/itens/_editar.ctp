<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $(".chosen").chosen({width:'100%'});
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'salvar-formulario',
            text:'Salvar'
        });
        $('.modal-footer').prepend(button);
        $("#salvar-formulario").click(function(e) {
            e.preventDefault();
            $('#formulario').trigger('submit');
        });
        $('#formulario').validate({
            sendForm : false,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o Campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var button = $('.modal-footer').find(':contains("Salva")');
                button.remove();
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            bootbox.hideAll();
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            }
        });
    })
</script>
<?=$form->create('Item', array(
    'url' => "/{$this->params['prefix']}/itens/editar",
    'id' => 'formulario'
));
echo $form->hidden('Item.id');
?>
<div class="row-fluid">
    <div class="span11">
        <label class="required">Itens Padr&atilde;o</label>
        <?=$form->input('agrupamento', array(
            'options' => $agrupamento,
            'type' => 'select',
            'class' => 'chosen',
            'empty' => 'Sem Padrão',
            'label' => false,
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span11">
        <label for="nome" class="required">Nome</label>
        <?=$form->input('Item.nome', array(
            'label' => false,
            'div' => 'input-control text',
            'data-required' => 'true',
            'id' => 'nome',
            'data-description' => 'notEmpty',
            'data-describedby' => 'nome',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span11">
        <label for="assunto" class="required">Assunto</label>
        <?=$form->input('Assunto.nome', array(
            'label' => false,
            'div' => 'input-control text',
            'data-required' => 'true',
            'id' => 'assunto',
            'data-description' => 'notEmpty',
            'data-describedby' => 'assunto',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span11">
        <label>Descri&ccedil;&atilde;o</label>
        <?=$form->input('Item.descricao', array(
            'label' => false,
            'rows' => 4,
            'div' => 'input-control textarea',
            'error' => false)); ?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>

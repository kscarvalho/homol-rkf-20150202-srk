<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Parceiro</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<span class="grid_4 alpha first">
			<?php 
							if (!empty($parceiro['Parceiro']['logo'])) {
								echo $html->image($caminho_raiz_logos.'/'.$parceiro['Parceiro']['logo'], array('style' => 'width: 90%;')); 
							} ?>
		</span>
	
		<div class="<?php echo empty($parceiro['Parceiro']['logo']) ? "grid_16" : "grid_12"; ?> alpha omega">
			<p class="grid_full alpha omega first">
				<label class="grid_full alpha omega">Parceiro</label>
				<span class="grid_full alpha first"> <?php echo $parceiro['Parceiro']['nome']?> </span>
			</p>
			
			<p class="grid_full alpha omega first">
				<label class="grid_full alpha omega">Descrição</label>
				<span class="grid_full alpha first"> <?php echo $parceiro['Parceiro']['descricao']?> </span>
			</p>
		</div>
		
		<p class="grid_4 alpha first">
			<label class="grid_full alpha omega">Site</label>
			<span class="grid_full alpha omega">
				<?php echo $html->link($parceiro['Parceiro']['site'], $site_completo, array('target' => '_blank')); ?>
			</span>
		</p>
		
		<p class="grid_12 alpha omega first">
			<label class="grid_full alpha omega">Ativo</label>
			<span class="grid_full alpha first"> <?php echo $parceiro['Parceiro']['ativo'] == 1 ? 'Sim' : 'Não'; ?> </span>
		</p>
		<?php if (!empty($parceiro['FotoParceiro'])) {?>
		
		<p class="grid_11 alpha omega first">
			<label class="grid_11 alpha omega">Fotos</label>
		</p>
		<div class="slider-imagens grid_full alpha omega" style="position:relative;height:300px;">
			<?php 
				foreach($parceiro['FotoParceiro'] as $foto) {
 
						echo $html->image($caminho_raiz_fotos.'/'.$foto['nome'], array('style' => 'max-height: 300px;max-width:250px;')); 

				}
			?>
		</div>
		<?php } ?>
		
		<p class="grid_16 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Parceiros', 'action' => 'editar', $parceiro['Parceiro']['id']), array('class' => 'submit ')); ?>
			<?php echo $html->link('Adicionar Outro', array($this->params['prefix'] => true, 'controller' => 'Parceiros', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>

<script language="javascript" type="text/javascript">
$(".slider-imagens").smoothDivScroll({ 
	mousewheelScrolling: true,
	manualContinuousScrolling: false,
	hotSpotScrolling : true,
	visibleHotSpotBackgrounds: "always",
	autoScrollingMode: ""
});

$('.scrollingHotSpotRightVisible').css('display', 'block');
</script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<style type="text/css">
    button{
        width: 65px;
        height: 26px
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('.remover').click(function() {
        dir = $(this).attr('dir');
        bootbox.dialog('Tem Certeza Que Deseja Apagar o Extra?',[{
            label: 'Apagar',
            class: 'bg-color-red',
            callback: function() {
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var url = '<?="/{$this->params['prefix']}/extras/remover/"?>'+dir;
                context.$data.showLoading(function() {
                    bootbox.hideAll();
                    $.ajax({
                        url : url,
                        type : "GET",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        },{
            label: 'Cancelar',
            class: 'bg-color-blueDark'
        }]);
    });
    $('.visualizar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/extras/exibir/"?>'+
                    $(this).attr('dir')
        });
    });
    $('.alterar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/extras/alterar/"?>'+
                    $(this).attr('dir')
        });
    });
    $('.inserir').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/extras/inserir/"?>'+
                    $(this).attr('dir')
        });
    });
});
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: function() { 
            reload() }"></a>
        <?php echo "Extras de " . $evento['Evento']['nome'];?>
        <?php if($this->params['prefix'] == 'planejamento'){ ?>
        <a class="button mini inserir bg-color-greenDark pull-right" dir="<?=$evento['Evento']['id']?>">
            Inserir Extras
            <i class='icon-folder-3'></i>
        </a>
        <?php } ?>
    </h2>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if(!empty($extras)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%">Id</th>
                <th scope="col" width="35%">Nome</th>
                <th scope="col" width="40%">Descrição</th>
                <th scope="col" width="20%">&nbsp</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($extras as $extra): ?>
            <tr>
                <td colspan="1"><?php echo $extra['Extra']['id'];?></td>
                <td colspan="1"><?php echo $extra['Extra']['nome'];?></td>
                <td colspan="1"><?php echo $extra['Extra']['descricao']?></td>
                <td colspan="1" style="text-align: center">
                    <button class="mini visualizar bg-color-greenDark" type="button" dir="<?=$extra['Extra']['id']?>">
                        Visualizar
                    </button>
                    <button class="mini remover bg-color-red" type="button" dir="<?=$extra['Extra']['id']?>">
                        Remover
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum Extra Encontrado Para Esta Turma</h2>
    <?php endif; ?>
</div>
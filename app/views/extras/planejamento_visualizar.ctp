<span id="conteudo-titulo" class="box-com-titulo-header"><?php echo "Extra de " . $extra['Evento']['nome'];?></span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<?php include('_visualizar.ctp');?>
		<p class="grid_6 alpha omega">
			<?php echo $html->link('Editar',array($this->params['prefix'] => true, 'controller' => 'extras' , 'action' => 'editar', $extra['Extra']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Excluir',array($this->params['prefix'] => true, 'controller' => 'extras' , 'action' => 'excluir', $extra['Extra']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index', $extra['Evento']['id']) ,array('class' => 'cancel')); ?>
		</p>
		
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Relatório Checkout
        </h2>
    </div>
</div>
<div id="row-fluid">	
            <table class="table table-condensed table-bordered">
                    <thead>
                                    <tr>
                                            <th>Codigo Formando</th>
                                            <th>Nome</th>
                                            <th>Mesas Contrato</th>
                                            <th>Convites Contrato</th>
                                            <th>Item Checkout</th>
                                            <th>Quantidade</th>
                                            <th>Extra</th>
                                            <th>Quantidade</th>
                                    </tr>
                    </thead>
                    <tbody>
                            <?php $isOdd = false; ?>
                            <?php foreach ($relatorio_checkout as $relatorio) :?>
                            <?php if($isOdd):?>
                            <tr class="odd" height="45">
                            <?php else:?>
                            <tr height="45">
                            <?php endif;?>
                                    <th collabel="1" style="text-align: center;"><?php echo $relatorio['formando']['codigo_formando']?></th>
                                    <th collabel="1" >
                                    <?php 
                                            if (($relatorio['formando']['nome']) == "")
                                                    echo "-";
                                            else
                                                    echo $relatorio['formando']['nome'];
                                    ?>
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['formando']['mesas_contrato']))
                                                    echo $relatorio['formando']['mesas_contrato']; 
                                            else
                                                    echo "-";
                                    ?>
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['formando']['convites_contrato']))
                                                    echo $relatorio['formando']['convites_contrato']; 
                                            else
                                                    echo "-";
                                    ?>
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['item']['item_checkout']))
                                                    echo $relatorio['item']['item_checkout']; 
                                            else
                                                    echo "-";
                                    ?>
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['checkout']['quantidade']))
                                                    echo $relatorio['checkout']['quantidade']; 
                                            else
                                                    echo "-";
                                    ?>						
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['extra']['extra']))
                                                    echo $relatorio['extra']['extra']; 
                                            else
                                                    echo "-";
                                    ?>
                                    </th>
                                    <th collabel="1" style="text-align: center;">
                                    <?php 
                                            if (isset($relatorio['extra']['quantidade']))
                                                    echo $relatorio['extra']['quantidade']; 
                                            else
                                                    echo "-";
                                    ?>
                                    </th>
                            </tr>
                            <?php $isOdd = !($isOdd); ?>
                            <?php endforeach; ?>
                    </tbody>
            </table>
</div>

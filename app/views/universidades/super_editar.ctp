<span id="conteudo-titulo" class="box-com-titulo-header">Universidades - Editar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Universidade', array('url' => "/{$this->params['prefix']}/universidades/editar")); ?>
		<?php include	('_form.ctp'); ?>
	
	<p class="grid_12 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
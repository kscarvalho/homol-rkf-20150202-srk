
<span id="conteudo-titulo" class="box-com-titulo-header">Informações da Universidade</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Nome</label>
			<span class="grid_8 alpha first"><?php echo $universidade['Universidade']['nome']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Sigla</label>
			<span class="grid_8 alpha first"><?php echo $universidade['Universidade']['sigla']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Universidades', 'action' => 'editar', $universidade['Universidade']['id']), array('class' => 'submit ')) ?>
			<?php echo $html->link('Adicionar Outra', array($this->params['prefix'] => true, 'controller' => 'Universidades', 'action' => 'adicionar'), array('class' => 'submit ')); ?>
		</p>
	</div>
</div>
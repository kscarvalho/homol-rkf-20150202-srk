<link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/tile.css?v=0.2">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/owl/owl.carousel.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.easing.1.3.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/dropdown.js?v=0.2"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/owl/owl.carousel.js"></script>
<style type="text/css">
.tile { width:48%; color: black!important }
#titulo { font-size:25px }
#titulo i { font-size:.8em; vertical-align: middle }
#titulo:hover { cursor:pointer }
.drop-categorias { display:inline-block; margin: 20px 0 }
.drop-categorias > .dropdown-menu { left:102%; top:-10px }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($(".metro-button.back")[0]);
        $('[data-parceria]').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar',
                class: 'bg-color-red'
            }],{
                remote: '/formando/parcerias/show/' + $(this).data("parceria")
            });
        });
        $('#titulo').dropdown();
        $(".drop-categorias > .dropdown-menu > li > a").click(function(e) {
            e.preventDefault();
            var url = '/formando/parcerias/categoria/' + $(this).data("categoria");
            context.$data.page(url);
        });
    });
</script>
<div class="row-fluid">
    <div class="dropdown drop-categorias">
        <h2>
            <a class="metro-button back" data-bind="click: function() { 
                page('/formando/parcerias/categorias') }">
            </a>
            <?=$categoria["Categoria"]["nome"]?>
        </h2>
        <span id="titulo" data-toggle="dropdown">
            <span>Escolha Uma Categoria</span>
            <i class='icon-arrow-6'></i>
        </span>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <?php foreach($menu as $item) : ?>
            <li>
                <a href="#" data-categoria="<?=$item["id"]?>"><?=$item["nome"]?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<br />
<div class="row-fluid">
<?php if (count($parcerias) > 0) { ?>
    <?php foreach ($parcerias as $parceria) : ?>
    <div  class="tile bg-color-gray"
        data-parceria="<?=$parceria["Parceria"]['id']?>">
        <div class="tile-content">
            <h4 class="lead">
                <img class="pull-left border-color-white img-rounded"
                style="margin-right:10px"
                src="/fotos/crop/<?=base64_encode("img/parceiros/logos/{$parceria["Parceiro"]["logo"]}")?>/100/100/middle" />
                <?= $parceria['Parceria']['titulo'] ?>
            </h4>
            <div class="span6">
                <h6 style="position: absolute; font-size: 12px; float: right; top: 60%; right: 12%">
                    <i class="icon-plus"></i>&nbsp Clique para mais informações
                </h6>
            </div>
        </div>
        <div class="brand"></div>
    </div>
    <?php endforeach; ?>
<?php } else { ?>
    <h2 class="fg-color-red">Nenhuma Promo&ccedil;&atilde;o Ativa</h2>
<?php } ?>
</div>
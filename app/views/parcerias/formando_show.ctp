<?php if($parceria) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/owl/owl.carousel.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/owl/owl.carousel.js"></script>
<style type="text/css">
.owl-carousel.fotos .owl-controls .owl-nav div {
    position: absolute;
    top:0;
    height:100%;
    background:transparent;
}
.owl-carousel.fotos .owl-controls .owl-nav i { position:relative;
    top:50%; font-size:20px; color:black }
.owl-carousel.fotos .owl-controls .owl-nav .owl-prev { left:-5px; }
.owl-carousel.fotos .owl-controls .owl-nav .owl-next { right:-5px; }
img.logo { border:solid 2px #F2F2F2 }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var fotos = $('.owl-carousel.fotos');
        fotos.owlCarousel({
            loop:true,
            lazyLoad:true,
            nav:true,
            navText: ["<i class='icon-arrow-9'></i>","<i class='icon-arrow-6'></i>"],
            items : 1
        });
    });
</script>
<div class="row-fluid">
    <div class="span5">
        <div class="row-fluid">
            <?php if(!empty($parceria["Parceiro"]['logo'])) : ?>
            <div class="span3">
                <div>
                    <img src="/fotos/crop/<?=base64_encode("img/parceiros/logos/{$parceria["Parceiro"]["logo"]}")?>/80/80/middle"
                        class="img-rounded logo" />
                </div>
            </div>
            <?php endif; ?>
            <div class="span<?=!empty($parceria["Parceiro"]['logo']) ? '9' : '12'?>">
                <h2 style="line-height: 1.2em; font-size:1.4em; margin:0">
                    <?=$parceria["Parceria"]["titulo"]?>
                    <?php if(!empty($parceria["Parceria"]["desconto"])) : ?>
                    <br />
                    <em style="font-size:.7em"
                           class="fg-color-red">
                        <?=$parceria["Parceria"]["desconto"]?> off
                     </em>
                    <?php endif; ?>
                </h2>
            </div>
        </div>
        <a style="margin-top:10px"
            href="/formando/parcerias/voucher/<?=$parceria["Parceria"]["id"]?>"
            class="bg-color-blue button mini input-block-level" target="_blank">
            Imprimir Voucher
            <i class="icon-printer"></i>
        </a>
        <br />
        <p>
            <?=$parceria["Parceria"]["descricao"]?>
            <br />
            <br />
            <strong>Local </strong><?=$parceria["Parceria"]["local"]?>
            <?php if(!empty($parceria["Parceria"]["data_fim"])) : ?>
            <br />
            <strong>Finaliza em </strong><?=date('d/m/Y',strtotime($parceria["Parceria"]["data_fim"]))?>
            <?php endif; ?>
            <?php if(!empty($parceria["Parceria"]["observacoes"])) : ?>
            <br />
            <br />
            <strong>Obs </strong><?=$parceria["Parceria"]["observacoes"]?>
            <?php endif; ?>
        </p>
    </div>
    <div class="span6 offset1">
        <?php if(count($parceria["FotoParceria"]) > 0) : ?>
        <div class="owl-carousel fotos">
            <?php foreach($parceria["FotoParceria"] as $a => $foto) : ?>
            <div class="item">
                <img src="/fotos/fit/<?=base64_encode("img/parcerias/fotos/{$foto["parceria_id"]}/{$foto["nome"]}")?>/300/350"
                    class="img-rounded logo" style="width:initial; margin:auto" />
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Parceria Não Encontrada</h2>
<?php endif; ?>

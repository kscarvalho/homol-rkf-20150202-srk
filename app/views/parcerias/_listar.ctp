<div class="row-fluid">
	<div class="span12">
		<h2>
			<a class="metro-button reload" data-bind="click: function() { reload() }"></a>
			Parcerias
		</h2>
	</div>
</div>
<br />
<div class="row-fluid">
	<?php if (count($parcerias) > 0) { ?>
	<?php foreach($parcerias as $parceria) : ?>
	<div  class="tile bg-color-gray" style="width:48%; color: black!important"
              data-bind="click: function() { page('<?= $this->webroot . $this->params['prefix'] ?>/parcerias/exibir/<?=$parceria['Parceria']['id']?>') }">
		<div class="tile-content">
			<img class="place-left border-color-white"
                        style="width: 100px!important; height: 100px!important; background-color: white!important" src="<?='img/'.$caminho_raiz_logos.'/'.$parceria['Parceiro']['logo'];?>" />
		  	<h4 class="lead"><?=$parceria['Parceria']['titulo']?></h4>
                  <div class="span6">
                        <h6 style="position: absolute; font-size: 12px; float: right; top: 60%; right: 12%">
                            <i class="icon-plus"></i>&nbsp Clique para mais informações
                        </h6>
                  </div>
		</div>
		<div class="brand"></div>
	</div>
	<?php endforeach; ?>
	<?php } else { ?>
	<h2 class="fg-color-red">Nenhuma Promo&ccedil;&atilde;o Ativa</h2>
	<?php } ?>



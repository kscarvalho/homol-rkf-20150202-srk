<span id="conteudo-titulo" class="box-com-titulo-header">Parcerias</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Nova Parceria',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Título', 'titulo'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Parceiro', 'parceiro'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Data de Início', 'data_inicio'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Data de Fim', 'data_fim'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Pública', 'publica'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Ativa', 'ativa'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="6"><?php echo $paginator->counter(array('format' => 'Parcerias %start% à %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($parcerias as $parceria): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="20%"><?php echo $parceria['Parceria']['titulo'];?></td>
					<td colspan="1" width="15%"><?php echo $parceria['Parceiro']['nome'];?></td>
					<td colspan="1" width="10%"><?php echo date('d/m/Y',strtotime($parceria['Parceria']['data_inicio'])); ?></td>
					<td colspan="1" width="10%">
						<?php 
							echo empty($parceria['Parceria']['data_fim']) ? "Sem limite" : date('d/m/Y',strtotime($parceria['Parceria']['data_fim'])); 
						?>
					</td>
					<td colspan="1" width="5%"><?php echo ($parceria['Parceria']['publica'] == 1) ? 'Sim' : 'Não';?></td>
					<td colspan="1" width="5.5%"><?php echo ($parceria['Parceria']['ativa'] == '1') ? 'Sim' : 'Não';?></td>
					<td colspan="1" width="35%">
						<?php 
							if (!$parceria['Parceria']['publica']) {
								echo $html->link('Vincular', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'vincular', $parceria['Parceria']['id']), array('class' => 'submit button'));
							} 
						?>
						
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'visualizar', $parceria['Parceria']['id']), array('class' => 'submit button')); ?>
						
						<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'editar', $parceria['Parceria']['id']), array('class' => 'submit button')); ?>
						
						<?php 
						if ($parceria['Parceria']['ativa'] == '1')
							echo $html->link('Desativar', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'desativar', $parceria['Parceria']['id']), array('class' => 'submit button')); 
						else
							echo $html->link('Ativar', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'ativar', $parceria['Parceria']['id']), array('class' => 'submit button')); 
						?>						
                                                <br/>
                                                <br/>
						<?php 
                                                echo $html->link('Relatório Visualizações', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'relatorio', $parceria['Parceria']['id']), array('class' => 'submit button')); 
                                                ?>
                                                </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
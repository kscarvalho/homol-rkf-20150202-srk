<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/chosen.css">

<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Parcerias
        </h2>
    </div>
</div>
<div id="conteudo-container" class="container-parcerias">
	<?php if (count($parcerias) == 0) { ?>
		<p>N&atilde;o existem parcerias ativas.</p>
	<?php } ?>
	<?php 
		$first = true;
		foreach($parcerias as $parceria):
			if ($first) {
	?>
				<div class="alpha grid_7 first detalhes">
		<?php } else {?>
				<div class="omega grid_7 detalhes">
			<?php } ?>
			
			<div class="grid_6 first alpha omega" style="margin-bottom: 15px">
				<?php if (!empty($parceria['Parceiro']['logo'])) {
						$logoParceiro = $html->image($caminho_raiz_logos.'/'.$parceria['Parceiro']['logo'], 
								array('style' => 'width: 230%; max-width: 120px; max-height: 100px;'));
						
						echo $html->link(
								$logoParceiro, 
								array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'visualizar', $parceria['Parceria']['id']), 
								array('escape' => false, 'class' => 'grid_6 alpha omega first'));
					}
				?>
			</div>
			
			<div class="grid_10 alpha omega">
			
				<h2>
					<?php 	
						echo $html->link(
								$parceria['Parceria']['titulo'], 
								array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'visualizar', $parceria['Parceria']['id']), 
								array('escape' => false, 'style' => 'color: #D31217; font-weight: bold;'));
					?>
				</h2>
				
				<p>
					<label style="color: #D31217; font-size: 9pt;">PROMO&Ccedil;&Atilde;O:</label>
					<?php 
						echo $parceria['Parceria']['descricao'];
					?>
				</p>
			</div>
			
			
			<div class="grid_full alpha omega first">
				<p class="grid_5 alpha omega">
					<label class="grid_full alpha omega" style="font-variant: small-caps">Data de In&iacute;cio</label>
					<span class="grid_full alpha first"> <?php echo date('d/m/Y', strtotime($parceria['Parceria']['data_inicio'])); ?> </span>
				</p>
				<p class="grid_4 alpha omega">
					<label class="grid_full alpha omega" style="font-variant: small-caps">Data de Fim</label>
					<span class="grid_full alpha first"> 
						<?php echo ($parceria['Parceria']['data_fim'] == "") ? "-" : date('d/m/Y', strtotime($parceria['Parceria']['data_fim'])); ?> 
					</span>
				</p>
				<?php if(!empty($parceria['Parceria']['valor'])) : ?>
				<p class="grid_4 omega">
					<label class="grid_full alpha omega" style="font-variant: small-caps">Valor</label>
					<span class="grid_full alpha first">R$ <?php echo $parceria['Parceria']['valor'] ?> </span>
				</p>
				<?php endif; ?>
				<p class="grid_4 alpha omega">
					<label class="grid_full alpha omega" style="font-variant: small-caps">Desconto</label>
					<span class="grid_full alpha first"> <?php echo $parceria['Parceria']['desconto'] ?> </span>
				</p>
			</div>
			
			<p class="grid_full alpha omega first">
				<label style="display: inline">Local:</label>
				<span><?php echo $parceria['Parceria']['local'] ?> </span>
			</p>
			<p class="grid_full alpha omega first" style="margin-bottom: 3px">
				<?php echo $html->link('Adquira j&aacute; o seu voucher', 
						array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'voucher', $parceria['Parceria']['id']), 
						array('escape' => false, 'class' => 'fundo-vermelho', 'target' => '_blank'), false); 
				?>
				<br />
				<br />
				<?php echo $html->link('<span class="icone-mais">+</span> INFORMA&Ccedil;&Otilde;ES', 
						array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'visualizar', $parceria['Parceria']['id']), 
						array('escape' => false, 'style' => 'font-weight: bold;'), false); 
				?>
			</p>
		</div>
	<?php 
		$first = !$first;
		endforeach; ?>
</div>
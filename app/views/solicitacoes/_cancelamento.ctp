<?=$html->css('messages');?>
<script type="text/javascript">
	jQuery(function($) {
		var dadosBancarios = {"nome-titular" : "Nome do Titular","cpf-titular" : "CPF do Titular","banco" : "Banco","agencia" : "Agencia","conta" : "Conta"};
 		$(".finalizar").click(function(e) {
 	 		e.preventDefault();
 	 		if("<?=$protocoloCancelamento['Protocolo']['status']?>" != "finalizado" && "<?=$dadosBancarios?>" == "1") {
	 	 		var erros = "";
	 			$.each(dadosBancarios,function(i,value) {
	 	 			if($('.'+i).attr('class') == undefined)
	 	 	 			erros+= value + "<br />";
	 			});
	 			if(erros != "") {
	 	 			$('#message').html("<h3>Erro, complete os campos a seguir</h3><br />" + erros);
	 	 			$("#message").addClass('error');
	 	 			$("#message").fadeIn(500);
	 			}
 	 		} else if(<?=$saldoCancelamento?> > 10) {
 	 			$('#message').html("<h3>Erro</h3><br />O Formando tem saldo a liquidar.");
 	 			$("#message").addClass('error');
 	 			$("#message").fadeIn(500);
 	 		}
		});
		$('.isento').click( function(event) {
			event.preventDefault();
			var td = $(this).parent();
			var html = "<div style='float:left; width:100%; margin:10px 0 0 0; display:none' class='isento-obs'>Observações:<br />";
			html+= "<textarea name='obs-isento' style='width:60%'></textarea><br /><br />";
			html+= "<input type='button' value='Finalizar Protocolo' class='submit button' id='enviar-isento' /></div>";
			td.html(html);
			$('.isento-obs').fadeIn(500);
		});
		$('#enviar-isento').live('click', function(event) {
			event.preventDefault();
			jQuery.colorbox({
				html : '<p id="p-colorbox">Aguarde</p>',
				overlayClose : false,
				escKey : false,
				width: 600,
				height: 500,
				onCleanup : function() { jQuery('#cboxClose').hide(); },
				onComplete : function() {
					jQuery.ajax({
						url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "solicitacoes", "action" => "cancelamento_isento"));?>",
						type : 'POST',
						data : { protocolo : '<?=$protocoloCancelamento['Protocolo']['protocolo']?>', obs : jQuery('textarea[name="obs-isento"]').val() },
						dataType : 'json',
						error: function(response) {
							jQuery('#p-colorbox').html('Ocorreu um erro ao solicitar o cancelamento. Por favor entre em contato conosco.');
							jQuery('#cboxClose').show();
						},
						success : function(response) {
							jQuery('#cboxClose').show();
							if(response.error)
								jQuery('#p-colorbox').html(response.message);
							else
								jQuery('#p-colorbox').html('Protocolo finalizado com sucesso!');
						}
					});
				},
				onClosed : function() {
					window.location.href = window.location.href;
				}
			});
		});
 	});
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Solicitações - Cancelamento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div style="clear:both;"></div>
	<div style="overflow:hidden; width:100%">
		<div class="legenda" style="width:40%">
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td colspan="2"><h2>Formando - Cancelamento de Contrato</h2></td>
							</tr>
							<tr>
								<td style='padding:10px 0 10px 0' colspan="2">
									<em>
									Esse protocolo foi aberto em <?=date('d/m/Y', strtotime($protocoloCancelamento['Protocolo']['data_cadastro']));?>
									&agrave;s <?=date('H:i', strtotime($protocoloCancelamento['Protocolo']['data_cadastro']));?> 
									pelo(a) <?=$usuarioCriador['Usuario']['nome']?>
									</em>
								</td>
							</tr>
							<tr>
								<td width="100">Protocolo</td>
								<td><em><?=$protocoloCancelamento['Protocolo']['protocolo']?></em></td>
							</tr>
							<tr>
								<td width="100">Nome</td>
								<td><em><?=$formando['Usuario']['nome']?></em></td>
							</tr>
							<tr>
								<td>Código</td>
								<td><em><?=$formando['Turma'][0]['id'].str_pad($formando['FormandoProfile']['codigo_formando'],3,'0',STR_PAD_LEFT)?></em></td>
							</tr>
							<tr>
								<td>Status</td>
								<td><em style='text-transform:capitalize;'><?=$protocoloCancelamento['Protocolo']['status']?></em></td>
							</tr>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['nome_titular'] != "") : ?>
							<tr>
								<td>Nome do Titular</td>
								<td class='dados-bancarios nome-titular' title='Nome do Titular'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['nome_titular']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['cpf_titular'] != "") : ?>
							<tr>
								<td>CPF do Titulat</td>
								<td class='dados-bancarios cpf-titular' title='CPF do Titular'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['cpf_titular']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['banco'] != "") : ?>
							<tr>
								<td>Banco</td>
								<td class='dados-bancarios banco' title='Banco'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['banco']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['agencia'] != "") : ?>
							<tr>
								<td>Agência</td>
								<td class='dados-bancarios agencia' title='Ag&ecirc;ncia'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['agencia']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['conta'] != "") : ?>
							<tr>
								<td>Conta</td>
								<td class='dados-bancarios conta' title='Conta'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['conta']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['ProtocoloCancelamento']['observacao'] != "") : ?>
							<tr>
								<td>Observação</td>
								<td title='Observação'><em><?=$protocoloCancelamento['ProtocoloCancelamento']['observacao']?></em></td>
							</tr>
							<?php endif; ?>
							<?php if($protocoloCancelamento['Protocolo']['status'] != "finalizado") { ?>
							<tr>
								<?php if($saldoCancelamento > 10) : ?>
								<td style='color:red'>Saldo Negativo</td>
								<td>
									<em style='color:red'>R$ <?=$saldoCancelamento?></em>
									&nbsp;&nbsp;&nbsp;
									<?php
									if($protocoloCancelamento['Protocolo']['status'] != 'finalizado')
										echo $html->link('Gerar Boleto Cancelamento',
												array($this->params['prefix'] => true, 'controller' => 'area_financeira',
														'action' => 'gerar_boleto_cancelamento',
														$formando['FormandoProfile']['usuario_id']),
												array('class' => 'submit botao-gerar-boleto-cancelamento button', 'target' => '_blank'));
									?>
								</td>
								<?php elseif($saldoCancelamento < 0) : ?>
								<td style='color:green'>Saldo Positivo</td>
								<td>
									<em style='color:green'>
										R$ <?=$saldoCancelamento*-1?>
										<?=$protocoloCancelamento['Protocolo']['status'] == 'finalizado' ? ' - PAGO' : ""?>
									</em>
								</td>
								<?php endif; ?>
							</tr>
							<?php if($dadosBancarios === FALSE && $protocoloCancelamento['ProtocoloCancelamento']['ordem_pagamento'] == 1) : ?>
							<tr>
								<td colspan='2' style='padding-top:15px'><a href='/<?=$this->params['prefix']?>/area_financeira/reverter_cancelamento/<?=$formando['FormandoProfile']['usuario_id']?>' class='submit button reverter'>Reverter Cancelamento</a></td>
							</tr>
							<?php endif; ?>
							<?php if($dadosBancarios === FALSE && $this->params['prefix'] == 'atendimento') : ?>
							<tr>
								<td colspan='2' style='padding-top:15px'><input type='button' class='submit button isento' value='Isento' /></td>
							</tr>
							<?php endif; ?>
							<?php if($dadosBancarios === TRUE && $protocoloCancelamento['ProtocoloCancelamento']['ordem_pagamento'] == 0 && $this->params['prefix'] == 'atendimento') : ?>
							<tr>
								<td colspan='2' style='padding-top:15px'>
									<a target='_blank' href='/<?=$this->params['prefix']?>/area_financeira/ordem_pagamento/<?=$protocoloCancelamento['Protocolo']['protocolo']?>' class='submit button orderm-pagamento'>
										Ordem De Pagamento
									</a>
								</td>
							</tr>
							<?php endif; ?>
							<tr>
								<td colspan="2" style='color:red'>
									<br />
									<?php if($saldoCancelamento < 0) { ?>
									Nossa equipe de atendimento entrar&aacute; em contato ap&oacute;s efetuar o dep&oacute;sito do seu cr&eacute;dito
									<?php } elseif($saldoCancelamento < 10) { ?>
									Nossa equipe de atendimento est&aacute; analizando seu contrato e entrar&aacute; em contato ap&oacute;s a conclus&atilde;o da an&aacute;lise
									<?php } ?>
								</td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan='2'>
									<div class="message" id='message' style='display:none'>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<?php if($dadosBancarios === TRUE && $protocoloCancelamento['ProtocoloCancelamento']['ordem_pagamento'] == 0) { ?>
		<div class="legenda" style="width:40%">
		<?php echo $form->create('Solicitacao', array('url' => "/{$this->params['prefix']}/solicitacoes/cancelamento/{$protocoloCancelamento['Protocolo']['protocolo']}", 'style' => 'display:block;')); ?>
		<p class="grid_16 alpha omega">
			<label class="grid_11 alpha omega">Nome do Titular</label>
			<?php echo $form->input('nome_titular', array('value' => $protocoloCancelamento['ProtocoloCancelamento']['nome_titular'],'class' => 'grid_7 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>
		<p class="grid_16 alpha omega">
			<label class="grid_11 alpha omega">CPF do Titular</label>
			<?php echo $form->input('cpf_titular', array('value' => $protocoloCancelamento['ProtocoloCancelamento']['cpf_titular'],'class' => 'grid_7 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>
		<p class="grid_16 alpha omega">
			<label class="grid_11 alpha omega">Banco</label>
			<?php echo $form->input('banco', array('value' => $protocoloCancelamento['ProtocoloCancelamento']['banco'],'class' => 'grid_7 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>
		<p class="grid_16 alpha omega">
			<label class="grid_11 alpha omega">Agência</label>
			<?php echo $form->input('agencia', array('value' => $protocoloCancelamento['ProtocoloCancelamento']['agencia'],'class' => 'grid_7 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>
		<p class="grid_16 alpha omega">
			<label class="grid_11 alpha omega">Conta</label>
			<?php echo $form->input('conta', array('value' => $protocoloCancelamento['ProtocoloCancelamento']['conta'],'class' => 'grid_7 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>
		<p class="grid_7 alpha omega">
			<?php echo $form->end(array('label' => "Salvar", 'div' => false, 'class' => 'submit'));?>
		</p>
		</div>
		<?php } ?>
		<div style="clear:both;"></div>
	</div>
</div>
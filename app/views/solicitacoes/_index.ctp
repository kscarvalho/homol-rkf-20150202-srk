<span id="conteudo-titulo" class="grid_full box-com-titulo-header">Solicitações</span>
<div id="conteudo-container" style="margin-left:20px">
	<?php 
		$session->flash();
		echo $form->create('Solicitacoes', array('url' => "/{$this->params['prefix']}/solicitacoes/"));
	?>
	<div style="width:400px">
		<p class="grid_full alpha omega">
			<label class="grid_full alpha omega">Protocolo</label>
			<?php echo $form->input('protocolo', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false)); ?>
		</p>
		<p class="grid_full alpha omega">
			<?php echo $form->end(array('label' => 'Buscar', 'div' => false, 'class' => 'submit'));?>
		</p>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/utils.js?v=0.1"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<style type="text/css">
.dl-horizontal dt { text-align:left }
#parcelas .label { padding:4px 8px; font-size:15px; font-weight:200;
    line-height: 16px; margin-bottom:5px }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $("#continuar").click(function(e) {
            e.preventDefault();
            $("#form").submit();
        });
        $(".chosen").chosen({width:'100%',dropdown_position: "top"});
        
        var planos = <?=json_encode($planos)?>;
        var parcelamentos = <?=json_encode($parcelamentos)?>;
        var planoSelecionado = '', mesAnoSelecionado = '';
        
        function listarMeses(plano) {
            planoSelecionado = plano;
            var select = $(".meses").children('.chosen');
            $("#parcelas").html('');
            select.html('<option value="">Selecione a data de início</option>').trigger("liszt:updated");
            $.each(parcelamentos[planoSelecionado],function(parcelamento) {
                select.append('<option value="'+parcelamento+'">'+parcelamento+'</option>');
            });
            select.trigger("liszt:updated");
            $(".meses").fadeIn(500);
        }
        
        function listarParcelas(mesAno) {
            mesAnoSelecionado = mesAno;
            $("#parcelas").html('');
            $.each(parcelamentos[planoSelecionado][mesAnoSelecionado],function(i,parcela) {
                valorTotal = number_format(parcela.valor,2,',','.');
                valorParcela = number_format(parcela.valor/parcela.parcelas,2,',','.');
                parcelas = str_pad(parcela.parcelas,2,'0','STR_PAD_LEFT');
                $("#parcelas").append("<span class='label label-warning'>" +
                    parcelas + " X de " + valorParcela + " (" + valorTotal +
                    ")</span><br />");
            });
        }
        
        $(".planos > .chosen").on('change',function() {
            listarMeses($(this).val());
        });
        
        $(".meses > .chosen").on('change',function() {
            listarParcelas($(this).val());
        });
        
        if(planos.length < 1)
            $("#formas-pagamento").hide();
        else if(planos.length == 1) {
            $(".planos").children('.chosen').next().fadeOut(500,function() {
                $(".planos").find('label').text(planos[0]);
                listarMeses(planos[0]);
            });
        } else {
            var select = $(".planos").children('.chosen');
            select.html('<option value="">Selecione um Plano</option>').trigger("liszt:updated");
            $.each(planos,function(i,plano) {
                select.append('<option value="'+plano+'">'+plano+'</option>');
            });
            select.trigger("liszt:updated");
        }
    });
</script>
<h2>Dados Da Turma</h2>
<br />
<div class="alert alert-info">
    Para prosseguir com o cadastro confirme os dados da Turma
</div>
<br />
<dl class="dl-horizontal">
    <dt>Nome da Turma</dt>
    <dd><?=$turma['Turma']['nome']?></dd>
    <dt>Data de Conclusão</dt>
    <dd><?= $turma['Turma']['ano_formatura'] ?> <?= $turma['Turma']['semestre_formatura'] ?>&ordm; Semestre</dd>
    <br />
    <dt>Cursos Participantes</dt>
    <?php
        $indiceDeCursos = 1;
        foreach ($info['universidades'] as $universidade) {
            foreach ($universidade['faculdades'] as $faculdade) {
                foreach ($faculdade['cursos'] as $curso) { ?>
        <dd><?= $indiceDeCursos . " - " . $universidade['nome'] . " - " . $faculdade['nome'] . " - " . $curso; ?></dd>
    <?php } } $indiceDeCursos++; } ?>
</dl>
<br />
<?=$form->create('Turma',array('url' => "cadastrar",'id' => 'form')); ?>
<?=$form->hidden('Turma.id'); ?>
<a class="button bg-color-red" href="/cadastro">
    Dados Incorretos, Alterar Turma
</a>
<a class="button bg-color-green" id="continuar">
    Dados Corretos, Prosseguir Com o Cadastro
</a>
<?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide'));?>
<div class="row-fluid" id="formas-pagamento">
    <h2 class="fg-color-red">
        Formas de pagamentos dispon&iacute;veis
    </h2>
    <br />
    <div class="row-fluid">
        <div class="span4">
            <div class="row-fluid planos">
                <select class="chosen">
                </select>
                <label class="label label-info row-fluid"></label>
            </div>
            <br />
            <div class="row-fluid meses hide">
                <select class="chosen">
                </select>
            </div>
            <br />
            <br />
            <br />
        </div>
        <div class="span4 offset1" id="parcelas">
            
        </div>
    </div>
</div>
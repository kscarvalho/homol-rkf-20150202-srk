<?php if($formando) { ?>
<style type="text/css">
.img-perfil { width:64px; height:64px; max-height:64px; overflow: hidden; }
.img-perfil img { width:auto; height:auto; max-width: 100%; max-height: 100%; }
</style>
<?php
$fotoPerfil = "img/uknown_user.gif";
if (isset($formando['ViewFormandos']['diretorio_foto_perfil']))
    if (!empty($formando['ViewFormandos']['diretorio_foto_perfil']))
        if (file_exists(APP . "webroot/{$formando['ViewFormandos']['diretorio_foto_perfil']}"))
            $fotoPerfil = $formando['ViewFormandos']['diretorio_foto_perfil'];
?>
<h2 class='fg-color-red'>
    <div class='img-perfil pull-left'>
        <img src="<?="{$this->webroot}{$fotoPerfil}" ?>" />
    </div>
    <span class='label label-important pull-right'>
        <?=ucfirst($formando['ViewFormandos']['situacao'])?>
    </span>
    <?=$formando['ViewFormandos']['nome']?> 
    <small>
        (<?=$formando['ViewFormandos']['codigo_formando']?>)
        <p class='fg-color-darken' style='font-size:14px; margin-top:5px'>
            <?=$formando['ViewFormandos']['email']?>
        </p>
    </small>
</h2>
<br />
<div class='row-fluid'>
    <h4>Turma / Curso / Sala</h4>
    <h5><?=$turma['Turma']['id']?> <?=$turma['Turma']['nome']?> / <?=$curso['Curso']['nome']?> / <?=$formando['ViewFormandos']['sala']?></h5>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Tel Res</h4>
        <h5>
            <?=$formando['ViewFormandos']['tel_residencial'];?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Celular</h4>
        <h5>
            <?=$formando['ViewFormandos']['tel_celular'];?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Tel Com</h4>
        <h5>
            <?=$formando['ViewFormandos']['tel_comercial'];?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Nextel</h4>
        <h5>
            <?=$formando['ViewFormandos']['nextel'];?>
        </h5>
    </div>
</div>
<br />
<?php
$endereco = utf8_encode($formando['ViewFormandos']['end_rua']) . ' ' . $formando['ViewFormandos']['end_numero'];
$endereco .= ', ' . utf8_encode($formando['ViewFormandos']['end_bairro']);
$endereco .= ', ' . utf8_encode($formando['ViewFormandos']['end_cidade']);
$endereco .= ' - ' . $formando['ViewFormandos']['end_uf'];
$endereco .= ' - ' . $formando['ViewFormandos']['end_cep'];
?>
<div class='row-fluid'>
    <h4>Endere&ccedil;o</h4>
    <h5>
        <?=$endereco?>
    </h5>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>RG</h4>
        <h5>
            <?=$formando['ViewFormandos']['rg'];?>
        </h5>
    </div>
    <div class='span5'>
        <h4>CPF</h4>
        <h5>
            <?=$formando['ViewFormandos']['cpf'];?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Havaiana</h4>
        <h5>
            <?=$formando['ViewFormandos']['numero_havaiana'];?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Camiseta</h4>
        <h5>
            <?=$formando['ViewFormandos']['tam_camiseta'];?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Data Adesão</h4>
        <h5>
            <?=date('d/m/Y',strtotime($formando['ViewFormandos']['data_adesao']))?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Forma Pagamento</h4>
        <h5>
            <?=$formando['ViewFormandos']['forma_pagamento'];?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Nome Pai</h4>
        <h5>
            <?=$formando['ViewFormandos']['nome_pai'];?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Nome Mãe</h4>
        <h5>
            <?=$formando['ViewFormandos']['nome_mae'];?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span6'>
        <h4>Realizou Checkout</h4>
        <h5>
            <?=$formando['ViewFormandos']['realizou_checkout'] == 1 ? 'Sim' : 'Não'?>
        </h5>
    </div>
</div>
<?php } else { ?>
<h2 class='fg-color-red'>
    Formando N&atilde;o Encontrado
</h2>
<?php } ?>

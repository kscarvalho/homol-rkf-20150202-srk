<?php 
$listaDeFormandosParaExcel = array();
	foreach ($formandos as $formando)
		$listaDeFormandosParaExcel[] = array(
				"Codigo do formando" => $formando['ViewFormandos']['codigo_formando'],
                                "Nome" =>  $formando['ViewFormandos']['nome'],
				"Email" => $formando['ViewFormandos']['email'],
                                "Rua" => $formando['ViewFormandos']['end_rua'],
                                "Numero" => $formando['ViewFormandos']['end_numero'],
                                "Bairro" => $formando['ViewFormandos']['end_bairro'],
                                "Cep" => $formando['ViewFormandos']['end_cep'],
                                "Cidade" => $formando['ViewFormandos']['end_cidade'],
                                "UF" => $formando['ViewFormandos']['end_uf'],
				"Tel Residencial" => $formando['ViewFormandos']['tel_residencial'],
				"Tel Celular" => $formando['ViewFormandos']['tel_celular'],
                                "Mãe" => $formando['ViewFormandos']['nome_mae'],
				"Tel Mãe" => $formando['ViewFormandos']['tel_cel_mae'],
				"Pai" => $formando['ViewFormandos']['nome_pai'],
				"Tel Pai" => $formando['ViewFormandos']['tel_cel_pai'],
			);
	
	$excel->generate($listaDeFormandosParaExcel, 'lista_formandos_'.date('d_m_Y'));
?>
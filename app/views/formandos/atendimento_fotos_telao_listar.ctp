<span id="conteudo-titulo" class="box-com-titulo-header">Listar Fotos Telão</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?php echo $html->link('Gerar excel',array($this->params['prefix'] => true, 'action' => 'excel_fotos_telao'), array('id' => 'link-gerar-excel','target' => '_blank')); ?>
		<div style="clear:both;"></div>
	</div>
    <div class="container-tabela">
    	<table>
            <thead>
                <thead>
            <tr>
                <th scope="col" width="10%"><?=$paginator->sort('Cod', 'ViewFormandos.codigo_formando'); ?></th>
                <th scope="col" width="40%"><?=$paginator->sort('Nome', 'ViewFormandos.nome'); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Foto Crianca', 'foto_crianca'); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Foto Adulto', 'foto_adulto'); ?></th>
            </tr>
        </thead>
            </thead>
            <tfoot>
            <tr>
                <td colspan="3">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="1">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Linhas')); ?>
                </td>
            </tr>
        </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach($formandos as $formando) : ?>
                <?php if(!empty($formando['ViewFormandos']['data_adesao'])){ ?>
                <tr class="hover<?=$isOdd ? " odd" : ""?>">
                    <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                    <td><?=$formando['ViewFormandos']['nome']; ?></td>
                    <td style="text-align: center">
                        <?php if(!empty($formando[0]['foto_crianca'])){ ?>
                        <a class="button mini bg-color-red" href="<?=$this->webroot.$formando[0]['foto_crianca'];?>" target="_blank">
                            Exibir Foto
                        </a>
                        <?php } else { ?>
                        <span>-</span>
                        <?php } ?>
                    </td>
                    <td style="text-align: center">
                        <?php if(!empty($formando[0]['foto_adulto'])){ ?>
                        <a class="button mini bg-color-red" href="<?=$this->webroot.$formando[0]['foto_adulto'];?>" target="_blank">
                            Exibir Foto
                        </a>
                        <?php } else { ?>
                        <span>-</span>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
                <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
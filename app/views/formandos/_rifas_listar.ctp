<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Rifas utilizadas
        </h2>
    </div>
</div>

<div class="row-fluid">
                <?php if(count($formandos) > 0) :?>
        <table class="table table-condensed table-striped">
                <thead>
                        <tr>
                            <th scope="col"><?=$paginator->sort('Código Formando', 'codigo_formando', $sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('Nome', 'nome', $sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('Cupons', 'cupons', $sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('Vendidos', 'status', $sortOptions); ?></th>
                        </tr>
                </thead>
                <tbody>
                <?php foreach($formandos as $formando): ?>
                        <tr>
                            <td  collabel="1"><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                            <td  collabel="1"><?=$formando['ViewFormandos']['nome']; ?></td>
                            <td  collabel="1"><?=$formando['RifaTurma']['cupons'] ?></td>
                            <td  collabel="1"><?=$formando[0]['status']; ?></td>
                        </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                        </td>
                        <td colspan="1" class="pull-right">
                            <?=$paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?>
                        </td>
                    </tr>
                </tfoot>
        </table>
                <?php  else : ?>
                        <h2>Nenhum formando utilizou rifas.</h2>
                <?php endif; ?>
</div>
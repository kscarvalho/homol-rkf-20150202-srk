<div class="row-fluid">
    <div class="span4">
        <label class="required" for="cep">Cep</label>
        <?=
        $form->input('FormandoProfile.end_cep',array(
            'label' => false,
            'div' => 'input-control text span8 marginLeft0',
            'error' => false,
            'data-required' => 'true',
            'id' => 'cep',
            'alt' => 'cep',
            'after' => $buttonAfter));
        ?>
        <a class="button" id="busca-cep">Buscar CEP</a>
    </div>
</div>
<div class="row-fluid">
    <div class="span7">
        <label class="required">Rua</label>
        <?=$form->input('FormandoProfile.end_rua',array(
            'label' => false,
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false,
            'data-required' => 'true',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label class="required">N&ordm;</label>
        <?=$form->input('FormandoProfile.end_numero',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'data-required' => 'true',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span2">
        <label>Compl</label>
        <?=$form->input('FormandoProfile.end_complemento',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'after' => $buttonAfter)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <label class="required">Bairro</label>
        <?=$form->input('FormandoProfile.end_bairro',array(
            'label' => false,
            'div' => 'input-control text',
            'data-required' => 'true',
            'error' => false,
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span4">
        <label class="required">Cidade</label>
        <?=$form->input('FormandoProfile.end_cidade',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'data-required' => 'true',
            'after' => $buttonAfter)); ?>
    </div>
    <?=$form->input('FormandoProfile.end_uf',array(
        'label' => array('class' => 'required','text' => 'UF'),
        'div' => 'input-control text span4',
        'error' => false,
        'type' => 'select',
        'data-required' => 'true',
        'options' => $form->listaUf(),
        'class' => 'chosen')); ?>
</div>
<?=$form->hidden('aderir')?>
<?php
$diaVencimento = array();
$diasDoMes = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
for($i = 1; $i <= $diasDoMes; $i++)
    $diaVencimento[$i] = $i;
?>
<?php if($planoEconomico) : ?>
<script type="text/javascript">
    $(document).ready(function() {
        var plano_economico = <?=json_encode($this->data['plano_economico'])?>;
        $("#abrir-plano-economico").click(function() {
            abrirPlanoEconomico();
        });
        $("body").on('change','.economico-planos',function() {
            $('#FormandoProfilePlanos').val($(this).val()).trigger("liszt:updated");
            plano_economico.parcelas = [];
            montarParcelas();
        })
        $("body").on('change','.economico-parcelas',function() {
            parcelamento = $(this).find('option[value="'+$(this).val()+'"]')
                    .text();
            parcelas = parseInt(parcelamento.split('x').shift());
            adesao = parcelamento.split('(').pop().replace(')','');
            plano_economico.valor = parseFloat(stringMoneyToNumber(adesao));
            valorParcela = plano_economico.valor/parcelas;
            plano_economico.parcelas = [];
            for(a = 0; a < parcelas; a++)
                plano_economico.parcelas.push({valor:valorParcela});
            montarParcelas();
        })
        $("body").on('blur','.input-parcelas',function() {
            $(this).val(number_format(stringMoneyToNumber(
                    $(this).val()),2,',','.'));
            validaValorParcelas();
        })
        $("body").on('shown','.bootbox',function() {
            montarParcelas();
        })
        function abrirPlanoEconomico() {
            bootbox.dialog($("#div-plano-economico").html(),[{
                label: 'Confirmar',
                class: 'bg-color-blue',
                callback: function() {
                    enviarParcelasParaView();
                }
            },{
                label: 'Cancelar',
                class: 'bg-color-red',
                callback: function() {
                    removerParcelasDaView();
                }
            }]);
        }
        
        function montarParcelas() {
            $(".modal-body").find('.plano-pagamentos').fadeOut(500,function() {
                var div = this;
                $(div).html('<label>&nbsp;</label>');
                var dataInicio = $('#FormandoProfileDataInicioPagamento')
                        .val().split('-');
                var data = new Date();
                data.setDate($("#FormandoProfileDiaVencimentoParcelas").val());
                data.setMonth(dataInicio[1]-1);
                data.setFullYear(dataInicio[0]);
                parcelas = plano_economico.parcelas.length;
                if(parcelas > 3)
                    var parcelasNaoEditaveis = 3;
                else
                    var parcelasNaoEditaveis = parcelas;
                $.each(plano_economico.parcelas,function(i,parcela) {
                    options = {
                        class: 'input-parcelas',
                        type: 'text',
                        value:number_format(parcela.valor,2,',','.')
                    };
                    if(parcelas-i <= parcelasNaoEditaveis)
                        options.readonly = 'readonly';
                    input = $("<input>",options);
                    mes = data.getMonth()+1;
                    dataVencimento = "&nbsp;"+data.getDate()+"/"+mes+
                            "/"+data.getFullYear();
                    data.setMonth(data.getMonth()+1);
                    span = $("<span>",{class:'help-inline',html:dataVencimento});
                    $(div).append(input).append(span);
                });
                $(div).fadeIn(500);
            })
        }
        
        function validaValorParcelas() {
            parcelas = plano_economico.parcelas.length;
            plano_economico.parcelas = [];
            if(parcelas > 3)
                var parcelasNaoEditaveis = 3;
            else
                var parcelasNaoEditaveis = parcelas;
            var valorMinimo = 50;
            var total = 0;
            $(".input-parcelas:visible").each(function(i) {
                valorInput = parseFloat(stringMoneyToNumber($(this).val()));
                if(valorInput < valorMinimo)
                    valorInput = valorMinimo;
                if(parcelas-i <= parcelasNaoEditaveis) {
                    valorInput = parseFloat((plano_economico.valor-total)/parcelasNaoEditaveis);
                    parcelasNaoEditaveis--;
                }
                valorInput = parseFloat(valorInput.toFixed(2));
                total+= valorInput;
                plano_economico.parcelas.push({valor:valorInput});
                $(this).val(number_format(valorInput,2,',','.'));
            })
        }
        
        function enviarParcelasParaView() {
            $(".input-plano-economico").remove();
            $.each(plano_economico.parcelas,function(i) {
                $("<input>",{
                    name:'data[plano_economico][parcelas][][valor]',
                    class: 'input-plano-economico',
                    type: 'hidden',
                    value:this.valor
                }).appendTo('#formulario');
            });
            $("<input>",{
                name:'data[plano_economico][valor]',
                class: 'input-plano-economico',
                type: 'hidden',
                value:plano_economico.valor
            }).appendTo('#formulario');
            if($("#FormandoProfileParcelas > option[value='plano_economico']").length == 0)
                $("#FormandoProfileParcelas")
                    .append('<option value="plano_economico">Condição Especial</option>');
            $("#FormandoProfileParcelas")
                .val('plano_economico')
                .trigger("liszt:updated");
        }
        
        function removerParcelasDaView() {
            plano_economico.parcelas = [];
            $(".plano-pagamentos").html('');
            $(".input-plano-economico").remove();
            if($("#FormandoProfileParcelas").val() == 'plano_economico') {
                $(".formando-parcelas").val('');
                $("#FormandoProfileParcelas").trigger("liszt:updated");
            }
        }
    
        $("#FormandoProfileParcelas").change(function(e) {
            console.log('sadf');
            if($(this).val() == 'plano_economico') {
                e.preventDefault();
                if(plano_economico.parcelas.length > 0)
                    enviarParcelasParaView();
                else
                    abrirPlanoEconomico();
            }
        })
    
        if('<?=$this->data['FormandoProfile']['parcelas']?>' == 'plano_economico')
            enviarParcelasParaView();
    })
</script>
<div class="hide" id="div-plano-economico">
    <h2 class="fg-color-red">Condição Especial</h2>
    <br />
    <div style="width:100%">
        Prezado formando, a condição especial permite que você escolha quanto irá pagar em cada mês.
    </div>
    <br />
    <div class="alert alert-danger">
        As últmas 3 parcelas não podem ter os valores alterados
        <br />
        O valor mínimo de cada parcela é R$50,00
    </div>
    <br />
    <div class="row-fluid" style="max-width:570px">
        <div class="span6" id="plano-opcoes">
            <label>Planos</label>
            <select class="formando-planos economico-planos"></select>
            <label>Parcelas</label>
            <select class="formando-parcelas economico-parcelas"></select>
        </div>
        <div class="span6 plano-pagamentos">
            
        </div>
    </div>
</div>
<?php endif; ?>
<div class="row-fluid">
    <br />
    <h2 class="fg-color-red">
        Dados financeiro de adesão
        <?php if($planoEconomico) : ?>
        <a class="button default pull-right" id="abrir-plano-economico">
            Condição Especial
        </a>
        <?php endif; ?>
    </h2>
    <br />
</div>
<?php if($turma['Turma']['id'] == 6028){
    $diaVencimento = array('5' => '5', '20' => '20');
}?>
<?php if($turma['Turma']['id'] == 6631){
    $diaVencimento = array('10' => '10', '20' => '20');
}?>
<div class="row-fluid">
    <div class="row-fluid com toggle-adesao">
        <div class="row-fluid">
            <?=$form->input('FormandoProfile.dia_vencimento_parcelas',array(
                'label' => array('text' => 'Dia Vencimento','class' => 'required'),
                'div' => 'input-control text span2',
                'error' => false,
                'type' => 'select',
                'data-conditional' => 'pagamento',
                'data-required' => 'false',
                'options' => $diaVencimento,
                'class' => 'chosen')); ?>
            <?=$form->input('FormandoProfile.data_inicio_pagamento',array(
                'label' => array('text' => 'Data Inicio Pagamento','class' => 'required'),
                'div' => 'input-control text span4',
                'error' => false,
                'type' => 'select',
                'data-conditional' => 'pagamento',
                'options' => $select_data_inicio_pagamento,
                'class' => 'chosen')); ?>
            <?=$form->input('FormandoProfile.forma_pagamento',array(
                'label' => array('text' => 'Forma de Pagamento','class' => 'required'),
                'div' => 'input-control text span6',
                'error' => false,
                'type' => 'select',
                'data-required' => 'false',
                'data-conditional' => 'pagamento',
                'options' => $select_forma_pagamento,
                'class' => 'chosen')); ?>
        </div>
        <div class="row-fluid">
            <?=$form->input('FormandoProfile.planos',array(
                'label' => array('text' => 'Planos','class' => 'required'),
                'div' => 'input-control text span6',
                'error' => false,
                'type' => 'select',
                'data-placeholder' => 'Selecione Um Plano',
                'options' => array(),
                'data-conditional' => 'pagamento',
                'data-description' => 'notEmpty',
                'data-required' => 'false',
                'data-describedby' => 'FormandoProfilePlanos',
                'data-error' => 'Selecione um Plano',
                'class' => 'chosen formando-planos')); ?>
            <?=$form->input('FormandoProfile.parcelas',array(
                'label' => array('text' => 'Parcelas','class' => 'required'),
                'div' => 'input-control text span6',
                'error' => false,
                'type' => 'select',
                'data-conditional' => 'pagamento',
                'data-required' => 'false',
                'data-description' => 'notEmpty',
                'data-describedby' => 'FormandoProfileParcelas',
                'data-error' => 'Selecione um Parcelamento',
                'data-placeholder' => 'Selecione a Data de Inicio',
                'options' => array(),
                'class' => 'chosen formando-parcelas')); ?>
        </div>
    </div>
    <div class="row-fluid sem toggle-adesao hide">

    </div>
</div>
<?php if($turma['Turma']['cadastro_sem_adesao'] == 1) : ?>
<div class="row-fluid">
    <div class="span4 com toggle-adesao">
        <button class="command-button bg-color-red default com toggle-adesao">
            Completar Cadastro Sem Ades&atilde;o
            <small>Sem Dados de Pagamento</small>
        </button>
    </div>
    <div class="span4 offset4 sem toggle-adesao pull-right">
        <button class="command-button bg-color-blue default sem toggle-adesao" type="button">
            Completar Cadastro Com Ades&atilde;o
            <small>Inserir Dados de Pagamento</small>
        </button>
    </div>
</div>
<?php endif; ?>

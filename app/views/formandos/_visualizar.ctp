
<span id="conteudo-titulo" class="box-com-titulo-header">Formandos</span>

<div id="conteudo-container painel_formando">
	<?php $session->flash(); ?>
	<!-- Container da esquerda -->
	<div class='grid_8 first'>
		<div class='grid_full painel_formando'>
			<span class="painel_titulo">
				Dados Pessoais
			</span>
			
			<span class="foto_formando">
				<?php echo $html->image('/' . $formando['FormandoProfile']['diretorio_foto_perfil'], array("alt" => "Brownies", 'class' => 'img_formando', 'escape' => false)); ?>
			</span>
			
			<p class="info_formando">
				<span class="nome_formando">
					<?php echo $formando['Usuario']['nome']; ?>
				</span>
				<span class="codigo_formando">
					<?php
					$codigo_formando = $formando['CursoTurma']['Turma']['id'] . str_pad($formando['FormandoProfile']['codigo_formando'],3,'0',STR_PAD_LEFT);
					$codigo_formando = str_pad($codigo_formando, 8, '0', STR_PAD_LEFT); 
					echo  "Cód.: " . $codigo_formando; 
					?>
				</span>
				
				<span class="email_formando">
					<?php echo $formando['Usuario']['email']; ?>
				</span>
				
				<span class="data_nascimento_sexo">
					<?php
					
					$data_nascimento = $formando['FormandoProfile']['data_nascimento'];

					
					if($formando['FormandoProfile']['sexo'] == 'M') {
						$sexo = 'Masculino';
					} else if($formando['FormandoProfile']['sexo'] == 'F') {
						$sexo = 'Feminino';
					} else { 
						$sexo = '';
					}
					
					if($sexo != '')
						echo $data_nascimento . ' - ' . $sexo; 
					else
						echo $data_nascimento	
					?>
				</span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_6 alpha first">Turma</label>
				<label class="grid_5 alpha">Curso</label>
				<label class="grid_5 alpha">Sala</label>
				<span class="grid_6 alpha first"><?php echo $formando['CursoTurma']['Turma']['nome']. ' (' . $formando['CursoTurma']['Turma']['id'] . ')';?></span>
				<span class="grid_5 alpha"><?php echo $formando['CursoTurma']['Curso']['nome'];?></span>
				<span class="grid_5 alpha"><?php echo $formando['FormandoProfile']['sala'];?></span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_full alpha">Realizou checkout?</label>
				<span class="grid_full alpha first"><?php echo ($formando['FormandoProfile']['realizou_checkout'] == 0) ? "N&atilde;o" : "Sim";?></label>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">Tel. Celular</label>
				<label class="grid_8 alpha">Tel. Residencial</label>

				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['tel_celular'];?></span>
				<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['tel_residencial'];?></span>

			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">Tel. Comercial</label>
				<label class="grid_8 alpha">Nextel</label>
				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['tel_comercial'];?></span>
				<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['nextel'];?></span>
			</p>

			<p class="grid_full alpha omega">
				<label class="grid_full alpha first">Endereço</label>
				<span class="grid_full alpha first">
					<?php
						$endereco = utf8_encode($formando['FormandoProfile']['end_rua']) . ' ' . $formando['FormandoProfile']['end_numero']; 
						 
						
						if($formando['FormandoProfile']['end_complemento']) {
							$endereco .= ', ' . $formando['FormandoProfile']['end_complemento'];
						}
						
						$endereco .= ', ' . utf8_encode($formando['FormandoProfile']['end_bairro']);
						$endereco .= ', ' . utf8_encode($formando['FormandoProfile']['end_cidade']);
						$endereco .= ' - ' . $formando['FormandoProfile']['end_uf'];
						if(strlen($formando['FormandoProfile']['end_cep']) == 8 )  {
							$endereco .= ' - ' . substr($formando['FormandoProfile']['end_cep'],0,5) . '-' . substr($formando['FormandoProfile']['end_cep'],5,3) ;
						}
						echo  $endereco;
					?>
				</span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">RG</label>
				<label class="grid_8 alpha">CPF</label>
				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['rg'];?></span>
				<span class="grid_8 alpha">
					<?php echo 
					substr($formando['FormandoProfile']['cpf'],0,3) . '.' . 
					substr($formando['FormandoProfile']['cpf'],3,3) . '.' .
					substr($formando['FormandoProfile']['cpf'],6,3) . '-' .  
					substr($formando['FormandoProfile']['cpf'],9,2)
					;?>
				</span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">Mãe</label>
				<label class="grid_8 alpha">Pai</label>
				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['nome_mae'];?><br><?php echo $formando['FormandoProfile']['tel_mae'];?></span>
				<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['nome_pai'];?><br><?php echo $formando['FormandoProfile']['tel_pai'];?></span>
			</p>
			
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">Camiseta</label>
				<label class="grid_8 alpha">Havaiana</label>
				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['tam_camiseta'];?></span>
				<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['numero_havaiana'];?></span>
			</p>
			
			
		</div>
		
		<div class='grid_full first painel_formando'>
			<span class="painel_titulo">
				Protocolos
			</span>
			<?php foreach($protocolo as $protocolos) :?>
			<p class="grid_full alpha omega painel_formando_financeiro">
				<label class="grid_4 alpha first">Nº Protocolo:</label>
				<span class="grid_4 alpha "><?php echo $protocolos['Protocolo']['protocolo']?></span><br>
				
				<label class="grid_4 alpha first">Tipo:</label>
				<span class="grid_4 alpha "><?php echo ucfirst($protocolos['Protocolo']['tipo'])?></span>

				<label class="grid_4 alpha ">Status:</label>
				<span class="grid_4 alpha "><?php echo ucfirst($protocolos['Protocolo']['status'])?></span>					
			</p>
			<?php endforeach; ?>
		</div>
	</div>
	<div class='grid_8' >
		<div class='grid_full painel_formando' >
			<span class="painel_titulo">
				Dados Financeiros
			</span>
			
			<p class="grid_full alpha omega painel_formando_financeiro">
				<label class="grid_5 alpha first">Situação</label>
				<span class="grid_8 alpha"><?php echo ucfirst($formando['FormandoProfile']['situacao']);?></span>
				<label class="grid_5 alpha first">Data de Adesão</label>
				<span class="grid_8 alpha">
					<?php 
					if(isset($formando['FormandoProfile']['data_adesao'])) {
						echo date('d/m/Y', strtotime($formando['FormandoProfile']['data_adesao']));
					} else {   
						echo '-';
					}
					?>
				</span>				
				<label class="grid_5 alpha first">Total Pago:</label>
				<span class="grid_8 alpha "><?php echo $total_pago; ?></span>
				<label class="grid_5 alpha first">Total Despesas:</label>
				<span class="grid_8 alpha "><?php echo $total_despesas; ?></span>
				<label class="grid_5 alpha first">Saldo Atual:</label>
				<span class="grid_8 alpha "><?php echo $saldo_atual; ?></span>
			</p>
			
			<p class="grid_full alpha omega painel_formando_financeiro">
				
				<label class="grid_5 alpha first">Valor Total Adesão:</label>
				<span class="grid_8 alpha ">R$<?php echo $formando['FormandoProfile']['valor_adesao']; ?></span>
				
				<label class="grid_5 alpha first">Parcelas:</label>
				<span class="grid_8 alpha "><?php echo $formando['FormandoProfile']['parcelas_adesao'] . ' de R$' . number_format($formando['FormandoProfile']['valor_adesao']/$formando['FormandoProfile']['parcelas_adesao'],2, ',','.');?></span>
				
				<!--
				//TODO: Implementar registro de mesas e convites da adesão na turma
				<label class="grid_5 alpha first">Mesas Adesão:</label>
				<span class="grid_8 alpha ">1</span>
				
				<label class="grid_5 alpha first">Convites Adesão:</label>
				<span class="grid_8 alpha ">11</span>
				-->
				
			</p>
			
			<!--
			//TODO: IMPLEMENTAR 
			<p class="grid_full alpha omega painel_formando_financeiro">
				<label class="grid_8 alpha first">Festa de Formatura - Convite Extra:</label>
				<span class="grid_8 alpha ">3 pagos / 0 em aberto</span>
				<label class="grid_8 alpha first">Festa de Formatura - Mesa Extra:</label>
				<span class="grid_8 alpha ">0 pagos / 0 em aberto</span>
				<label class="grid_8 alpha first">Churrasco - Camisetas:</label>
				<span class="grid_8 alpha ">1 pagos / 0 em aberto</span>
				<label class="grid_8 alpha first">Cervejada - Caneca:</label>
				<span class="grid_8 alpha ">1 pagos / 2 em aberto</span>
				
			</p>
			-->
			
		</div>
	
		<!--
		//TODO: IMPLEMENTAR PAINEL OUTRAS INFORMACOES
		<div class='grid_full painel_formando'>
			<span class="painel_titulo">
				Outras Informações
			</span>
			
			<p class="grid_full alpha omega">
				<span class="correct">Foto do perfil</span>
				<span class="correct">Medida Smoking</span>
				<span class="wrong">Confirmação de presença na festa</span>
				<span class="correct">Perfil completo?</span>
				<span class="wrong">Fotos de criança e atual</span>
			</p>

		</div>
		-->
	</div>
</div>
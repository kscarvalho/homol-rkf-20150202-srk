<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Pagamentos efetuados<?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>

<div class="conteudo-container grid_16 first checkout-secao-container" id='secao-pagamentos'>	
	<br/>
		<?php 
			$totalPago = 0;
			if (count($pagamentosEfetuados) > 0) {
					
		?>
		<div class="container-tabela-financeiro first">
			<table class="tabela-financeiro tabela-boletos"	id="lista-pagamentos-efetuados">
				<thead>
					<tr>			
                    	<th scope="col">Tipo</th>
	                    <th scope="col">Valor pago</th>
						<th scope="col">Data de liquida&ccedil;&atilde;o</th>
						<th scope="col">Oservações</th>
					</tr>
				</thead>
				<tbody>
				<?php $isOdd = false;?>
				<?php foreach($pagamentosEfetuados as $pagamentoEfetuado) { 
					$totalPago += $pagamentoEfetuado['Pagamento']['valor_pago'];
				?>
							<?php if($isOdd):?>
							<tr class="odd">
							<?php else:?>
							<tr>
							<?php endif;?>
								<td width="10%"><?php echo $pagamentoEfetuado['Pagamento']['tipo'];?></td>
								<td width="10%"><?php echo $pagamentoEfetuado['Pagamento']['valor_pago'];?></td>
								<td width="10%"><?php echo date('d/m/Y', strtotime($pagamentoEfetuado['Pagamento']['dt_liquidacao']));?></td>	
								<td width="70%"><?php echo $pagamentoEfetuado['Pagamento']['observacao'];?></td>	
							</tr>
							<?php $isOdd = !($isOdd); ?>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<?php } else { ?>
			<span>N&atilde;o existem pagamentos contabilizados.</span>
		<?php } ?>
</div>
<?php 
	$listaDeFormandosParaExcel = array();

	foreach ($formandos as $formando)
		$listaDeFormandosParaExcel[] = array(
				"Codigo do formando" => str_pad($turmaLogada['Turma']['id'], 5, 0 , STR_PAD_LEFT) . str_pad($formando['FormandoProfile']['codigo_formando'], 4, 0 , STR_PAD_LEFT)." ",
				"Nome" =>  $formando['Usuario']['nome'],
                                "Nome Telão" =>  $formando['FormandoProfile']['nome_telao'],
				"Email" => $formando['Usuario']['email'],
				"RG" => $formando['FormandoProfile']['rg'],
				"CPF" => $formando['FormandoProfile']['cpf'],
				"Endereco" => "Rua: ".$formando['FormandoProfile']['end_rua']." - ".$formando['FormandoProfile']['end_numero']." ".$formando['FormandoProfile']['end_complemento']." - Bairro: ".$formando['FormandoProfile']['end_bairro']." - CEP: ".$formando['FormandoProfile']['end_cep']." - Cidade: ".$formando['FormandoProfile']['end_cidade']." - UF: ".$formando['FormandoProfile']['end_uf'],
				"Data Adesao" => $formando['FormandoProfile']['data_adesao'],
				"Numero Havaiana" => $formando['FormandoProfile']['numero_havaiana'],
				"Tamanho da Camiseta" => $formando['FormandoProfile']['tam_camiseta'],
                                "Tamanho do Moleton" => $formando['FormandoProfile']['tam_moleton'],
				"Turma" => $turmaLogada['Turma']['nome'],
				"Curso" => 	((isset($formando['CursoTurma']['Curso'])) ? $formando['CursoTurma']['Curso']['nome'] : "-"),
				"Sala" => $formando['FormandoProfile']['sala'],
				"Tel Residencial" => $formando['FormandoProfile']['tel_residencial'],
				"Tel Celular" => $formando['FormandoProfile']['tel_celular'],
				"Mae" => $formando['FormandoProfile']['nome_mae'],
				"Pai" => $formando['FormandoProfile']['nome_pai'],
				"Situacao" => $formando['FormandoProfile']['situacao'],
				"Forma de Pagamento" => str_replace("_"," ", $formando['FormandoProfile']['forma_pagamento']),
				"Realizou Checkout?" => (($formando['FormandoProfile']['realizou_checkout'] == 0) ? "Nao" : "Sim")
			);
	
	$excel->generate($listaDeFormandosParaExcel, 'lista_formandos_turma_'.$turmaLogada['Turma']['id']);
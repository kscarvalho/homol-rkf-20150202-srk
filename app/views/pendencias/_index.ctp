<?=$html->script('jquery.rotate2.2');?>
<style type='text/css'>
.pendencias { width:100%; float:left; font-size:14px; margin-top:10px }
.pendencias ul.ul-sessao { position:relative; width:100%; float:left; margin:0; padding:0 }
.pendencias ul.ul-sessao li.sessao { position:relative; maring:0; padding:5px; text-indent:10px; cursor:pointer; font-weight:bold }
.pendencias ul.ul-sessao li .seta { margin-left:5px; margin-right:5px; width:10px }
.pendencias .subsessao { margin:10px 0 10px 20px!important; display:none; font-size:12px }
.pendencias .subsessao ul.ul-subsessao li { margin-bottom:5px; font-weight:400 }
.pendencias .subsessao ul.ul-subsessao li a:hover { cursor:pointer; color:red }
</style>
<script type="text/javascript">
	jQuery(function($) {
 		$(".sessao").click(function() {
 			if($(this).hasClass('fechada')) {
 	 			$(this).children('.seta').rotate({animateTo:90});
 	 			$(this).children('.subsessao').fadeIn(500);
 	 			$(this).removeClass('fechada');
 			} else {
 				$(this).children('.seta').rotate({animateTo:0});
 				$(this).children('.subsessao').fadeOut(500);
 	 			$(this).addClass('fechada');
 			}
		});
 	});
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Pendências</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<h2>Você tem <?=$qtdePendencias?> pendências</h2>
	<div class='pendencias'>
	<?php if($qtdePendencias > 0) : ?>
	<ul class='ul-sessao'>
		<?php foreach($pendencias as $pendencia) : ?>
			<li id='<?=$pendencia['titulo']?>' class='sessao fechada'>
			<img src="<?=$this->webroot?>img/seta_grande.jpg" class='seta' />
			<?=str_replace('_'," ",$pendencia['titulo'])?> (<?=count($pendencia['itens'])?>)
			<div class='subsessao'>
				<ul class='ul-subsessao'>
					<?php
						if(count($pendencia['itens']) > 0)
							foreach($pendencia['itens'] as $item)
								echo $item;
						else
							echo "<li>Nenhuma pendência neste item</li>";
					?>
				</ul>
			</div>
			</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('.historico').click(function(e) {
        e.preventDefault();
        url = "/<?=$this->params['prefix']?>/mensagens/historico_assunto/" + $(this).data('id');
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: url
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Pendencias
        </h2>
    </div>
</div>
<?php
if(sizeof($assuntos) > 0) :
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis'); ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="40%">
                    <?=$paginator->sort('Item', 'Item.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="40%">
                    <?=$paginator->sort('Assunto', 'assunto.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="20%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($assuntos as $assunto) : ?>
            <tr>
                <td><?=$assunto['Item']['nome']; ?></td>
                <td><?=$assunto['Assunto']['nome']; ?></td>
                <td>
                    <a class="button mini historico default"
                        href="#"
                        data-id="<?=$assunto['Assunto']['id']; ?>">
                        Historico de conversa
                        <i class="icon-history"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="1">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma Pendencia Encontrada</h2>
<?php endif; ?>
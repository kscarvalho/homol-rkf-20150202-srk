<?php if($formando) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<style type="text/css">
    .exibir-assunto { margin-bottom:0 }
    label { margin-right:0 }
    .anexos i:hover { color:red }
    input[type=text] { margin-bottom: 0 }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $("#formulario").submit(function(e) {
            e.preventDefault();
        });
        $('.fileupload').bind('loaded',function(e) {
            indice = $('.anexos').length;
            src = e.src.substring(e.src.indexOf('base64,') + 7);
            inputSrc = '<input type="hidden" value="' + src + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][src]" class="input-anexo" />';
            inputSize = '<input type="hidden" value="' + e.file.size + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tamanho]" class="input-anexo" />';
            inputType = '<input type="hidden" value="' + e.file.type + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tipo]" class="input-anexo" />';
            inputName = '<input type="hidden" value="' + e.file.name + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][nome]" class="input-anexo" />';
            buttonAnexo = "<button type='button' class='anexos input-anexo' dir=" + indice +
                    ">" + e.file.name + "<i class='icon-remove remover-anexo'></i></button>";
            $('.fileupload')
                .after(inputSrc)
                .after(inputSize)
                .after(inputType)
                .after(inputName)
                .after(buttonAnexo)
                .fileupload('reset');
            return;
        });
        $('#formulario').on('click','.remover-anexo',function(e) {
            $(".input-anexo[dir="+$(this).parent().attr('dir')+"]").remove();
        });
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'link', '|', 'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        $('#texto-nova-mensagem').redactor({
            buttons:buttons,
            minHeight:150
        });
        var botaoEnviar = $("<button>",{
            class : "bg-color-green",
            text : "Enviar",
            type : "button"
        });
        $(".bootbox.modal").find(".modal-footer").prepend(botaoEnviar);
        botaoEnviar.click(function() {
            if($("<div/>").html($('#texto-nova-mensagem').val()).text() == "") {
                alert("Digite o texto da mensagem");
            } else {
                var data = $('#formulario').serialize();
                var url = $("#formulario").attr('action');
                $(".modal-footer").hide();
                botaoEnviar.remove();
                $(".modal-body").html('<h2>Enviando mensagem...</h2>');
                $.ajax({
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    url: url,
                    success: function(response) {
                        if(response.erro) {
                            if(response.mensagem != "")
                                $(".modal-body").html('<h2 class="fg-color-red">'+response.mensagem+'</h2>');
                            else
                                $(".modal-body").html('<h2 class="fg-color-red">Erro ao inserir dados no servidor. Tente novamente</h2>');
                        } else {
                            $(".modal-body").html('<h2 class="fg-color-green">Mensagem enviada com sucesso</h2>');
                        }
                    },
                    error: function() {
                        $(".modal-body").html('<h2 class="fg-color-red">Erro ao conectar o servidor. Tente novamente</h2>');
                    },
                    complete : function() {
                        $(".modal-footer").find("a").text("Fechar");
                        $(".modal-footer").show();
                    }
                });
            }
        });
    });
</script>
<?=$form->create('Turma', array(
    'url' => $this->here,
    'id' => 'formulario'
)); ?>
<h3 class="fg-color-blueDark">
<?=$formando["ViewFormandos"]["codigo_formando"];?> - <?=$formando["ViewFormandos"]["nome"];?>
</h3>
<br />
<div class="row-fluid">
    <div class="fileupload fileupload-new" data-provides="fileupload" data-reader>
        <span class="btn-file">
            <a class="button default fileupload-new">Adicionar Anexo</a>
            <span class="fileupload-exists">Change</span>
            <input type="file" id="anexo-nova-mensagem" />
        </span>
        <span class="fileupload-preview"></span>
    </div>
</div>
<br />
<div class="row-fluid">
    <?=$form->input('Mensagem.texto', array(
        'class' => 'redactor',
        'label' => false,
        'id' => 'texto-nova-mensagem',
        'error' => false,
        'div' => false));
    ?>
</div>
<?= $form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
<?php else : ?>
<h2 class="fg-color-red">Formando não encontrado</h2>
<?php endif; ?>

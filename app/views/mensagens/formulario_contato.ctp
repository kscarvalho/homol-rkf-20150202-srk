<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'link', '|', 'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        $('.redactor').redactor({
            buttons:buttons,
            minHeight:150
        });
        $('.fileupload').bind('loaded',function(e) {
            indice = $('.anexos').length;
            src = e.src.substring(e.src.indexOf('base64,') + 7);
            inputSrc = '<input type="hidden" value="' + src + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][src]" class="input-anexo" />';
            inputSize = '<input type="hidden" value="' + e.file.size + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tamanho]" class="input-anexo" />';
            inputType = '<input type="hidden" value="' + e.file.type + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tipo]" class="input-anexo" />';
            inputName = '<input type="hidden" value="' + e.file.name + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][nome]" class="input-anexo" />';
            $('.fileupload')
                .after(inputSrc)
                .after(inputSize)
                .after(inputType)
                .after(inputName);
            return;
        });
        $("#formulario").submit(function(e) {
            e.preventDefault();
            if($("<div/>").html($('#texto-formulario').val()).text() == "") {
                alert('Selecione um Assunto');
            } else if($('#assunto-formulario').val() == "") {
                alert('Digite o texto da mensagem');
            } else {
                var dados = $(this).serialize();
                var url = $(this).attr('action');
                var context = ko.contextFor($("#content-body")[0]);
                context.$data.showLoading(function() {
                    $.ajax({
                        type: 'POST',
                        data: dados,
                        dataType: 'json',
                        url: url,
                        error: function() {
                            bootbox.alert('Erro ao conectar servidor. Tente novamente mais tarde');
                        },
                        complete : function() {
                            context.$data.reload();
                            bootbox.hideAll();
                        }
                    });
                });
            }
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Formul&aacute;rio de contato
    </h2>
</div>
<?php $session->flash(); ?>
<?=$form->create('Mensagem', array(
    'url' => "/mensagens/formulario_contato",
    'id' => 'formulario'
)); ?>
    <div class="row-fluid">
        <div class="span11">
            <?=$form->input('assunto',array(
                'options' => $assuntos,
                'type' => 'select',
                'empty' => "Selecione",
                'class' => 'selectpicker',
                'id' => 'assunto-formulario',
                'data-width' => '100%',
                'label' => 'Assunto',
                'div' => 'input-control text')); ?>
            <div class="fileupload fileupload-new" data-provides="fileupload" data-reader="true">
                <div class="input-append">
                    <div class="uneditable-input span3">
                        <i class="icon-file fileupload-exists"></i> 
                        <span class="fileupload-preview"></span>
                    </div>
                    <span class="button btn-file default fileupload-new">
                        <span>Anexar Arquivo</span>
                        <input type="file"/>
                    </span>
                    <span class="button btn-file bg-color-orange fileupload-exists">
                        <span>Alterar</span>
                        <input type="file"/>
                    </span>
                    <span class="button btn-file fileupload-exists bg-color-red" data-dismiss="fileupload">
                        <span>Remover</span>
                        <input type="file"/>
                    </span>
                </div>
            </div>
            <?=$form->input('Mensagem.texto', array(
                'class' => 'redactor',
                'label' => 'Mensagem',
                'id' => 'texto-formulario',
                'error' => false,
                'div' => false));
            ?>
        </div>
    </div>
</form>
<?= $form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
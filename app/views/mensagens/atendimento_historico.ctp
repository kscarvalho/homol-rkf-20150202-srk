<?php if(count($mensagens) > 0) : ?>
<?php foreach($mensagens as $mensagem) : ?>
<h3 class="fg-color-blue">
    <?=$mensagem["Usuario"]["nome"]?> 
    <small class="fg-color-red">
        <?=$mensagem["Assunto"]["nome"]?>
    </small>
</h3>
<i class="fg-color-blueDark">
    <?=date("d/m/Y \a\s H:i",strtotime($mensagem["Mensagem"]["data"]))?>
</i>
<br />
<?php if(count($mensagem["Arquivo"]) > 0) : ?>
<br />
<?php foreach($mensagem["Arquivo"] as $arquivo) : ?>
<a class="button mini" target="_blank"
    href="/<?=$atendente['Usuario']['grupo']?>/arquivos/baixar/<?=$arquivo['id']?>">
    <?=$arquivo['nome']?>
    <i class='icon-cloud-download'></i>
</a>
<?php endforeach; ?>
<br />
<?php endif; ?>
<br />
<?=html_entity_decode($mensagem['Mensagem']['texto'])?>
<br />
<hr />
<br />
<?php endforeach; ?>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma mensagem encontrada para esse usu&aacute;rio</h2>
<?php endif; ?>

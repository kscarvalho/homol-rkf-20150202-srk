<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Atendentes
    </h2>
</div>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th width="25%">Nome</th>
                <th width="20%">Email</th>
                <th width="15%">Tempo Respostas</th>
                <th width="15%">Respondidas</th>
                <th width="15%">N&atilde;o<br />Respondidas</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($atendentes as $atendente) : ?>
            <tr>
                <td><?=$atendente['Usuario']['nome']; ?></td>
                <td><?=$atendente['Usuario']['email']; ?></td>
                <td><?=$atendente['atendimentos']['media']; ?></td>
                <td><?=$atendente['atendimentos']['respostas']; ?></td>
                <td><?=$atendente['nao_respondidas']; ?></td>
                <td>
                    <a href="/<?=$this->params['prefix']?>/mensagens/fale_conosco_atendente/<?=$atendente['Usuario']['id']?>"
                        class="button mini default"
                        data-bind="click: loadThis">
                        Detalhes
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
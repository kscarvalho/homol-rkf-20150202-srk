<?php if($this->data['Mensagem']) { ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<style type="text/css">
    .exibir-assunto { margin-bottom:0 }
    label { margin-right:0 }
    .anexos i:hover { color:red }
    input[type=text] { margin-bottom: 0 }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
            width : '100%'
        });
        $('.fileupload').bind('loaded',function(e) {
            indice = $('.anexos').length;
            src = e.src.substring(e.src.indexOf('base64,') + 7);
            inputSrc = '<input type="hidden" value="' + src + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][src]" class="input-anexo" />';
            inputSize = '<input type="hidden" value="' + e.file.size + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tamanho]" class="input-anexo" />';
            inputType = '<input type="hidden" value="' + e.file.type + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tipo]" class="input-anexo" />';
            inputName = '<input type="hidden" value="' + e.file.name + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][nome]" class="input-anexo" />';
            buttonAnexo = "<button type='button' class='anexos input-anexo' dir=" + indice +
                    ">" + e.file.name + "<i class='icon-remove remover-anexo'></i></button>";
            $('.fileupload')
                .after(inputSrc)
                .after(inputSize)
                .after(inputType)
                .after(inputName)
                .after(buttonAnexo)
                .fileupload('reset');
            return;
        });
        
        $('#formulario').on('click','.remover-anexo',function(e) {
            $(".input-anexo[dir="+$(this).parent().attr('dir')+"]").remove();
        });
        
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'link', '|', 'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        
        $('#texto-nova-mensagem').redactor({
            buttons:buttons,
            minHeight:250
        });
        
        $('#texto-nova-mensagem').redactor('focus');
        
        $("#formulario").submit(function(e) {
            e.preventDefault();
            if($("<div/>").html($('#texto-nova-mensagem').val()).text() == "") {
                alert('Digite o texto da mensagem');
            } else {
                var button = $('.modal-footer').find(':contains("Envia")');
                if(button.attr('disabled') != 'disabled') {
                    button
                        .attr('disabled','disabled')
                        .removeClass('bg-color-red')
                        .addClass('bg-color-green')
                        .text('Enviando');
                    var data = $('#formulario').serialize();
                    var url = $("#formulario").attr('action');
                    $.ajax({
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        url: url,
                        success: function(response) {
                            var context = ko.contextFor($("#content-body")[0]);
                            context.$data.page('/mensagens/email');
                            bootbox.hideAll();
                        },
                        error: function() {
                            bootbox.alert('Erro ao conectar servidor. Tente novamente mais tarde',function() {
                                bootbox.hideAll();
                            });
                        }
                    })
                }
            }
        });
    })
</script>
<?=$form->create('Turma', array(
    'url' => "/mensagens/inserir",
    'id' => 'formulario'
));
?>
<?=$form->hidden('Item.id')?>
<?=$form->hidden('Mensagem.assunto_id')?>
<?=$form->hidden('Mensagem.mensagem_id')?>
<div class="row-fluid">
    <div class="span5">
        <h4><?=$this->data['Assunto']['Item']['nome']?></h4>
        <h4><?=$this->data['Assunto']['nome']?></h4>
        <br />
        <?php if($usuario['Usuario']['grupo'] == 'planejamento') : ?>
        <div class="row-fluid">
            <?=$form->input('Assunto.pendencia', array(
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $pendencias,
                'label' => 'Pendência',
                'div' => false));
            ?>
            <br />
            <br />
            <?=$form->input('Assunto.resolvido', array(
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $resolvido,
                'label' => 'Resolvido',
                'div' => false));
            ?>
        </div>
        <br />
        <?php else : ?>
        <?=$form->hidden('Assunto.pendencia',array('value' => 'As'))?>
        <?=$form->hidden('Assunto.resolvido',array('value' => 0))?>
        <?php endif; ?>
        <div class="row-fluid">
            <div class="fileupload fileupload-new" data-provides="fileupload" data-reader>
                <span class="btn-file">
                    <a class="button default fileupload-new">Adicionar Anexo</a>
                    <span class="fileupload-exists">Change</span>
                    <input type="file" id="anexo-nova-mensagem" />
                </span>
                <span class="fileupload-preview"></span>
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <span class="label label-info">Mensagem</span>
            <br />
            <br />
            <?=$texto?>
        </div>
    </div>
    <div class="span7">
        <?=$form->input('Mensagem.texto', array(
            'class' => 'redactor',
            'label' => false,
            'id' => 'texto-nova-mensagem',
            'error' => false,
            'div' => false));
        ?>
    </div>
</div>
<?= $form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
<?php } else { ?>
<h2 class='fg-color-red'>Erro ao buscar mensagem</h2>
<?php } ?>

<?php if($mensagem) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<?php if($mensagem['Assunto']['resolvido'] == 0) : ?>
<style type="text/css">
    .anexos i:hover { color:red }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.fileupload').bind('loaded',function(e) {
            indice = $('.anexos').length;
            src = e.src.substring(e.src.indexOf('base64,') + 7);
            inputSrc = '<input type="hidden" value="' + src + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][src]" class="input-anexo" />';
            inputSize = '<input type="hidden" value="' + e.file.size + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tamanho]" class="input-anexo" />';
            inputType = '<input type="hidden" value="' + e.file.type + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tipo]" class="input-anexo" />';
            inputName = '<input type="hidden" value="' + e.file.name + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][nome]" class="input-anexo" />';
            buttonAnexo = "<button type='button' class='anexos input-anexo' dir=" + indice +
                    ">" + e.file.name + "<i class='icon-remove remover-anexo'></i></button>";
            $('.fileupload')
                .after(inputSrc)
                .after(inputSize)
                .after(inputType)
                .after(inputName)
                .after(buttonAnexo)
                .fileupload('reset');
            return;
        });
        $('#formulario').on('click','.remover-anexo',function(e) {
            $(".input-anexo[dir="+$(this).parent().attr('dir')+"]").remove();
        });
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        $('#texto-nova-mensagem').redactor({
            buttons:buttons,
            minHeight:150
        });
        $('#texto-nova-mensagem').redactor('focus');
    })
</script>
<?php endif; ?>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span6">
        <?php if($mensagem['Assunto']['resolvido'] == 0) : ?>
        <?php
            echo $form->create('Turma', array(
                'url' => "/mensagens/responder_deslogado/{$mensagemUsuarioId}",
                'id' => 'formulario'
            ));
            echo $form->hidden('Mensagem.usuario_id');
            echo $form->hidden('Mensagem.turma_id');
            echo $form->hidden('Mensagem.assunto_id');
        ?>
        <?=$form->input('Mensagem.texto', array(
            'class' => 'redactor',
            'label' => false,
            'id' => 'texto-nova-mensagem',
            'error' => false,
            'div' => false));
        ?>
        <br />
        <div class="row-fluid">
            <div class="fileupload fileupload-new" data-provides="fileupload" data-reader>
                <span class="btn-file">
                    <a class="button default fileupload-new">Anexar Arquivo</a>
                    <span class="fileupload-exists">Alterar</span>
                    <input type="file" id="anexo-nova-mensagem" />
                </span>
                <span class="fileupload-preview"></span>
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <button type="submit" class="input-block-level bg-color-blue">
                Responder Mensagem
            </button>
        </div>
        <?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>
        <?php else : ?>
        <div class="alert alert-error">
            O assunto dessa mensagem foi resolvido e fechado para novas mensagens
        </div>
        <?php endif; ?>
    </div>
    <div class="span6">
        Enviada por <span class="label label-inverse"><?=$mensagem['Usuario']['nome']?></span> em 
        <span class="label label-warning"><?=date('d/m/Y',strtotime($mensagem['Mensagem']['data']))?></span>
        <br />
        <br />
        <h4 class="fg-color-red">
            <?=$mensagem['Assunto']['Item']['nome']?> <?=$mensagem['Assunto']['nome']?>
        </h4>
        <br />
        <div style="padding:10px; background:#f3f3f3; border-radius: 4px">
            <?=html_entity_decode($mensagem['Mensagem']['texto'])?>
        </div>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Mensagem não encontrada</h2>
<?php endif; ?>
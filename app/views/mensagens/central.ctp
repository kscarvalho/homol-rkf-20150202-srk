<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/mailbox.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/max/knockout/mailbox.js"></script>
<script type='text/javascript'>
    (function($) {
        
    })(jQuery)
</script>
<script type="text/html" id="tmpMailboxMensagens">
    <li data-bind="click:function() { $root.exibirMensagem($index()) },
        css:{ active: selecionada() }, event: {
            contextmenu: function() { selecionada(!selecionada()) }}">
        <div class="image">
            <!-- ko if: Usuario.diretorio_foto_perfil != null -->
            <!--<img data-bind="attr: { width: 40,
                src: '<?= $this->webroot ?>'+
                    Usuario.diretorio_foto_perfil }" />-->
            <img data-bind="attr: { width: 40,
                src: '<?= $this->webroot ?>img/uknown_user.gif' }" />
            <!-- /ko -->
            <!-- ko if: Usuario.diretorio_foto_perfil == null -->
            <img data-bind="attr: { width: 40,
                src: '<?= $this->webroot ?>img/uknown_user.gif' }" />
            <!-- /ko -->
        </div>
        <div data-bind="css:{ title: true, 'fg-color-red': lida() == 0 }">
            <div class="name pull-left" data-bind="text: Usuario.nome"></div>
            <div class="data pull-right" data-bind="text:MensagemUsuario.data"></div>
        </div>
        <br />
        <div data-bind="css:{ title: true, 'fg-color-red': lida() == 0 }">
            <div class="subject pull-left" data-bind="text: Mensagem.assunto"></div>
            <div class="status pull-right">
                <i data-bind="css: { favorita: MensagemUsuario.favorita == 1, 'icon-heart':true }"></i>
            </div>
        </div>
    </li>
</script>
<script type="text/html" id="tmpMailboxMensagemBody">
    <!-- ko if: $data.error == false -->
    <h1 class="subject" data-bind="text:$data.Mensagem.assunto"></h1>
    <div class="detail">
        <div class="image">
            <img src="/img/uknown_user.gif" width="40">
        </div>
    </div>
    <div class="text" data-bind="html: $data.Mensagem.texto"></div>
    <!-- /ko -->
    <!-- ko if: $data.error != false -->
        <h1 class="subject" data-bind="text:$data.message"></h1>
    <!-- /ko -->
</script>
<div class="row-fluid" data-bind="stopBinding: true">
    <div class="mailbox" id='mailbox'>
        <ul class="mailbox-navigation nav nav-tabs">
            <li data-bind="css: { active: tipo() == 'todas' },
                click:function() { tipo('todas') }">
                <a>Entrada</a>
            </li>
            <li data-bind="css: { active: tipo() == 'favoritas' },
                click:function() { tipo('favoritas') }">
                <a>Favoritas</a>
            </li>
            <li data-bind="css: { active: tipo() == 'nao-lidas' },
                click:function() { tipo('nao-lidas') }">
                <a>N&atilde;o Lidas</a>
            </li>
            <li data-bind="css: { active: tipo() == 'enviadas' },
                click:function() { tipo('enviadas') }">
                <a>Enviadas</a>
            </li>
            <li>
                <a>Lixeira</a>
            </li>
        </ul>
        <div class="mailbox-contents">
            <div class="mailbox-messages">
                <ul class="preview"
                     data-bind="template: { name: 'tmpMailboxMensagens', foreach: itensPaginaAtual,
                        afterAdd: showMessage, beforeRemove: hideMessage }">
                </ul>
            </div>
            <div class="mailbox-panel">
                <div class="loading" data-bind="fadeVisible:atualizando()">Carregando</div>
                <div class="body" data-bind="fadeVisible:!atualizando(),
                    template: { name: 'tmpMailboxMensagemBody', if: $root.mensagemCarregada() != false,
                    data: $root.mensagemCarregada() }">
                </div>
            </div>
        </div>
    </div>
</div>
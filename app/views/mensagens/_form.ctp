<script type="text/javascript">
 	jQuery(function($) {
		$('.checkbox_resolvido').click(function() {
				if($(this).is(':checked')) {
					resp = confirm('Ao marcar um assunto como resolvido, não será possível enviar novas mensagens a menos que ele seja reaberto. Deseja continuar?');
					if(!resp) {
						$(this).attr('checked',false);
					}
				}
			}
		);
 	});
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>
<?php echo $form->hidden('assunto_id', array('value' => $assunto['Assunto']['id']));?>

<?php
/*
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Exibicao</label>
	<?php echo $form->input('exibir', array(
	        'type' => 'select',
	        'options' => array(0 => 'Desabilitada', 1 => 'Habilitada'), 'class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
*/
?>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Pendência</label>
	<?php echo $form->input('pendencia', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<?php
if(in_array($this->params['prefix'],array('planejamento','comercial'))) {
?>
<p class="grid_11 alpha omega">
	<?php echo $form->checkbox('Assunto.resolvido', array('value' => true, 'class' => 'checkbox_resolvido') );?>
	Resolvido
</p>
<?php
}
?>


<p class="grid_11 alpha omega" id="arquivos-anexos">
	<label class="grid_5 alpha">Anexos</label>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Texto</label>
	<?php echo $form->textarea('texto', array('id' => 'texto-mensagem', 'class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<script  type="text/javascript">
	
	function updateAssuntos() {
		$.get('<?php echo $this->webroot;?>' + "/ajax/mensagens/assuntos/" + $('#item_box option:selected').val(), function(data) {
			$('#assunto_box option').remove();
			
			$('#assunto_box').attr('disabled', 'disabled');
			
			if ((jQuery.parseJSON(data)).length == 0)
				$('#assunto_box').append(new Option('---', -1, true, true));
			else 
				$('#assunto_box').removeAttr('disabled');
				
			$.each(jQuery.parseJSON(data), function() {
				$('#assunto_box').append(new Option(this.Assunto.nome, this.Assunto.id, true, true));
			});
		});
	}
	
	var botaoAnexarOutroArquivo = '<a class="adicionar-anexo">Adicionar outros anexos</a>';
	var inputArquivo = '<?php echo $form->file('tituloInputArquivo') ?>';
	var countInputs = 1;
	$(function(){
		$('#arquivos-anexos').append(inputArquivo.replace(/tituloInputArquivo/i, 'arquivo'+countInputs));
		$('#arquivos-anexos').append(botaoAnexarOutroArquivo);
		
		$('#arquivos-anexos a.adicionar-anexo').live('click', function(){
				countInputs++;
				$(this).remove();
				$('#arquivos-anexos').append(inputArquivo.replace(/tituloInputArquivo/i, 'arquivo'+countInputs));
				$('#arquivos-anexos').append(botaoAnexarOutroArquivo);
		});
	})
	
</script>

<?php 
echo $javascript->link('ckeditor/ckeditor'); 
?>

<script type="text/javascript">
	CKEDITOR.replace( 'texto-mensagem', {
	    removePlugins: 'elementspath' 
	} );
</script>

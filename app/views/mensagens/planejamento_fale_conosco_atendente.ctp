<script type="text/javascript">
$(document).ready(function() {
    $(".modal-texto").click(function() {
        var div = $("<div>");
        div.append($("<h3>",{
            class : 'fg-color-blue',
            text : $(this).data('autor')
        }));
        div.append("<br />");
        div.append($(this).data('texto'));
        bootbox.alert(div.html());
    });
})
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Atendente <?=$atendente['Usuario']['nome']?>
        <a href="/<?=$this->params['prefix']?>/mensagens/fale_conosco_atendentes"
            class="button mini default pull-right"
            data-bind="click: loadThis">
            Voltar
        </a>
    </h2>
</div>
<div class="row-fluid">
    <h3>Perguntas respondidas: <span class="fg-color-blue"><?=count($perguntas)?></span></h3>
    <h3>Perguntas n&atilde;o respondidas: <span class="fg-color-blue"><?=count($naoRespondidas)?></span></h3>
    <h3>Tempo m&eacute;dio por atendimento: <span class="fg-color-blue"><?=$mediaTotal?></span></h3>
    <br />
    <div class="alert alert-danger">
        <strong>Aten&ccedil;&atilde;o</strong>
        <br />
        O tempo m&eacute;dio por atendimento &eacute; baseado nas perguntas respondidas
    </div>
</div>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th width="10%">Assunto</th>
                <th width="20%">Formando</th>
                <th colspan="2" width="28%">Pergunta</th>
                <th colspan="2" width="28%">Resposta</th>
                <th width="14%">Tempo</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($naoRespondidas as $mensagem) : ?>
            <tr>
                <td><?=$mensagem['Assunto']['nome']; ?></td>
                <td><?=$mensagem['Usuario']['nome']; ?></td>
                <td>
                    <a href="javascript:" class="button mini bg-color-blueDark modal-texto"
                        data-autor="<?=$mensagem['Usuario']['nome']; ?>"
                        data-texto="<?=htmlspecialchars($mensagem['Mensagem']['texto'])?>">
                        Texto
                    </a>
                </td>
                <td style="text-transform: capitalize">
                    <?=date('H:i:s',strtotime($mensagem['Mensagem']['data'])); ?>
                    <br />
                    <?=substr($form->diasDaSemana[date('w',strtotime($mensagem['Mensagem']['data']))],0,3); ?>
                    <?=date('d/m/y',strtotime($mensagem['Mensagem']['data'])); ?>
                </td>
                <td colspan="3">N&atilde;o respondida</td>
            </tr>
            <?php endforeach; ?>
            <?php foreach($perguntas as $pergunta) : ?>
            <tr>
                <td><?=$pergunta['resposta']['Assunto']['nome']; ?></td>
                <td><?=$pergunta['pergunta']['Usuario']['nome']; ?></td>
                <td>
                    <a href="javascript:" class="button mini bg-color-blueDark modal-texto"
                        data-autor="<?=$pergunta['pergunta']['Usuario']['nome']; ?>"
                        data-texto="<?=htmlspecialchars($pergunta['pergunta']['Mensagem']['texto'])?>">
                        Texto
                    </a>
                </td>
                <td style="text-transform: capitalize">
                    <?=date('H:i:s',strtotime($pergunta['pergunta']['Mensagem']['data'])); ?>
                    <br />
                    <?=substr($form->diasDaSemana[date('w',strtotime($pergunta['pergunta']['Mensagem']['data']))],0,3); ?>
                    <?=date('d/m/y',strtotime($pergunta['pergunta']['Mensagem']['data'])); ?>
                </td>
                <td>
                    <a href="javascript:" class="button mini bg-color-greenDark modal-texto"
                        data-autor="<?=$pergunta['resposta']['Usuario']['nome']; ?>"
                        data-texto="<?=htmlspecialchars($pergunta['resposta']['Mensagem']['texto'])?>">
                        Texto
                    </a>
                </td>
                <td style="text-transform: capitalize">
                    <?=date('H:i:s',strtotime($pergunta['resposta']['Mensagem']['data'])); ?>
                    <br />
                    <?=substr($form->diasDaSemana[date('w',strtotime($pergunta['resposta']['Mensagem']['data']))],0,3); ?>
                    <?=date('d/m/y',strtotime($pergunta['resposta']['Mensagem']['data'])); ?>
                </td>
                <td><?=$pergunta['tempo']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
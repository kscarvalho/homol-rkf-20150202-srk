<div class="row-fluid">
    <?php if($mensagem) : ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var context = ko.contextFor($("#content-body")[0]);
            var id = '<?=$mensagem['MensagemUsuario']['id']?>';
            context.$data.removerAviso(id,'mensagem');
            <?php if($podeResponder) : ?>
            $("<a/>",{
               class : 'button bg-color-green',
               href : "#",
               id : 'responder-mensagem',
               html : "<i class='icon-reply'></i>Responder"
            }).appendTo($('.modal-footer'));
            $("#responder-mensagem").click(function() {
                bootbox.dialog('Carregando',[{
                    label: 'Enviar',
                    'class': 'bg-color-red',
                    callback: function() {
                        $("#formulario").trigger('submit');
                        return false;
                    }
                },{
                    label: 'Fechar',
                    'class': 'bg-color-blue'
                }],{
                    remote: '/mensagens/responder/<?=$mensagem['Mensagem']['id']?>'
                });
            });
            <?php endif; ?>
        });
    </script>
    <h3 class="fg-color-red">
        <?=$mensagem['Mensagem']['Assunto']['Item']['nome']?> - 
        <?=$mensagem['Mensagem']['Assunto']['nome']?>
    </h3>
    <div class="text">
        <?=html_entity_decode($mensagem['Mensagem']['texto'])?>
    </div>
    <div class="row-fluid">
        <?php foreach($mensagem['Mensagem']["Arquivo"] as $arquivo) : ?>
        <a class="button mini" target="_blank"
            href="/<?=$usuario['Usuario']['grupo']?>/arquivos/baixar/<?=$arquivo['id']?>">
            <?=$arquivo['nome']?>
            <i class='icon-cloud-download'></i>
        </a>
        <?php endforeach; ?>
    </div>
    <?php else : ?>
    <h2 class="fg-color-red">Mensagem Não Encontrada</h2>
    <?php endif; ?>
</div>
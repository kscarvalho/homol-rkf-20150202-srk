<div class="alert alert-block alert-error fade in">
	<a class="close" data-dismiss="alert" href="#"></a>
	<h4 class="alert-heading">Erro</h4>
	<p>
	<?php if(is_array($content_for_layout)) : foreach($content_for_layout as $message) : echo "$message<br />"; endforeach; ?>
	<?php  else : echo $content_for_layout; endif; ?>
	</p>
</div>
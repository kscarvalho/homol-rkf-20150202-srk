<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <?=$html->charset(); ?>
    <title><?=$title_for_layout; ?></title>
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/default.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/responsive.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/core.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/fonts.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/typography.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/color.css?v=0.1"
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/form.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/metroui/button.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/default.css?v=0.1">
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?=$this->webroot ?>metro/css/min/bootmetro/icons-ie7.css?v=0.1">
    <![endif]-->
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/modernizr-2.6.2.js?v=0.1"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/knockout/knockout-2.2.1.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.1"></script>
    <script>window.jQuery || document.write("<script src='<?=$this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.1'>\x3C/script>")</script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/utils.js?v=0.1"></script>
</head>
<body>
<div id="menu-topo" class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container todo">
            <a class="brand">
                <div class="logo-min-branco"></div>
            </a>
            <div class="nav-collapse collapse">
                
            </div>
        </div>
    </div>
</div>
<div class="container todo">
    <header id="nav-bar" class="container" style="width:100%">
        <div class="row-fluid">
            <div class="span12">
                <div id="header-container">
                </div>
                <div id="top-info" class="pull-right">
                    <?php if($usuario) : ?>
                    <?php
                    $fotoPerfil = "img/uknown_user.gif";
                    if (isset($usuario['Usuario']['diretorio_foto_perfil']))
                        if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
                            if (file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
                                $fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
                    ?>
                    <a href="javascript:void(0)" class="pull-right perfil pointer">
                        <img src="<?="{$this->webroot}{$fotoPerfil}" ?>" />
                    </a>
                    <?php endif; ?>
                    <?php if($turma) : ?>
                    <div class="pull-left user-info">
                        <p>
                            <span class="label label-important pointer" id='info-turma-logada'
                                rel="tooltip" title='Informações da Turma'>
                                <?=$turma['Turma']['id']?> <?=$turma['Turma']['nome']?>
                            </span>
                        </p>
                    </div>
                    <?php endif; ?>
                    <?php if($usuario) : ?>
                    <div class="pull-left">
                        <h2 class='fg-color-red'><?=$usuario['Usuario']['nome'] ?></h2>
                        <h4 class="dados-usuario grupo"><?=$usuario['Usuario']['grupo']?></h4>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>
    <div class="row-fluid" id="conteudo">
        <?=$content_for_layout; ?>
    </div>
</div>
</body>
</html>

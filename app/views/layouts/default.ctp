<?php
/* SVN FILE: $Id$ */

/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <?php echo $html->charset(); ?>
          
            <title>
                <?php echo $title_for_layout; ?>
            </title>
            
            <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
			<link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
           
            <?php
            echo $html->css('estrutura');
            echo $html->css('forms');
            echo $html->css('grid');
            echo $html->css('reset');
            echo $html->css('tabelas');
            echo $html->css('texto');
            echo $html->css('calendario');
            echo $html->css('jquery-ui');
			echo $html->css('contratos');
			echo $html->css('colorbox');
			echo $html->css('painel_formando');
			echo $html->css('smoothDivScroll');
			echo $html->css('jquery.lightbox-0.5');
			
            echo $scripts_for_layout;
            
            echo $html->script('jquery-1.7.2.min');
            echo $html->script('jquery-ui-1.8.14.min');
            echo $html->script('jquery-ui-datepicker-pt-BR');
            echo $html->script('jquery-ui-timepicker-addon');
            echo $html->script('jquery.meio.mask.js');
            echo $html->script('jquery.imgareaselect.min');
			echo $html->script('jquery.colorbox');
			echo $html->script('jquery.mousewheel.min');
			echo $html->script('jquery.smoothdivscroll-1.2-min');
			echo $html->script('jquery.lightbox-0.5.min');
			echo $html->script('analytics');
			echo $html->script('functions');
			
			?>
            <!--[if IE 6]><?php echo $html->css('grid_ie6'); ?><![endif]--> 
            <!--[if IE 7]><?php echo $html->css('grid_ie7'); ?><![endif]-->
            <script type="text/javascript">
			jQuery(function($) {
				initRelogio();
				initCalendar();
			});
			</script>
    </head>
    <body>
        <div class="container_16">

            <!-- header start -->
            <div id="header" class="grid_full alpha omega">
                <div class="grid_4 alpha" id="logo">
                <a href="
                	<?php 
                		if(isset($this->params['prefix'])) {
                			echo $html->url(array($this->params['prefix'] => true, 'controller' => 'principal', 'action' => 'index'));
                		} else {
                			echo $html->url(array('controller' => 'principal', 'action' => 'index'));
                		}
                	?> 
                	">
                <?php echo $html->image('logo.jpg'); ?>
                    </a>
                </div>
                <div class="grid_12 omega">
                    <!--
                    <div  id="usuario-logado">
                        <p id="usuario-logado-nome">
                            <?php
                            if (isset($usuario)) {
                                $nome = $usuario['Usuario']['nome'];
                                
                                //echo $nome;
								if ($this->params['prefix'] != 'formando') {
                                	//echo $html->link('Agenda', array($this->params['prefix'] => true, 'controller' => 'agendas', 'action' => 'index'), array('id' => 'usuario-logado-agenda'));
                            	}	                                
                                	
                            }
                            ?>
                        </p>	
                        <?php
                        if (isset($usuario) && ($this->params['prefix'] == 'formando' || $this->params['prefix'] == 'comissao')) {
                                	if($codigo_formando != "" && $codigo_formando != null)
                                		echo '<p id="usuario-logado-codigo-formando">';	
                                		echo "Código: " . str_pad($codigo_formando,8,'0',STR_PAD_LEFT);
                                		echo '</p>';
                                }
                        ?>
                        <p id="usuario-logado-secao">
                            <?php if (isset($usuario))
                                echo $this->params['prefix']; ?>
                                 
                       		<?php
                       		
                            if (isset($usuario) && $exibirSwitchComissaoFormando) {
								if ($usuario['Usuario']['grupo'] == 'comissao') {
									echo " - ";
                                	echo $html->link('Entrar como formando', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' => 'mudar_grupo'));
                            	} else if($usuario['Usuario']['grupo'] == 'formando') {
                            		if ($session->read('mudouGrupo')) {
	                            		echo " - ";
	                            		echo $html->link('Entrar como comissao', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' => 'mudar_grupo'));
                            		}	                                
								}
                            }
                            ?>         
                       </p>
                    </div>
                    -->
                    <?php if(isset($usuario)) : ?>
                    <?php
                    	$fotoPerfil = "img/uknown_user.gif";
                    	if(isset($usuario['FormandoProfile']['diretorio_foto_perfil']))
                    		if(!empty($usuario['FormandoProfile']['diretorio_foto_perfil']))
                    			$fotoPerfil = $usuario['FormandoProfile']['diretorio_foto_perfil'];
                    ?>
                    <div class='info-user'>	
                    	<div class='box-info'>
                    		<div style="float:left; width:100%; margin-bottom:10px">
                    			<img src="<?=$this->webroot?>img/relogio.png" title="Relogio" alt='Relogio' style="float:left; margin-right:5px; width:13px" />
                    			<span id='horario'></span>
                    		</div>
                    		<?php if(false) { ?>
                    		<div style="float:left; width:100%; margin-bottom:10px">
	                    		<span style='float:left; position:relative; margin-right:8px'>
	                    			<img src="<?=$this->webroot?>img/mail.png" title="MailBox" alt='MailBox' style="float:left; width:20px" />
	                    			<span style="position:absolute; background-color:#CC2229; color:white; font-weight:bold; font-size:9px; top:-3px; right:-3px; padding:0 2px 0 2px">
	                    			1
	                    			</span>
	                    		</span>
	                    		Mensagens
	                    	</div>
	                    	<?php } ?>
	                    	<?php if ($this->params['prefix'] != 'formando') { ?>
	                    	<div style='float:left; width:100%; margin-bottom:10px'>
	                    		<a href="<?="{$this->webroot}{$this->params['prefix']}/agendas"?>">
	                    		<div class='item'>
		                    		<span class='img'>
		                    			<img src="<?=$this->webroot?>img/calendar.png" title="Calendar" alt='Calendar' />
		                    			<span id='calendario-dia'>
		                    		</span>
		                    		Agenda
		                    	</div>
		                    	<!--
	                    		<span style='float:left; position:relative; margin-right:8px'>
	                    			<img src="<?=$this->webroot?>img/calendar.png" title="Calendar" alt='Calendar' style="float:left; width:20px" />
	                    			</span>
	                    		</span>
	                    		-->
	                    		</a>
	                    	</div>
	                    	<?php } ?>
                    	</div>
                    	<?php if((isset($turmaLogada) && !empty($turmaLogada['Turma']['nome'])) ||
                    			(isset($usuario) && ($this->params['prefix'] == 'formando' || $this->params['prefix'] == 'comissao')) || $exibirSwitchComissaoFormando) : ?>
                    	<div class='box-info'>
                    		<?php if(isset($turmaLogada) && !empty($turmaLogada['Turma']['nome'])) { ?>
	                    	<div class='item'>
	                    		<span class='img'>
	                    			<img src="<?=$this->webroot?>img/formando.jpg" title="Turma" alt='Turma' />
	                    		</span>
	                    		<?="<strong>{$turmaLogada['Turma']['nome']} ({$turmaLogada['Turma']['id']})</strong>"?>
	                    	</div>
	                    	<?php } ?>
	                    	<?php if(isset($usuario) && ($this->params['prefix'] == 'formando' || $this->params['prefix'] == 'comissao')) { ?>
	                    	<div class='item'>
	                    		<span class='img'>
	                    			<img src="<?=$this->webroot?>img/barcode.png" title="Código" alt='Código' />
	                    		</span>
	                    		<?="<strong>" . str_pad($codigo_formando,8,'0',STR_PAD_LEFT) . "</strong>"?>
	                    	</div>
	                    	<?php } ?>
	                    	<?php if (isset($usuario) && $exibirSwitchComissaoFormando) { ?>
	                    	<div class='item'>
	                    		<span class='img'></span>
	                    		<?php
	                    		if ($usuario['Usuario']['grupo'] == 'comissao')
	                    			echo $html->link('Entrar como formando', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' => 'mudar_grupo'));
	                    		else if($usuario['Usuario']['grupo'] == 'formando')
	                    			if ($session->read('mudouGrupo'))
	                    				echo $html->link('Entrar como comissao', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' => 'mudar_grupo'));
	                    		?>
	                    	</div>
	                    	<?php } ?>
                    	</div>
                    	<?php endif; ?>
                    	<div class='box-info'>
                    		<span class='image-profile'>
                    			<a href="<?=$this->webroot.$this->params['prefix']?>/usuarios/editar">
                    			<img src="<?="{$this->webroot}{$fotoPerfil}"?>" width="50" />
                    			</a>
                    		</span>
                    		<div class='nome'>
                    			<a href="<?=$this->webroot.$this->params['prefix']?>/usuarios/editar"><?=$usuario['Usuario']['nome']?></a>
                    			<br />
                    			<?=isset($usuario) ? "<em>{$this->params['prefix']}</em>" : ""?>
                    		</div>
                    		&Uacute;ltimo Login<br />
                    		<?=date('d/m/Y', strtotime($usuario['Usuario']['ultimo_acesso']));?>&nbsp;
                    		&agrave;s&nbsp;
                    		<?=date('H:i:s', strtotime($usuario['Usuario']['ultimo_acesso']));?>
                    		<a class="logout" href="<?$this->webroot.$this->params['prefix']?>/usuarios/logout">
                    			<img src="<?=$this->webroot?>img/logout.png" title="Logout" alt='Logout' width="14" style='float:left; margin-right:3px' />
                    			Sair
                    		</a>
                    	</div>
                    </div>
                    <?php endif; ?>
                    <div id="turma-selecionada">
                            <? //if (!empty($turma)):   ?>
                        <p id="turma-selecionada-nome">
                            <?php
                            /*
                            if (isset($turmaLogada) && !empty($turmaLogada['Turma']['nome']))
                                echo $turmaLogada['Turma']['nome'] . ' (' . $turmaLogada['Turma']['id'] . ')';
                                */
                            ?>
                        </p>
                        <? //endif  ?>
<?php //echo $html->link('Sair', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' => 'logout'), array('id' => 'turma-selecionada-logout')); ?>
                    </div> 
                </div>
                <div class="grid_full first" id="menu-horizontal">
                    <div id="menu-horizontal-links">
                        <?php
                        if(isset($configuracaoTopbarMenu))
                            echo $topbarMenu->exibir($configuracaoTopbarMenu['links'], $this->params['controller']);
                        ?>
                    </div>
                </div>
            </div>
            <!-- header end -->

            <!-- #menu-lateral start -->
            <div id="menu-lateral" class="box-com-titulo"> 
                <div>
                    <?php
                    if(isset($configuracaoSidebarMenu)) {
						if (isset($turmaLogada['Turma']))
                           	echo $sidebarMenu->exibir($configuracaoSidebarMenu['titulo'] . ' - ' . $turmaLogada['Turma']['nome'] , $configuracaoSidebarMenu['links'], $this->params['controller'], $this->params['action'], $configuracaoSidebarMenu['sublinks']);
                       	else
							echo $sidebarMenu->exibir($configuracaoSidebarMenu['titulo'], $configuracaoSidebarMenu['links'], $this->params['controller'], $this->params['action'],  $configuracaoSidebarMenu['sublinks']);
                    }
                    ?>

                    <?php
                    	if(isset($itensPendentesSidebar)) {
                        	if(isset($turmaLogada['Turma']['nome'])) echo $sidebarItens->exibir("Itens - " . $turmaLogada['Turma']['nome'] , $itensPendentesSidebar);
                        	else echo $sidebarItens->exibir("Itens" , $itensPendentesSidebar);
                    	}
                    ?>
                </div>
            </div>
            <!-- #menu-lateral end -->

            <!-- #conteudo start -->
            <div id="conteudo-wrap">
                <div id="conteudo"  class="box-com-titulo">
<?php echo $content_for_layout; ?>
                </div>
            </div>
            <!-- #conteudo end -->

            <!-- #footer start -->
            <div id="footer"  class="grid_full">
			<?=$this->element('default/footer'); ?>
			</div>
            <!-- #footer end -->
        </div>
    </body>
</html>

<style type="text/css">
    .table-bordered td:first-child { border-left: 1px solid #ccc; }
    td.pergunta {
        border-right: 1px #c3c3c3 solid!important;
        border-bottom: 1px #c3c3c3 solid!important;
        border-left:none!important;
    }
    td.formando-resposta { border-bottom: 1px #c3c3c3 solid!important; vertical-align: top!important }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $('#tab-enquetes li a[data-toggle="tab"]').on('show', function (e) {
            atualizaAltura($($(e.target).attr('href')).height()+$("#cabecalho").height()+60);
        });
        $("#tab-enquetes li a[href='#resumo']").tab('show');
    });
</script>
<div class="row-fluid" id="cabecalho">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Enquetes
        <a href="/enquetes/listar" class="button mini default pull-right"
            data-bind="click: loadThis">Voltar</a>
    </h2>
    <p><?=$enquete['Enquete']['texto']?></p>
    <br />
</div>
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id='tab-enquetes'>
            <li>
                <a href="#resumo" data-toggle="tab">Resumo</a>
            </li>
            <li>
                <a href="#respostas" data-toggle="tab">Respostas</a>
            </li>
        </ul>
        <div class="tab-content" id="tab-content-enquetes">
            <div class="tab-pane fade in" id="resumo">
                <table class="table table-condensed table-striped">
                    <tbody>
                        <tr class="header">
                            <td>Responderam</td>
                            <td>Deixaram para depois</td>
                            <td>Rejeitaram</td>
                        </tr>
                        <tr>
                            <td><?=$totais['finalizou']?></td>
                            <td><?=$totais['adiou']?></td>
                            <td><?=$totais['rejeitou']?></td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <br />
                <table class="table table-condensed table-striped">
                    <tbody>
                    <?php foreach($listaPerguntas as $listaPergunta) : ?>
                        <tr class="header">
                            <td colspan="2">
                                <?=$listaPergunta['pergunta_texto']?>
                            </td>
                        </tr>
                        <?php foreach($listaPergunta['alternativas'] as $alternativas) : ?>
                        <tr>
                            <td><?=$alternativas['alternativa_texto']?></td>
                            <td><?=$alternativas['total_respostas']?></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="respostas">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">Cod</th>
                            <th width="25%">Nome</th>
                            <th width="35%">Pergunta</th>
                            <th width="10%">Resposta</th>
                            <th width="25%">Obs</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($resumoFormandos as $usuario) : ?>
                                <?php 
                                $count = count($usuario['EnqueteAlternativas']);
                                if($count > 0) :
                                ?>
                                <?php foreach($usuario['EnqueteAlternativas'] as $i => $resposta) : ?>
                                <tr>
                                    <?php if($i == 0) : ?>
                                    <td class="formando-resposta" rowspan="<?=$count?>">
                                        <?=isset($usuario['EnqueteUsuarios']['codigo_formando']) ? $usuario['EnqueteUsuarios']['codigo_formando'] : "-"?>
                                    </td>
                                    <td class="formando-resposta" rowspan="<?=$count?>"><?=$usuario['EnqueteUsuarios']['nome']?></td>
                                    <?php endif; ?>
                                    <td class="pergunta"><?=$usuario['EnquetePerguntas'][$i]?></td>
                                    <td class="pergunta"><?=$usuario['EnqueteAlternativas'][$i]?></td>
                                    <?php if($i == 0) : ?>
                                    <td class="formando-resposta" rowspan="<?=$count?>"><?=$usuario['EnqueteUsuarios']['observacoes']?></td>
                                    <?php endif; ?>
                                </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td class="formando-resposta"><?=isset($usuario['EnqueteUsuarios']['codigo_formando']) ? $usuario['EnqueteUsuarios']['codigo_formando'] : "-"?></td>
                                    <td class="formando-resposta"><?=$usuario['EnqueteUsuarios']['nome']?></td>
                                    <td class="formando-resposta" colspan="2"><?=$usuario['EnqueteUsuarios']['rejeitou'] == 1 ? "Rejeitou" : "Deixou pra depois"?></td>
                                    <td class="formando-resposta"><?=$usuario['EnqueteUsuarios']['observacoes']?></td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
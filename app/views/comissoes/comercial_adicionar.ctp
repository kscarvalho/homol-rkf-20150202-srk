<span id="conteudo-titulo" class="box-com-titulo-header">Membros da Comissão - Adicionar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create(null, array('url' => "/{$this->params['prefix']}/comissoes/adicionar")); ?>
	<?php include('_form.ctp'); ?>
	
	<p class="grid_12 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
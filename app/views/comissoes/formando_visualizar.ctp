<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Membro da Comissão</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Nome</label>
			<span class="grid_8 alpha first"><?php echo $formando['Usuario']['nome']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Telefone Residencial</label>
			<label class="grid_6 alpha">Telefone Comercial</label>
			<span class="grid_5 alpha first"><?php echo $formando['FormandoProfile']['tel_residencial']; ?></span>
			<span class="grid_6 alpha"><?php echo $formando['FormandoProfile']['tel_comercial']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Telefone Celular</label>
			<label class="grid_6 alpha">E-mail</label>
			<span class="grid_5 alpha first"><?php echo $formando['FormandoProfile']['tel_celular']; ?></span>
			<span class="grid_6 alpha"><?php echo $formando['Usuario']['email']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Cargo</label>
			<span class="grid_11 alpha first"><?php echo $formando['FormandoProfile']['cargo_comissao']; ?></span>			
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>
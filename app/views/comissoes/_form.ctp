<?php 
if (!$usuarioEditandoEhDaComissao) {
	echo $form->input('FormandoProfile.id',  array('hiddenField' => true));
	echo $form->input('Usuario.id',  array('hiddenField' => true));
}
?>

<p class="grid_full alpha omega">
	<label class="grid_5 alpha">Nome</label>
	<span class="grid_8 alpha first"><?php echo $form->input('Usuario.nome', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>

<p class="grid_full alpha omega">
	<label class="grid_5 alpha">RG</label>
	<span class="grid_8 alpha first"><?php echo $form->input('FormandoProfile.rg', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Telefone Residencial</label>
	<span class="grid_11 alpha first"><?php echo $form->input('FormandoProfile.tel_residencial', array('class' => 'grid_6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_6 alpha">Telefone Comercial</label>
	<span class="grid_11 alpha first"><?php echo $form->input('FormandoProfile.tel_comercial', array('class' => 'grid_6 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_6 alpha">E-mail</label>
	<span class="grid_11 alpha first"><?php echo $form->input('Usuario.email', array('class' => 'grid_11 alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Telefone Celular</label>
	<span class="grid_11 alpha first"><?php echo $form->input('FormandoProfile.tel_celular', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Cargo</label>
	<span class="grid_11 alpha first"><?php echo $form->input('FormandoProfile.cargo_comissao', array('class' => 'grid_6 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>
<?php if (!$usuarioEditandoEhDaComissao) { ?>
	<p class="grid_11 alpha omega">
	<label class="grid_6 alpha">Informações Extras</label>
	<span class="grid_11 alpha"><?php echo $form->input('FormandoProfile.anotacoes_comissao', array('class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?></span>
</p>
<?php	
}
?>


<div class="row">
    <div class="col-lg-10">
        <?php foreach($catalogos as $i => $catalogo) : ?>
        <div class="panel panel-default lista-item">
            <div class="panel-body">
                <a class="lista-titulo text-danger text-uppercase"
                    href="/<?=$this->params['prefix']?>/material_venda/detalhes/<?=$catalogo['MaterialVenda']['id']?>">
                    <?=$catalogo['MaterialVenda']['nome']?>
                    <span class="faicon faicon-angle-right faicon-lg pull-right"></span>
                    <span class="faicon faicon-refresh pull-right" style="margin-right: 10%">
                        <?=date("d/m",strtotime($catalogo['MaterialVenda']['data_ultima_alteracao']))?>
                    </span>
                </a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="row grade-itens">
    <?php foreach($catalogos as $i => $catalogo) : ?>
    <a class="col-lg-3" href="/<?=$this->params['prefix']?>/material_venda/detalhes/<?=$catalogo['MaterialVenda']['id']?>">
        <div class="thumbnail">
            <div class="caption">
                <span class="text-danger pull-right">
                    <i class="faicon faicon-refresh"></i><?=date("d/m",strtotime($catalogo['MaterialVenda']['data_ultima_alteracao']))?>
                </span>
            </div>
            <?php if(count($catalogo["MaterialVendaFoto"]) > 0) : ?>
            <img src="/fotos/fit/<?=base64_encode("upload/material_venda_fotos/{$catalogo["MaterialVendaFoto"][0]["id"]}/default.{$catalogo["MaterialVendaFoto"][0]["ext"]}")?>/400/150" />
            <?php else : ?>
            <img src="/fotos/notFound/400/300" />
            <?php endif; ?>
            <div class="caption">
                <span class="text-danger grade-titulo">
                    <?=$catalogo['MaterialVenda']['nome']?>
                </span>
            </div>
        </div>
    </a>
    <?=($i+1) % 4 == 0 ? '</div><div class="row grade-itens">' : ""?>
    <?php endforeach; ?>
</div>
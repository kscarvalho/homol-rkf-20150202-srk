<span id="conteudo-titulo" class="box-com-titulo-header">Tracking</span>

<div id="conteudo-container">
	<?php $session->flash();?>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Nome', 'Usuario.nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('E-mail', 'Usuario.email'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Ultimo Acesso','Usuario.ultimo_acesso'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Total de Acessos','Usuario.numero_acesso'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  'Membro(s) da Comissão')); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($trackingFormandos as $formando): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="30%"><?php echo $formando['Usuario']['nome']; ?></td>
					<td  colspan="1" width="40%"><?php echo $formando['Usuario']['email']; ?></td>
					<td  colspan="1" width="20%">
						<?php 
							if (!empty($formando['Usuario']['ultimo_acesso']))
								echo date('d/m/Y',strtotime($formando['Usuario']['ultimo_acesso'])); 
							else
								echo "-";
						?>
					</td>
					<td  colspan="1" width="10%"><?php echo $formando['Usuario']['numero_acesso']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	
	<p class="titulo-secao" style="margin-top:50px;">Estat&iacute;sticas</p>
	
	<div id="grafico-membros-ativos" style="margin-top: 20px;">Carregando gr&aacute;fico...</div>
	
	<?php echo $javascript->link('jscharts.js');?>
	
	<script type="text/javascript">
		var myData = new Array();
		var colors = new Array();		
		var coresDeCadaBarra = ['#E8B4B1','#E8B4B1','#E8B4B1','#DAA9A6','#D59490','#E78883','#DF6862','#C36864','#BB433D','#A71009'];
		
		<?php
		
			function removerAcentosDaPalavra($palavra) {
				$acentos = array(
				'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
				'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
				'C' => '/&Ccedil;/',
				'c' => '/&ccedil;/',
				'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
				'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
				'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
				'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
				'N' => '/&Ntilde;/',
				'n' => '/&ntilde;/',
				'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
				'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
				'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
				'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
				'Y' => '/&Yacute;/',
				'y' => '/&yacute;|&yuml;/',
				'a.' => '/&ordf;/',
				'o.' => '/&ordm;/');
				return preg_replace($acentos,array_keys($acentos),htmlentities($palavra));
			}
			
			foreach($estatisticasDosMembros as $membro) {
				$membro['Usuario']['nome'] = removerAcentosDaPalavra($membro['Usuario']['nome']);
				if (strlen($membro['Usuario']['nome']) >= 35) {
					$membro['Usuario']['nome'] = substr($membro['Usuario']['nome'], 0, 35)."...";
				}
				echo "\n adicionarUsuarioNoArrayDeDados(\"{$membro['Usuario']['nome']}\", {$membro['Usuario']['numero_acesso']});";
			}
			
		?>

		var myChart = new JSChart('grafico-membros-ativos', 'bar');
		myChart.setDataArray(myData);
		myChart.colorizeBars(colors);
		myChart.setTitle('10 membros mais ativos');
		myChart.setTitleColor('#D31217');
		myChart.setAxisNameX('');
		myChart.setAxisNameY('');
		myChart.setAxisNameFontSize(14);
		myChart.setAxisNameColor('#000');
		myChart.setAxisValuesAngle(45);
		myChart.setAxisValuesColor('#000');
		myChart.setAxisColor('#aaa');
		myChart.setAxisWidth(1);
		myChart.setBarValuesColor('#555');
		myChart.setBarOpacity(0.5);
		myChart.setAxisPaddingTop(60);
		myChart.setAxisPaddingBottom(150);
		myChart.setAxisPaddingLeft(100);
		myChart.setTitleFontSize(11);
		myChart.setBarBorderWidth(0);
		myChart.setBarSpacingRatio(50);
		myChart.setBarOpacity(0.9);
		myChart.setFlagRadius(6);
		myChart.setSize(616, 450);
		myChart.draw();
		
		function adicionarUsuarioNoArrayDeDados(nomeUsuario, numeroAcessos) {
			var novoUsuario = [nomeUsuario, numeroAcessos];
			myData.push(novoUsuario);
			colors.push(coresDeCadaBarra.pop());
		}
		
		
	</script>
	
</div>

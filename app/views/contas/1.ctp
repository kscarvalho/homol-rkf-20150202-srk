<style type="text/css">
form p .submit, form p .cancel { float:left }
form p .cancel { margin-left:0 }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">
    <?php if ($conta) : ?>
        <?= $conta['BancoConta']['cedente'] ?> - Editar
    <?php else : ?>
        Conta - Adicionar
    <?php endif; ?>
</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <?php echo $form->create('BancoConta', array('url' => "/{$this->params['prefix']}/contas/editar")); ?>
    <?= $form->hidden('BancoConta.id'); ?>
    <p class="grid_10 alpha omega first">
        <p class="grid_4 alpha omega first">
            <label class="grid_full alpha omega">Banco</label>
            <?php echo $form->input('banco_id',array(
                'class' => 'grid_15 alpha omega first',
                'type' => 'select',
                'options' => $bancos,
                'empty' => 'Selecione',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_full alpha omega">CPF/CNPJ&nbsp;<small>Apenas numeros</small></label>
            <?php echo $form->input('cpf_cnpj',array(
                'class' => 'grid_15 alpha omega',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
    </p>
    <p class="grid_10 alpha omega first">
        <p class="grid_4 alpha omega first">
            <label class="grid_full alpha omega">Cedente&nbsp;<small>Ex AS Eventos</small></label>
            <?php echo $form->input('cedente',array(
                'class' => 'grid_15 alpha omega first',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_full alpha omega">Cod Cliente&nbsp;<small>Ex 4346920</small></label>
            <?php echo $form->input('codigo_cliente',array(
                'class' => 'grid_15 alpha omega',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
    </p>
    <p class="grid_10 alpha omega first">
        <p class="grid_3 alpha omega first">
            <label class="grid_full alpha omega">Agencia&nbsp;<small>Apenas numeros</small></label>
            <?php echo $form->input('agencia',array(
                'class' => 'grid_15 alpha omega first',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_2 omega">
            <label class="grid_full alpha omega">Agencia Dv</label>
            <?php echo $form->input('agencia_dv',array(
                'class' => 'grid_8 alpha omega',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_full alpha omega">Conta&nbsp;<small>Apenas numeros</small></label>
            <?php echo $form->input('conta',array(
                'class' => 'grid_15 alpha omega',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>-
        <p class="grid_2 omega">
            <label class="grid_full alpha omega">Digit Ver</label>
            <?php echo $form->input('conta_dv',array(
                'class' => 'grid_8 alpha omega',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
    </p>
    <p class="grid_10 alpha omega first">
        <p class="grid_4 alpha omega first">
            <label class="grid_full alpha omega">Carteira</label>
            <?php echo $form->input('carteira',array(
                'class' => 'grid_15 alpha omega first',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_full alpha omega">Cateira Descri&ccedil;&atilde;o&nbsp;<small>Ex COBRAN&Ccedil;A SIMPLES - CSR</small></label>
            <?php echo $form->input('carteira_descricao',array(
                'class' => 'grid_15 alpha omega',
                'label' => false,
                'div' => false,
                'error' => false
            )); ?>
        </p>
    </p>
    <p class="grid_10 alpha omega first">
        <label class="grid_full alpha omega">Endereco&nbsp;<small>Ex Avenida Jos&eacute; Maria Whitaker, 882 | Planalto Paulista</small></label>
        <?php echo $form->input('endereco',array(
            'class' => 'grid_15 alpha omega first',
            'label' => false,
            'div' => false,
            'error' => array('wrap' => 'span', 'class' => 'grid_full')
        )); ?>
    </p>
    <p class="grid_10 alpha omega first">
        <p class="grid_6 alpha omega first">
            <label class="grid_full alpha omega">Cidade</label>
            <?php echo $form->input('cidade',array(
                'class' => 'grid_15 alpha omega first',
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_full alpha omega">UF</label>
            <?php echo $form->input('uf',array(
                'class' => 'grid_15 alpha omega',
                'type' => 'select',
                'empty' => 'Selecione',
                'options' => $form->listaUF(),
                'label' => false,
                'div' => false,
                'error' => array('wrap' => 'span', 'class' => 'grid_full')
            )); ?>
        </p>
    </p>
    <p class="grid_10 alpha omega first">
        <label class="grid_full alpha omega">Instru&ccedil;&otilde;es (Quadro de cima)</label>
        <?php echo $form->input('instrucoes_1',array(
            'class' => 'grid_15 alpha omega first',
            'label' => false,
            'type' => 'textarea',
            'div' => false,
            'error' => array('wrap' => 'span', 'class' => 'grid_full')
        )); ?>
    </p>
    <p class="grid_10 alpha omega first">
        <label class="grid_full alpha omega">Instru&ccedil;&otilde;es (Quadro de baixo)</label>
        <?php echo $form->input('instrucoes_2',array(
            'class' => 'grid_15 alpha omega first',
            'label' => false,
            'type' => 'textarea',
            'div' => false,
            'error' => array('wrap' => 'span', 'class' => 'grid_full')
        )); ?>
    </p>
    <p class="grid_10 alpha omega first">
        <br />
        <?php echo $form->input('padrao',array(
            'type' => 'checkbox',
            'label' => false,
            'div' => false,
            'error' => false
        )); ?>
        <b>&nbsp;Essa conta deve ser a padr&atilde;o para novas turmas</b>
    </p>
    <p class="grid_12 alpha omega first">
        <br />
        <?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'action' => 'index'), array('class' => 'cancel')); ?>
        <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit')); ?>
    </p>
</div>
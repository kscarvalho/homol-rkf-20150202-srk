<span id="conteudo-titulo" class="box-com-titulo-header">Contas Banc&aacute;rias</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <div class="tabela-adicionar-item">
        <?php echo $html->link('Adicionar Conta', array($this->params['prefix'] => true, 'action' => 'editar')); ?>
        <div style="clear:both;"></div>
    </div>
    <?php if(count($contas) > 0) : ?>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col" width="20%"><?php echo $paginator->sort('Banco', 'Banco.nome'); ?></th>
                    <th scope="col" width="20%"><?php echo $paginator->sort('Cod Cliente', 'codigo_cliente'); ?></th>
                    <th scope="col" width="20%"><?php echo $paginator->sort('Cedente', 'cedente'); ?></th>
                    <th scope="col" width="10%"><?php echo $paginator->sort('Padrão', 'padrao'); ?></th>
                    <th scope="col" width="30%"> &nbsp;</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?php echo $paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' . $this->name)); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach($contas as $conta): ?>
                    <?php if ($isOdd): ?>
                        <tr class="odd">
                        <?php else: ?>
                        <tr>
                        <?php endif; ?>
                        <td  colspan="1"><?php echo $conta['Banco']['nome']; ?></td>
                        <td  colspan="1"><?php echo $conta['BancoConta']['cedente']; ?></td>
                        <td  colspan="1"><?php echo $conta['BancoConta']['codigo_cliente']; ?></td>
                        <td  colspan="1"><?php echo $conta['BancoConta']['padrao'] == 1 ? 'Sim' : 'Não'; ?></td>
                        <td  colspan="1">
                            <?php echo $html->link('Ver Boleto',array($this->params['prefix'] => true, 'controller' => 'contas', 'action' => 'modelo_boleto', $conta['BancoConta']['id']), array('class' => 'submit button','target' => '_blank')); ?>
                            <?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'contas', 'action' => 'editar', $conta['BancoConta']['id']), array('class' => 'submit button')) ?>
                            <?php echo $html->link('Deletar', array($this->params['prefix'] => true, 'controller' => 'contas', 'action' => 'deletar', $conta['BancoConta']['id']), array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o item {$conta['BancoConta']['cedente']}?')) return true; else return false;"));
                            ?>
                        </td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php else : ?>
    <h2 style="color:red">Nenhuma conta cadastrada</h2>
    <?php endif; ?>
</div>
<?php
/* SVN FILE: $Id$ */

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.model
 * @since         CakePHP(tm) v 0.2.9
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Application model for Cake.
 *
 * This is a placeholder class.
* Create the same file in app/app_model.php
* Add your application-wide methods to the class, your models will inherit them.
*
* @package       cake
* @subpackage    cake.cake.libs.model
*/
class AppModel extends Model {
    /**
     * checks is the field value is unqiue in the table
     * note: we are overriding the default cakephp isUnique test as the original appears to be broken
     *
     * @param string $data Unused ($this->data is used instead)
     * @param mnixed $fields field name (or array of field names) to validate
     * @return boolean true if combination of fields is unique
     */
    function checkUnique($data, $fields) {
        if (!is_array($fields))
            $fields = array($fields);
        foreach($fields as $key)
            $tmp[$key] = $this->data[$this->name][$key];
        if (isset($this->data[$this->name][$this->primaryKey]))
            $tmp[$this->primaryKey] = "<>".$this->data[$this->name][$this->primaryKey];
        return $this->isUnique($tmp, false);
    }

    function begin() {
        $db =& ConnectionManager::getDataSource($this->useDbConfig);
        $db->begin($this);
    }


    function commit() {
        $db =& ConnectionManager::getDataSource($this->useDbConfig);
        $db->commit($this);
    }


    function rollback() {
        $db =& ConnectionManager::getDataSource($this->useDbConfig);
        $db->rollback($this);
    }

    function unbindModelAll() { 
        foreach(array( 
            'hasOne' => array_keys($this->hasOne), 
            'hasMany' => array_keys($this->hasMany), 
            'belongsTo' => array_keys($this->belongsTo), 
            'hasAndBelongsToMany' => array_keys($this->hasAndBelongsToMany)
        ) as $relation => $model) {
            $this->unbindModel(array($relation => $model),false); 
        }
    }
    
    function get($filter,$create = array()) {
        $i = $this->find('first',array(
            'conditions' => $filter
        ));
        if(!$i && !empty($create)) {
            $i = $this->create();
            $i = $create;
            if($this->saveAll($i))
                $i = $this->find('first',array(
                    'conditions' => $filter
                ));
        }
        return $i;
    }
    
    function obterNomeDoArquivo($path) {
        $nome = "";
        if(strpos($path,"/") !== false)
            $nome = array_pop(explode("/",$path));
        return $nome;
    }
    
    function obterExtensaoDoArquivo($nome) {
        $ext = "";
        if(strpos($nome,".") !== false)
            $ext = ".".array_pop(explode(".",$nome));
        return $ext;
    }

    function getLastQuery()
    {
        $dbo = $this->getDatasource();
        $logs = $dbo->_queriesLog;

        return end($logs);
    }
}
?>
//Namespace para modulo Mapas de Mesas
var MapaDeMesa = MapaDeMesa || {

    // Namespace para aplicação de agendamento de mesas
    AgendadorMesas: {}
};

// Valores dos possíveis status de uma mesa
MapaDeMesa.MESA_LIVRE       = 0;
MapaDeMesa.MESA_AGENDADA    = 1;
MapaDeMesa.MESA_ESCOLHIDA   = 2;

// Definição de cores padrões para cada estado da mesa
MapaDeMesa.CORES_LIVRE   = {
        'preenchimento': '#b7ff90',
        'contorno'     : '#8e3',
        'sombra'       : '#2a411d'
    };
MapaDeMesa.CORES_AGENDADA = {
        'preenchimento': '#decd87',
        'contorno'     : '#baa862',
        'sombra'       : '#8a7f54'
    };
MapaDeMesa.CORES_ESCOLHIDA = {
        'preenchimento': '#fd573c',
        'contorno'     : '#b53a26',
        'sombra'       : '#2a411d'
    };

/**
 * Define a quantidade de mesas que formando pode escolher
 * @type {Number}
 */
MapaDeMesa.QUANTIDADE_MESAS = 3;

/**
 * Codigo do formando
 * @type {Number}
 */
MapaDeMesa.ID_FORMANDO   = 0;
MapaDeMesa.NOME_FORMANDO = 'FORMANDO';

// Apenas criando um atalho para o namespace
var AgendadorMesas = MapaDeMesa.AgendadorMesas;

/**
 * Objeto responsável por controlar a aplicação
 * @param {[String]} canvas_id ID da tag do canvas
 */
AgendadorMesas.Palco = function(canvas_id) {
    MapaDeMesa.Palco.call(this,canvas_id);

    // Lista com as mesas selecionadas para o formando em questão
    this.mesasSelecionadas = [];
    this.listaFormandos    = [];

    this.serializarEscolhidas = function() {
        mesasSelecionadas = [];
        for(i in this.mesasSelecionadas) {
            mesa = this.mesasSelecionadas[i];
            m = {
                nome: mesa.nome,
                texto: mesa.texto,
                idFormando: mesa.idFormando,
                letra: mesa.formaMesa.letra,
                numero: mesa.formaMesa.numero
            }
            mesasSelecionadas[mesasSelecionadas.length] = m;
        }
        json = JSON.stringify(mesasSelecionadas);
        return json;
    }

    this.desserializarEscolhidas = function(json) {
        
        if(!json)
            return this;

        this.mesasSelecionadas = [];
        mesasOcupadas = JSON.parse( json );
        for(i in mesasOcupadas) {
            m = mesasOcupadas[i];
            mesa = this.retornarObjetoPeloNome(m.nome);
            if(mesa){
                mesa.ocuparMesa(m.idFormando,m.texto);
                if(m.idFormando == MapaDeMesa.ID_FORMANDO)
                    mesa.escolherMesa(m.idFormando,m.texto);
            }else{
                var regex = m.nome.match(/mesa_([0-9]*)_([0-9]*)_(.*)/);
                grupo = this.retornarObjetoPeloNome(regex[3]);
                dx = 2*parseInt(grupo.raioMesas)+(parseInt(grupo.raioMesas)/2)+parseInt(grupo.distanciaMesas);
                dy = 2*parseInt(grupo.raioMesas)+(parseInt(grupo.raioMesas)/2)+parseInt(grupo.distanciaMesas);
                x = Math.round((dx*1)+50);
                y = Math.round((dy*1)+10);
                linha = Math.round((regex[2] - 10) / dy);
                coluna = Math.round((regex[1] - 50) / dx);
                mesa = this.retornarObjetoPeloNome("mesa_"+linha+"_"+coluna+"_"+regex[3]);
                if(mesa){
                    mesa.ocuparMesa(m.idFormando,m.texto);
                    if(m.idFormando == MapaDeMesa.ID_FORMANDO)
                        mesa.escolherMesa(m.idFormando,m.texto);
                }
            }


        }
        return this;
    }

    this.aoPassarEmCimaMesa = function(mesa) {}

    this.aoPassarEmCimaMesaListener = function(aoPassarEmCima) {
        this.aoPassarEmCimaMesa = aoPassarEmCima;
    }
}

/**
 * [ObjetoMesa description]
 * @param {[MapaDeMesa.Palco]} palco  [description]
 * @param {[String]}           nome   [description]
 * @param {[String]}           status [description]
 */
AgendadorMesas.ObjetoMesa = function(palco,nome,status) {
    MapaDeMesa.IObjeto.call(this,palco,nome);

    this.status     = MapaDeMesa.MESA_LIVRE || 0;
    this.texto      = 'Livre';
    this.idFormando = 0;
    this.idMesa     = 0;
    this.arrastavel = false;

    this.cores = MapaDeMesa.CORES_LIVRE;

    /**
     * Retorna um objeto com as características graficas do objeto
     * @return {[Object]} [description]
     */
    this.retornarForma = function() {
        return {
            type: 'arc',
            name: this.nome,
            layer: true,
            fillStyle: this.cores.preenchimento,
            x: this.x, y: this.y,
            radius: this.raio,
            rotate: this.rotacao,
            strokeStyle: this.cores.contorno,
            //strokeWidth: 1,
            shadowColor: this.cores.sombra,
            shadowBlur: 2,
            shadowX: 2, shadowY: 2,
            click: (function(caller) { return function() { caller.aoClicar.apply(caller, arguments); } })(this),
            mouseover: (function(caller) { return function() { caller.mouseOverHanddler.apply(caller, arguments); } })(this),
            mouseout: (function(caller) { return function() { caller.mouseOutHanddler.apply(caller, arguments); } })(this)
        };
    }

    /**
     * Muda o status da mesa
     * @param  {[String]} status [description]
     * @return {[AgendadorMesas.ObjetoMesa]} [description]
     */
    this.ajustarStatus = function(status) {
        this.status = status;
        switch(this.status) {
            case 0:
                this.cores = MapaDeMesa.CORES_LIVRE;
                break;
            case 1:
                this.cores = MapaDeMesa.CORES_AGENDADA;
                break;
            case 2:
                this.cores = MapaDeMesa.CORES_ESCOLHIDA;
                break;
        }

        this.desenhar();
        return this;
    }

    this.mouseOverHanddler = function(layer) {
        this.palco.aoPassarEmCimaMesa(this);
        this.palco.canvas.removeLayerGroup('tooltip');
        var width = (this.texto.length * 8) + 30;
        var raio  = parseInt(this.raio);
        var x     = parseInt(this.x) + (width / 2) + raio;
        var y     = parseInt(this.y) + 20 + raio;
        if(x + (width / 2) > this.palco.canvas.width()){
            x = this.palco.canvas.width() - (width / 2) - raio;
        }
        this.palco.canvas.drawRect({
            fillStyle: '#ededed',
            strokeStyle: '#000',
            strokeWidth: 1,
            x: x, y: y,
            width: width,
            height: 40,
            cornerRadius: 10,
            layer: true,
            groups: ["tooltip"]
        });
        this.palco.canvas.drawText({
            layer: true,
            fillStyle: '#000',
            x: x, y: y,
            fontSize: 16,
            fontFamily: 'Verdana, sans-serif',
            text: this.formaMesa.letra+this.formaMesa.numero+"\n"+this.texto,
            groups: ['tooltip']
        });
    }

    this.mouseOutHanddler = function(layer) {
        this.palco.canvas.removeLayerGroup('tooltip');
    }

    this.ocuparMesa = function(idFormando,texto,status) {
        status = status || MapaDeMesa.MESA_AGENDADA;
        this.ajustarStatus(status);
        this.idFormando = idFormando;
        this.texto = texto;
    }

    /**
     * Metodo utilziado para sinalizar a escolha da mesa
     * @return {[AgendadorMesas.ObjetoMesa]} [description]
     */
    this.escolherMesa = function(idFormando,texto) {
        if (MapaDeMesa.QUANTIDADE_MESAS <= this.palco.mesasSelecionadas.length)
            return this;

        this.ocuparMesa(idFormando,texto,MapaDeMesa.MESA_ESCOLHIDA);
        this.palco.mesasSelecionadas[this.palco.mesasSelecionadas.length] = this;

        this.idFormando = MapaDeMesa.ID_FORMANDO;
        this.texto = MapaDeMesa.NOME_FORMANDO;
        return this;
    }

    this.liberarMesa = function() {
        index = this.palco.mesasSelecionadas.indexOf(this);
        this.texto = 'Livre';

        if (index < 0)
                return this;

        this.palco.mesasSelecionadas.splice(index,1);
        this.ajustarStatus(MapaDeMesa.MESA_LIVRE);
        return this;
    }

    /**
     * [ajustarCores description]
     * @param  {[String]} preenchimento  Cor do preenchimento
     * @param  {[String]} contorno       Cor do contorno
     * @return {[AgendadorMesas.ObjetoMesa]} [description]
     */
    this.ajustarCores = function(preenchimento,contorno) {
        this.cores.preenchimento = preenchimento || MapaDeMesa.CORES_LIVRE.preenchimento;
        this.cores.contorno      = contorno || MapaDeMesa.CORES_LIVRE.contorno;
        return this;
    }

    this.aoClicar = function(layer) {

        if(MapaDeMesa.PREFIXO != "planejamento" && MapaDeMesa.PREFIXO != "atendimento" && MapaDeMesa.PREFIXO != "producao"){
            if (this.status === MapaDeMesa.MESA_ESCOLHIDA){
                this.liberarMesa();
                return this;
            }

            if (this.status === MapaDeMesa.MESA_LIVRE){
                this.escolherMesa(MapaDeMesa.ID_FORMANDO,MapaDeMesa.NOME_FORMANDO);
                return this;
            }
        }

        this.palco.aoPassarEmCimaMesa(this);
    }
};

/**
 * Objeto usado para agrupar outro objetos
 * @param {[Palco]}  palco      Objeto de palco
 * @param {[String]} nomeGrupo  Nome do grupo que será criado
 */
AgendadorMesas.GrupoMesas = function(palco,nomeGrupo) {
    MapaDeMesa.GrupoMesas.call(this,palco,nomeGrupo);

    this.retornarForma = function() {
        return {}
    }

    this.criarForma = function(linha, coluna){
        var formaMesa = {
            type: 'arc',
            layer: true,
            strokeStyle: this.corMesa.contorno,
            fillStyle: this.corMesa.preenchimento,
            radius: this.raioMesas,
            groups: [this.nome]
        };
        var diametroMesa = 2 * formaMesa.radius;

        formaMesa.y = this.y + (linha * diametroMesa) + (linha * this.distanciaMesas);
        formaMesa.x = this.x + (coluna * diametroMesa) + (coluna * this.distanciaMesas);
        formaMesa.name = this.nome+"_"+linha+"_"+coluna;
        this.formasMesas.push(formaMesa);

        return formaMesa;
    }
}

/**
 * Objeto usado para agrupar outro objetos
 * @param {[Palco]}  palco      Objeto de palco
 * @param {[String]} nomeGrupo  Nome do grupo que será criado
 */
AgendadorMesas.GrupoObjetos = function(palco,nomeGrupo) {
    MapaDeMesa.GrupoObjetos.call(this,palco,nomeGrupo);

    this.retornarForma = function() {
        return {}
    }
}

/**
 * Responsável por retornar novas mesas e novos grupos de mesas
 * @param {[MapaDeMesa.Palco]} palco [description]
 */
AgendadorMesas.MesaFactory = function(palco,raioMesa,distancia) {
    MapaDeMesa.IMesaFactory.call(this,palco,raioMesa,distancia);

    /**
     * [criarMesa description]
     * @param  {[int]} x Valor de traslado x em relação ao ObjetoGrupo
     * @param  {[int]} y Valor de traslado y em relação ao ObjetoGrupo
     * @return {[MapaDeMesa.Mesa]} [description]
     */
    this.criarMesa = function(x,y,id, formaMesa, raioMesa) {
        id = id || ((Math.random() * 100) + 1).toFixed(2);
        nomeMesa = 'mesa_'+id;
        var mesa = new AgendadorMesas.ObjetoMesa(this.palco,nomeMesa);
        mesa.id = id;
        mesa.raio = raioMesa;
        mesa.posicionar(x,y);
        mesa.formaMesa = formaMesa;
        return mesa;
    };

    /**
     * [criarGrupoMesa description]
     * @return {[AgendadorMesas.GrupoObjetos]} [description]
     */
    this.criarGrupoMesa = function(objeto_grupo) {
        var nomeGrupo = objeto_grupo.nome || 'grupo_mesas';

        grupo = new AgendadorMesas.GrupoMesas(this.palco, nomeGrupo);
        grupo.x = objeto_grupo.x;
        grupo.y = objeto_grupo.y;
        grupo.ajustarLinhas(objeto_grupo.linhas)
            .ajustarColunas(objeto_grupo.colunas)
            .ajustarRaioMesas(objeto_grupo.raioMesas)
            .ajustarDistanciaMesas(objeto_grupo.distanciaMesas)
            .ajustarSetor(objeto_grupo.setor);

        for(var i=0; i < objeto_grupo.colunas ; i++ ) {
            var x = grupo.x + (i * (objeto_grupo.raioMesas * 2)) + (i * objeto_grupo.distanciaMesas);
            
            for(var j=0; j < objeto_grupo.linhas ; j++ ) {
                var y = grupo.y + (j * (objeto_grupo.raioMesas * 2)) + (j * objeto_grupo.distanciaMesas);
                var formaMesa = grupo.criarForma(j,i);
                mesa = this.criarMesa(x,y,j+'_'+i+'_'+nomeGrupo, formaMesa, objeto_grupo.raioMesas);
                if(MapaDeMesa.SETOR_FORMANDO != '' && grupo.setor != MapaDeMesa.SETOR_FORMANDO){
                    mesa.ajustarStatus(MapaDeMesa.MESA_AGENDADA);
                }
                palco.adicionarObjeto(mesa);
            }
        }
        return grupo;
    }
}

/**
 * Classe responśavel por 
 * @param {[MapaDeMesa.Palco]} palco [description]
 */
AgendadorMesas.GerenciadorLayout = function(palco) {
    MapaDeMesa.GerenciadorLayout.call(this,palco)

    /**
     * [desserializarLayout description]
     * @param  {[String]} json [description]
     * @return {[MapaDeMesa.GerenciadorLayout]} [description]
     */
    this.desserializarLayout = function(json,factory,factoryRetangulo) {
        var factory =  factory || new MapaDeMesa.MesaFactory(this.palco);
        var factoryRetangulo =  factoryRetangulo || new AgendadorMesas.RectangleFactory(this.palco);

        if (!json)
            return this;

        var grupos = JSON.parse( json );
        
        for(i in grupos) {
            g = grupos[i];

            if(g.tipo == "grupo_mesas"){
                grupo = factory.criarGrupoMesa(g);
                grupo.posicionar(g.x,g.y);

                this.palco.adicionarObjeto(grupo);
            }else if(g.tipo == "retangulo"){
                var retangulo = factoryRetangulo.criarRetangulo(g.texto, g.corPreenchimento, g.corTexto, g.altura, g.largura, g.x, g.y, g.nome)
                this.palco.adicionarObjeto(retangulo);
            }
        }
        this.palco.mapearGrupoMesas();
        return this;
    }
}

AgendadorMesas.RectangleFactory = function(palco){
    MapaDeMesa.IRectangleFactory.call(this,palco);

    this.criarRetangulo = function(texto,corPreenchimento,corTexto,altura,largura,x,y,nome){
        nome = nome || 'retangulo_'+(new Date().getTime());

        var altura = altura || 50;
        var largura = largura || 100;

        var retangulo = new AgendadorMesas.Rectangle(this.palco, nome);
        retangulo.ajustarTexto(texto).ajustarCorPreenchimento(corPreenchimento).ajustarAltura(altura).ajustarLargura(largura).ajustarCorTexto(corTexto).ajustarLarguraBorda(1);
        retangulo.x = x || 100;
        retangulo.y = y || 100;

        return retangulo;
    };
}

AgendadorMesas.Rectangle = function(palco,nome){
    MapaDeMesa.Rectangle.call(this,palco,nome);

    this.retornarForma = function(){
        var forma = {
            type: 'rectangle',
            name: this.nome,
            layer: true,
            strokeStyle: this.corBorda,
            fillStyle: this.corPreenchimento,
            strokeWidth: this.larguraBorda,
            x: this.x, 
            y: this.y,
            width: this.largura,
            height: this.altura
        };
        return forma;
    }

    this.retornarFormaTexto = function(){
        var formaTexto = {
            type: 'text',
            name: this.nome+"_texto",
            layer: true,
            fillStyle: this.corTexto,
            strokeWidth: 1,
            x: this.x, 
            y: this.y,
            fontSize: 14,
            fontFamily: 'Verdana, sans-serif',
            text: this.texto
        };
        return formaTexto;
    }

    this.desenhar = function(){
        this.forma = this.retornarForma();
        this.formaTexto = this.retornarFormaTexto();

        if (!this.layer)
            this.palco.canvas.addLayer(this.forma).drawLayers();
        else
            this.palco.canvas.setLayer(this.nome, this.forma).drawLayers();
        this.layer = this.palco.canvas.getLayer(this.nome);

        if (!this.layerTexto)
            this.palco.canvas.addLayer(this.formaTexto).drawLayers();
        else
            this.palco.canvas.setLayer(this.nome+"_texto", this.formaTexto).drawLayers();
        this.layerTexto = this.palco.canvas.getLayer(this.nome+"_texto");

        return this;
    }
}
(function($) {
    if(window.FileToBase64 == undefined) {
        var id = "filetobase64-" + (new Date().getUTCMilliseconds());
        window.FileToBase64 = function(options){
            var defaults = $.extend({
                'onload' : function() {},
                'onerror' : function() {},
                'type' : '',
                'multiple' : false,
                'min' : 0,
                'max' : 0
            },options);
            if (!window.File || !window.FileReader || !window.FileList || !window.Blob)
                defaults.onerror();
            else {
                $.each(this,function(){
                    var $this = $(this);
                    $this.bind("click",function() {
                        if($("#"+id).length == 0) {
                            var c = {
                                type : "file",
                                style : "display:none",
                                id : id
                            };
                            $("<input/>",c).appendTo($("body"));
                        }
                        var input = $("#"+id);
                        if(defaults.multiple)
                            input.attr("multiple","multiple");
                        else
                            input.removeAttr("multiple");
                        input.one("change",function(e) {
                            if(e.target.files.length > 0) {
                                var list = [],
                                    type = $.isArray(defaults.type) ? defaults.type.join('|') : defaults.type;
                                function add(f) {
                                    list.push(f);
                                    if(e.target.files.length == list.length) {
                                        input.val("");
                                        if(defaults.multiple)
                                            defaults.onload(list);
                                        else
                                            defaults.onload(list[0]);
                                    }
                                };
                                $.each(e.target.files,function(i,file) {
                                    file.valid = false;
                                    if(type != "" && !file.type.match('('+type+')')) {
                                        file.error = "type";
                                        add(file);
                                    } else if(defaults.min != 0 && file.size < defaults.min) {
                                        file.error = "min";
                                        add(file);
                                    } else if(defaults.max != 0 && file.size > defaults.max) {
                                        file.error = "max";
                                        add(file);
                                    } else {
                                        file.valid = true;
                                        var reader = new FileReader();
                                        reader.onload = function(e) {
                                            file.data = e.target.result;
                                            add(file);
                                        };
                                        reader.readAsDataURL(file);
                                    }
                                });
                            }
                        }).trigger("click");
                    });
                });
            }
        };
        $.extend( true, jQuery.fn, {
            filetobase64: window.FileToBase64
        });
    }
})(jQuery);
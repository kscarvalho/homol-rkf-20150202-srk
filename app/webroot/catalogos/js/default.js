(function($) {
    $(document).ready(function() {
        $("#icone-busca").click(function() {
            if($("#input-busca").val() != "") {
                $.ajax({
                    url : $("#url-busca").data("url"),
                    type : "POST",
                    data : {
                        data : {
                            filtroCatalogo : $("#input-busca").val(),
                            filtroMaterialVenda : $("#input-busca").val()
                        }
                    },
                    dataType : 'json',
                    success : function() {
                        window.location.href = $("#url-busca").data("url");
                    },
                    error : function() {
                        alert("Erro ao conectar ao servidor");
                    }
                });
            } else {
                alert("Digite o texto da busca");
                $("#input-busca").focus();
            }
        });
    });
})(jQuery);
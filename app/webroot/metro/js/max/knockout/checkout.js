$(document).ready(function() {
    
    ko.bindingHandlers.valueWithInit = {
        init: function(element, valueAccessor, allBindingsAccessor, data, context) {
            var property = valueAccessor(),
                    value = element.value;
            if (!ko.isWriteableObservable(data[property]))
                data[property] = ko.observable();
        
            data[property](value);
        }
    };
    
    var CheckoutModel = function() {
        var self = this;
        self.pagamentos = ko.observableArray();
        self.somaPagamentos = ko.observable(0);
        self.formaDePagamento = ko.observable('');
        self.dataBase = ko.observable('');
        self.qtdeParcelas = ko.observable(0);
        self.valores = ko.observableArray();
        self.itens = ko.observableArray();
        self.saldo = ko.observable(0);
        self.exibirFinalizar = ko.observable(false);
        self.textoSaldo = ko.observable('');
        self.usuario = ko.observable(0);
        self.carregando = ko.observable(true);
        
        self.formaDePagamentoAlterada = ko.computed(function () {
            var forma = self.formaDePagamento();
            self.pagamentos([]);
            self.dataBase('');
            var d = new Date();
            if(forma == 'dinheiro') {
                self.pagamentos([{
                    valor : ko.observable(Math.abs(self.saldo())+""),
                    data_vencimento : ko.observable(str_pad(d.getDate(),2,'0','STR_PAD_LEFT') +
                            "/" + str_pad((d.getMonth()+1),2,'0','STR_PAD_LEFT') + "/" + d.getFullYear()),
                    tipo : ko.observable('dinheiro'),
                    observacoes : ko.observable('')
                }]);
            }
        },self);
        
        self.pagamentosAlterados = ko.computed(function() {
            var soma = 0;
            $.each(self.pagamentos(),function(i,p) {
                if(p.valor() != "")
                    soma+= parseFloat(p.valor().replace(',','.'));
            });
            soma = parseFloat(soma.toFixed(2));
            self.somaPagamentos(soma);
        },self);
        
        self.dataBaseAlterada = ko.computed(function () {
            var data = self.dataBase();
            var forma = self.formaDePagamento();
            if(data != '' && forma == 'cheque')
                self.pagamentos([{
                    valor : ko.observable(Math.abs(self.saldo())+""),
                    data_vencimento : ko.observable(data),
                    tipo : ko.observable('cheque'),
                    observacoes : ko.observable('')
                }]);
        },self);
        
        self.qtdeParcelasAlterada = ko.computed(function () {
            self.exibirFinalizar(self.saldo() + self.somaPagamentos() == 0);
        },self);
        
        self.camposPagamento = function(e) {
            $(e).find('.selectpicker').selectpicker();
            $(e).find('.datepicker').datepicker();
        };
        
        self.gerarParcelas = function(uid, campanhaUsuario) {
            var data = self.dataBase().split('/').reverse();
            self.pagamentos([]);
            var total = 0;
            var adesaoComIgpm = (self.valores().adesao() + self.valores().igpm()) * -1;
            var qtdeParcelas = self.qtdeParcelas();
            $.getJSON("/checkout/obterSaldoCampanhasEAdesao/" + uid,function(response) {
                total = response.saldoCampanhas;
                if(total > 0 || self.valores().checkout() * -1 > 0){
                    qtdeParcelas = parseInt(qtdeParcelas) + 1;
                }else{
                    qtdeParcelas = parseInt(qtdeParcelas);
                }
                var primeiroMes = data[1]-1,
                    dia = data[2],
                    ano = data[0],
                    valorParcela = 0,
                    p = [];
                for(var a = 1; a <= qtdeParcelas; a++) {
                    if(a == 1){
                        if(total > 0){
                            valorParcela = total;
                        }
                        if(self.valores().checkout() * -1 > 0){
                            valorParcela += (self.valores().checkout() * -1);
                        }
                    }
                    if(valorParcela == 0)
                        valorParcela = parseFloat(((self.saldo() * -1) - (self.valores().checkout() * -1) - (total)).toFixed(2) / (self.qtdeParcelas()));
                    var d = new Date(ano, primeiroMes, dia, 0, 0, 0, 0);
                    primeiroMes++;
                    p.push({
                        valor : ko.observable(Math.abs(valorParcela).toFixed(2)),
                        data_vencimento : ko.observable(str_pad(d.getDate(),2,'0','STR_PAD_LEFT') +
                                "/" + str_pad((d.getMonth()+1),2,'0','STR_PAD_LEFT') + "/" + d.getFullYear()),
                        tipo : ko.observable('cheque'),
                        observacoes : ko.observable(''),
                    });
                    if(response.adesao == adesaoComIgpm)
                        break;
                    if((response.adesao + (self.valores().igpm() * -1)) == adesaoComIgpm)
                        break;
                    valorParcela = 0;
                }
                self.pagamentos(p);
                $.each(p,function(i) {
                    $($('.grupo')[i]).attr('data-id', i);
                });
                qtdeParcelas = 0;
            });
        };
        
        self.novaParcela = function() {
            var pagamentos = self.pagamentos();
            pagamentos.push({
                valor : ko.observable(0),
                data_vencimento : ko.observable(''),
                tipo : ko.observable('dinheiro'),
                observacoes : ko.observable('')
            });
            self.pagamentos(pagamentos);
        };
        
        self.alterarCampanha = function(campanhaUsuario, grupo) {
            var span = $("[data-campanha="+campanhaUsuario+"]");
            if(span.attr('data-cancelada') == 0) {
                var url = "/"+grupo+"/checkout/cancelar_campanha",
                    mensagem = "Confirma o cancelamento desta campanha?",
                    sucesso = "desfazer cancelamento",
                    cancelada = 1;
            } else {
                var url = "/"+grupo+"/checkout/reverter_cancelamento_campanha",
                    mensagem = "Deseja desfazer o cancelamento desta campanha?",
                    sucesso = "cancelar compra",
                    cancelada = 0;
            }
            if(confirm(mensagem)) {
                span.text("Aguarde...");
                $.ajax({
                    url : url,
                    data : {
                        usuario : self.usuario(),
                        campanha : campanhaUsuario
                    },
                    type : "POST",
                    dataType : "json",
                    success : function(response) {
                        if(response.error) {
                            alert(response.message);
                        }
                        self.pagamentos([]);
                        span.attr('data-cancelada', cancelada).text(sucesso);
                        if(sucesso == 'cancelar compra'){
                            span.parent().children(':first').text('INADIMPLENTE');
                            $.each(response.idDespesas.pagas,function(i,t) {
                                var id = $("[data-campanha-usuario="+campanhaUsuario+"]").children('tbody').find($("[data-despesa="+t+"]"));
                                id.find($("[data-status='status']")).text('paga');
                            });
                            $.each(response.idDespesas.abertas,function(i,t) {
                                var id = $("[data-campanha-usuario="+campanhaUsuario+"]").children('tbody').find($("[data-despesa="+t+"]"));
                                id.find($("[data-status='status']")).text('aberta');
                            });
                            var e = self.valores().extras() - parseFloat(response.totalCampanha);
                            self.valores().extras(e);
                        }else{
                            span.parent().children(':first').text('CANCELADA');
                            var id = $("[data-campanha-usuario="+campanhaUsuario+"]").children('tbody').find($("[data-status='status']"));
                            id.text('cancelada');
                            var e = self.valores().extras() + parseFloat(response.totalCampanha);
                            self.valores().extras(e);
                        }
                    },
                    error : function() {
                        alert("Erro ao listar parcelas. Tente novamente mais tarde.");
                    }
                });
            }
        };
        
        self.comprarItens = function(itemId) {
            var td = $("[data-item="+itemId+"]");
            var valor = td.data('valor');
            var quantidade = td.find('select').val();
            var item = {
                id : itemId,
                label : td.data('label'),
                valor : valor,
                quantidade : quantidade
            },
                itens = self.itens(),
                index = itens.length;
            $.each(itens,function(i,t) {
                if(t.id == itemId) {
                    index = i;
                    return false;
                }
            });
            itens[index] = item;
            self.itens(itens);
            td.data('saldo',valor*quantidade);
            if(valor*quantidade > 0)
                td.find('.total').text("R$"+self.formatarValor(valor*quantidade));
            else
                td.find('.total').text("R$0,00");
            var saldo = 0;
            $("[data-item]").each(function() {
                saldo-=$(this).data('saldo');
            });
            self.valores().checkout(saldo);
        };
        
        self.usuarioAlterado = ko.computed(function() {
            if(self.usuario() > 0)
                self.iniciar();
        },self);
        
        self.saldoAlterado = ko.computed(function() {
            var valores = self.valores();
            self.pagamentos([]);
            if(typeof valores.adesao != 'undefined') {
                var saldo = 0.0;
                $.each(valores,function(i,v) {
                    saldo+= v();
                });
                var texto = "-";
                if(saldo > 0)
                    texto = "<span class='fg-color-greenDark'>R$"+
                        number_format(saldo,2,',','.')+
                        "<i class='icon-plus-3'></i></span>";
                else if(saldo < 0)
                    texto = "<span class='fg-color-red'>R$"+
                        number_format(Math.abs(saldo),2,',','.')+
                        "<i class='icon-minus-3'></i></span>";
                self.saldo(parseFloat(saldo.toFixed(2)));
                self.textoSaldo(texto);
            }
        },self);
        
        self.formatarValor = function(valor) {
            if(valor != 0)
                return 'R$' + number_format(Math.abs(valor),2,',','.');
            else
                return '-';
        };
        
        self.finalizarCheckout = function() {
            if($("#interesse-album").val() == "") {
                bootbox.alert('<h3>Confirme o interesse do formando no álbum de ' +
                    'formatura <br />Aproveite para confirmar os dados ' +
                    'cadastrais dele</h3>',function() {
                        $('[data-target="#step1"]').trigger('click');
                    });
            } else {
                bootbox.confirm('Confirma o envio dos dados?',function(result) {
                    if(result) {
                        var modal = $(".bootbox.modal"),
                            modalBody = modal.find('.modal-body');
                        modal.find('.modal-footer').remove();
                        modalBody.html('<h3>Aguarde</h3>');
                        $("li[data-target]:not(.cadastro)").each(function() {
                            $($(this).data('target')).find(':input').attr('disabled','disabled');
                        });
                        var formulario = $('#formulario-checkout');
                        formulario.append($("<input/>",{
                            type : 'hidden',
                            name : 'data[saldo]',
                            value : self.saldo()
                        }));
                        $.each(self.itens(),function(i,e) {
                            if(e.quantidade > 0) {
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[itens]['+i+'][id]',
                                    value : e.id
                                }));
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[itens]['+i+'][quantidade]',
                                    value : e.quantidade
                                }));
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[itens]['+i+'][saldo]',
                                    value : (e.quantidade*e.valor)
                                }));
                            }
                        });
                        var grupos = $('.grupo');
                        var referente = [];
                        $.each(grupos,function(i,e) {
                            $.each($(e).children(),function(c, k) {
                                if($(k).data('referente') != 0){
                                    referente[i] += $(k).data('referente') + ';';
                                }
                            });
                            formulario.append($("<input/>",{
                                type : 'hidden',
                                name : 'data[pagamentos]['+i+'][referente]',
                                value : referente[i]
                            }));
                        });
                        $.each(self.pagamentos(),function(i,e) {
                            if(e.valor() > 0) {
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[pagamentos]['+i+'][tipo]',
                                    value : e.tipo()
                                }));
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[pagamentos]['+i+'][valor]',
                                    value : e.valor()
                                }));
                                formulario.append($("<input/>",{
                                    type : 'hidden',
                                    name : 'data[pagamentos]['+i+'][data_vencimento]',
                                    value : e.data_vencimento()
                                }));
                            }
                        });
                        $.ajax({
                            url : formulario.attr('action'),
                            type : 'POST',
                            data : formulario.serialize(),
                            dataType : 'json',
                            success : function(response) {
                                var texto = $("<div>");
                                if(response.mensagens.length > 0)
                                    texto.append($("<h3>",{
                                        html : 'Protocolo finalizado com pendências<br />' +
                                                response.mensagens.join('<br />'),
                                        'class' : 'fg-color-red'
                                    }));
                                else
                                    texto.append($("<h3>",{
                                        html : 'Protocolo finalizado com sucesso<br />',
                                        'class' : 'fg-color-green'
                                    }));
                                texto.append('<br />');
                                if(response.protocolo)
                                    texto.append($("<a>",{
                                        'class' : 'button default',
                                        target : '_blank',
                                        text : 'Protocolo de Retirada',
                                        href : '/atendimento/solicitacoes/imprimir_checkout/' + response.protocolo
                                    }));
                                texto.append('<br />');
                                texto.append($("<a>",{
                                    'class' : 'button bg-color-blueDark',
                                    text : 'Fechar',
                                    href : '/'
                                }));
                                modalBody.html(texto.html());
                            },
                            error : function() {
                                modalBody.html('<h3 class="fg-color-red">Erro ao enviar dados ao servidor. ' +
                                        'Tente novamente mais tarde</h3>');
                            }
                        });
                        return false;
                    }
                });
            }
        };
        
        self.iniciar = function() {
            self.valores({
                adesao : ko.observable(0),
                igpm : ko.observable(0),
                extras : ko.observable(0),
                checkout : ko.observable(0),
                pago : ko.observable(0)
            });
            var url = "/checkout/saldo_formando/"+self.usuario();
            $.getJSON(url).done(function(response) {
                if(response.saldo) {
                    $.each(response.saldo,function(c,v) {
                        self.valores()[c](v);
                    });
                    self.carregando(false);
                } else {
                    if(response.mensagem)
                        alert(response.mensagem);
                    else
                        alert('Erro ao buscas dados do formando');
                }
            }).fail(function() {
                alert('Erro ao conectar servidor');
            });
        };
    };
    var checkout = new CheckoutModel();
    ko.applyBindings(checkout, document.getElementById('checkout'));
});
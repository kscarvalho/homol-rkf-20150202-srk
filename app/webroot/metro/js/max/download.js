(function($) {
    $.download = function(settings) {
        var defaults = {
            url : null,
            urlUpdate : null,
            begin : $.noop,
            update : $.noop,
            error : $.noop,
            prepare : $.noop,
            success : $.noop
        };
        plugin = {};
        plugin.fileDefault = {
            data : ''
        };
        plugin.options = $.extend({}, defaults, settings);
        plugin.getChunk = function() {
            $.getJSON(plugin.options.urlUpdate+'/'+plugin.file.data.length,function(response) {
                if(response.error) {
                    if(response.message != undefined)
                        plugin.options.error(response.message,plugin.options);
                    else
                        plugin.options.error("Erro ao atualizar arquivo",plugin.options);
                } else {
                    plugin.file.data+= response.data.chunk;
                    plugin.porcent = (plugin.file.data.length*100)/plugin.file.size;
                    plugin.options.update(plugin.porcent);
                    if(response.data.end) {
                        plugin.options.prepare();
                        var byteChar = atob(plugin.file.data);
                        var arrayChar = new Array(byteChar.length);
                        for(var a = 0; a < byteChar.length; a++)
                            arrayChar[a] = byteChar.charCodeAt(a);
                        var byteArray = new Uint8Array(arrayChar);
                        var blob = new Blob([byteArray], {type: response.data.mime});
                        plugin.timer = null;
                        plugin.options.success(window.URL.createObjectURL(blob),response.data.name);
                    } else {
                        plugin.getChunk();
                    }
                }
            }).fail(function() {
                plugin.options.error("Erro ao conectar ao servidor",plugin.options);
            });
        };
        plugin.get = function() {
            if(plugin.options.url == null) {
                plugin.options.error("insira a url de download",plugin.options);
            } else {
                $.getJSON(plugin.options.url,function(response) {
                    if(response.error) {
                        if(response.message != undefined)
                            plugin.options.error(response.message,plugin.options);
                        else
                            plugin.options.error("O servidor encontrou um erro ao baixar o arquivo",plugin.options);
                    } else {
                        if(plugin.options.urlUpdate == null) {
                            if(response.file.url != undefined)
                                plugin.options.urlUpdate = response.file.url;
                            else
                                plugin.options.error("Url de download nao informada pelo servidor");
                        }
                        plugin.file = $.extend({}, plugin.fileDefault, response.file);
                        plugin.options.begin();
                        setTimeout(function() {
                            plugin.getChunk();
                        },1000);
                    }
                }).fail(function() {
                    plugin.options.error("Erro ao conectar ao servidor",plugin.options);
                });
            }
        };
        plugin.init = function() {
            plugin.porcent = 0.0;
            plugin.timer = null;
            plugin.get();
        };
        plugin.init();
    };
})(jQuery);
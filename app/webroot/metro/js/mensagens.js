    var mensagens = [];
    var timeMensagens;
	
	function finalizarMensagens() {
		clearInterval(timeMensagens);
	}
	
	function iniciarMensagens() {
		finalizarMensagens();
		timeMensagens = setInterval(function() { carregarMensagens(); },10000);
	}
	
    function carregarMensagens() {
	    var time = 0;
	    $.each(mensagens,function(i,mensagem) {
		    if(parseInt(mensagem.MensagemUsuario.time) > time)
			    time = parseInt(mensagem.MensagemUsuario.time);
	    });
	    var url = "/mensagens/listar/"+time;
	    $.getJSON(url, {
		    
	    }).done(function(response) {
		    var q = mensagens.length;
	    	$.each(response.mensagens, function(i, item) {
	    		var id = i + q;
	    		item.id = id;
	    		mensagens.push(item);
    			$("#mensagens ul").prepend(tmpl("mensagem-preview", item));
    			$("#mensagens ul li:first").fadeIn(500,function() { t(this); });
	    	});
		});
    }

	function t(obj) {
		$(obj).popover({
			placement: 'left',
			offset: 15,
			trigger: 'manual',
			delay: { show: 350, hide: 100 },
			html: true,
			content: '<div class="corpo"><h2>Carregando</h2></div>',
			template: '<div class="popover"><div class="arrow"></div><div class="popover-content mensagem"></div></div>',
			loaded: false
		});
		var timer, popover_parent;
	    function hidePopover(elem) {
	        $(elem).popover('hide');
	    }
	    $(obj).hover(
	        function() {
	          var self = this;
	          clearTimeout(timer);
	          $('.popover').hide();
	          popover_parent = self;
	          $(self).popover('show');
	          var id = $(obj).attr('dir') != '' ? $(obj).attr('dir') : 0;
	          if(!$(obj).data('popover').options.loaded) {
		          $.ajax({
			 	 		url : "/mensagens/exibir",
			 	 		data : { mensagem : mensagens[id].Mensagem.id },
			 	 		type : "POST",
			 	 		dataType : "json",
			 	 		success : function(response) {
				 	 		var div = $("<div>",{class:"mensagem"});
			 	 			if(response.error) {
			 	 				div.append(response.message.join("<br />"));
			 	 			} else {
			 	 				mensagens[id].Mensagem.MensagemUsuario = response.mensagem.MensagemUsuario;
			 	 				mensagens[id].Mensagem.Arquivos = response.mensagem.Arquivo;
			 	 				mensagens[id].Mensagem.texto = decode(response.mensagem.Mensagem.texto);
			 	 				$(obj).data('popover').options.content = tmpl("mensagem",mensagens[id]);
			 	 			}
			 	 			$(obj).data('popover').options.loaded = true;
					        var popover = $(obj).data('popover');
					        popover.updateContent();
					        //$(obj).popover('show');
			 	 		},
			 	 		error : function() {
			 	 			console.log('erro');
			 	 		}
			 	 	});
	          }
	        },
	        function() {
	          var self = this;
	          timer = setTimeout(function(){hidePopover(self)},300);
	    });
	    $(document).on({
	      mouseenter: function() {
	        clearTimeout(timer);
	      },
	      mouseleave: function() {
	        var self = this;
	        timer = setTimeout(function(){ hidePopover(popover_parent) },300);
	      }
	    }, '.popover');
	}
	carregarMensagens();
	iniciarMensagens();
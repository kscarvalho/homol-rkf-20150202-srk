(function($) {
    $(document).ready(function() {
        if ($.fullCalendar == undefined) {
            var s = document.getElementsByTagName('script')[0];
            var head = document.head;
            var calendar = document.createElement('script');
            calendar.type = 'text/javascript';
            calendar.async = false;
            calendar.src = "/metro/js/fullcalendar/fullcalendar.min.js";
            var jqueryui = document.createElement('script');
            jqueryui.type = 'text/javascript';
            jqueryui.async = false;
            jqueryui.src = "/metro/js/fullcalendar/jquery-ui.custom.min.js";
            head.appendChild(jqueryui);
            jqueryui.onload = jqueryui.onreadystatechange = function() {
                if ((jqueryui.readyState && jqueryui.readyState != "complete" && jqueryui.readyState != "loaded") || jqueryui.finished)
                    return;
                jqueryui.onload = jqueryui.onreadystatechange = null;
                jqueryui.finished = true;
                head.appendChild(calendar);
            };
            calendar.onload = calendar.onreadystatechange = function() {
                if ((calendar.readyState && calendar.readyState != "complete" && calendar.readyState != "loaded") || calendar.finished)
                    return;
                calendar.onload = calendar.onreadystatechange = null;
                calendar.finished = true;
                $("#calendar").trigger({type: 'carregado'});
            };
        } else {
            setTimeout(function() {
                $("#calendar").trigger({type: 'carregado'});
            }, 25);
        }
    });
})(jQuery);
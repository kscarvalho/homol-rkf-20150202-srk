(function($) {
    $(document).ready(function() {
        
        var context = ko.contextFor($("#content-body")[0]);
        var months = ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio",
            "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
        var days = ['Domingo', 'Segunda', "Terca", 'Quarta', 'Quinta', 'Sexta', 'Sabado'];
        var daysShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var atividades = [];
        var itens = [];
        var tipoSelecionado = 'todos';
        var tipos = [];
        var meus = false;
        
        $("#calendar").bind('carregado',function() {
            $('#calendar').fullCalendar({
                header: {
                    left: false,
                    center: false,
                    right: false
                },
                viewRender: function(view,element) {
                    listar(view.start.getTime()/1000,view.end.getTime()/1000);
                },
                viewDisplay: function(view) {
                    $("#current-month").html(months[$.fullCalendar.formatDate($('#calendar').fullCalendar('getDate'), 'M') - 1]);
                    $("#current-year").html($.fullCalendar.formatDate($('#calendar').fullCalendar('getDate'), 'yyyy'));
                },
                allDayText: "Dia Inteiro",
                dayNames: days,
                dayNamesShort: daysShort,
                editable: false,
                allDaySlot : false,
                eventClick: function(calEvent, jsEvent, view) {
                    var dados = calEvent.id.split('_');
                    var url = "/calendario/visualizar/"+dados[0]+"/"+dados[1];
                    carregar(url,calEvent.title);
                }
            });
            
            setTimeout(function() {
                context.$data.loaded(true);
            },500);
            
        });
        
        $(".calendario-legenda > div[data-tipo]").click(function() {
            if($(this).hasClass('filtrado')) {
                if($(this).hasClass('meus'))
                    meus = false;
                else
                    tipos.splice($.inArray($(this).data('tipo'),tipos),1);
            } else {
                if($(this).hasClass('meus'))
                    meus = true;
                else
                    tipos.push($(this).data('tipo'));
            }
            $(this).toggleClass('filtrado');
            visualizacao();
        });
        
        $("#custom-content-reveal").bind('alocar-data',function(e) {
            carregar("/calendario/alocar_salas/"+e.dia,"Alocar Salas");
        });
        
        $(".calendario-legenda > .alocar-salas").click(function() {
            var dia = parseInt($('#calendar').fullCalendar('getDate').getTime()/1000);
            carregar("/calendario/alocar_salas/"+dia,"Alocar Salas");
        });
        
        $('#custom-prev').click(function() {
            $('#calendar').fullCalendar('prev');
        });

        $('#custom-next').click(function() {
            $('#calendar').fullCalendar('next');
        });

        $('#view-month').click(function() {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        $('#view-week').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        $('#view-day').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });
        
        $("#view-go").datepicker({
            update: function(date) {
                d = date.split("/");
                $('#calendar').fullCalendar('gotoDate',d[2],d[1]-1,d[0]);
                this.selectDate(new Date(d[2], d[1] - 1, d[0]));
            }
        });
        
        $("#custom-content-reveal").on('click','.editar-atividade',function() {
            carregar("/calendario/editar/"+$(this).data('id'),$(this).data('nome'));
        });
        
        $("#custom-content-reveal").on('click','.apagar-atividade',function() {
            var button = this;
            bootbox.confirm('Deseja apagar o evento?',function(response) {
                if(response) {
                    context.$data.showLoading(function() {
                        var dados = {
                            data : {
                                id : $(button).data('id')
                            }
                        };
                        var url = "/calendario/apagar/";
                        $.ajax({
                            url : url,
                            data : dados,
                            type : "POST",
                            dataType : "json",
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                }
            });
        });
        
        $('#view-nova').click(function() {
            carregar("/calendario/editar/","Novo Evento");
        });
        
        $("#calendar-container").on('click','#fechar-evento',function(e) {
            $("#custom-content-reveal").css('top', '100%');
        });
        
        function listar(inicio,fim) {
            var url = "/calendario/listar/"+inicio+"/"+fim;
            $(".carregando-eventos").fadeIn(500,function() {
                $.getJSON(url).done(function(response) {
                    itens = [];
                    $("#calendar").fullCalendar('removeEvents');
                    $.each(response.lista, function(i, item) {
                        item.start = new Date(item.data_inicio*1000);
                        item.end = new Date(item.data_fim*1000);
                        itens.push(item);
                        $("#calendar").fullCalendar('renderEvent',item,false);
                        
                    });
                    visualizacao();
                    $(".carregando-eventos").fadeOut(500);
                });
            });
        }
        
        function visualizacao() {
            $.each(itens,function(i,item) {
                if(tiposPermitidos(item.filtro) && usuarioId(item.usuario_id))
                    item.className = '';
                else
                    item.className = 'hide';
                if($("#usuario_id").val() == item.usuario_id)
                    item.className+= ' meu';
                else if(item.usuario_grupo == 'comissao')
                    item.className+= ' comissao';
                if(item.google == 1)
                    item.backgroundColor = 'rgb(8, 161, 18)!important';
                $('#calendar').fullCalendar('updateEvent', item);
            });
        }
        
        function tiposPermitidos(tipo) {
            if(tipos.length > 0)
                return $.inArray(tipo,tipos) >= 0;
            else
                return true;
        }
        
        function usuarioId(id) {
            if(meus)
                return id == $("#usuario_id").val();
            else
                return true;
        }
        
        function carregar(url,titulo) {
            $("#titulo-evento").html(titulo);
            $("#conteudo-evento").html("Carregando");
            $("#fechar-evento").hide();
            $("#custom-content-reveal").animate({
                top: '0%'
            },0,function() {
                setTimeout(function() {
                    $("#conteudo-evento").load(url,function() {
                        $("#fechar-evento").show();
                    });
                },800);
            });
        }
        
    });
})(jQuery);
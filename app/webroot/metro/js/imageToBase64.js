(function($) {
    $(document).ready(function() {
        $.extend( true, jQuery.fn, {
            imageToBase64: function(options){
                var defaults = {
                    'success' : $.noop,
                    'error' : $.noop,
                    'size' : {
                        'min' : 0,
                        'max' : 0
                    }
                };
                if(options)
                    $.extend(true,defaults,options);
                $.each(this,function(){
                    var $this = $(this);
                    $this.bind("click",function() {
                        if($("#input-imageTobase64").length == 0) {
                            var input = $("<input/>",{
                                type : "file",
                                style : "display:none",
                                id : "input-imageTobase64"
                            });
                            input.appendTo($("body"));
                        } else {
                            var input = $("#input-imageTobase64");
                        }
                        input.one("change",function(e) {
                            if(e.target.files.length > 0) {
                                var file = e.target.files[0];
                                if(!file.type.match('image.*')) {
                                    if(defaults.error) defaults.error("type");
                                    return;
                                } else if(defaults.size.min != 0 && file.size < defaults.size.min) {
                                    if(defaults.error) defaults.error("small");
                                } else if(defaults.size.max != 0 && file.size > defaults.size.max) {
                                    if(defaults.error) defaults.error("big");
                                }
                                var reader = new FileReader();
                                reader.onload = (function(f) {
                                    return function(file) {
                                        f.data = file.target.result;
                                        if(defaults.success) defaults.success(f);
                                        input.remove();
                                    };
                                })(file);
                                reader.readAsDataURL(file);
                            }
                        }).trigger("click");
                    });
                });
            }
        });
    });
})(jQuery);
<?php

class IgpmComponent extends Object {

    var $uses = array('ViewFormandos', 'Despesa', 'DespesaPagamento', 'Pagamento');

    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }

    function gerarDespesa($formando) {
        $despesas = $this->Despesa->find('all', array('conditions' =>
            array(
                'Despesa.usuario_id' => $formando['id'],
                'status' => 'paga',
                "(status_igpm not in ('pago','recriado') or status_igpm is null)",
                'tipo' => 'adesao',
                'correcao_igpm > 0',
                'correcao_igpm is not null'
        )));
        $valor = 0;
        foreach ($despesas as $despesa)
            $valor+= $despesa['Despesa']['correcao_igpm'];
        if ($valor > 0) {
            $despesaIgpm = array(
                'valor' => $valor,
                'parcela' => '0',
                'total_parcelas' => '0',
                'tipo' => 'igpm',
                'usuario_id' => $formando['id'],
                'data_cadastro' => date('Y-m-d H:i:s')
            );
            $this->Despesa->create();
            if ($this->Despesa->save(array('Despesa' => $despesaIgpm))) {
                $this->Despesa->updateAll(
                        array('Despesa.status_igpm' => "'recriado'"), array(
                    'Despesa.usuario_id' => $formando['id'],
                    "Despesa.status" => 'paga',
                    "(status_igpm not in ('pago','recriado') or status_igpm is null)",
                    'tipo' => 'adesao',
                    'correcao_igpm > 0',
                    'correcao_igpm is not null'
                        )
                );
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function atualizarDespesa($formando) {
        $despesaIgpm = $this->Despesa->find('first',
                array('conditions' => array('Despesa.usuario_id' => $formando['id'], 'tipo' => 'igpm')));
        if($despesaIgpm) {
            $despesas = $this->Despesa->find('all', array('conditions' =>
                array(
                    'Despesa.usuario_id' => $despesaIgpm['Despesa']['usuario_id'],
                    'status' => 'paga',
                    'tipo' => 'adesao',
                    'status_igpm' => 'nao_pago'
            )));
            $valor = 0;
            $ids = array();
            foreach ($despesas as $despesa) {
                $valor+= $despesa['Despesa']['correcao_igpm'];
                $ids[] = $despesa['Despesa']['id'];
                /*
                $valorDespesa = $despesa['Despesa']['correcao_igpm']+
                        $despesa['Despesa']['valor']+$despesa['Despesa']['multa'];
                if($despesa['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $valorDespesa)
                    $valor+= $valorDespesa-$despesa['DespesaPagamento'][0]['Pagamento']['valor_pago'];
                 * 
                 */
            }
            $novoValor = $despesaIgpm['Despesa']['valor'] + $valor;
            if ($despesaIgpm['Despesa']['valor'] < $novoValor) {
                if ($this->Despesa->updateAll(
                        array('Despesa.status_igpm' => "'recriado'"),
                        array(
                            'Despesa.usuario_id' => $despesaIgpm['Despesa']['usuario_id'],
                            'Despesa.id' => $ids
                        )))
                    return $this->Despesa->updateAll(
                        array('Despesa.valor' => $novoValor, 'Despesa.status' => "'aberta'"),
                        array('Despesa.id' => $despesaIgpm['Despesa']['id'])
                    );
                else
                    return false;
            } elseif ($despesaIgpm['Despesa']['valor'] <= $this->obterTotalPago($formando) && $despesaIgpm['Despesa']['status'] != "paga") {
                return $this->Despesa->updateAll(
                        array('Despesa.status' => "'paga'"), array('Despesa.id' => $despesaIgpm['Despesa']['id'])
                );
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    function obterDespesa($formando) {
        return $this->Despesa->find('first', array('conditions' =>
                    array('Despesa.usuario_id' => $formando['id'], 'Despesa.tipo' => 'igpm')));
    }

    function obterTotalPago($formando) {
        $totalPago = $this->Pagamento->find('first', array(
            'conditions' => array('usuario_id' => $formando['id'], 'tipo' => 'boleto_igpm', 'status' => 'pago'),
            'fields' => array('SUM(valor_pago) as totalPago')
        ));
        return $totalPago[0]['totalPago'];
    }

    function gerarParcela($formando, $valor, $data = null) {
        $pagamento = array(
            'usuario_id' => $formando['id'],
            'dt_cadastro' => date('Y-m-d H:i:s'),
            'dt_vencimento' => $data ? $data : NULL,
            'codigo' => str_pad($formando['codigo_formando'], 8, '1', STR_PAD_LEFT) . str_pad($this->obterNumeroProximaParcela($formando), 5, '9', STR_PAD_LEFT),
            'tipo' => 'boleto_igpm',
            'status' => 'aberto',
            'valor_nominal' => $valor,
            'valor_pago' => 0
        );
        $this->Pagamento->create();
        return $this->Pagamento->save(array('Pagamento' => $pagamento));
    }

    function obterNumeroProximaParcela($formando) {
        return $this->Pagamento->find('count', array('conditions' => array('Pagamento.usuario_id' => $formando['id'], 'Pagamento.tipo' => 'boleto_igpm'))) + 1;
    }

    function obterParcelas($formando) {
        return $this->Pagamento->find('all', array('conditions' => array(
                        'Pagamento.usuario_id' => $formando['id'], 'Pagamento.tipo' => 'boleto_igpm', "Pagamento.codigo <> ''"),
                    'order' => array('Pagamento.status desc')));
    }

    function obterParcelaPorId($idParcela) {
        return $this->Pagamento->find('first', array('conditions' => array('Pagamento.tipo' => 'boleto_igpm', 'Pagamento.id' => $idParcela)));
    }

}

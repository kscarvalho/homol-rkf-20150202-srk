<?php

/**
 * Classe para fazer conexão com o wordpress e gerenciar as páginas
 */
class WpGerenciadorComponent extends Object {
    
    function __construct() {
        parent::__construct();
        $this->wpDomain       = Configure::read('WpGerenciador.wpDomain');
        $this->wpTema         = Configure::read('WpGerenciador.wpTheme');
    }
    
    /**
     * Verifica a disponibilidade de um domínio
     * 
     * @param string $slug Slug a ser verificada
     * 
     * @return boolean
     */
    public function verificaDisponibilidade($slug) {
        $id = $this->_funcaoCodex('', 'get_id_from_blogname', [$slug]);
        return $id ? true : false;
    }
    
    /**
     * Cria um novo site no Wordpress
     * @param string $slugSite  Slug do novo site
     * @param int    $turmaId   Id da turma
     * @param string $turmaNome Nome da turma
     * 
     * @return boolean
     */
    public function criarSite($slugSite, $turmaId, $turmaNome) {
        $domain  = $this->wpDomain;
        $path    = '/'.$slugSite;
        $title   = $turmaNome;
        $user_id = '';
        $meta    = ['turma' => $turmaId];
        $parametros = [$domain, $path, $title, $user_id, $meta];
        $blog_id = $this->_funcaoCodex('', 'simple_webservice_create_blog', $parametros);
        $this->_funcaoCodex($slugSite, 'switch_theme', [$this->wpTema]);
        return $blog_id;
    }
    
    /**
     * Atualiza o facebook do site
     * @param string $slugSite Slug do site
     * @param string $facebook Facebook da turma
     * @return boolean
     */
    public function atualizaFacebookFanPage($slugSite, $facebook) {
        $option    = 'facebook_fan_page';
        $new_value = $facebook;
        return $this->_funcaoCodex($slugSite, 'update_option', [$option, $new_value]);
    }
    
    /**
     * Atualiza a logo do site
     * @param string $slugSite     Slug do site
     * @param string $base64String String base64 da imagem
     * @return attachment_id
     */
    public function atualizaLogo($slugSite, $base64String){
        $params  = ['type' => 'insert_attachment', 'file' => $base64String];
        $idMidia = $this->_fazerCurl($slugSite, $params);
        
        $option    = 'logo_turma';
        $new_value = $idMidia;
        $this->_funcaoCodex($slugSite, 'update_option', [$option, $new_value]);
        
        return $idMidia;
    }
    
    /**
     * Adiciona uma foto ao carrossel da home
     * @param string $slugSite    Slug do site
     * @param string $dirUpload   Diretorio do arquivo
     * @param string $nomeArquivo Nome do arquivo
     * @param string $descricao   Texto descritivo da imagem
     * @param string $local       Local de exibição da imagem
     * @return boolean
     */
    public function adicionaFotoHome($slugSite, $base64String, $local, $descricao){
        $params     = ['type' => 'insert_attachment', 'file' => $base64String, 'content' => $descricao];
        $idMidia    = $this->_fazerCurl($slugSite, $params);
        $post_id    = $idMidia;
        $meta_key   = 'local_midia';
        $meta_value = $local;
        $this->_funcaoCodex($slugSite, 'update_post_meta', [$post_id, $meta_key, $meta_value]);
        return $idMidia;
    }

    /**
     * Remove uma foto da home
     * @param string $slugSite    Slug do site
     * @param string $post_id     Id do post
     */
    public function removeFotoHome($slugSite, $post_id){
        return $this->_funcaoCodex($slugSite, 'wp_delete_post', [$post_id, TRUE]);
    }

    /**
     * Remove uma página
     * @param string $slugSite    Slug do site
     * @param string $term_id     Id do term
     */
    public function removePagina($slugSite, $term_id){
        return $this->_funcaoCodex($slugSite, 'wp_delete_term', [$term_id, 'category']);
    }
    
    /**
     * Adiciona uma foto em uma pagina
     * @param string $slugSite     Slug do site
     * @param string $base64String String base64 da imagem
     * @param string $descricao    Descrição da imagem
     * @param int    $idPagina     Id da página
     * @return int
     */
    public function adicionaFotoPagina($slugSite, $base64String, $descricao, $idPagina){
        $params      = ['type' => 'insert_attachment', 'file' => $base64String, 'content' => $descricao];
        $idMidia     = $this->_fazerCurl($slugSite, $params);
        $idCategoria = $this->_funcaoCodex($slugSite, 'get_option', ['pagina_'.$idPagina]);
        if($idCategoria){
            $this->_funcaoCodex($slugSite, 'wp_set_post_categories', [
                $idMidia,
                [$idCategoria]
            ]);
        }
        return $idMidia;
    }

    /**
     * Remove uma página
     * @param string $slugSite    Slug do site
     * @param string $term_id     Id do term
     * @param string $args        Parâmetros que serão atualizados
     */
    public function atualizaPagina($slugSite, $term_id, $args){
        return $this->_funcaoCodex($slugSite, 'wp_update_term', [$term_id, 'category', $args]);
    }
    
    /**
     * Cria uma categoria para ser utilizada como página
     * @param string $slugSite  Slug do site
     * @param string $nome      Nome da categoria
     * @param string $descricao Descrição da categoria
     * @param string $idPagina  Id da pagiana para associar
     * @return int
     */
    public function criarCategoria($slugSite, $nome, $descricao, $idPagina){
        $parametros  = ['description' => $descricao];
        $idCategoria = $this->_funcaoCodex($slugSite, 'wp_insert_term', [$nome, "category", $parametros])->term_taxonomy_id;
        $option      = 'pagina_'.$idPagina;
        $new_value   = $idCategoria;
        $this->_funcaoCodex($slugSite, 'update_option', [$option, $new_value]);
        return $idCategoria;
    }
    
    /**
     * Retorna a url de uma imagem a partir do id do attachment
     * @param string $slugSite     Slug do site
     * @param int    $idAttachment Id do arrachment
     * @return string
     */
    public function getImagemUrlDaMidia($slugSite, $idAttachment){
        return $this->_funcaoCodex($slugSite, 'wp_get_attachment_url', [$idAttachment]);
    }
    
    /**
     * Executa uma função do Codex
     * @param string $slugSite   Slug do site aonde a função será executada
     * @param string $funcao     Nome da função
     * @param array  $parametros Parâmetros que serão passados para a função
     * @return mixed O retorno da função do Codex
     */
    private function _funcaoCodex($slugSite, $funcao, $parametros = []){
        $params['type']      = 'function';
        $params['function']  = $funcao;
        $params['arguments'] = $parametros;
        return $this->_fazerCurl($slugSite, $params);
    }
    
    /**
     * Executa uma chamada CURL
     * @param string $slugSite
     * @param array  $params
     * @return mixed
     */
    private function _fazerCurl($slugSite, $params = []){
        $params['webservicekey'] = Configure::read('WpGerenciador.webServiceKey');
        $ch  = curl_init();
        $url = $this->wpDomain . '/' . $slugSite;
        $field_string = http_build_query($params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
    
}
<?php

class FaleConoscoComponent extends Object {

    var $uses = array('Mensagem', 'Assunto', 'Item',
        'MensagemUsuario', 'Usuario');
    var $itemAtendimento;
    var $horasTrabalhadas = 8;
    var $horaIntervalo = 13;
    var $horaFinal = 18;
    var $horaInicio = 9;

    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
        $this->Mensagem->unbindModel(array(
            'hasAndBelongsToMany' => array("Arquivo")
        ),false);
        $this->Mensagem->Usuario->unbindModel(array(
            'hasOne' => array('FormandoProfile'),
            'hasMany' => array('Despesa','UsuarioConta','FormandoFotoTelao','Cupom'),
            'hasAndBelongsToMany' => array('Turma','Campanhas')
        ),false);
        $this->Assunto->unbindModel(array(
            'hasMany' => array('Mensagem'),
            'belongsTo' => array('Turma','Item')
        ),false);
        $this->MensagemUsuario->unbindModel(array(
            'belongsTo' => array('Usuario','Mensagem')
        ),false);
    }
    
    function obterItem() {
        if(!$this->itemAtendimento)
            $this->itemAtendimento = $this->Item->find('first',array(
                'conditions' => array(
                    'Item.nome' => 'Fale Conosco'
                )
            ));
        return $this->itemAtendimento;
    }
    
    function mensagensNaoRespondidasPorAtendente($atendenteId) {
        $item = $this->obterItem();
        $options = array(
            'conditions' => array(
                'MensagemUsuario.usuario_id' => $atendenteId,
                'Assunto.item_id' => $item['Item']['id'],
                "Mensagem.usuario_id <>" => $atendenteId,
                'Mensagem.pendente' => 1
            ),
            'joins' => array(
                array(
                    'table' => 'mensagens_usuarios',
                    'alias' => 'MensagemUsuario',
                    'conditions' => array(
                        'Mensagem.id = MensagemUsuario.mensagem_id'
                    )
                )
            )
        );
        $this->Mensagem->unbindModelAll();
        $this->Mensagem->bindModel(array(
            'belongsTo' => array("Usuario","Assunto")
        ),false);
        $mensagens = $this->Mensagem->find('all',$options);
        return $mensagens;
    }
    
    function mensagensRespondidasPorAtendente($atendenteId) {
        $item = $this->obterItem();
        $this->Mensagem->bindModel(array(
            'belongsTo' => array("Assunto")
        ),false);
        $this->Mensagem->bindModel(array(
            'hasMany' => array(
                'MensagemUsuario' => array(
                    'className' => 'MensagemUsuario',
                    'finderQuery' => 'select `MensagemUsuario`.`data_envio`,'
                    . ' `MensagemUsuario`.`data_favorita`, `MensagemUsuario`.`data_cadastro`,'
                    . 'Usuario.* from mensagens_usuarios as MensagemUsuario inner join '
                    . 'usuarios as Usuario on MensagemUsuario.usuario_id = Usuario.id '
                    . 'where MensagemUsuario.mensagem_id in({$__cakeID__$}) and '
                    . '(Usuario.grupo = \'formando\')',
                    'foreignKey' => false
                )
            )
        ),false);
        $mensagens = $this->Mensagem->find('all',array(
            'conditions' => array(
                'Mensagem.usuario_id' => $atendenteId,
                'Assunto.item_id' => $item['Item']['id']
            )
        ));
        return $mensagens;
    }
    
    function perguntaRespondida($formandoId,$atendenteId,$mensagemId) {
        $item = $this->obterItem();
        $this->Mensagem->unbindModelAll();
        $this->Mensagem->bindModel(array(
            'belongsTo' => array("Usuario")
        ),false);
        $options = array(
            'conditions' => array(
                'Assunto.item_id' => $item['Item']['id'],
                'MensagemUsuario.usuario_id' => $atendenteId,
                'Mensagem.pendente' => 0,
                'Mensagem.usuario_id' => $formandoId,
                'Mensagem.id < ' => $mensagemId
            ),
            'joins' => array(
                array(
                    'table' => 'mensagens_usuarios',
                    'alias' => 'MensagemUsuario',
                    'conditions' => array(
                        'Mensagem.id = MensagemUsuario.mensagem_id'
                    )
                ),
                array(
                    'table' => 'assuntos',
                    'alias' => 'Assunto',
                    'conditions' => array(
                        'Assunto.id = Mensagem.assunto_id'
                    )
                )
            ),
            'order' => array(
                'Mensagem.id' => 'desc'
            )
        );
        $pergunta = $this->Mensagem->find('first',$options);
        return $pergunta;
    }
    
    function detalhesPorAtendente($atendenteId) {
        $mensagens = $this->mensagensRespondidasPorAtendente($atendenteId);
        $perguntas = 0;
        $tempoTotal = 0;
        $media = '';
        if($mensagens) {
            foreach($mensagens as $mensagem) {
                if(isset($mensagem['MensagemUsuario'][0]['Usuario'])) {
                    $resposta = $this->perguntaRespondida(
                        $mensagem['MensagemUsuario'][0]['Usuario']['id'],
                        $atendenteId,
                        $mensagem['Mensagem']['id']);
                    $tempo = $this->tempoDeResposta($resposta['Mensagem']['data'],
                            $mensagem['Mensagem']['data']);
                    $tempoTotal+= $tempo['segundos'];
                    $perguntas++;
                }
            }
            $tempoMedio = floor($tempoTotal/$perguntas);
            $media = $this->converterSegundosParaTexto($tempoMedio);
        }
        return array(
            'media' => $media,
            'respostas' => $perguntas
        );
    }
    
    function tempoDeResposta($dataPergunta,$dataResposta) {
        
        if(date('H',strtotime($dataPergunta)) >= $this->horaFinal &&
                date('d',strtotime($dataPergunta)) < date('d',strtotime($dataResposta)))
            $dataPergunta = date("Y-m-d 0{$this->horaInicio}:00:00",
                    strtotime($dataPergunta . ' + 1 days'));
            
        if((int)date('N',strtotime($dataPergunta)) >= 6 &&
                (int)date('N',strtotime($dataResposta)) < 6)
            $dataPergunta = date("Y-m-d 0{$this->horaInicio}:00:00",
                    strtotime($dataPergunta . ' + ' . (8-(int)date('N',strtotime($dataPergunta))) .
                        ' days'));
        
        if(date('H',strtotime($dataPergunta)) < $this->horaInicio)
            $dataPergunta = date("Y-m-d 0{$this->horaInicio}:00:00",
                    strtotime($dataPergunta));
        
        $timePergunta = strtotime($dataPergunta);
        $timeResposta = strtotime($dataResposta);
        
        if(date('d',$timePergunta) < date('d',$timeResposta)) {
            
            //subtrai uma hora de perguntas respondidas depois do almoco
            if(date('H',$timeResposta) > $this->horaIntervalo)
                $timeResposta = strtotime(date("Y-m-d H:i:s",
                        strtotime($dataResposta . ' - 1 hours')));
            
            //adiciona uma hora a perguntas feitas antes do almoco
            if(date('H',$timePergunta) < $this->horaIntervalo)
                $timePergunta = strtotime(date("Y-m-d H:i:s",
                        strtotime($dataPergunta . ' + 1 hours')));
            
            //calcula tempo da resposta em relacao ao inicio do expediente 09:00
            $segundos = $this->tempoParaFimDeExpediente($timePergunta);
            //calcula o restante dos dias baseado na jornada de 8 horas de trabalho
            $segundos+= $this->tempoAposInicioDeExpediente($timeResposta);
            $dias = (date('d',$timeResposta) - date('d',$timePergunta))-1;
            
            for($a = 1; $a <= $dias; $a++)
                if((int)date('N',strtotime($dataPergunta . " + {$a} days")) < 6)
                    $segundos+= 1*$this->horasTrabalhadas*60*60;
        } else {
            
            //adiciona uma hora a perguntas feitas antes do almoco e respondidas depois
            if(date('H',$timePergunta) < $this->horaIntervalo &&
                    date('H',$timeResposta) > $this->horaIntervalo)
                $timePergunta = strtotime(date("Y-m-d H:i:s",
                        strtotime($dataPergunta . ' + 1 hours')));
            
            $segundos = $timeResposta-$timePergunta;
        }
        return array(
            'label' => $this->converterSegundosParaTexto($segundos),
            'segundos' => $segundos,
            'pergunta' => date('Y-m-d H:i:s',$timePergunta),
            'resposta' => date('Y-m-d H:i:s',$timeResposta),
        );
    }
    
    function tempoParaFimDeExpediente($time) {
        $horas = ($this->horaFinal-1)-date('H',$time);
        if($horas >= $this->horasTrabalhadas) $horas = $this->horasTrabalhadas-1;
        $minutos = 59-date('i',$time);
        $segundos = 60-date('s',$time);
        return ($horas*3600)+($minutos*60)+$segundos;
    }
    
    function tempoAposInicioDeExpediente($time) {
        $horas = date('H',$time)-$this->horaInicio;
        if($horas >= $this->horasTrabalhadas) $horas = $this->horasTrabalhadas-1;
        return ($horas*3600)+(date('i',$time)*60)+date('s',$time);
    }
    
    function converterSegundosParaTexto($segundos,$horasPorDia = false) {
        if(!$horasPorDia)
            $horasPorDia = $this->horasTrabalhadas;
        $tempo = array();
        if(floor($segundos/60/60/$horasPorDia) > 0)
            $tempo[] = floor($segundos/60/60/$horasPorDia) . "d";
        if(floor(($segundos/60/60%$horasPorDia)) > 0)
            $tempo[] = floor(($segundos/60/60%$horasPorDia)) . "h";
        if(floor($segundos/60%60) > 0)
            $tempo[] = floor($segundos/60%60) . "min";
        if(floor($segundos%60) > 0)
            $tempo[] = floor($segundos%60) . "s";
        return implode(' ',$tempo);
    }
    
}
<?php

class AndroidNotificationsComponent {

    var $url = 'https://android.googleapis.com/gcm/send';
    var $serverApiKey = "AIzaSyC78ciKkA_hzmz6hS6aedIv7qYCklojOR8";
    var $devices = array();

    function setDevices($deviceIds) {

        if (is_array($deviceIds)) {
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    }

    function send($data) {

        if (!is_array($this->devices) || count($this->devices) == 0)
            return false;

        if (strlen($this->serverApiKey) < 8)
            return false;

        $fields = array(
            'registration_ids' => $this->devices,
            'data' => $data,
        );

        $headers = array(
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $this->url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        $result = curl_exec($ch);
        
        curl_close($ch);

        return $result;
    }

}

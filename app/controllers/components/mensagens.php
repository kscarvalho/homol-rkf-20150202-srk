<?php

class MensagensComponent extends Object {

    var $uses = array('Item', 'Assunto', 'Mensagem',
        'MensagemUsuario','Arquivo','Usuario');

    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    function inserir($data) {
        $this->Mensagem->create();
        $return = array('erro' => true,'mensagem' => array(),'mensagem_id' => false);
        if($this->Mensagem->save($data)) {
            $return['erro'] = false;
            $mensagem = array('id' => $this->Mensagem->getLastInsertId());
            $return['mensagem_id'] = $mensagem['id'];
            if(isset($data['Arquivo']))
                foreach($data['Arquivo'] as $arquivo) {
                    $arquivo['nome_secreto'] = $this->Arquivo->generateUniqueId();
                    $arquivo['diretorio'] = $this->Arquivo->file_path();
                    $arquivo['path'] = $arquivo['diretorio'].
                            $arquivo['nome_secreto'];
                    if($this->Arquivo->saveBase64File($arquivo['src'],
                            $arquivo['path'])) {
                        $this->Arquivo->create();
                        $arquivo['usuario_id'] = $data['Mensagem']['usuario_id'];
                        $arquivo['turma_id'] = $data['Mensagem']['turma_id'];
                        $arquivo['formando_id'] = null;
                        $arquivo['criado'] = date('Y-m-d H:i:s');
                        if(!$this->Arquivo->save(array('Arquivo' => $arquivo,'Mensagem' => $mensagem),
                                array('callbacks' => false))) {
                            $return['mensagem'][] = "Erro ao inserir anexos";
                            break;
                        }
                    }
                }
        } elseif(isset($this->Mensagem->validationErrors)) {
            $return['mensagem'] = array_merge($return['mensagem'],array_values($this->Mensagem->validationErrors));
        }
        return $return;
    }
    
    function alocarDestinatarios($usuarios,$mensagem) {
        if (!empty($mensagem)) {
            $hoje = date('Y-m-d H:i:s');
            foreach ($usuarios as $usuario) {
                $save = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'mensagem_id' => $mensagem['Mensagem']['id'],
                    'data_cadastro' => $hoje);
                if ($usuario['Usuario']['id'] == $mensagem['Usuario']['id'])
                    $save['lida'] = 1;
                $this->MensagemUsuario->create();
                $this->MensagemUsuario->save($save);
            }
        }
    }
    
    function buscarDestinatarios($mensagemId,$mensagem = false) {
        
    }
}
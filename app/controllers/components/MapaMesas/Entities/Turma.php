<?php

class MapaDeMesasTurma {

	/**
	 * Identificação única da turma
	 * @var Integer
	 */
	private $id;

	/**
	 * Identificação única da turma
	 * @var Integer
	 */
	private $titulo;

	/**
	 * Lista de Formandos
	 * @var Array<MapaDeMesasFormando>
	 */
	private $formandos;

	/**
	 * Lista de eventos
	 * @var MapaDeMesasEvento
	 */
	private $eventos;

	public function __construct() {
	}
}
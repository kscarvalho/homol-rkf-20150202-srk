<?php

class MaterialVendaController extends AppController {

	var $uses = array("MaterialVenda","MaterialVendaItem","MaterialVendaFoto",
		"MaterialVendaAnexo","TiposEvento","MaterialVendaEventoItem");
	
	private function _index() {
		$this->layout = "material_venda";
		$this->Session->delete("eventoId");
		$this->Session->delete("itemId");
		$this->Session->delete("materialVendaId");
		$this->Session->delete("filtroMaterialVenda");
		$this->_itens();
	}
	
	function comercial_index() {
		$this->_index();
	}
	
	function marketing_index() {
		$this->_index();
	}

	function planejamento_index() {
		$this->_index();
	}

	private function _evento($eventoId) {
		$this->layout = "material_venda";
		$this->Session->write("eventoId",$eventoId);
		$this->Session->delete("itemId");
		$this->Session->delete("materialVendaId");
		$this->Session->delete("filtroMaterialVenda");
		$this->_itens();
	}
	
	function comercial_evento($eventoId) {
		$this->_evento($eventoId);
	}
	
	function marketing_evento($eventoId) {
		$this->_evento($eventoId);
	}

	function planejamento_evento($eventoId) {
		$this->_evento($eventoId);
	}
	
	private function _item($itemId, $tipo) {
		$this->layout = "material_venda";
		if(!$this->Session->check("eventoId"))
			$this->redirect("/comercial/material_venda");
		$this->Session->delete("materialVendaId");
		$this->Session->delete("filtroMaterialVenda");
		$this->Session->write("itemId",$itemId);
		$this->_itens();
		$view = $tipo == 'lista' ? 'lista' : 'grade';
		$this->set('view',$view);
	}
	
	function comercial_item($itemId, $tipo = 'grade') {
		$this->_item($itemId,$tipo);
	}

	function comercial_busca($tipo = 'grade') {
		$this->_busca($tipo);
	}
	
	function marketing_item($itemId, $tipo = 'grade') {
		$this->_item($itemId,$tipo);
	}
	
	function marketing_busca($tipo = 'grade') {
		$this->_busca($tipo);
	}

	function planejamento_item($itemId, $tipo = 'grade') {
		$this->_item($itemId,$tipo);
	}
	
	function planejamento_busca($tipo = 'grade') {
		$this->_busca($tipo);
	}
	
	private function _busca($tipo) {
		$this->layout = "material_venda";
		if($this->data["filtroMaterialVenda"]) {
			$this->autoRender = false;
			Configure::write(array('debug' => 0));
			$this->Session->write("filtroMaterialVenda",$this->data["filtroMaterialVenda"]);
			echo json_encode(array());
		} elseif(!$this->Session->check("filtroMaterialVenda")) {
			$this->redirect("/comercial/material_venda");
		} else {
			$this->_itens();
			$view = $tipo == 'lista' ? 'lista' : 'grade';
			$filtro = $this->Session->read("filtroMaterialVenda");
			$f = strtolower($filtro);
			$this->set('view',$view);
			$this->set('filtro',$filtro);
			$conditions = array(
				"(lower(MaterialVenda.nome) like('%{$f}%') or lower(MaterialVenda.informacoes) like('%{$f}%'))"
			);
			if($this->Session->check("itemId"))
				$conditions['MaterialVendaEventoItem.material_venda_item_id'] = $this->Session->read("itemId");
			$catalogos = $this->MaterialVenda->listarCatalogos($conditions);
			$this->set("catalogos",$catalogos);
		}
	}
	
	function comercial_detalhes($catalogoId) {
		$this->_detalhes($catalogoId);
	}
	
	function marketing_detalhes($catalogoId) {
		$this->_detalhes($catalogoId);
	}

	function planejamento_detalhes($catalogoId) {
		$this->_detalhes($catalogoId);
	}
	
	function _detalhes($catalogoId) {
		$this->layout = "material_venda";
		$this->Session->write("materialVendaId",$catalogoId);
		$this->Session->delete("filtroMaterialVenda");
		$this->_itens();
	}
	
	function marketing_baixar_anexo($id) {
		$this->_baixar_anexo($id);
	}

	function comercial_baixar_anexo($id) {
		$this->_baixar_anexo($id);
	}

	function planejamento_baixar_anexo($id) {
		$this->_baixar_anexo($id);
	}
	
	private function _baixar_anexo($id) {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		if($id) {
			$this->MaterialVendaAnexo->id = $id;
			$arquivo = $this->MaterialVendaAnexo->read();
			if (!empty($arquivo)) {
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private", false);
				header("Content-Type: " . $arquivo['MaterialVendaAnexo']['tipo']);
				header("Content-Disposition: attachment; filename=\"" . $arquivo['MaterialVendaAnexo']['titulo'] . "\";");
				header("Content-Length: " . $arquivo['MaterialVendaAnexo']['tamanho']);
				readfile(APP . "/webroot/img/anexos/" . $arquivo['MaterialVendaAnexo']['nome']);
			} 
			else {
				exit();
			}
		} else {
			exit();
		}
	}
	
	private function listarItensDeEvento($tiposEventoId) {
		$options = array(
			'joins' => array(
				array(
					"table" => "material_venda_itens",
					"type" => "inner",
					"alias" => "MaterialVendaItem",
					"conditions" => array(
						"MaterialVendaItem.tipos_evento_id = TiposEvento.id"
					)
				),
				array(
					"table" => "material_venda_eventos_itens",
					"type" => "inner",
					"alias" => "MaterialVendaEventoItem",
					"conditions" => array(
						"MaterialVendaItem.id = MaterialVendaEventoItem.material_venda_item_id"
					)
				)
			),
			'order' => array('MaterialVendaItem.ordem'),
			'group' => array('MaterialVendaItem.id'),
			'conditions' => array(
				"TiposEvento.id" => $tiposEventoId
			),
			'fields' => array('MaterialVendaItem.*')
		);
		return $this->TiposEvento->find("all",$options);
	}
	
	private function listarEventos() {
		$options = array(
			'joins' => array(
				array(
					"table" => "material_venda_itens",
					"type" => "inner",
					"alias" => "MaterialVendaItem",
					"conditions" => array(
						"MaterialVendaItem.tipos_evento_id = TiposEvento.id"
					)
				),
				array(
					"table" => "material_venda_eventos_itens",
					"type" => "inner",
					"alias" => "MaterialVendaEventoItem",
					"conditions" => array(
						"MaterialVendaItem.id = MaterialVendaEventoItem.material_venda_item_id"
					)
				)
			),
			'group' => array('TiposEvento.id'),
			'conditions' => array(
				"TiposEvento.ativo" => 1
			),
			'fields' => array('TiposEvento.*')
		);
		return $this->TiposEvento->find("all",$options);
	}

	private function _itens() {
		$usuario = $this->obterUsuarioLogado();
		$this->set('usuarioLogado',$usuario);
		$eventos = $this->listarEventos();
		$this->set('eventos',$eventos);
		if($this->Session->check("eventoId")) {
			$eventoId = $this->Session->read("eventoId");
			$this->set('eventoSelecionado',$this->TiposEvento->read(null,$eventoId));
			$itens = $this->listarItensDeEvento($eventoId);
			$this->set('itens',$itens);
			if($this->Session->check("itemId")) {
				$itemId = $this->Session->read("itemId");
				$this->set('itemSelecionado',$this->MaterialVendaItem->read(null,$itemId));
				$catalogos = $this->MaterialVenda->listarCatalogosDeItem($itemId);
				$this->set('catalogos',$catalogos);
			}
		}
		if($this->Session->check("materialVendaId")) {
			$this->MaterialVenda->bindModel(array(
				"hasMany" => array('MaterialVendaFoto' => array(
					'className' => 'MaterialVendaFoto',
					'order' => 'ordem'
				),'MaterialVendaAnexo')
			),false);
			$catalogo = $this->MaterialVenda->read(null,$this->Session->read("materialVendaId"));
			$this->set("catalogo",$catalogo);
		}
	}
	
	function marketing_editar_evento($tipoEventoId = false) {
		if($this->data) {
			$this->autoRender = false;
			Configure::write(array('debug' => 0));
			if(substr($this->data['TiposEvento']['nome'], 0, 8) == 'Material'){
				$nome = true;
			}
			if($nome){
				if($this->TiposEvento->save($this->data)){
					$this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
				}else{
					$this->Session->setFlash("Erro ao alterar dados", 'metro/flash/error');
				}
			}elseif(!$nome){
				$this->Session->setFlash('Erro ao alterar dados, o nome deve iniciar com "Material"', 'metro/flash/error');
			}
			echo json_encode(array());
		} elseif($tipoEventoId) {
			$this->data = $this->TiposEvento->read(null,$tipoEventoId);
		}
	}
	
	function marketing_editar_item($tipoEventoId, $itemId = false) {
		if($this->data) {
			$this->autoRender = false;
			Configure::write(array('debug' => 0));
			if($this->MaterialVendaItem->save($this->data))
				$this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
			else
				$this->Session->setFlash("Erro ao alterar dados", 'metro/flash/error');
			echo json_encode(array());
		} else {
			if($itemId)
				$this->data = $this->MaterialVendaItem->read(null,$itemId);
			else {
				$maior = $this->MaterialVendaItem->find("first",array(
					"fields" => array("max(ordem)+1 as maior"),
					'conditions' => array(
						"MaterialVendaItem.tipos_evento_id" => $tipoEventoId
					)
				));
				$ordem = !empty($maior[0]["maior"]) ? $maior[0]["maior"] : 1;
				$this->data = array(
					"MaterialVendaItem" => array(
						"ordem" => $ordem,
						"tipos_evento_id" => $tipoEventoId
					)
				);
			}
			$this->set("evento",$this->TiposEvento->read(null,$tipoEventoId));
		}
	}
	
	function marketing_status_evento($tipoEventoId) {
		$tipoEvento = $this->TiposEvento->read(null,$tipoEventoId);
		if($tipoEvento) {
			$tipoEvento["TiposEvento"]["ativo"] = $tipoEvento["TiposEvento"]["ativo"] == 1 ? 0 : 1;
			if($this->TiposEvento->save($tipoEvento)) {
				$this->Session->setFlash("Evento alterado com sucesso", 'metro/flash/success');
			} else {
				$this->Session->setFlash("Erro ao alterar evento", 'metro/flash/error');
			}
		} else {
			$this->Session->setFlash("Evento não encontrado", 'metro/flash/error');
		}
		$eventos = $this->TiposEvento->find("all");
		$this->set("eventos",$eventos);
		$this->render("marketing_listar_eventos");
	}
	
	function marketing_listar($eventoId = false, $itemId = false) {
		if(!$eventoId) {
			$eventos = $this->TiposEvento->find("all");
			$this->set("eventos",$eventos);
			$this->render("marketing_listar_eventos");
		} elseif($itemId) {
			$catalogos = $this->MaterialVenda->listarCatalogosDeItem($itemId,$eventoId);
			$this->set("evento",$this->TiposEvento->read(null,$eventoId));
			$this->set('item',$this->MaterialVendaItem->read(null,$itemId));
			$this->set("catalogos",$catalogos);
			$this->render("marketing_listar_material_venda");
		} else {
			$itens = $this->MaterialVendaItem->find("all",array(
				'conditions' => array(
					"tipos_evento_id" => $eventoId
				)
			));
			$this->set("evento",$this->TiposEvento->read(null,$eventoId));
			$this->set("itens",$itens);
			$this->render("marketing_listar_itens");
		}
	}
	
	function marketing_ordem_itens() {
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		if(isset($this->data["itens"])) {
			if($this->MaterialVendaItem->saveAll($this->data["itens"])) {
				$this->Session->setFlash("Alterações salvas", 'metro/flash/success');
			} else {
				$this->Session->setFlash("Erro ao salvar alterações", 'metro/flash/error');
			}
		}
		echo json_encode(array());
	}
	
	function marketing_listar_itens($eventoId) {
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		$itens = $this->MaterialVendaItem->find("list",array(
			'conditions' => array(
				"tipos_evento_id" => $eventoId
			),
			'fields' => array('nome')
		));
		echo json_encode(array('itens' => $itens));
	}
	
	/**
	 * Cria ou edita as informações de um catalogo
	 *
	 * @param $eventoId
	 * @param $itemId
	 *
	 * @return
	 */
	function marketing_editar($eventoId, $itemId, $id = false) {
		$catalogo = null;
		if($id)
			$catalogo = $this->MaterialVenda->read(null,$id);

		if(!empty($_FILES)){
			$this->upload_anexo($_FILES);
		}
		if($this->data) {
			$this->autoRender = false;
			Configure::write(array('debug' => 0));
			$data = $this->data;
			$data["MaterialVenda"]['data_ultima_alteracao'] = date("Y-m-d H:i:s");
			if(empty($data['MaterialVenda']['nome'])){
				exit;
			}
			if($this->MaterialVenda->saveAll($data)) {
				$catalogoId = !empty($data["MaterialVenda"]["id"]) ? $data["MaterialVenda"]["id"] : $this->MaterialVenda->getLastInsertId();
				if(isset($data["fotos"])) {
					foreach($data["fotos"] as $foto) {
						$this->MaterialVendaFoto->create();
						$this->MaterialVendaFoto->save(array(
							'material_venda_id' => $catalogoId,
							'data_cadastro' => date('Y-m-d H:i:s'),
							'ext' => $foto['ext'],
							'src' => $foto['src'],
							'ordem' => $foto['ordem']
						));
					}
				}
				if(isset($data["itens"])) {
					foreach($data["itens"] as $item) {
						$this->MaterialVendaEventoItem->create();
						$this->MaterialVendaEventoItem->save(array(
							'material_venda_id' => $catalogoId,
							'data_cadastro' => date('Y-m-d H:i:s'),
							'material_venda_item_id' => $item
						));
					}
				}
				if(isset($_SESSION['arquivos'])){
					foreach($_SESSION['arquivos'] as $key => $arquivo) {
						$this->MaterialVendaAnexo->create();
						$arquivo['arquivo']['src'] = 'img/anexo/'.$arquivo['arquivo']['name'];
						$dados = array(
							'material_venda_id' => $catalogoId,
							'data_cadastro' => date('Y-m-d H:i:s'),
							'nome' => hash("MD5", $arquivo['arquivo']['name']),
							'titulo' => $arquivo['arquivo']['name'],
							'tipo' => $arquivo['arquivo']['type'],
							'tamanho' => $arquivo['arquivo']['size'],
							'data' => hash("MD5", $arquivo['arquivo']['src'])
						);
						$this->MaterialVendaAnexo->save($dados);
						unset($_SESSION['arquivos']);
					}
				}
				if(isset($data["remover_fotos"]))
					foreach($data['remover_fotos'] as $foto)
						$this->MaterialVendaFoto->delete($foto);
				if(isset($data["remover_itens"]))
					foreach($data['remover_itens'] as $item)
						$this->MaterialVendaEventoItem->delete($item);
				if(isset($data["remover_anexos"]))
					foreach($data['remover_anexos'] as $anexo){
						$dados = $this->MaterialVendaAnexo->findById($anexo);
						unlink(APP . "/webroot/img/anexos/".$dados['MaterialVendaAnexo']['nome']);
						$this->MaterialVendaAnexo->delete($anexo);
					}
				$this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
			} else {
				if($titulo)
					$this->Session->setFlash("O título deve ser inserido", 'metro/flash/error');

				$this->Session->setFlash("Erro ao enviar dados", 'metro/flash/error');
			}
			echo json_encode(array());
		} else {
			$eventosCadastrados = array($eventoId => $eventoId);
			$this->data = $catalogo;
			$itens = array();
			if($catalogo["MaterialVendaEventoItem"]) {
				foreach($catalogo["MaterialVendaEventoItem"] as $eventoItem) {
					$catalogoItem = $this->MaterialVendaItem->read(null,$eventoItem["material_venda_item_id"]);
					$itens[] = array(
						"id" => $eventoItem["id"],
						"item" => $eventoItem["material_venda_item_id"],
						'nome' => "{$catalogoItem["TiposEvento"]["nome"]} - {$catalogoItem["MaterialVendaItem"]["nome"]}"
					);
				}
			}
			$eventos = $this->TiposEvento->find('list',array(
				'conditions' => array(
					'TiposEvento.ativo' => 1
				),
				"fields" => array('nome')
			));
			$evento = $this->TiposEvento->read(null,$eventoId);
			if(!empty($_FILES)){
				$this->set("arquivo", $_FILES);
			}
			$this->set("evento",$evento);
			$this->set('item',$this->MaterialVendaItem->read(null,$itemId));
			$this->set("eventosCadastrados",json_encode($eventosCadastrados));
			$this->set("eventos",$eventos);
			$this->set("itens",$itens);
		}
		$this->set("catalogo",$catalogo);
	}

	function upload_anexo($arquivo){
		Configure::write(array('debug' => 0));
		$_UP['pasta'] = APP . "/webroot/img/anexos/";
		$_UP['extensoes'] = array('jpg', 'png', 'gif');
		$_UP['renomeia'] = false;
		$_UP['erros'][0] = 'Não houve erro';
		$_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
		$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
		$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
		$_UP['erros'][4] = 'Não foi feito o upload do arquivo';
		if ($arquivo['arquivo']['error'] != 0) {
			die("Não foi possível fazer o upload, erro:<br />" . $_UP['erros'][$arquivo['arquivo']['error']]);
			exit; 
		}
		else {
			if ($_UP['renomeia'] == true) {
				$nome_final = time().'.jpg';
			} else {
				$nome_final = hash("md5", $arquivo['arquivo']['name']);
			}
			if (move_uploaded_file($arquivo['arquivo']['tmp_name'], $_UP['pasta'].$nome_final)) {
				return true;
			} else {
				echo "Não foi possível enviar o arquivo, tente novamente";
				return false;
			}

		}
	}

	function marketing_deletar_arquivo(){
		$this->deletar_arquivo();
	}
	function deletar_arquivo(){
		Configure::write(array('debug' => 0));
		$keys = count($_SESSION['arquivos']);
		for ($i=0; $i <= $keys; $i++) { 
			if(isset($_SESSION['arquivos'][$i]['arquivo'])){
				$key = array_search($_POST['name'], $_SESSION['arquivos'][$i]['arquivo']);
			}
			if($key == 'name'){
				unset($_SESSION['arquivos'][$i]);
			}
		}
		unlink(APP . "/webroot/img/anexos/".hash("MD5", $_POST['name']));
		exit;
	}

}
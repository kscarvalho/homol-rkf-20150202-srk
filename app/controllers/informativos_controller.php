<?php

class InformativosController extends AppController {
	
	var $name = "Informativos";
	var $helpers = array('Html');
	var $components = array('Session','Mailer');
	var $uses = array('Informativo','InformativoUsuario','Turma','Usuario','TurmasUsuario');
	var $mailer;
	
	function beforeFilter(){
		App::import('Component','ListaSidebarMenu');
		ListaSidebarMenuComponent::adicionarLinkSidebar('Universidades', 'universidades', 'index', 'atendimento');
		parent::beforeFilter();
	}
	
	function atendimento_enviar() {
		$usuario = $this->Session->read('Usuario');
		$this->set('destinatarios',array('' => 'Selecione','formando' => 'Um Formando','todos_formandos' => 'Todos os Formandos','turma' => 'Turma'));
		$this->TurmasUsuario->bindModel(array('hasOne' => array('Turma' => array('foreignKey' => 'id' , 'recursive' => 3))), false);
		$turmas = $this->TurmasUsuario->find('all',array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));
		$selectTurmas = array('todas' => 'Todas');
		foreach($turmas as $turma)
			$selectTurmas[$turma['Turma']['id']] = "{$turma['Turma']['id']} - {$turma['Turma']['nome']}";
		$this->set('turmas',$selectTurmas);
		if($this->data) {
			if(empty($this->data['Informativo']['destinatario'])) {
				$this->Session->setFlash("Selecione um destinatário para enviar o informativo.", 'flash_erro');
			} else {
				$destinatario = $this->data['Informativo']['destinatario'];
				$titulo = $this->data['Informativo']['titulo'];
				$mensagem = $this->data['Informativo']['mensagem'];
				$informativo = array(
					'titulo' => $titulo,
					'mensagem' => $mensagem,
					'tipo_destino' => $destinatario,
					'usuario_id' => $usuario['Usuario']['id']
				);
				if($destinatario == "formando") {
					if(empty($this->data['Informativo']['codigo_formando'])) {
						$this->Session->setFlash("Digite o código do formando.", 'flash_erro');
					} else {
						$codigoFormando = $this->data['Informativo']['codigo_formando'];
						$informativo['tipo_destino'] = 'usuario';
						$response = $this->_adicionarInformativoPorCodigoFormando($codigoFormando,$informativo);
						if($response !== TRUE)
							$this->Session->setFlash($response, 'flash_erro');
						else
							$this->Session->setFlash("Informativo enviado com sucesso", 'flash_sucesso');
					}
				} elseif($destinatario == "todos_formandos") {
                                    ini_set('memory_limit', '256M');
                                    set_time_limit(3000);
					if($this->_adicionarInformativo($informativo)) {
						$this->loadModel('ViewFormandos');
						$options['joins'] = array(
							array(
								"table" => "turmas_usuarios",
								"type" => "inner",
								"alias" => "TurmaUsuario",
								"conditions" => array("ViewFormandos.turma_id = TurmaUsuario.turma_id")
							),
						);
						$options['conditions'] = array("TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}","ViewFormandos.grupo" => 'formando');
						$formandos = $this->ViewFormandos->find('all',$options);
						foreach($formandos as $formando)
                                                        ini_set('memory_limit', '256M');
                                                        set_time_limit(3000);
							$this->_adicionarInformativoUsuario(array(
									'usuario_id' => $formando['ViewFormandos']['id'],'informativo_id' => $this->Informativo->getLastInsertID(),
									'titulo' => $informativo['titulo'], 'mensagem' => $informativo['mensagem']));
						$this->Session->setFlash("Informativo enviado com sucesso", 'flash_sucesso');
					} else {
						$this->Session->setFlash("Erro ao inserir informativo.", 'flash_erro');
					}
				} elseif($destinatario == "turma") {
					if($this->_adicionarInformativo($informativo)) {
						$this->loadModel('ViewFormandos');
						$options['joins'] = array(
							array(
								"table" => "turmas_usuarios",
								"type" => "inner",
								"alias" => "TurmaUsuario",
								"conditions" => array("ViewFormandos.turma_id = TurmaUsuario.turma_id")
							),
						);
						$options['conditions'] = array("TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}","ViewFormandos.grupo" => 'formando');
						$options['group'] = array('ViewFormandos.id');
						if($this->data['Informativo']['turma'] != "todas")
							$options['conditions'] = array("TurmaUsuario.turma_id = {$this->data['Informativo']['turma']}");
						$formandos = $this->ViewFormandos->find('all',$options);
						foreach($formandos as $formando)
							$this->_adicionarInformativoUsuario(array(
									'usuario_id' => $formando['ViewFormandos']['id'],'informativo_id' => $this->Informativo->getLastInsertID(),
									'titulo' => $informativo['titulo'], 'mensagem' => $informativo['mensagem']));
						$this->Session->setFlash("Informativo enviado com sucesso", 'flash_sucesso');
					} else {
						$this->Session->setFlash("Erro ao inserir informativo.", 'flash_erro');
					}
				}
			}
		}
	}
	
	function atendimento_enviados() {
		$usuario = $this->Session->read('Usuario');
		$this->Informativo->unbindModel(array('hasMany' => array('InformativoUsuario')), false);
		$this->paginate = array('conditions' => array('usuario_id' => $usuario['Usuario']['id']),'limit' => 50, 'order' => array('id' => 'desc'));
		$informativos = $this->paginate('Informativo');
		$this->set('informativos',$informativos);
	}
		
	private function _adicionarInformativoPorCodigoFormando($codigoFormando,$informativo) {
		$return = TRUE;
		$this->loadModel('ViewFormandos');
		if(!$formando = $this->ViewFormandos->find('first',array('conditions' => array('codigo_formando' => $codigoFormando))))
			$return = "Código do formando não encontrado";
		elseif(!$this->_adicionarInformativo($informativo))
			$return = "Erro ao inserir informativo";
		elseif(!$this->_adicionarInformativoUsuario(array(
				'usuario_id' => $formando['ViewFormandos']['id'],'informativo_id' => $this->Informativo->getLastInsertID(),
				'titulo' => $informativo['titulo'], 'mensagem' => $informativo['mensagem'])))
			$return = "Erro ao inserir destinatário";
		return $return;
	}
	
	private function _adicionarInformativo($dados) {
		if(!$this->Informativo->saveAll($dados))
			return FALSE;
		else
			return TRUE;
	}
	
	private function _adicionarInformativoDestinatario($dados) {
		if(!$this->InformativoDestinatario->saveAll($dados))
			return FALSE;
		else
			return TRUE;
	}
	
	private function _adicionarInformativoUsuario($dados) {
		$usuario = $this->Usuario->find('first',array('conditions' => array('Usuario.id' => $dados['usuario_id'])));
		if(empty($this->mailer))
			$this->mailer = new MailerComponent();
		if(!empty($usuario['Usuario']['email']))
			$this->mailer->enviarInformativo($dados['titulo'],$dados['mensagem'],$usuario['Usuario']['email'],$usuario['Usuario']['nome']);
		if(!$this->InformativoUsuario->saveAll($dados))
			return FALSE;
		else
			return TRUE;
	}
}
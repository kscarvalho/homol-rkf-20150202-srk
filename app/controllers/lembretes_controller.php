<?php
class LembretesController extends AppController {

	var $name = 'Lembretes';
	
	// Padrão de paginação
	var $paginate = array(
		'limit' => 5,
		'order' => array(
   			'Lembrete.titulo' => 'asc'
			)
		);
		
	
	/*** COMERCIAL ***/
	function comercial_adicionar() {
            $this->super_adicionar();
	}
	
	function comercial_editar($id = null) {
            $this->super_editar($id);
	}
	
	function comercial_deletar($id = null) {
            $this->super_deletar($id);
	}
                
	/*** PLANEJAMENTO ***/
	function planejamento_index() {
            $this->super_index();
	}
	
	function planejamento_adicionar() {
            $this->super_adicionar();
	}
	
	function planejamento_editar($id = null) {
            $this->super_editar($id);
	}
	
	function planejamento_visualizar($id = null) {
            $this->super_visualizar($id);
	}
	
	function planejamento_deletar($id = null) {
            $this->super_deletar($id);
	}
	
	
	/*** SUPER ***/
	
	function super_index() {
		$this->set('lembretes', $this->paginate('Lembrete', array('usuario_id' => $this->Session->read('Usuario.Usuario.id'))));
	}
	
	function super_adicionar() {
		if (!empty($this->data)) {
			$this->data['Lembrete']['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
			if ($this->Lembrete->save($this->data)) {
				$this->Session->setFlash('Lembrete foi salvo corretamente.', 'flash_sucesso');
				$this->redirect( "/{$this->params['prefix']}/principal/index");
			}
			else {
				$this->Session->setFlash('Um erro ocorreu na inserção de dados.', 'flash_erro');
			}
		}
		
	}
	
	function super_editar($id = null) {
		$this->Lembrete->id = $id;
		if (!empty($this->data)) {

			if ($this->Lembrete->save($this->data)){
				$this->Session->setFlash('Lembrete foi salvo corretamente.', 'flash_sucesso');
				$this->redirect("/{$this->params['prefix']}/principal/index");
			}
			else {
				$this->Session->setFlash('Um erro ocorreu na atualização de dados.', 'flash_erro');
			}

		}
		else {
			$this->data = $this->Lembrete->read();
			if(empty($this->data)) {
				$this->redirect("/{$this->params['prefix']}/principal/index");
				$this->Session->setFlash('Lembrete não existente.', 'flash_erro');
			}
		}
			
	}
	
	function super_visualizar($id = null) {
		
		$lembrete = $this->Lembrete->find(array('id' => $id));

		if (empty($lembrete)) {
			$this->redirect("/{$this->params['prefix']}/principal/index");
			$this->Session->setFlash('Lembrete não existente.', 'flash_erro');
		}
		$this->set('lembrete', $lembrete);
		
	}
	
	function super_deletar($id = null) {
		
		if ($this->Lembrete->delete($id))
			$this->Session->setFlash('Lembrete removido.', 'flash_erro');
		else
			// TODO melhorar estas frases de erro
			// Talvez colocá-las em um lugar unificado seja interessante
			$this->Session->setFlash('Erro ao remover lembrete.', 'flash_erro');
			
		$this->redirect("/{$this->params['prefix']}");
		
	}
}
?>
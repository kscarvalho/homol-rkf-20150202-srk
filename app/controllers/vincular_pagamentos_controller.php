<?php

class VincularPagamentosController extends AppController {
	
	var $name = "VincularPagamentos";
	var $uses = array();
	
	function planejamento_index() {
		$this->loadModel('ViewFormandos');
		$joins = array(
			array(
				"table" => "turmas",
				"type" => "inner",
				"alias" => "Turma",
				"conditions" => array("ViewFormandos.turma_id = Turma.id")
			),array(
				"table" => "pagamentos",
				"type" => "inner",
				"alias" => "Pagamentos",
				"conditions" => array("ViewFormandos.id = Pagamentos.usuario_id")
			),
			array(
				"table" => "despesas_pagamentos",
				"type" => "left",
				"alias" => "DespesasPagamentos",
				"conditions" => array("DespesasPagamentos.pagamento_id = Pagamentos.id")
			)
		);
		$fields = array(
			"ViewFormandos.codigo_formando",
			"ViewFormandos.nome",
			"ViewFormandos.email",
			"ViewFormandos.curso_nome",
			"Turma.nome",
			"if(sum(`DespesasPagamentos`.`despesa_id`) is null,'Sim','Não') as nao_vinculado"
		);
		$options['ViewFormandos'] = array(
			"group" => "ViewFormandos.id",
			"limit" => 50,
			"order" => "nao_vinculado desc",
			"fields" => $fields,
			"joins" => $joins
		);
		$options["ViewFormandos"]["conditions"] = array("Pagamento.status = 'pago'");
		if($this->data) {
			$this->set("valor",$this->data['filtro-id']);
			$options["ViewFormandos"]["limit"] = $this->data["qtde-por-pagina"];
			if($this->data['filtro-tipo'] == "turma_id")
				$options["ViewFormandos"]["conditions"][] = "ViewFormandos.turma_id = {$this->data['filtro-id']}";
			else
				$options["ViewFormandos"]["conditions"][] = "ViewFormandos.{$this->data['filtro-tipo']} like('%{$this->data['filtro-id']}%')";
			$this->Session->write("VincularPagamentos.conditions",$options["ViewFormandos"]["conditions"]);
		} else {
			$options["ViewFormandos"]["conditions"] = $this->Session->read("VincularPagamentos.conditions");
		}
		$this->paginate = $options;
		$formandos = $this->paginate('ViewFormandos');
		$arrayOptions = array(
			"codigo_formando" => "Código do Formando",
			"turma_id" => "Turma",
			"nome" => "Nome"
		);
		$this->set("arrayOptions",$arrayOptions);
		$this->set("formandos",$formandos);
	}
	
	function atendimento_listar_despesas_formando($usuarioId) {
		$this->planejamento_listar_despesas_formando($usuarioId);
	}
	
	function planejamento_listar_despesas_formando($usuarioId) {
		$usuario = $this->Session->read('Usuario');
		$uid = $usuarioId ? $usuarioId : $usuario['Usuario']['id'];
		App::import("Controller","AreaFinanceira");
		$areaFinanceira = new AreaFinanceiraController();
		$areaFinanceira->constructClasses();
		$areaFinanceira->params = $this->params;
		$areaFinanceira->formando_index($uid);
		$this->viewVars = $areaFinanceira->viewVars;
	}
	
	function atendimento_vincular() {
		$this->planejamento_vincular();
	}
	
	function planejamento_vincular() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		$this->loadModel('Pagamento');
		$this->loadModel('DespesaPagamento');
		$this->loadModel('Despesa');
		$despesaId = $_POST['despesa'];
		$pagamentoId = $_POST['pagamento'];
		$usuarioId = $_POST['usuario'];
		$this->Pagamento->id = $pagamentoId;
		$this->Pagamento->set("perdido",0);
		$this->Pagamento->set("status",'pago');
		$erro = true;
		if($this->Pagamento->save()) {
			$this->DespesaPagamento->recursive = 1;
			$despesa = $this->DespesaPagamento->find('all',array('conditions' => array("despesa_id = $despesaId")));
			if(!empty($despesa))
				$thid->DespesaPagamento->id = $despesaId;
			$this->Despesa->id = $despesaId;
			if($this->Despesa->field('usuario_id') == "")
				$this->Despesa->set('usuario_id',$usuarioId);
			$dataPagamento = $this->Pagamento->field('dt_liquidacao');
			$dataPagamento = date('Y-m-d H:i:s', strtotime($dataPagamento)) == $dataPagamento ? $dataPagamento : date('Y-m-d H:i:s');
			$this->Despesa->set('data_pagamento',$dataPagamento);
			$this->Despesa->set('status','paga');
			if($this->Despesa->save()) {
				$dados = array(
					'pagamento_id' => $pagamentoId,
					'despesa_id' => $despesaId,
					'valor' => $this->Despesa->field('valor')
				);
				if($this->DespesaPagamento->save($dados))
					$erro = false;
			}
		}
		echo json_encode(array('erro' => $erro));
	}
	
	function atendimento_despesas() {
		$this->planejamento_despesas();
	}
	
	function planejamento_despesas() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		$this->loadModel('Despesa');
		$usuarioId = $_POST['usuario'];
		$despesas = $this->Despesa->find('all',array(
				'conditions' => array('usuario_id' => $usuarioId),
				'order' => array('tipo')));
		$despesasNaoVinculadas = array();
		foreach($despesas as $despesa)
			if(sizeof($despesa['DespesaPagamento']) == 0)
				$despesasNaoVinculadas[] = $despesa;
		echo json_encode($despesasNaoVinculadas);
	}
}
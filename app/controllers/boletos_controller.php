<?php

class BoletosController extends AppController {

    var $name = 'Boletos';
    var $uses = array('Banco', 'BancoConta', 'Despesa','Pagamento',
        'DespesaPagamento','Usuario','Turma');
    var $components = array('BoletoConta');
    var $conta = null;
    
    function financeiro_marcar_enviado($usuarioId) {
        $this->autoRender = false;
        $this->Usuario->recursive = 1;
        $usuario = $this->Usuario->read(null,$usuarioId);
        if($usuario) {
            $this->FormandoProfile->id = $usuario['FormandoProfile']['id'];
            if($this->FormandoProfile->saveField("boleto_enviado", 1))
                $this->Session->setFlash('Boletos marcados como enviados', 'metro/flash/success');
            else
                $this->Session->setFlash('Erro ao marcar como enviado', 'metro/flash/error');
        } else {
            $this->Session->setFlash('Erro ao buscar formando', 'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function atendimento_marcar_enviado($usuarioId){
        $this->financeiro_marcar_enviado($usuarioId);
    }
    
    function financeiro_gerar_todos($usuarioId) {
        $this->autoRender = false;
        $this->Usuario->unbindModel(array(
            'hasAndBelongsToMany' => array('Campanhas','Turma'),
            'hasMany' => array('UsuarioConta','FormandoFotoTelao','Cupom')
        ),false);
        $this->Usuario->recursive = 2;
        $usuario = $this->Usuario->read(null,$usuarioId);
        if($usuario) {
            $this->Turma->unbindModelAll();
            $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
            if($turma) {
                $gerados = 0;
                ob_start();
                foreach($usuario['Despesa'] as $despesa)
                    if($despesa['tipo'] == 'adesao' && $despesa['status'] == 'aberta') {
                        $gerados++;
                        $this->_gerar($despesa['id'], $usuario, $turma);
                    }
                $boletos = ob_get_contents();
                ob_end_clean();
                if($gerados > 0) {
                    $this->loadModel('FormandoProfile');
                    $this->FormandoProfile->id = $usuario['FormandoProfile']['id'];
                    $this->FormandoProfile->saveField("boleto_impresso", 1);
                    echo $boletos;
                } else {
                    $this->Session->setFlash('Formando não tem boletos para gerar', 'metro/flash/error');
                    $this->redirect('/financeiro');
                }
            } else {
                $this->Session->setFlash('Erro ao buscar turma do formando', 'metro/flash/error');
                $this->redirect('/financeiro');
            }
        }
    }
    
    function atendimento_gerar_todos($usuarioId){
        $this->financeiro_gerar_todos($usuarioId);
    }
    
    function formando_gerar($despesaId,$verificado = 0) {
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');
        $turma = $this->_obterTurma($turma['Turma']['id']);
        if($verificado == 0) {
            $this->layout = 'metro/externo';
            $this->BoletoConta->iniciar($despesaId, $usuario, $turma);
            $this->set('boleto',$this->BoletoConta->dadosboleto);
            $this->set('despesa',$this->BoletoConta->despesaId);
            $this->set('banco',$this->BoletoConta->dadosboleto['nome_configuracao']);
            $this->render('/area_financeira/verificar_boleto');
        } else {
            $this->_gerar($despesaId, $usuario, $turma);
        }
    }
    
    function atendimento_gerar($despesaId) {
        $despesa = $this->Despesa->read(null,$despesaId);
        if($despesa) {
            $usuario = $this->Usuario->read(null,$despesa['Despesa']['usuario_id']);
            $turma = $this->_obterTurma($usuario['ViewFormandos']['turma_id']);
            $this->_gerar($despesaId, $usuario, $turma);
        } else {
            //$this->Session->setFlash('Despesa não encontrada', 'metro/flash/error');
            $this->redirect('/');
        }
    }
    
    function gerar($despesaId,$verificado = 0) {
        $this->formando_gerar($despesaId,$verificado);
    }
    
    private function _gerar($despesaId, $usuario, $turma) {
        $this->autoRender = false;
        $this->BoletoConta->dadosboleto["nosso_numero"] = "";
        $this->BoletoConta->iniciar($despesaId, $usuario, $turma);
        $this->BoletoConta->gerarBoleto($this->webroot);
    }
    
    function formando_gerar_com_multa($despesaId,$verificado = 0) {
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');
        $turma = $this->_obterTurma($turma['Turma']['id']);
        if($verificado == 0) {
            $this->layout = 'metro/externo';
            $this->BoletoConta->iniciar($despesaId, $usuario, $turma);
            $this->set('boleto',$this->BoletoConta->dadosboleto);
            $this->set('despesa',$this->BoletoConta->despesaId);
            $this->set('banco',$this->BoletoConta->dadosboleto['nome_configuracao']);
            $this->render('/area_financeira/verificar_boleto');
        } else {
            $this->_gerar_com_multa($despesaId, $usuario, $turma);
        }
    }
    
    function atendimento_gerar_com_multa($despesaId) {
        $despesa = $this->Despesa->read(null,$despesaId);
        if($despesa) {
            $usuario = $this->Usuario->read(null,$despesa['Despesa']['usuario_id']);
            $turma = $this->_obterTurma($usuario['ViewFormandos']['turma_id']);
            $this->_gerar_com_multa($despesaId, $usuario, $turma);
        } else {
            //$this->Session->setFlash('Despesa não encontrada', 'metro/flash/error');
            $this->redirect('/');
        }
    }
    
    function gerar_com_multa($despesaId,$verificado = 0) {
        $this->formando_gerar_com_multa($despesaId,$verificado);
    }
    
    private function _gerar_com_multa($despesaId, $usuario, $turma) {
        $despesa = $this->Despesa->read(null,$despesaId);
        $diasAtraso = (strtotime(date('Y-m-d')) - strtotime($despesa["Despesa"]["data_vencimento"])) / (60 * 60 * 24);
        if(strpos($turma['Turma']['multa_por_atraso'],"R$") !== false) {
            $this->BoletoConta->multa = str_replace(",",".",str_replace(".","",str_replace("R$","", $turma['Turma']['multa_por_atraso'])));
        } elseif(strpos($turma['Turma']['multa_por_atraso'],"%") !== false) {
            $porcent = str_replace("%","",$turma['Turma']['multa_por_atraso']) / 100;
            $this->BoletoConta->multa = $despesa['Despesa']['valor'] * $porcent;
        }
        if(!empty($turma['Turma']['juros_mora'])) {
            $mora = str_replace(",",".",str_replace(".","",str_replace("R$","", $turma['Turma']['juros_mora'])));
            $this->BoletoConta->multa+= $diasAtraso * $mora;
        }
        $this->BoletoConta->dataVencimento = date('d/m/Y');
        $this->_gerar($despesaId, $usuario, $turma);
    }
    
    function formando_gerar_igpm($pagamentoId) {
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');
        $turma = $this->_obterTurma($turma['Turma']['id']);
        $this->_gerar_igpm($pagamentoId, $usuario, $turma);
    }
    
    private function _gerar_igpm($pagamentoId, $usuario, $turma) {
        $this->autoRender = false;
        $this->dataVencimento = date('d/m/Y', strtotime("+1 day"));
        $this->conta = $this->BancoConta->read(null, $turma['Turma']['banco_conta_id']);
        if ($this->conta) {
            $this->_configurarDadosBanco();
            $this->_configurar_via_igpm($pagamentoId,$usuario,$turma);
            $this->BoletoConta->gerarBoleto($this->conta['Banco']['nome_configuracao'],$this->webroot);
        }
    }
    
    private function _obterTurma($turmaId) {
        return $this->Turma->read(null,$turmaId);
    }

}

<?php
class MigracaoController extends AppController {
	var $name = 'Migracao';

	//var $uses = array('Despesa','Pagamento','Boleto');
	var $uses = array (
		'Usuario',
		'FormandoProfile',
		'FormandoAntigo',
		'Turma'
	);

	var $helpers = array (
		'Html',
		'Form'
	);

	function super_index() {

		// listar turmas elegíveis para migração
		$this->Turma->recursive = 0;
		$this->Turma->CursoTurma->recursive = 1;

		$turmas = $this->Turma->find('all', array (
			'order' => 'id DESC',
			'fields' => array (
				'id',
				'nome'
			)
		));
		$array_turmas = array ();

		foreach ($turmas as $turma) {
			$array_turmas[$turma['Turma']['id']] = $turma['Turma']['id'] . " - " . $turma['Turma']['nome'];

		}

		$this->set('turmas', $array_turmas);
		//debug($turmas);

	}

	function super_escolher_curso_default($turma_id = null) {
		// Verifica se veio por formulário ou por GET
		if ($turma_id == null) {
			$turma_id_post = $this->data['Turma']['turma_id'];
			if ($turma_id_post != null)
				$turma_id = $turma_id_post;
			else {
				$this->Session->setFlash('Turma inválida. ', 'flash_erro');
				$this->redirect("/{$this->params['prefix']}/migracao");
			}
		}

		// Listar cursos da turma:
		$this->Turma->CursoTurma->Curso->recursive = 2;
		$this->Turma->CursoTurma->recursive = 3;
		$this->Turma->CursoTurma->Curso->Faculdade->unBindModel(array (
			'hasMany' => array (
				'Curso'
			)
		));
		$this->Turma->CursoTurma->Curso->unBindModel(array (
			'hasMany' => array (
				'CursoTurma'
			)
		));
		$this->Turma->CursoTurma->unBindModel(array (
			'belongsTo' => array (
				'Turma'
			),
			'hasMany' => array (
				'FormandoProfile'
			)
		));

		$cursos = $this->Turma->CursoTurma->find('all', array (
			'conditions' => array (
				'turma_id' => $turma_id
			)
		));
		$array_cursos = array ();

		$array_cursos[-1] = "Criar novo curso padrão 'Curso Geral'";
		foreach ($cursos as $curso) {
			$texto = $curso['Curso']['nome'] . ' - ' . $curso['Curso']['Faculdade']['nome'] . ' - ' . $curso['Curso']['Faculdade']['Universidade']['nome'];
			$array_cursos[$curso['CursoTurma']['id']] = $texto;
		}

		//debug($cursos);
		//debug($array_cursos);

		$this->set('array_cursos', $array_cursos);
		$this->set('turma_id', $turma_id);
	}
	
	function super_migrar_todas_turmas() {
		// Buscar todos as turmas
		$turmas = array();
		$this->Turma->recursive = 2;
		$this->Turma->contain(array(
			'CursoTurma.Curso'
			));
		$this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
		$turmas =  $this->Turma->find('all', array('conditions' => array('status' => array('aberta', 'fechada'))));
		
		$arr_final_resultados = array(); 
		$arr_final_resultados['turmas'] = array();
		$arr_final_resultados['falha'] = 0;
		$arr_final_resultados['sucesso'] = 0;
		
		debug("## Total de turmas: " . sizeof($turmas));
		
		$cont = 0;
		// Para cada turma, chamar a migração
		foreach($turmas as $turma) {
			// Verificar o curso_default
			debug("## Iniciando turma $cont : " . $turma['Turma']['id']);
			$cont++;
			
			$cursos_turma = $turma['CursoTurma'];
			$curso_default = 0;
			//debug("Turma $turma[Turma][id]");
			if(sizeof($cursos_turma) > 1) {
				// Usar curso geral
				// Verificar se já existe
				$tem_geral = false;
				foreach($cursos_turma as $curso_turma) {
					if($curso_turma['Curso']['nome'] == "Curso Geral") {
						$curso_default = $curso_turma['id'];
						$tem_geral = true;
					}
				} 
				
				if($tem_geral == false) {
					// Senão, criar um novo
					$curso_default = -1;
				}

			} else if( sizeof($cursos_turma) == 1) {
				$curso_default = $cursos_turma[0]['id'];
			}
				
				
			// Chamar migração 
			$resultado = $this->migrar_turma($turma['Turma']['id'], $curso_default);
			
			$arr_final_resultados['turmas'][$turma['Turma']['id']] = $resultado;
			$arr_final_resultados['sucesso'] += $resultado['sucesso'];
			$arr_final_resultados['falha'] += $resultado['falha'];
		} 
		debug("### ---------- FINAL ----------- ###");
		debug($arr_final_resultados);
		
	}
	
	function super_iniciar_migrar_turma() {
		Configure::write('debug', 2);
		$turma_id = $this->data['Turma']['turma_id'];
		if ($turma_id == null) {
			$this->Session->setFlash('Turma inválida. ', 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/migracao");
		}

		$curso_default = $this->data['Turma']['curso_default'];
		if ($curso_default == null) {
			$this->Session->setFlash('Curso padrão inválido. ', 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/migracao/escolher_curso_default/$turma_id");
		} 
		
		$resultado = $this->migrar_turma($turma_id, $curso_default);
		
		debug($resultado);
		Configure::write('debug', 0);
	}
	
	private function migrar_turma($turma_id = null, $curso_default = null) {
		// Pegar a turma e criar os eventos



		$this->Turma->CursoTurma->recursive = 2;
		$this->Turma->CursoTurma->Curso->unBindModel(array (
			'hasMany' => array (
				'CursoTurma'
			)
		));
		$this->Turma->CursoTurma->unBindModel(array (
			'belongsTo' => array (
				'Turma'
			),
			'hasMany' => array (
				'FormandoProfile'
			)
		));

		if ($curso_default == -1) {
			// Buscar um curso da turma para usar id da faculdade
			$curso_da_turma = $this->Turma->CursoTurma->find('first', array (
				'conditions' => array (
					'turma_id' => $turma_id
				)
			));
			
			// Criar novo curso padrão e usá-lo para migrar os formandos
			$curso_novo = $this->Turma->Curso->create();
			$curso_novo['Curso']['nome'] = 'Curso Geral';
			$curso_novo['Curso']['faculdade_id'] = $curso_da_turma['Curso']['faculdade_id'];

			// CursoTurma
			$curso_novo['CursoTurma'][0]['turma_id'] = $turma_id;
			$curso_novo['CursoTurma'][0]['turno'] = "outros";

			if( !$this->Turma->CursoTurma->Curso->saveAll($curso_novo) ) {
				return array('erro' => 'Erro ao criar curso geral!');
			}
			
			$curso_default = $this->Turma->Curso->getLastInsertID();

		}

		//Buscar fk_turma
		$turma = $this->Turma->find('first', array (
			'conditions' => array (
				'id' => $turma_id
			)
		));

		$fk_turma = $turma['Turma']['fk_turma'];

		// Selecionar alunos da turma que ainda não foram migrados
		$this->virtualFields = $this->FormandoAntigo->virtualFields;
		$alunos = $this->FormandoAntigo->find('all', array (
			'conditions' => array (
				'fk_turma' => $fk_turma,
				'cancelamento' => array(null, '0') 
			),
			'fields' => array (
				'*',
				'DATE_FORMAT( dtprimparcela , "%e") as diavencparc'
			)
//			,'limit' => 10 
		));
		$array_resultados = array();
		$array_resultados['resumo']['sucesso'] = 0;
		$array_resultados['resumo']['falha'] = 0;
		$array_resultados['detalhes'] = array();

		//Selecionar curso default para os formandos
		$conta_existente = 0;
		
		foreach ($alunos as $aluno) {
			//debug("Tratando aluno: " . $aluno['FormandoAntigo']['id']);
			
			// TODO: verificação de email desabilitada
			if (true || $aluno['FormandoAntigo']['email'] != '' || $aluno['FormandoAntigo']['email'] != null) {
				
				// Verificar se o formando já possui um registro como comissão no sistema atual
				// Caso exista, só atualizar
				$this->Usuario->contain('FormandoProfile', 'Turma');
				$usuarios_existentes = $this->Usuario->find('all', array( 
					'conditions' => array(
						'Usuario.email' => $aluno['FormandoAntigo']['email'],
						'Usuario.id <' => 1060 
						))
					);

				$existente = false;
				$usuario = $this->Usuario->create();
				
				//debug($usuarios_existentes);
				if(sizeof($usuarios_existentes) > 0 ) {
					$usuario = $usuarios_existentes[0];
					$conta_existente++;
					debug("## Achou existente! ## $conta_existente");
					$existente = true;
					//$idturma = $usuario['Turma']['id'];
					//$usuario['Turma'] = array('id' => $idturma);
					//unset($usuario['Turma']);
					//debug($usuario);
				} else {
					$usuario = $this->Usuario->create();
					
				}
				
				
				// Tratar nome
				$nome = mb_strtolower(utf8_encode($aluno['FormandoAntigo']['nome']), "UTF-8");
				$contagem_palavra_comissao = 0;
				$search = array("comissão", "comissao", "comissÃo");
				$nome = str_replace($search, "", $nome, $contagem_palavra_comissao);
				$nome = preg_replace("/[,\.\-\/()]/","",$nome);
				$nome = trim(mb_convert_case($nome, MB_CASE_TITLE, "utf-8"));
				
					
				if($contagem_palavra_comissao > 0)
					$aluno['FormandoAntigo']['comissao'] = 1;
				
				if(!$existente)									
					$usuario['Usuario']['nome'] = $nome;
					
				$email = trim(strtolower($aluno['FormandoAntigo']['email']));
				$arr_email = array();
				preg_match("/[a-zA-Z0-9]{1}[a-zA-Z0-9._%+-]+[a-zA-Z0-9_]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/", $email, $arr_email);
				if(isset($arr_email[0])) {
					$email_tratado = $arr_email[0];	
				} else {
					if(trim($email) != "")
						debug("Invalidando email: $email");
					$email_tratado = null;
				}
				 
				if(!$existente ) { 
					$usuario['Usuario']['email'] = $email_tratado;
					$usuario['Usuario']['nivel'] = 'basico';
					$usuario['Usuario']['ativo'] = 1;
					
					//verificar se é da comissão
					if ($aluno['FormandoAntigo']['comissao'] == 0) {
						$usuario['Usuario']['grupo'] = 'formando';
						$usuario['Usuario']['ativo'] = 1;
						$usuario['FormandoProfile']['pertence_comissao'] = 0;
					} else {
						$usuario['Usuario']['grupo'] = 'comissao';
						$usuario['FormandoProfile']['pertence_comissao'] = 1;
						//Todo usuário da comissão que for cadastrado pela migração comecará inativo
						$usuario['Usuario']['ativo'] = 0;
					}
					
					$usuario['Usuario']['data_criacao'] = Date('Y-m-d H:i:s');
					
					//Gerar senha para o formando
					$usuario['Usuario']['senha'] = Security::hash($aluno['FormandoAntigo']['senha'], 'sha1', true);
					unset($this->Usuario->validate['senha']);
					unset($this->Usuario->validate['confirmar']);
					
				} else {
					//$usuario['Usuario']['grupo'] = 'comissao';
					$usuario['FormandoProfile']['pertence_comissao'] = 1;
				}
				
				// Processar endereço
				$arr_end = $this->processa_endereco($endereco_completo = $aluno['FormandoAntigo']['endereco']);
				$end_rua = $arr_end[0];
				$end_numero = $arr_end[1];
				$end_complemento = $arr_end[2];
				
				
				// Estados para verificação
				$estados = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");
				$estados_flip = array_flip($estados);
				
				$estado_aluno = preg_replace("/[,.-\/]/","",trim($aluno['FormandoAntigo']['estado']));

				
				if( array_key_exists($estado_aluno,$estados)) {
					$uf_aluno = $estado_aluno;
				} else if ( in_array($estado_aluno, $estados )) {
					$uf_aluno = $estados_flip[$estado_aluno];
				} else if ( levenshtein($estado_aluno, 'São Paulo', 1) <= 5) {
					// Se a distancia levenshtein entre o 'SP' e a estado_aluno 
					// vindo da tabela for menor ou igual a 5, considerar 'SP'
					$uf_aluno = 'SP';
				} else {
					$uf_aluno = null;
				}
					
				// Formatar tamcamiseta
				$tam_camiseta = $aluno['FormandoAntigo']['tamcamiseta'];
				if(!in_array($tam_camiseta,array('P','M', 'G'))) {
					$tam_camiseta = null;
				}
				
				// Formatar numhavaiana
				preg_match("/([0-9]{2})\//", $aluno['FormandoAntigo']['numhavaiana'], $arr_havaiana);
				$num_havaiana = "";
				if(isset($arr_havaiana[1]))
					$num_havaiana = $arr_havaiana[1];
				
				if($num_havaiana = "")
					$num_havaiana = null;
				
				// Tratar tam_camiseta
				if($tam_camiseta == "")
					$tam_camiseta = null;

				// Data de Nascimento
				$data_nascimento = $aluno['FormandoAntigo']['dtnasc'];
				$data_nascimento = trim($data_nascimento);
				if(strlen($data_nascimento) == 10 ) {
					$datetime = $this->create_date_time_from_format('Y-m-d', $data_nascimento);
					$mes = date_format($datetime, "m");
					$ano = date_format($datetime , "Y");
					$dia = date_format($datetime, "d");
					
					if( $ano > (date('Y', strtotime('now')) - 7) || !checkdate($mes, $dia, $ano)) {
						// data de nascimento inválida
						$data_nascimento = null;
					}
				} else {
					$data_nascimento = null;
				}


				// Formatar sexo
				$sexo = $aluno['FormandoAntigo']['sexo'];
				if(!in_array($sexo, array('M','F'))) 
					$sexo = null;
					
				// Tratar forma de pagamento
				$forma_pagamento = $aluno['FormandoAntigo']['formapagamento'];
				if(!in_array($forma_pagamento, array('boleto','cheque')))
					$forma_pagamento = null;
					
				// Tratar dtprimparcela
				$dataPrimeiraParcela = $aluno['FormandoAntigo']['dtprimparcela'];
				if($dataPrimeiraParcela == "" || $dataPrimeiraParcela == '0000-00-00')
					$dataPrimeiraParcela = null;
				
				// Tratar dia venc parcelas
				$diaVencimentoParcelas = $aluno['FormandoAntigo']['diavencparc'];
				if($diaVencimentoParcelas == "" || $diaVencimentoParcelas == '0' || $diaVencimentoParcelas == '00')
					$diaVencimentoParcelas = 10;	
				
				// Dados do Formando_Profile
				$usuario['FormandoProfile']['curso_turma_id'] = intval($curso_default);
				if(!$existente)
					$usuario['FormandoProfile']['rg'] = $this->somente_numeros($aluno['FormandoAntigo']['rg']);
				$usuario['FormandoProfile']['rg'] = preg_replace("/[,.-]/", "", trim($usuario['FormandoProfile']['rg'])); 
					
				$usuario['FormandoProfile']['data_nascimento'] = $data_nascimento;
				$usuario['FormandoProfile']['sexo'] = $sexo;
				//$usuario['FormandoProfile']['sexo'] = 'M';
				$usuario['FormandoProfile']['end_rua'] = $this->myucwords($end_rua);
				$usuario['FormandoProfile']['end_numero'] = $end_numero;
				$usuario['FormandoProfile']['end_complemento'] = $end_complemento;
				$usuario['FormandoProfile']['end_cep'] = $this->somente_numeros($aluno['FormandoAntigo']['cep']);
				$usuario['FormandoProfile']['end_bairro'] = $this->myucwords($aluno['FormandoAntigo']['bairro']);
				$usuario['FormandoProfile']['end_cidade'] = $this->myucwords($aluno['FormandoAntigo']['cidade']);
				$usuario['FormandoProfile']['end_uf'] = $uf_aluno;
				$usuario['FormandoProfile']['tel_residencial'] = $this->minimo(8,$this->somente_numeros($aluno['FormandoAntigo']['telres']));
				$usuario['FormandoProfile']['tel_comercial'] = $this->minimo(8,$this->somente_numeros($aluno['FormandoAntigo']['telcom']));
				$usuario['FormandoProfile']['tel_celular'] = $this->minimo(8,$this->somente_numeros($aluno['FormandoAntigo']['telcel']));
				//$usuario['FormandoProfile']['operadora_celular'] = 
				$usuario['FormandoProfile']['nome_mae'] =  $this->formata_nome($aluno['FormandoAntigo']['nomemae']);
				$usuario['FormandoProfile']['tel_mae'] = $this->somente_numeros($aluno['FormandoAntigo']['celmae']);
				$usuario['FormandoProfile']['nome_pai'] = $this->formata_nome($aluno['FormandoAntigo']['nomepai']);
				$usuario['FormandoProfile']['tel_pai'] = $this->somente_numeros($aluno['FormandoAntigo']['celpai']);
				$usuario['FormandoProfile']['nextel'] = $aluno['FormandoAntigo']['telnextel'];
				$usuario['FormandoProfile']['tam_camiseta'] = $tam_camiseta;
				$usuario['FormandoProfile']['numero_havaiana'] = $num_havaiana;
				$usuario['FormandoProfile']['cpf'] = intval($this->somente_numeros($aluno['FormandoAntigo']['cpf']));
				$usuario['FormandoProfile']['curso_turma_id'] = $curso_default;
				$usuario['FormandoProfile']['dia_vencimento_parcelas'] = $diaVencimentoParcelas;
				
				$usuario['FormandoProfile']['forma_pagamento'] = $forma_pagamento;
				$usuario['FormandoProfile']['boleto_correio'] = 0;
				$usuario['FormandoProfile']['obs'] = $aluno['FormandoAntigo']['obs'];
				$usuario['FormandoProfile']['interesse_fotos'] = $aluno['FormandoAntigo']['interessefotos'];
				$usuario['FormandoProfile']['album_produzido'] = 0;
				$usuario['FormandoProfile']['album_vendido'] = 0;
				$usuario['FormandoProfile']['data_venda_album'] =  $aluno['FormandoAntigo']['dtvenda'];
				$usuario['FormandoProfile']['valor_venda_album'] =  $aluno['FormandoAntigo']['valorvenda'];
				$usuario['FormandoProfile']['login_antigo'] =  $aluno['FormandoAntigo']['login'];
				$usuario['FormandoProfile']['senha_antiga'] =  $aluno['FormandoAntigo']['senha'];
				
				
				//TODO: Verificar
				if($aluno['FormandoAntigo']['cancelamento'] == 1) {
					$usuario['FormandoProfile']['situacao'] = 'cancelado';
				} else {
					$usuario['FormandoProfile']['situacao'] = 'ativo';
				}
				 
				$usuario['FormandoProfile']['data_adesao'] = $aluno['FormandoAntigo']['dtcadastro'];
				
				// Tomar cuidado quando tem compra de extra na adesao
				//  R: Não há nenhum formando no conjunto de pessoas que comprou extra na adesão
				$usuario['FormandoProfile']['valor_adesao'] = $aluno['FormandoAntigo']['valor'];
				$usuario['FormandoProfile']['parcelas_adesao'] = $aluno['FormandoAntigo']['parcelas'];
				
				$usuario['FormandoProfile']['codigo_formando'] = substr($aluno['FormandoAntigo']['codigo'], strlen($aluno['FormandoAntigo']['codigo']) - 3, 3);
				$usuario['FormandoProfile']['realizou_checkout'] = 0;
				$usuario['FormandoProfile']['retirou_convites_contrato'] = 0;
				$usuario['FormandoProfile']['retirou_mesas_contrato'] = 0;
			
				
				// Gerar as despesas
				$arr_despesas = $this->gera_despesas_adesao(
									$usuario['FormandoProfile']['parcelas_adesao'] , 
									$usuario['FormandoProfile']['valor_adesao'],
									$dataPrimeiraParcela,
									$aluno['FormandoAntigo']['dtcadastro'],
									$usuario['FormandoProfile']['dia_vencimento_parcelas']
									);
									
				if(is_array($arr_despesas) && sizeof($arr_despesas) > 0)
					$usuario['Despesa'] = $arr_despesas;

				
				unset($usuario['Turma']);
				if($this->Usuario->saveAll($usuario)) {
					//debug('Usuario criado com sucesso.' . $usuario['FormandoProfile']['codigo_formando']);
					
					$usuario_id = $this->Usuario->getLastInsertID();

					
					
									 
				 	
					if(!$existente) {
						if($this->Turma->alocarUsuario($usuario_id, $turma_id)) {
							$array_resultados['resumo']['sucesso'] += 1;
							$array_resultados['detalhes'][] = array('dados' => $usuario, 'resultado' => 'sucesso', 'erros' => null);
						} else {
							$array_resultados['resumo']['falha'] += 1;
							debug("Usuario criado com sucesso, mas houve falha ao alocar na turma");
							$array_resultados['detalhes'][] = array('dados' => $usuario, 'resultado' => 'sucesso_parcial', 'erros' => $this->Usuario->validationErrors);
						}
					} else {
						$array_resultados['resumo']['sucesso'] += 1;
						$array_resultados['detalhes'][] = array('dados' => $usuario, 'resultado' => 'sucesso', 'erros' => null);
					}

				} else {
					$array_resultados['resumo']['falha'] += 1;
					debug("Erro ao criar o usuário # $existente: "  . $usuario['FormandoProfile']['rg'] . "####");
					debug($arr_email);
					$array_resultados['detalhes'][] = array('dados' => $usuario, 'resultado' => 'falha', 'erros' => $this->Usuario->validationErrors);
					
					debug($this->Usuario->validationErrors);
				}
				//debug($usuario);
			} else {

			}

		}

		return $array_resultados['resumo'];

	}
	
	
	function minimo($tam, $str) {
		if(strlen($str) < $tam)
			return null;
		else
			return $str;
	}
	
	function formata_nome($nome) {
		$nome = mb_strtolower(utf8_encode($nome), "UTF-8");
		$nome = preg_replace("/[,\.\-\/()]/","",$nome);
		$nome = trim(mb_convert_case($nome, MB_CASE_TITLE, "utf-8"));
		
		return $nome;
	}
	
	function gera_despesas_adesao($num_parcelas, $valor_adesao, $data_inicio_pagamento, $data_cadastro, $dia_vencimento) {
		$despesasArray = array();
		$valor_adesao = floatval($valor_adesao);
		
		if($valor_adesao > 0) {
				if($num_parcelas <= 0) $num_parcelas = 1;
			
	    	$valor_parcela = $valor_adesao / $num_parcelas;
	    	
	
	    	//Cria um array com as informações da data de início de parcelamento
	    	$dtInicioPagamento =  $this->create_date_time_from_format('Y-m-d', $data_inicio_pagamento);
	    	$mes = date_format($dtInicioPagamento, "m");
			$ano = date_format($dtInicioPagamento , "Y");
			$dia = date_format($dtInicioPagamento, "d");
	    	if( $dtInicioPagamento == "0000-00-00" || $dtInicioPagamento == "" || !checkdate($mes,$dia,$ano) ) {
	    		$dtInicioPagamento = $this->create_date_time_from_format('Y-m-d', $data_cadastro);
	    	}
	    	
	
	    	//Com os dados do parcelamento, preencher as linhas necessárias na tabela Despesas
	    	for($i = 0 ; $i < $num_parcelas ; $i++) {
	    		$diaDeVencimento = $dia_vencimento;
	    		
	    		$despesasArray[] = array(
	    			'valor' => str_replace(",",".",$valor_parcela), 
	    			'data_cadastro' => date('Y-m-d H:i:s',strtotime('now')) ,
	    			'tipo' => 'adesao',
	    			'parcela' => $i+1,
	    			'total_parcelas' => $num_parcelas,
	    			'data_vencimento' =>  date('Y-m-d', mktime(0, 0, 0, date_format($dtInicioPagamento , 'm') + $i  , $diaDeVencimento ,  date_format( $dtInicioPagamento, 'Y' )))
	    		);
	    	}
		}    	
    	return $despesasArray;
	}

	function somente_numeros($str) {
		$numero = preg_replace("/[^0-9]/", "" , $str);
		return $numero;
	}

	function myucwords($str) {
		$str = trim($str);
		setlocale(LC_ALL, 'pt_BR');
		$str = strtolower($str);
//		$from = 'ÀÁÃÂÉÊÍÓÕÔÚÜÇ';
//		$to	  = 'àáãâéêíóõôúüç';
//   	$str2 = strtr($str, $from, $to);
//		$str = preg_replace("/[^0-9a-zA-Zàáãâéêíóõôúüç \(\)\- ]/", "", $str);
		$str = ucwords($str);
		
		return $str;
		
	}
	
	function processa_endereco($endereco_completo) {
		$end_rua = "";
		$end_numero = "";
		$end_complemento = "";
		
		if (strpos($endereco_completo, ",") === false) {
			// Não foi encontrada vírgula no endereço, colocar tudo no campo 'rua'

			$end_rua = $endereco_completo;
		} else {
			$arr_end = explode(',', $endereco_completo, 2);

			$end_rua = $arr_end[0];
			$restante = trim($arr_end[1]);

			//Verificar se possui apenas números e colocar no numero. Caso contrario, colocar em complemento
			if (strlen($restante) > 0) {
				preg_match_all("/\s*\A\s*([0-9]+)\s*(.*)\z/", $restante, $arr_restante);
				
				if (sizeof($arr_restante) == 2) {
					// só tem o numero
					$end_numero = trim($restante);
				} else {
					if (sizeof($arr_restante) == 3) {
						// tem numero + complemento
						// Ex: 574 ap. 33
						$arr_end_numero = $arr_restante[1];
						if(sizeof($arr_end_numero) > 0)
							$end_numero = trim($arr_end_numero[0]);
						else
							$end_numero = "";
						
						$arr_end_complemento = $arr_restante[2];
						if(sizeof($arr_end_complemento) > 0)
							$end_complemento = trim($arr_end_complemento[0]);
						else
							$end_complemento = "";
					} else {
						// Começa com texto
						// Colocar tudo no complemento
						$end_complemento = $restante;
					}
				}
				if($end_complemento != "" && $end_complemento[0] == ',') $end_complemento = substr($end_complemento,1,strlen($end_complemento));
			}
		}
		
		return array( $end_rua, $end_numero, $end_complemento);
	}
	
	

}
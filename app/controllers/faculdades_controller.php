<?php

class FaculdadesController extends AppController {

    var $name = 'Faculdades';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Faculdade.id' => 'desc'
        )
    );
    
    private function _remover($universidadeId,$faculdadeId) {
        $this->layout = false;
        $this->loadModel('Curso');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $data = $this->data;
            $cursos = $this->Curso->find('count',array(
                'conditions' => array(
                    'Curso.faculdade_id' => $data['Faculdade']['id']
                )
            ));
            if($cursos > 0 && empty($data['Faculdade']['substituir'])) {
                $this->Session->setFlash('Selecione uma Faculdade para substituir a removida', 'metro/flash/error');
            } elseif($cursos > 0) {
                if(!$this->Curso->updateAll(
                        array('Curso.faculdade_id' => $data['Faculdade']['substituir']),
                        array('Curso.faculdade_id' => $data['Faculdade']['id'])
                ))
                    $this->Session->setFlash("Erro ao substituir faculdade", 'metro/flash/error');
                elseif(!$this->Faculdade->delete($data['Faculdade']['id']))
                    $this->Session->setFlash("Erro ao remover faculdade", 'metro/flash/error');
                else
                    $this->Session->setFlash("Faculdade removida com sucesso", 'metro/flash/success');
            } else {
                if(!$this->Faculdade->delete($data['Faculdade']['id']))
                    $this->Session->setFlash("Erro ao remover faculdade", 'metro/flash/error');
                else
                    $this->Session->setFlash("Faculdade removida com sucesso", 'metro/flash/success');
            }
            echo json_encode(array());
        } elseif($faculdadeId) {
            $this->data['Faculdade']['id'] = $faculdadeId;
            $cursos = $this->Curso->find('count',array(
                'conditions' => array(
                    'Curso.faculdade_id' => $faculdadeId
                )
            ));
            if($cursos > 0) {
                $faculdades = $this->Faculdade->find('list',array(
                    'conditions' => array(
                        "Faculdade.id <> $faculdadeId",
                        'Faculdade.universidade_id' => $universidadeId
                    ),
                    'fields' => 'Faculdade.nome',
                    'order' => 'Faculdade.nome'
                ));
                $this->set('faculdades',$faculdades);
                $this->set('permitido',false);
            } else {
                $this->set('faculdades',array());
                $this->set('permitido',true);
            }
            $this->render('_remover');
        }
    }
    
    function comercial_remover($universidadeId,$faculdadeId) {
        $this->_remover($universidadeId,$faculdadeId);
    }
    
    function super_remover($universidadeId,$faculdadeId) {
        $this->_remover($universidadeId,$faculdadeId);
    }
    
    private function _editar($universidadeId,$faculdadeId = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->Faculdade->save($this->data))
                $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
            else {
                $erros = array('Erro ao salvar dados');
                if(!empty($this->Faculdade->validationErrors))
                    $erros = array_merge($erros,array_values($this->Faculdade->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $this->loadModel('Universidade');
            if($faculdadeId)
                $this->data = $this->Faculdade->read(null,$faculdadeId);
            else {
                $this->Universidade->recursive = 0;
                $this->data = $this->Universidade->read(null,$universidadeId);
                $this->data['Faculdade']['universidade_id'] = $universidadeId;
            }
            $universidades = $this->Universidade->find('list',array(
                'fields' => 'Universidade.nome',
                'order' => 'Universidade.nome'
            ));
            $this->set('universidades',$universidades);
            $this->render('_editar');
        }
    }
    
    function comercial_editar($universidadeId,$faculdadeId = false) {
        $this->_editar($universidadeId,$faculdadeId);
    }
    
    function super_editar($universidadeId,$faculdadeId = false) {
        $this->_editar($universidadeId,$faculdadeId);
    }
    
    private function _dados($modelo,$campo,$id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel($modelo);
        $this->$modelo->recursive = 0;
        $data = $this->$modelo->find('all',array(
            'conditions' => array(
                "$modelo.$campo" => $id
            )
        ));
        echo json_encode(array(
            $modelo => $data
        ));
    }
    
    function comercial_dados($modelo,$campo,$id) {
        $this->_dados($modelo, $campo, $id);
    }
    
    function super_dados($modelo,$campo,$id) {
        $this->_dados($modelo, $campo, $id);
    }
    
    private function _gerenciar() {
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        $this->loadModel('Universidade');
        $options = array(
            'order' => array(
                'Universidade.nome' => 'asc'
            )
        );
        if(!in_array($usuario['Usuario']['nivel'], array('administrador', 'gerencial')))
            $options['joins'] = array(
                array(
                    "table" => "universidades_usuarios",
                    "type" => "inner",
                    "alias" => "UniversidadeUsuario",
                    "conditions" => array(
                        "Universidade.id = UniversidadeUsuario.universidade_id",
                        "UniversidadeUsuario.usuario_id = {$usuario['Usuario']['id']}"
                    )
                )
            );
        $this->Universidade->recursive = 0;
        $universidades = $this->Universidade->find('all',$options);
        $this->set('universidades',$universidades);
        $this->render('_gerenciar');
    }
    
    function comercial_gerenciar() {
        $this->_gerenciar();
    }
    
    function super_gerenciar() {
        $this->_gerenciar();
    }

    function comercial_relatorio() {
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.universidades",
                    $this->data);
        } else {
            $this->loadModel('Universidade');
            $options = array(
                'limit' => 25,
                'order' => array(
                    'Universidade.nome' => 'desc'
                )
            );
            $options['joins'] = array(
                array(
                    "table" => "universidades_usuarios",
                    "type" => "left",
                    "alias" => "UniversidadeUsuario",
                    "conditions" => array(
                        "Universidade.id = UniversidadeUsuario.universidade_id"
                    )
                ),
                array(
                    'table' => 'usuarios',
                    'alias' => 'Usuario',
                    "type" => "left",
                    'foreignKey' => false,
                    'conditions' => array(
                        'UniversidadeUsuario.usuario_id = Usuario.id',
                        'Usuario.grupo' => 'comercial'
                    )
                )
            );
            if($usuario['Usuario']['nivel'] != 'administrador')
                $options['conditions']['UniversidadeUsuario.usuario_id'] = $usuario['Usuario']['id'];
            $this->Universidade->bindModel(array(
                'hasAndBelongsToMany' => array(
                    'UniversidadeUsuario' => array(
                        'className' => 'Usuario',
                        'joinTable' => 'universidades_usuarios',
                        'foreignKey' => 'universidade_id',
                        'associationForeignKey' => 'usuario_id',
                        'unique' => false
                    )
                )
            ), false);
            if($this->Session->check("filtros.{$this->params['prefix']}.universidades")) {
                $config = $this->Session->read("filtros.{$this->params['prefix']}.universidades");
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $this->paginate['Universidade'] = $options;
            $universidades = $this->paginate('Universidade');
            $this->set('faculdades',$universidades);
        }
    }
    
    function super_relatorio(){
        $this->comercial_relatorio();
    }
    
    function comercial_relatorio_cursos($universidadeId) {
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        $this->loadModel('Turma');
        $options['joins'] = array(
            array(
                'table' => 'turmas_usuarios',
                'alias' => 'TurmaUsuario',
                'foreignKey' => false,
                'conditions' => array(
                    'Turma.id = TurmaUsuario.turma_id',
                    "TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}"
                )
            )
        );
        $turmas = $this->Turma->find('list',$options);
        $this->loadModel('Universidade');
        $this->Universidade->recursive = 2;
        $universidade = $this->Universidade->read(null,$universidadeId);
        $this->loadModel('CursoTurma');
        $this->CursoTurma->unbindModel(array('hasMany' => array('FormandoProfile')),false);
        $anos = array();
        $cursos = array();
        $faculdades = array();
        foreach($universidade['Faculdade'] as $faculdade) {
            foreach($faculdade['Curso'] as $curso) {
                $cursosTurmas = $this->CursoTurma->find('all',array(
                    'conditions' => array('CursoTurma.curso_id' => $curso['id'])
                ));
                foreach($cursosTurmas as $cursoTurma) {
                    $anoFormatura = "{$cursoTurma['Turma']['ano_formatura']}.{$cursoTurma['Turma']['semestre_formatura']}";
                    if(in_array($cursoTurma['Turma']['id'], $turmas)) {
                        $anos[] = $anoFormatura;
                        $faculdades[$faculdade['id']]['nome'] = $faculdade['nome'];
                        $faculdades[$faculdade['id']]['cursos'][$curso['id']]['nome'] = $curso['nome'];
                        $inserir = true;
                        if(isset($faculdades[$faculdade['id']]['cursos'][$curso['id']][$anoFormatura]))
                            foreach($faculdades[$faculdade['id']]['cursos'][$curso['id']][$anoFormatura] as $turmaAnoFormatura)
                                if($turmaAnoFormatura['id'] == $cursoTurma['Turma']['id'])
                                    $inserir = false;
                        if($inserir)
                            $faculdades[$faculdade['id']]['cursos'][$curso['id']][$anoFormatura][] = $cursoTurma['Turma'];
                    }
                }
            }
        }
        $anos = array_unique($anos);
        sort($anos);
        $this->set('faculdade',$universidade);
        $this->set('anos',$anos);
        $this->set('faculdades',$faculdades);
    }
    
    function super_relatorio_cursos($universidadeId){
        $this->comercial_relatorio_cursos($universidadeId);
    }
    
    function super_index() {
        $this->set('faculdades', $this->paginate('Faculdade'));
    }

    function super_procurar() {
        $chave = empty($this->data['Faculdades']['chave']) ? "" : $this->data['Faculdades']['chave'];
        $chaves = preg_split("[, -.]", $this->data['Faculdades']['chave']);

        function construirQuery($c) {
            return array('Faculdade.nome LIKE' => '%' . $c . '%');
        }

        $chaves = array_map('construirQuery', $chaves);

        $conditions = empty($chaves) ? array() : array('OR' => $chaves);
        $this->set('faculdades', $this->paginate('Faculdade', $conditions));

        $this->set('chave', $chave);
        $this->render('super_index');
    }

    function super_adicionar() {

        $this->_selectDeUniversidades();

        if (!empty($this->data)) {
            if (is_numeric($this->data['Faculdade']['universidade_id'])) {
                if ($this->Faculdade->save($this->data['Faculdade'])) {
                    $this->Session->setFlash('Faculdade foi salva com sucesso', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/faculdades/visualizar/{$this->Faculdade->id}");
                }
                else
                    $this->Session->setFlash('Ocorreu um erro ao salvar a faculdade.', 'flash_erro');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao salvar a faculdade. Selecione uma universidade.', 'flash_erro');
            }
        }
    }

    function super_editar_antigo($id = null) {

        $this->Faculdade->id = $id;
        $this->_selectDeUniversidades();

        if (!empty($this->data)) {

            if (is_numeric($this->data['Faculdade']['universidade_id'])) {
                if ($this->Faculdade->save($this->data['Faculdade'])) {
                    $this->Session->setFlash('Faculdade foi atualizada com sucesso', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/faculdades/visualizar/{$this->Faculdade->id}");
                }
                else
                    $this->Session->setFlash('Ocorreu um erro ao salvar a faculdade.', 'flash_erro');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao salvar a faculdade.', 'flash_erro');
            }
        }
        else
            $this->data = $this->Faculdade->read();

        if (!$this->data)
            $this->Session->setFlash('Faculdade não existente', 'flash_erro');
    }

    function super_visualizar($id = null) {

        $faculdade = $this->Faculdade->find(array('Faculdade.id' => $id));

        if (!$faculdade) {
            $this->Session->setFlash('Faculdade não existente', 'flash_erro');
        }
        $this->set('faculdade', $faculdade);
    }

    function super_deletar($id = null) {

        if ($this->Faculdade->delete($id))
            $this->Session->setFlash('Faculdade excluida com sucesso.', 'flash_sucesso');
        else
        // TODO melhorar estas frases de erro
        // Talvez colocá-las em um lugar unificado seja interessante
            $this->Session->setFlash('Erro ao excluir a faculdade. Verifique se ela não possui cursos atrelados.', 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/faculdades");
    }

    private function _selectDeUniversidades() {
        $this->loadModel('Universidade');

        $universidades = $this->Universidade->find('list', array('fields' => array('Universidade.nome')));
        $this->set('universidade_select', $universidades);
    }

}

?>
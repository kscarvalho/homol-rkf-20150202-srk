<?php

class EntregaController extends AppController {
	
	var $name = 'Entrega';
	
	var $uses = array('Despesa','Pagamento','DespesaPagamento', 'FormandoProfile', 'CampanhasUsuario', 'Turma',
			'Usuario','ViewFormandos','ViewRelatorioExtras','CheckoutUsuarioItem','CheckoutFormandoPagamento');
	
	var $paginate = array('limit' => 50);
	
	var $helpers = array('Html','Form');
	
	var $nomeDoTemplateSidebar = 'Entrega';
	
	function planejamento_itens() {
		$options = array('contitions' => array());
		$resumo = true;
		if($this->data) {
			if($this->data['filtro'] == "turma")
				$resumo = true;
				$options['conditions']["ViewFormandos.{$this->data['filtro']}"] = $this->data['campo'];
		}
		$this->paginate['ViewFormandos'] = $options;
		if($resumo)
			$formandos = $this->paginate('ViewFormandos');
		else
			$formandos = $this->ViewFormandos->find('all',$options);
		foreach($formandos as $indice => $formando) {
			$formandos[$indice]['checkout']['itens'] = $this->CheckoutUsuarioItem->find('all',array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id'])));
			if(!empty($formandos[$indice]['checkout']['itens']))
				$formandos[$indice]['checkout']['totalPago'] = $this->CheckoutFormandoPagamento->sum('valor',
						array('protocolo_id' => $formandos[$indice]['checkout']['itens'][0]['CheckoutUsuarioItem']['protocolo_id']));
			foreach($this->ViewRelatorioExtras->find('all',array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id']))) as $extra) {
				$formandos[$indice]['campanhas'][$extra['ViewRelatorioExtras']['campanha_id']]['extras'][] = $extra;
			}
		}
		$this->set('resumo',$resumo);
	}
	
	function planejamento_resumo_itens() {
		$turma = $this->Session->read('turma');
		$this->set('turma',$turma);
		$formandos = $this->ViewFormandos->find('all',
				array('conditions' => array('turma_id' => $turma['Turma']['id'])));
		$itens = array(
				'checkout' => array(),
				'extras' => array(),
				'contrato' => array(
						'mesas' =>array('quantidade_retirada' => 0, 'quantidade_total' => 0),
						'convites' =>array('quantidade_retirada' => 0, 'quantidade_total' => 0)));
		$extras = $this->ViewRelatorioExtras->find('all',
			array(
				'conditions' => array('turma_id' => $turma['Turma']['id'],'cancelada' => 0),
				'fields' => array(
						"sum(quantidade_total) as quantidade_total",
						"sum(quantidade_retirada) as quantidade_retirada",
						"sum(quantidade_cancelada) as quantidade_cancelada",
						'extra',
						'extra_id'),
				'group' => array('extra_id')
			)
		);
		$itens['extras'] = $extras;
		foreach($formandos as $formando) {
			//debug("{$formando['ViewFormandos']['id']} - {$formando['ViewFormandos']['nome']} | {$formando['ViewFormandos']['retirou_itens_contrato']}");
			$itens['contrato']['mesas']['quantidade_total']+= $formando['ViewFormandos']['mesas_contrato'];
			$itens['contrato']['convites']['quantidade_total']+= $formando['ViewFormandos']['convites_contrato'];
			if($formando['ViewFormandos']['retirou_itens_contrato'] == 1) {
				$itens['contrato']['mesas']['quantidade_retirada']+= $formando['ViewFormandos']['mesas_contrato'];
				$itens['contrato']['convites']['quantidade_retirada']+= $formando['ViewFormandos']['convites_contrato'];
			}
			//debug("mesas - retirou {$formando['ViewFormandos']['retirou_itens_contrato']} - qtde {$itens['contrato']['mesas']['quantidade_total']} - retirada {$itens['contrato']['mesas']['quantidade_retirada']}");
			$itensCheckout = $this->CheckoutUsuarioItem->find('all',
					array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id'])));
			foreach($itensCheckout as $item) {
				if(isset($itens['checkout'][$item['CheckoutItem']['id']])) {
					$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_total']+=
						$item['CheckoutUsuarioItem']['quantidade'];
					if($item['CheckoutUsuarioItem']['retirou'] == 1)
						$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_retirada']+=
							$item['CheckoutUsuarioItem']['quantidade'];
				} else {
					if($item['CheckoutUsuarioItem']['retirou'] == 1)
						$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_retirada'] =
							$item['CheckoutUsuarioItem']['quantidade'];
					else
						$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_retirada'] = 0;
					$itens['checkout'][$item['CheckoutItem']['id']]['titulo'] = $item['CheckoutItem']['titulo'];
					$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_total'] = $item['CheckoutUsuarioItem']['quantidade'];
				}
				//debug("{$itens['checkout'][$item['CheckoutItem']['id']]['titulo']} - qtde {$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_total']} - retirada  - {$itens['checkout'][$item['CheckoutItem']['id']]['quantidade_retirada']}");
			}
		}
		$this->set('itens',$itens);
	}
	
	function planejamento_confirmar() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		$turma = $this->Session->read('turma');
		if(!isset($_POST['codigoFormando'])) {
			echo json_encode(array('message' => array('Dados inv&aacute;lidos'), 'error' => true));
		} else {
			$json = array('message' => array(), 'error' => true);
			$codigoFormando = $_POST['codigoFormando'];
			$this->ViewFormandos->bindModel(array('hasOne' => array('Turma' => array('foreignKey' => 'id' , 'recursive' => 1))), false);
			$formando = $this->ViewFormandos->find('first',array('conditions' => array('codigo_formando' => $codigoFormando)));
			if(empty($formando)) {
				$json['message'][] = "Formando n&atilde;o encontrado - $codigoFormando";
			} elseif($formando['ViewFormandos']['turma_id'] != $turma['Turma']['id']) {
				$json['message'][] = "Formando n&atilde;o pertence &agrave; turma selecionada";
			} else {
				$json['formando'] = $formando['ViewFormandos'];
				$this->loadModel('Protocolo');
				$protocolo = $this->Protocolo->find('first', array('conditions' => array('Protocolo.usuario_id' => $formando['ViewFormandos']['id'], 'Protocolo.tipo' => 'checkout')));
				if(empty($protocolo)) {
					$json['message'][] = "N&atilde;o realizou checkout";
				} else {
					$json['itens'] = array('contrato' => array(), 'checkout' => array(),'extras' => array());
					$json['error'] = false;
					if($formando['ViewFormandos']['retirou_itens_contrato'] != "0") {
						$json['itens']['contrato']['message'] = "Itens de contrato j&aacute; foram retirados<br />";
					} else {
						if($this->FormandoProfile->updateAll(
								array('FormandoProfile.retirou_itens_contrato' => 1),
								array('FormandoProfile.usuario_id' => $formando['ViewFormandos']['id'])
						)) {
							$json['itens']['contrato']['mesas'] = !empty($formando['ViewFormandos']['mesas_contrato']) ?
							$formando['ViewFormandos']['mesas_contrato'] : $turma['Turma']['mesas_contrato'];
							$json['itens']['contrato']['convites'] = !empty($formando['ViewFormandos']['convites_contrato']) ?
							$formando['ViewFormandos']['convites_contrato'] : $turma['Turma']['convites_contrato'];
						} else {
							$json['itens']['contrato']['message'] = "Erro ao confirmar retirada. Tente novamente<br />";
						}
					}
					$checkoutItens = $this->CheckoutUsuarioItem->find('all',array('conditions' =>
							array('protocolo_id' => $protocolo['Protocolo']['id'], 'usuario_id' => $formando['ViewFormandos']['id'])));
					if(empty($checkoutItens)) {
						$json['itens']['checkout']['message'] = "Formando n&atilde;o comprou itens durante o checkout<br />";
					} else {
						if($this->CheckoutUsuarioItem->updateAll(
								array('CheckoutUsuarioItem.retirou' => "'1'"),
								array('CheckoutUsuarioItem.protocolo_id' => $protocolo['Protocolo']['id'],
										'usuario_id' => $formando['ViewFormandos']['id'])
						)) {
							foreach($checkoutItens as $checkoutItem) {
								if($checkoutItem['CheckoutUsuarioItem']['retirou'] == "0") {
									if(isset($json['itens']['checkout'][$checkoutItem['CheckoutItem']['id']]))
										$json['itens']['checkout'][$checkoutItem['CheckoutItem']['id']]['quantidade']+=$checkoutItem['CheckoutUsuarioItem']['quantidade'];
									else
										$json['itens']['checkout'][$checkoutItem['CheckoutItem']['id']]['quantidade'] = $checkoutItem['CheckoutUsuarioItem']['quantidade'];
								}
								if(!isset($json['itens']['checkout'][$checkoutItem['CheckoutItem']['id']]['titulo']))
									$json['itens']['checkout'][$checkoutItem['CheckoutItem']['id']]['titulo'] = $checkoutItem['CheckoutItem']['titulo'];
							}
						} else {
							$json['itens']['checkout']['message'] = "Erro ao confirmar retirada. Tente novamente<br />";
						}
					}
					$extras = $this->ViewRelatorioExtras->find('all',array('conditions' =>
							array('codigo_formando' => $codigoFormando, 'quantidade_retirada' => 0)));
					$campanhasUsuario = $this->CampanhasUsuario->find('all',array(
							'conditions' => array('usuario_id' => $formando['ViewFormandos']['id'])));
					if(empty($extras) || empty($campanhasUsuario)) {
						$json['itens']['extras']['message'] = "Formando n&atilde;o comprou itens de campanhas<br />";
					} else {
						$idCampanhas = "";
						foreach($campanhasUsuario as $campanhaUsuario)
							$idCampanhas.= "{$campanhaUsuario['CampanhasUsuario']['id']},";
						$this->loadModel('CampanhasUsuarioCampanhasExtra');
						if($this->CampanhasUsuarioCampanhasExtra->updateAll(
								array('CampanhasUsuarioCampanhasExtra.retirou' => "'1'"),
								array("CampanhasUsuarioCampanhasExtra.campanhas_usuario_id in(" . rtrim($idCampanhas,',') . ")")
						)) {
							foreach($extras as $extra) {
								if($extra['ViewRelatorioExtras']['retirou'] == "0") {
									if(isset($json['itens']['extras'][$extra['ViewRelatorioExtras']['extra_id']]))
										$json['itens']['extras'][$extra['ViewRelatorioExtras']['extra_id']]['quantidade']+= $extra['ViewRelatorioExtras']['quantidade'];
									else
										$json['itens']['extras'][$extra['ViewRelatorioExtras']['extra_id']]['quantidade'] = $extra['ViewRelatorioExtras']['quantidade'];
								}
								if(!isset($json['itens']['extras'][$extra['ViewRelatorioExtras']['extra_id']]['titulo']))
									$json['itens']['extras'][$extra['ViewRelatorioExtras']['extra_id']]['titulo'] = $extra['ViewRelatorioExtras']['item'];
							}
						} else {
							$json['itens']['extras']['message'] = "Erro ao confirmar retirada. Tente novamente $idCampanhas<br />";
						}
					}
				}
			}
			echo json_encode($json);
		}
	}
	
}
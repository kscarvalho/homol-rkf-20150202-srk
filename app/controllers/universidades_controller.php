<?php

class UniversidadesController extends AppController {

    var $name = 'Universidades';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 55,
        'order' => array(
            'Universidade.nome' => 'asc'
        )
    );
    
    var $nomeDoTemplateSidebar = 'turmas';
    
    
    private function _remover($universidadeId) {
        $this->layout = false;
        $this->loadModel('Faculdade');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $data = $this->data;
            $faculdades = $this->Faculdade->find('count',array(
                'conditions' => array(
                    'Faculdade.universidade_id' => $data['Universidade']['id']
                )
            ));
            if($faculdades > 0 && empty($data['Universidade']['substituir'])) {
                $this->Session->setFlash('Selecione uma Universidade para substituir a removida', 'metro/flash/error');
            } elseif($faculdades > 0) {
                if(!$this->Faculdade->updateAll(
                        array('Faculdade.universidade_id' => $data['Universidade']['substituir']),
                        array('Faculdade.universidade_id' => $data['Universidade']['id'])
                ))
                    $this->Session->setFlash("Erro ao substituir universidade", 'metro/flash/error');
                else {
                    $q = "update universidades_usuarios set universidade_id = {$data['Universidade']['substituir']} ".
                            "where universidade_id = {$data['Universidade']['id']}";
                    $this->Universidade->query($q);
                    if(!$this->Universidade->delete($data['Universidade']['id']))
                        $this->Session->setFlash("Erro ao remover universidade", 'metro/flash/error');
                    else
                        $this->Session->setFlash("Universidade removida com sucesso", 'metro/flash/success');
                }
            } else {
                if(!$this->Universidade->delete($data['Universidade']['id']))
                    $this->Session->setFlash("Erro ao remover universidade", 'metro/flash/error');
                else
                    $this->Session->setFlash("Universidade removida com sucesso", 'metro/flash/success');
            }
            echo json_encode(array());
        } elseif($universidadeId) {
            $this->data['Universidade']['id'] = $universidadeId;
            $faculdades = $this->Faculdade->find('count',array(
                'conditions' => array(
                    'Faculdade.universidade_id' => $universidadeId
                )
            ));
            if($faculdades > 0) {
                $universidades = $this->Universidade->find('list',array(
                    'conditions' => array(
                        "Universidade.id <> $universidadeId"
                    ),
                    'fields' => 'Universidade.nome',
                    'order' => 'Universidade.nome'
                ));
                $this->set('universidades',$universidades);
                $this->set('permitido',false);
            } else {
                $this->set('universidades',array());
                $this->set('permitido',true);
            }
            $this->render('_remover');
        }
    }
    
    function comercial_remover($universidadeId) {
        $this->_remover($universidadeId);
    }
    
    function super_remover($universidadeId) {
        $this->_remover($universidadeId);
    }
    
    private function _editar($universidadeId = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->Universidade->save($this->data))
                $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
            else {
                $erros = array('Erro ao salvar dados');
                if(!empty($this->Universidade->validationErrors))
                    $erros = array_merge($erros,array_values($this->Universidade->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            if($universidadeId)
                $this->data = $this->Universidade->read(null,$universidadeId);
            $this->render('_editar');
        }
    }
    
    function comercial_editar($universidadeId = false) {
        $this->_editar($universidadeId);
    }
    
    function super_editar($universidadeId = false) {
        $this->_editar($universidadeId);
    }

    function super_index() {
        $this->set('universidades', $this->paginate('Universidade'));
    }
    
    function super_procurar() {
        $chave = empty($this->data['Universidades']['chave']) ? "" : $this->data['Universidades']['chave'];
        $chaves = preg_split("[, -.]", $this->data['Universidades']['chave']);

        function construirQuery($c) {
            return array('Universidade.nome LIKE' => '%' . $c . '%');
        }

        $chaves = array_map('construirQuery', $chaves);

        $conditions = empty($chaves) ? array() : array('OR' => $chaves);
        $this->set('universidades', $this->paginate('Universidade', $conditions));

        $this->set('chave', $chave);
        $this->render('super_index');
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            if ($this->Universidade->save($this->data['Universidade'])) {
                $this->Session->setFlash('Universidade foi salva com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/universidades/visualizar/{$this->Universidade->id}");
            }
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar a universidade.', 'flash_erro');
        }
    }

    function super_editar_antigo($id = null) {

        $this->Universidade->id = $id;

        if (!empty($this->data)) {

            if ($this->Universidade->save($this->data['Universidade'])) {
                $this->Session->setFlash('Universidade foi salva com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/universidades/visualizar/{$this->Universidade->id}");
            }
        }
        else
            $this->data = $this->Universidade->read();

        if (!$this->data)
            $this->Session->setFlash('Universidade não existente.', 'flash_erro');
    }

    function super_visualizar($id = null) {

        $universidade = $this->Universidade->find(array('id' => $id));

        if (!$universidade) {
            $this->Session->setFlash('Universidade não existente.', 'flash_erro');
        }
        $this->set('universidade', $this->Universidade->find(array('id' => $id)));
    }

    function super_deletar($id = null) {

        if ($this->Universidade->delete($id))
            $this->Session->setFlash('Universidadde excluida com sucesso', 'flash_sucesso');
        else
            $this->Session->setFlash('Ocorreu um erro ao excluir a universidade. Verifique se não existe um curso atrelado a ela.', 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/universidades");
    }

}

?>
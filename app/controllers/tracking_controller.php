<?php

class TrackingController extends AppController {

    var $name = 'Tracking';
    var $uses = array('Turma', 'RegistrosContato', 'FormandoProfile', 'Usuario');
    var $nomeDoTemplateSidebar = 'turmas';
    var $paginate = array(
        'limit' => 55,
        'order' => array(
            'Usuario.nome' => 'asc'
        )
    );
    
    private function _exibir() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $cond = array(
            'TurmasUsuario.turma_id' => $turma['Turma']['id'],
            'Usuario.grupo' => 'comissao'
        );
        $this->set('trackingFormandos', $this->paginate('Usuario', $cond));
        $this->set('estatisticasDosMembros', $this->Usuario->find('all',
                array('limit' => 10, 'conditions' => $cond)));
        $this->render('_exibir');
    }
    
    function comercial_exibir() {
        $this->_exibir();
    }    
    
    function planejamento_exibir() {
        $this->_exibir();
    }

    function comercial_index() {
        $this->enviarParaViewInformacoesDeTracking();
    }

    function planejamento_index() {
        $this->enviarParaViewInformacoesDeTracking();
    }

    function enviarParaViewInformacoesDeTracking() {
        $turma = $this->Session->read('turma');

        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);

        $cond = array(
            'TurmasUsuario.turma_id' => $turma['Turma']['id'],
            'Usuario.grupo' => 'comissao');

        $this->set('trackingFormandos', $this->paginate('Usuario', $cond));



        $this->set('estatisticasDosMembros', $this->Usuario->find('all', array('limit' => 10, 'conditions' => $cond)));
    }

}

?>
<?php

class AvisosController extends AppController {

    var $uses = array('Usuario','RegistroContatoUsuario','Assunto','Mensagem','MensagemUsuario','Atividade');
    var $avisos = array();
    
    function carregar() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $festas = $this->RegistroContatoUsuario->find('all',array(
            'conditions' => array(
                'RegistroContatoUsuario.usuario_id' => $usuario['Usuario']['id'],
                "RegistroContatoUsuario.data_confirmacao is null",
                'RegistrosContato.tipo' => 'festa',
                "RegistrosContato.usuario_id <> '{$usuario['Usuario']['id']}'"
            )
        ));
        if($festas)
            foreach($festas as $festa)
                array_unshift($this->avisos,array(
                    'link' => "/registros_contatos/confirmar_presenca/{$festa['RegistroContatoUsuario']['id']}",
                    'label' => "<i class='icon-star-3 fg-color-yellow'></i>&nbsp;Convite para " .
                            "festa {$festa['RegistrosContato']['assunto']}<br /><em class='fg-color-gray'>" .
                            "Local: {$festa['RegistrosContato']['local']}<br />Data: " .
                            date('d/m/Y',strtotime($festa['RegistrosContato']['data'])) . "</em>",
                    'id' => $festa['RegistroContatoUsuario']['id'],
                    'tipo' => 'convite-festa'
                ));
        $this->MensagemUsuario->recursive = 3;
        $this->Usuario->unbindModelAll();
        $this->Assunto->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Mensagem'))
        , false);
        $this->Mensagem->bindModel(array(
            'belongsTo' => array('Assunto'),
        ), false);
        $this->Mensagem->unbindModel(array(
            'hasAndBelongsToMany' => array('Arquivo'))
        , false);
        $mensagens = $this->MensagemUsuario->find('all',array(
            'conditions' => array(
                'MensagemUsuario.usuario_id' => $usuario['Usuario']['id'],
                'MensagemUsuario.lida' => 0
            ),
            'order' => array('MensagemUsuario.data_cadastro' => 'desc'),
            'limit' => 100
        ));
        foreach($mensagens as $mensagem) {
            $this->avisos[] = array(
                'link' => "/mensagens/exibir/{$mensagem['MensagemUsuario']['id']}",
                'label' => "<i class='icon-mail'></i>&nbsp;{$mensagem['Mensagem']['turma_id']} - " .
                        "{$mensagem['Mensagem']['Usuario']['nome']} Te mandou uma mensagem<br />" .
                        "<em class='fg-color-gray'>{$mensagem['Mensagem']['Assunto']['Item']['nome']}" .
                        " - {$mensagem['Mensagem']['Assunto']['nome']}</em>",
                'id' => $mensagem['MensagemUsuario']['id'],
                'tipo' => 'mensagem'
            );
        }
        if($usuario["Usuario"]['grupo'] == 'comercial')
            $this->carregarAtividades ($usuario);
        echo json_encode(array('avisos' => $this->avisos));
    }
    
    private function carregarAtividades($usuario) {
        $turmas = $this->TurmasUsuario->find('list',array(
            'conditions' => array('usuario_id' => $usuario['Usuario']['id']),
            'joins' => array(
                array(
                    'table' => 'turmas',
                    'foreignKey' => false,
                    'alias' => 'Turma',
                    'conditions' => array(
                        'Turma.id = TurmasUsuario.turma_id'
                    )
                )
            ),
            'fields' => array('Turma.id','Turma.id')
        ));
        $atividades = $this->Atividade->listarAtividadesPorUsuario($usuario,$turmas,date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59'));
        foreach($atividades as $atividade)
            array_unshift($this->avisos,array(
                'link' => "/calendario/visualizar/atividade/{$atividade['Atividade']['id']}",
                'label' => "<i class='icon-calendar'></i>&nbsp;{$this->Atividade->tipos[$atividade['Atividade']['tipo']]} " .
                        "{$atividade['Atividade']['nome']}  <br /><em class='fg-color-gray'>" .
                        date('H:i',strtotime($atividade['Atividade']['data_inicio'])) .
                        "<br />Local {$atividade['Atividade']['local']}</em>",
                'id' => $atividade['Atividade']['id'],
                'tipo' => 'atividade'
            ));
    }
    
}
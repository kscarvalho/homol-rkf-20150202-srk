<?php 
class TurmasUsuariosController extends AppController {
	
	var $name = 'TurmasUsuarios';
	var $uses = array('Turma', 'Usuario');
	
	
	// Padrão de paginação
	var $paginate = array(
		'limit' => 25,
		'order' => array(
   			'Turma.id' => 'asc'
			)
		);

		
	//var $nomeDoTemplateSidebar = 'Turmas';
	
	function beforeFilter() {
		parent::beforeFilter();		
	}
	
	function super_index() {
		$this->set('statuses', array_merge(array('' => 'Todas'), $this->Turma->status ));
		$this->Turma->unBindModel(array('hasMany' => array('CursoTurma')));
		$this->set('turmas', $this->paginate('Turma'));
	}
	

	function super_procurar() {
		$this->Turma->unBindModel(array('hasMany' => array('CursoTurma')));
		$chave = empty($this->data['Turmas']['chave']) ? "" : $this->data['Turmas']['chave'];
		$chaves = preg_split("/[, -.]/", $this->data['Turmas']['chave']);
		$status = $this->data['Turmas']['status'];
		$chaves = array_unique($chaves);
		unset($chaves[array_search('', $chaves)]);
		function construirQuery($c) {
			//CakeLog::write('teste', var_dump(array('Turma.id LIKE' => '%' . $c . '%', 'Turma.status LIKE' => '%' . $s . '%')));
			return array('Turma.id LIKE' => '%' . $c . '%');
		}
		$chaves = array_map('construirQuery', $chaves);
		$conditions = empty($chaves) ? array() : array('AND' => array('OR' => $chaves, 'Turma.status LIKE' => '%' . $status . '%'));
		$usuario = $this->Session->read('Usuario');
		if($usuario['Usuario']['grupo'] != 'super') {
			$this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
			$this->set('turmas', $this->paginate('Turma', array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'],
									$conditions)));
		} else 
			$this->set('turmas', $this->paginate('Turma', $conditions));

		$this->set('statuses', array_merge(array('' => 'Todas'), $this->Turma->status ));
		$this->set('chave', $chave);

		$this->render($this->params['prefix'] . '_index');
	}

	function super_alocar_usuario($id_turma = null) {
		
		$this->Turma->unBindModel(array('hasMany' => array('CursoTurma')));
		$turma = empty($id_turma) ? array() : $this->Turma->find(array('id' => $id_turma));
		$this->set('turma', $turma);
		if(!empty($turma)) {
			if($turma['Usuario'] != null) {
				
				foreach ($turma['Usuario'] as $usuario) {
					
					if ($usuario['grupo'] == 'comercial')
						$usuario_comercial = $this->Usuario->find(array('id' => $usuario['id']));
					else if($usuario['grupo'] == 'planejamento') 
						$usuario_planejamento = $this->Usuario->find(array('id' => $usuario['id'])) ;
				}
				
			}
		} else {
			$this->Session->setFlash('Turma não existente', 'flash_erro');
		}
		
		$this->Usuario->recursive = 0;
		$usuarios = $this->Usuario->find('all', array('OR' => array('Usuario.grupo' => 'comercial', 'Usuario.grupo' => 'planejamento')));

		if (!empty($usuario_comercial)) 
			unset($usuarios[array_search(array('Usuario' => $usuario_comercial), $usuarios)]);
		else
			$usuario_comercial = array();
			
		if(!empty($usuario_planejamento))
			unset($usuarios[array_search(array('Usuario' => $usuario_planejamento), $usuarios)]);
		else
			$usuario_planejamento = array();

		$this->set('usuario_comercial', $usuario_comercial);
		$this->set('usuario_planejamento', $usuario_planejamento);
		$this->set('grupos', array_merge(array('' => 'Todos'), $this->Usuario->grupos));
	}

}
?>
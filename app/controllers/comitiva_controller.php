<?php

class ComitivaController extends AppController {

    var $components = array('Auth');
    var $name = 'Comitiva';
    var $uses = array('FormandoProfile', 'Usuario', 'Turma', 'Curso');
    var $layout = 'externo';

    function beforeFilter() {
        $this->Auth->allow('index','selecionar_turma','confirmar_turma', 'cadastrar', 'obrigado');
        parent::beforeFilter();
    }
    
    function confirmar_turma() {
        $this->layout = 'metro/externo';
        if ($this->data['Turma']['id'] != null) {
            $this->Turma->id = $this->data['Turma']['id'];
            $turma = $this->Turma->findById($this->data['Turma']['id']);
        }
        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente', 'metro/flash/error');
            $this->redirect("/cadastro");
        }
        $this->set('turma', $turma);
        $info = null;
        $selectCurso = null;
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];
            if (empty($info['universidades'][$universidade['id']]['nome']))
                $info['universidades'][$universidade['id']]['nome'] = $universidade['nome'];
            if (empty($info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome']))
                $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome'] = $faculdade['nome'];
            $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['cursos'][] = $curso['Curso']['nome'] .
                    ' - ' . $cursoTurma['turno'];
            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' .
                    $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('info', $info);
    }
    
    function index(){
        $this->layout = 'metro/externo';
    }
    
    function cadastrar() {
        $this->layout = 'metro/externo';
        $this->Turma->id = $this->data['Turma']['id'];

        $turma = $this->Turma->findById($this->data['Turma']['id']);
        $selectCurso = null;
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];

            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' . $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('cursoTurma', $selectCurso);

        if (!empty($this->data['Usuario'])) {
            $this->data['Usuario']['ativo'] = 0;
            $this->data['Usuario']['grupo'] = 'comissao';
            $this->data['FormandoProfile']['pertence_comissao'] = 1;
            $this->data['FormandoProfile']['end_cep'] = 00000-000;
            $this->data['FormandoProfile']['realizou_checkout'] = 0;

            //desabilitando algumas validações para a comissão
            unset($this->FormandoProfile->validate['data_nascimento']['required']);
            unset($this->FormandoProfile->validate['sexo']['required']);
            unset($this->FormandoProfile->validate['cpf']['numeric']['required']);
            unset($this->FormandoProfile->validate['numero_havaiana']['required']);
            unset($this->FormandoProfile->validate['tam_camiseta']['required']);
            unset($this->FormandoProfile->validate['end_rua']['required']);
            unset($this->FormandoProfile->validate['end_numero']['required']);
            unset($this->FormandoProfile->validate['end_bairro']['required']);
            unset($this->FormandoProfile->validate['end_cidade']['required']);
            unset($this->FormandoProfile->validate['end_uf']['required']);
            unset($this->FormandoProfile->validate['end_cep']['numeric']['required']);
            unset($this->FormandoProfile->validate['forma_pagamento']);
            unset($this->FormandoProfile->validate['parcelas']);
            
            if ($this->Usuario->saveAll($this->data, array('validate' => 'only'))) {
                if ($this->Usuario->saveAll($this->data)) {
                    $this->Session->setFlash(__('Cadastro efetuado com sucesso.', true), 'metro/flash/success');
                    $this->render("obrigado");
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro.', 'metro/flash/error');
                }
            } else {
                $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro.', 'metro/flash/error');
            }
        }

        $turma = $this->Turma->read();

        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente,', 'metro/flash/error');
        } else
            $this->set('turma', $turma);
            
        // debug($this->FormandoProfile->validationErrors);
    }
    
    function comercial_adicionar() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->data['Turma']['id'] = $turma['Turma']['id'];
        if(!empty($this->data['Usuario'])) {
            $this->autoRender = false;
          //  Configure::write(array('debug' => 0));
            $this->data['Usuario']['ativo'] = 1;
            $this->data['Usuario']['grupo'] = 'comissao';
            $this->data['FormandoProfile']['pertence_comissao'] = 1;
            $this->data['FormandoProfile']['end_cep'] = '00000000';
            $this->data['FormandoProfile']['realizou_checkout'] = 0;
            unset($this->FormandoProfile->validate);
            $erro = true;
            if($this->Usuario->saveAll($this->data, array('validate' => 'only'))) {
                if($this->Usuario->saveAll($this->data)) {
                    $erro = false;
                    $this->Session->setFlash('Membro adicionado com sucesso', 'metro/flash/success');
                }
            }
            if($erro) {
                $erros = array('Erro ao adicionar membro');
                if(!empty($this->Usuario->validationErrors))
                    $erros = array_merge($erros,array_values($this->Usuario->validationErrors));
                if(!empty($this->FormandoProfile->validationErrors))
                    $erros = array_merge($erros,array_values($this->FormandoProfile->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
        }
    }
    
    function planejamento_adicionar(){
        $this->comercial_adicionar();
    }

    function adicionar() {
        $this->Turma->id = $this->data['Turma']['id'];

        $turma = $this->Turma->findById($this->data['Turma']['id']);
        $selectCurso = null;
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];

            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' . $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('cursoTurma', $selectCurso);

        if (!empty($this->data['Usuario'])) {
            $this->data['Usuario']['ativo'] = 0;
            $this->data['Usuario']['grupo'] = 'comissao';
            $this->data['FormandoProfile']['pertence_comissao'] = 1;
            $this->data['FormandoProfile']['end_cep'] = 00000-000;
            $this->data['FormandoProfile']['realizou_checkout'] = 0;

            //desabilitando algumas validações para a comissão
            unset($this->FormandoProfile->validate['data_nascimento']['required']);
            unset($this->FormandoProfile->validate['sexo']['required']);
            unset($this->FormandoProfile->validate['cpf']['numeric']['required']);
            unset($this->FormandoProfile->validate['numero_havaiana']['required']);
            unset($this->FormandoProfile->validate['tam_camiseta']['required']);
            unset($this->FormandoProfile->validate['end_rua']['required']);
            unset($this->FormandoProfile->validate['end_numero']['required']);
            unset($this->FormandoProfile->validate['end_bairro']['required']);
            unset($this->FormandoProfile->validate['end_cidade']['required']);
            unset($this->FormandoProfile->validate['end_uf']['required']);
            unset($this->FormandoProfile->validate['end_cep']['numeric']['required']);
            unset($this->FormandoProfile->validate['forma_pagamento']);
            unset($this->FormandoProfile->validate['parcelas']);
            
            if ($this->Usuario->saveAll($this->data, array('validate' => 'only'))) {
                if ($this->Usuario->saveAll($this->data)) {
                    $this->Session->setFlash(__('Cadastro efetuado com sucesso.', true), 'flash_sucesso');
                    $this->redirect("/comitiva/obrigado");
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro.', 'flash_erro');
                }
            } else {
                $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro.', 'flash_erro');
            }
        }

        $turma = $this->Turma->read();

        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente,', 'flash_erro');
            $this->redirect("/comitiva");
        } else
            $this->set('turma', $turma);
            
        debug($this->FormandoProfile->validationErrors);
    }

    function obrigado() {
        
    }

}

?>

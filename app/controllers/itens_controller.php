<?php

class ItensController extends AppController {

    var $name = 'itens';
    var $uses = array('Item', 'Assunto', 'Usuario', 'Mensagem', 'Cronograma');
    var $nomeDoTemplateSidebar = 'mensagens';

    /* ================== PARTE COMERCIAL ================== */
    
    
    private function _editar($itemId = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $usuario = $this->obterUsuarioLogado();
            $this->data['Item']['grupo'] = $usuario['Usuario']['grupo'];
            $data = $this->data;
            $nome = $data['Assunto']['nome'];
            if(!empty($data['Item']['agrupamento']))
                $data['Item']['nome'] = "{$data['Item']['agrupamento']}: {$data['Item']['nome']}";
            $turma = $this->obterTurmaLogada();
            if($this->Item->save($data)) {
                $assunto = array(
                    'item_id' => $this->Item->getLastInsertId(),
                    'pendencia' => 'As',
                    'nome' => $nome,
                    'turma_id' => $turma['Turma']['id']
                );
                if($this->Assunto->save($assunto))
                    $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao salvar dados', 'metro/flash/error');
            } else {
                $erros = array('Erro ao salvar dados');
                if(!empty($this->Item->validationErrors))
                    $erros = array_merge($erros,array_values($this->Item->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
        } else {
            $itens = $this->Item->find('all',array(
                'conditions' => array("nome like('%:%')"),
                'group' => array("substring_index(nome,':',1)"),
                'fields' => array("substring_index(nome,':',1) as agrupamento")
            ));
            $agrupamento = array();
            foreach($itens as $item)
                $agrupamento[$item[0]['agrupamento']] = $item[0]['agrupamento'];
            $this->set('agrupamento',$agrupamento);
            $this->render('_editar');
        }
    }
    
    function planejamento_editar($itemId = false) {
        $this->_editar($itemId);
    }

    function comercial_index() {
        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                'Item.grupo' => 'comercial'
            )
        );
        $this->set('itens', $this->paginate('Item'));
    }

    function comercial_visualizar($id = null) {
        $this->planejamento_visualizar($id);
    }

    /* ================== PARTE COMISSAO ================== */

    function comissao_index() {
        $turma = $this->Session->read('turma');
        $itens = $this->Item->obterPorTurmaId($turma['Turma']['id']);
        $itens_a = array();
        //print_r($itens);
        foreach ($itens as $item) {
            $itens_a[] = $item['Item']['id'];
        }

        if (count($itens_a) == 1) {
            $itens_a = array('Item.id' => $itens_a[0]);
        } else if (count($itens_a) > 1) {
            $itens_a = array('Item.id' => '(' . implode(",", $itens_a) . ')');
        }

        $this->paginate = array(
            'limit' => 20,
            'conditions' => $itens_a
        );

        $this->set('itens', $this->paginate('Item'));
    }

    function comissao_visualizar($id = null) {
        $turma = $this->Session->read('turma');

        $formando = $this->Session->read('Usuario');

        $this->paginate = array(
            'limit' => 20,
            'conditions' => array('Assunto.item_id' => $id, 'Assunto.turma_id' => $turma['Turma']['id']),
            'contain' => array('Mensagem'),
            'order' => array(
                'Assunto.id' => 'desc'
            )
        );
        $this->set('item', $this->Item->find('first', array('conditions' => array('Item.id' => $id))));
        $this->set('assuntos', $this->paginate('Assunto'));
    }

    /* ================== PARTE PLANEJAMENTO ================== */

    function planejamento_index() {
        $turma = $this->Session->read('turma');

        $itens_disponiveis = $this->Cronograma->find('list', array('fields' => array('item_id'), 'conditions' => array('turma_id' => $turma['Turma']['id'])));

        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                'Item.grupo' => 'planejamento',
                'Item.id' => $itens_disponiveis
            )
        );

        $this->set('itens', $this->paginate('Item'));
    }

    function planejamento_visualizar($id = null) {

        $turma = $this->Session->read('turma');

        if ($turma != null) {
            $conditions = array('Assunto.item_id' => $id, 'Assunto.turma_id' => $turma['Turma']['id']);
        } else {
            $conditions = array('Assunto.item_id' => $id);
        }

        $this->paginate = array(
            'limit' => 20,
            'conditions' => $conditions,
            'contain' => array('Mensagem', 'Turma'),
            'order' => array(
                'Assunto.id' => 'desc'
            )
        );
        $this->set('item', $this->Item->find('first', array('conditions' => array('Item.id' => $id))));
        $this->set('assuntos', $this->paginate('Assunto'));
    }

    /* ================== PARTE SUPER ================== */

    function super_index() {

        $this->set('itens', $this->paginate('Item'));
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            $this->Item->create();
            if ($this->Item->save($this->data)) {
                $this->Session->setFlash(__('O item foi salvo com sucesso', true), 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/itens");
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o item.', true), 'flash_erro');
            }
        }

        $this->set('grupos', $this->Item->grupos);
    }

    function super_editar($id = null) {

        $this->Item->id = $id;
        $this->Item->recursive = 2;
        if (!empty($this->data)) {
            if ($this->Item->save($this->data['Item'])) {
                $this->Session->setFlash(__('O item foi salvo com sucesso', true), 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/itens");
            } else {
                $this->Session->setFlash(__('Ocorreu um erro ao salvar o item.', true), 'flash_erro');
            }
        }
        else
            $this->data = $this->Item->read();

        if (!$this->data)
            $this->Session->setFlash('Item não existente');

        $this->set('grupos', $this->Item->grupos);
    }

    function super_visualizar($id = null) {
        $this->paginate = array(
            'limit' => 20,
            'conditions' => array(
                'Assunto.turma_id' => 12,
                'Assunto.item_id' => $id
            ),
            'contain' => array('Mensagem'),
            'order' => array(
                'Assunto.id' => 'desc'
            )
        );

        $this->set('item', $this->Item->find('first', array('conditions' => array('Item.id' => $id))));
        $this->set('assuntos', $this->paginate('Assunto'));
    }

    function super_deletar($id = null) {

        //		if ($this->Universidade->delete($id))
        //			$this->Session->setFlash('Deletado: universidade número ' . $id);
        //		else
        //			// TODO melhorar estas frases de erro
        //			// Talvez colocá-las em um lugar unificado seja interessante
        //			$this->Session->setFlash('Erro ao deletar, universidade selecionada é valida?');
        //			
        //		$this->redirect(array('action' => 'index'));
        //		
    }

}

?>

<?php

class FormandosShell extends Shell {

    var $uses = array('ViewFormandos');
    
    function exportarPorAno() {
        if(count($this->args) > 0) {
            $options = array(
                'joins' => array(
                    array(
                        'table' => 'turmas',
                        'alias' => 'Turma',
                        'conditions' => array('ViewFormandos.turma_id = Turma.id')
                    )
                ),
                'conditions' => array(
                    "concat(Turma.ano_formatura,'.',Turma.semestre_formatura) = '{$this->args[0]}'"
                )
            );
            $formandos = $this->ViewFormandos->find('all',$options);
            $this->out(count($formandos));
        }
    }
    
}
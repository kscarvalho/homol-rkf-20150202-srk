<?php

class RifasShell extends Shell {

    var $uses = array('CupomApp', 'Cupom', 'Rifa', 'RifaTurma', 'ViewFormandos');

    function gerarCupons() {
        if (count($this->args) == 2) {
            $usuario = $this->ViewFormandos->read(null, $this->args[0]);
            if ($usuario) {
                $cupomApp = $this->obterCupomApp($usuario['ViewFormandos']['id']);
                if($cupomApp) {
                    $numeroCuponsDoFormando = $this->obterCuponsPorUsuario($usuario['ViewFormandos']['id']);
                    $rifa = $this->RifaTurma->find('first', array(
                        'conditions' => array(
                            'rifa_id' => $this->args[1],
                            'turma_id' => $usuario['ViewFormandos']['turma_id']
                        )
                    ));
                    if(!$rifa) {
                        $cupomApp['CupomApp']['status'] = 'erro';
                        $cupomApp['CupomApp']['mensagem'] = 'Rifa não encontrada';
                    } elseif($rifa['Rifa']['ativa'] == 0 || $rifa['RifaTurma']['ativo'] == 0) {
                        $cupomApp['CupomApp']['status'] = 'erro';
                        $cupomApp['CupomApp']['mensagem'] = 'Rifa inativa';
                    } elseif($numeroCuponsDoFormando >= $rifa['RifaTurma']['cupons']) {
                        $cupomApp['CupomApp']['status'] = 'erro';
                        $cupomApp['CupomApp']['mensagem'] = 'Cupons ja foram gerados';
                    } else {
                        $qtdeCupons = $rifa['RifaTurma']['cupons'] - $numeroCuponsDoFormando;
                        $cupons_gerados = array();
                        $exp_numero = DboSource::expression("( SELECT  IF(RIGHT(IFNULL(MAX(c.numero),0),5) >= " . 
                                str_pad($this->Rifa->numero_maximo, 5, '0', STR_PAD_LEFT) .
                                ", MAX(c.numero) + 100000 - RIGHT(IFNULL(MAX(c.numero),0),5), IFNULL(MAX(c.numero),0)) " .
                                "FROM cupons c WHERE rifa_id = " . $rifa['Rifa']['id'] . ") + 1", false);
                        $cupomApp['CupomApp']['status'] = 'iniciado';
                        $cupomApp['CupomApp']['data_inicio'] = date('Y-m-d H:i:s');
                        for($a = 0; $a < $qtdeCupons;$a++) {
                            $cupons_gerados[] = array(
				'Cupom' => array( 
					'numero' => $exp_numero,
					'status' => 'aberto',
					'usuario_id' => $usuario['ViewFormandos']['id'],
					'rifa_id' => $rifa['Rifa']['id']
                                    )
				);
                        }
                        if(@$this->Cupom->saveAll($cupons_gerados)) {
                            $cupomApp['CupomApp']['status'] = 'finalizado';
                            $cupomApp['CupomApp']['mensagem'] = '';
                            $cupomApp['CupomApp']['data_fim'] = date('Y-m-d H:i:s');
                            $cupomApp['CupomApp']['cupons_gerados'] = count($cupons_gerados);
                        } else {
                            $cupomApp['CupomApp']['status'] = 'erro';
                            $cupomApp['CupomApp']['mensagem'] = 'Erro ao gerar cupons';
                        }
                    }
                    $this->CupomApp->save($cupomApp);
                }
            }
        }
    }

    private function obterCuponsPorUsuario($usuarioId) {
        return $this->Rifa->Cupom->find('count', array(
                'conditions' => array(
                    'usuario_id' => $usuarioId,
                    'not' => array('status' => 'cancelado')
                )
        ));
    }

    private function obterCupomApp($usuarioId) {
        $cupomApp = $this->CupomApp->find('first', array(
            'conditions' => array(
                'usuario_id' => $usuarioId
            )
        ));
        if (!$cupomApp) {
            $this->CupomApp->create();
            if ($this->CupomApp->save(array(
                        'CupomApp' => array(
                            'data_cadastro' => date('Y-m-d H:i:s'),
                            'usuario_id' => $usuarioId
                        )
                    )))
                $cupomApp = $this->obterCupomApp($usuarioId);
        }
        return $cupomApp;
    }

}
